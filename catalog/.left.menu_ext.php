<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

if(CModule::IncludeModule("iblock"))
{

$IBLOCK_ID = 7;        //здесь необходимо указать ID Вашего инфоблока

$arOrder = Array("SORT"=>"ASC");
$arSelect = array('ID', 'ACTIVE', 'IBLOCK_ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID');
$arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "<=DEPTH_LEVEL"=>2);
$res = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect, false);

    while($ob = $res->GetNext())
    {
        $aMenuLinksExt[] = Array(
            $ob['NAME'],
            $ob['SECTION_PAGE_URL'],
            Array(),
            Array(),
            ""
            );
    }
}

$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);
?>
