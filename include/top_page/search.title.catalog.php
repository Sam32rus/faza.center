<?$APPLICATION->IncludeComponent(
	"bitrix:search.title", 
	"catalog", 
	array(
		"CATEGORY_0" => array(
			0 => "iblock_catalog",
		),
		"CATEGORY_0_TITLE" => GetMessage("CATEGORY_PRODUСTCS_SEARCH_NAME"),
		"CATEGORY_0_iblock_aspro_optimus_catalog" => array(
			0 => "52",
		),
		"CHECK_DATES" => "Y",
		"CONTAINER_ID" => "title-search",
		"CONVERT_CURRENCY" => "N",
		"INPUT_ID" => "title-searchs-input",
		"NUM_CATEGORIES" => "1",
		"ORDER" => "date",
		"PAGE" => SITE_DIR."catalog/",
		"PREVIEW_HEIGHT" => "38",
		"PREVIEW_TRUNCATE_LEN" => "50",
		"PREVIEW_WIDTH" => "38",
		"PRICE_CODE" => array(
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"SHOW_ANOUNCE" => "N",
		"SHOW_INPUT" => "Y",
		"SHOW_OTHERS" => "N",
		"SHOW_PREVIEW" => "Y",
		"TOP_COUNT" => "8",
		"USE_LANGUAGE_GUESS" => "N",
		"COMPONENT_TEMPLATE" => "catalog",
		"CATEGORY_0_iblock_catalog" => array(
			0 => "7",
		)
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);?>