<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
// var_dump($_POST);
if (CModule::IncludeModule("catalog") && CModule::IncludeModule("sale")) {

	// ADD TO CART
	if ($_GET['action'] == 'ADD2BASKET' && $_GET['id']) {
		$propArr = array();
		$count = ($_GET['count']) ? $_GET['count'] : 1;

		Add2BasketByProductID(
			$_GET['id'],
			$count,
			$propArr
		);
	}

	// UPDATE QUANTITY
	if ($_POST['update'] == 'Y' && $_POST['id'] && $_POST['count']) {
		$arFields = array(
			"QUANTITY" => $_POST['count'],
		);
		CSaleBasket::Update($_POST['id'], $arFields);
		echo "успешно";
	}

	// DELETE PRODUCT
	if ($_GET['delete'] == 'Y' && $_GET['id']) {
		CSaleBasket::Delete($_GET['id']);
	}


}
$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line", 
	"template_smile_basket", 
	array(
		"HIDE_ON_BASKET_PAGES" => "N",
		"PATH_TO_AUTHORIZE" => SITE_DIR."auth/",
		"PATH_TO_BASKET" => SITE_DIR."basket/",
		"PATH_TO_ORDER" => SITE_DIR."personal/order/",
		"PATH_TO_PERSONAL" => SITE_DIR."personal/",
		"PATH_TO_PROFILE" => SITE_DIR."personal/",
		"PATH_TO_REGISTER" => SITE_DIR."login/",
		"POSITION_FIXED" => "N",
		"SHOW_AUTHOR" => "Y",
		"SHOW_EMPTY_VALUES" => "Y",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_PRODUCTS" => "N",
		"SHOW_REGISTRATION" => "N",
		"SHOW_TOTAL_PRICE" => "N",
		"COMPONENT_TEMPLATE" => "template_smile_basket"
	),
	false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>