<div class="user-menu__inner">
    <div class="user-menu__item-point user-menu__item-point_compare"><a href="/catalog/compare/">Сравнить</a></div>
    <div id="want" class="user-menu__item-point user-menu__item-point_heart">
     	<?if(!$USER->IsAuthorized()){
     		$arElements = unserialize($APPLICATION->get_cookie('favorites'));
     		if(is_array($arElements)){
     		?>
                <div class="user-menu__heart-sum col"><?echo count($arElements);?></div>
            <?}else{?>
                <div class="user-menu__heart-sum col">0</div>
            <?}
        }else{?>
            <div class="user-menu__heart-sum col"><?
                $idUser = $USER->GetID();
                $rsUser = CUser::GetByID($idUser);
                $arUser = $rsUser->Fetch();
                $arElements = $arUser['UF_FAVORITES'];
                echo count($arElements);?>
            </div>
    	<?}?>
        <a href="/personal/izbrannye-tovary/">Избранное</a>
    </div>
    <?if ($USER->IsAuthorized()):
		$name = trim($USER->GetFullName());
		if (! $name)
			$name = trim($USER->GetLogin());
		if (mb_strlen($name) > 15)
			$name = mb_substr($name, 0, 12).'...';
		?>
        <div class="user-menu__item-point user-menu__item-point_user" ><a href="/personal/"><?=$name;?></a></div> <!--data-popup-btn="login"-->
	<?else:?>
        <div class="user-menu__item-point user-menu__item-point_user" data-popup-btn="login">Вход</div>
    <?endif;?>
    <?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line",
	"template_smile_basket",
    	array(
    		"HIDE_ON_BASKET_PAGES" => "N",
    		"PATH_TO_AUTHORIZE" => SITE_DIR."auth/",
    		"PATH_TO_BASKET" => SITE_DIR."basket/",
    		"PATH_TO_ORDER" => SITE_DIR."personal/order/",
    		"PATH_TO_PERSONAL" => SITE_DIR."personal/",
    		"PATH_TO_PROFILE" => SITE_DIR."personal/",
    		"PATH_TO_REGISTER" => SITE_DIR."login/",
    		"POSITION_FIXED" => "N",
    		"SHOW_AUTHOR" => "Y",
    		"SHOW_EMPTY_VALUES" => "Y",
    		"SHOW_NUM_PRODUCTS" => "Y",
    		"SHOW_PERSONAL_LINK" => "N",
    		"SHOW_PRODUCTS" => "N",
    		"SHOW_REGISTRATION" => "N",
    		"SHOW_TOTAL_PRICE" => "N",
    		"COMPONENT_TEMPLATE" => "template_smile_basket"
    	),
    	false
    );?>
</div>
