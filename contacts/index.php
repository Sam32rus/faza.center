<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");

//Выборка всех активных складов:
$rsStore = \Bitrix\Catalog\StoreTable::getList(array(
    'filter' => array('ACTIVE' >= 'Y'),
    'select' => ['*', 'UF_*']
));
while ($arStore = $rsStore->fetch()) {
    if ($arStore['IMAGE_ID']) {
        $arStore['PICTURE'] = CFile::GetFileArray($arStore['IMAGE_ID']);
    } else {
        $arStore['PICTURE']['ALT'] = 'Фото временно отсутствует';
        $arStore['PICTURE']['SRC'] = '/local/templates/faza/assets/i/store_no_photo.png';
    }
    $arData[$arStore['UF_CITY']][] = $arStore;
    $arCoords[$arStore['UF_CITY']][] = [
        'TITLE' => $arStore['TITLE'],
        'PHONE' => $arStore['PHONE'],
        'ADDRESS' => $arStore['ADDRESS'],
        'LAT' => $arStore['GPS_N'],
        'LONG' => $arStore['GPS_S']
    ];
}
$activeCity = array_key_first($arData);
?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=b4b3d40b-a558-4a75-b4b5-f6b632b6c0be" type="text/javascript"></script>

<main class="main">
    <h1 class="title-page"><span class="content">Контакты</span></h1>

    <section class="basket">
        <div class="content">
            <div class="sorting">
                <div class="sorting__inner">
                    <div class="sorting__found"><span><?= count($arData) ?></span> магазинов</div>
                </div>
            </div>
            <div class="contacts__inner">
                <div class="contacts__panel">
                    <? foreach ($arData as $city => $arStores) : ?>
                        <a href="javascript:void(0);" class="switch-unit js__switch-unit<?= $activeCity == $city ? ' active' : '' ?>" data-city="<?= $city ?>" data-lat="<?= current($arStores)['GPS_N'] ?>" data-long="<?= current($arStores)['GPS_S'] ?>">
                            <span class="switch-unit__title"><?= $city ?></span>
                            <span class="link-arrow switch-unit__link js__active">Перейти</span>
                            <img src="/f/i/icons/location-pointer.svg" class="switch-unit__point-img">
                        </a>
                    <? endforeach ?>
                </div>

                <div class="contacts__main">
                    <div class="contacts__title">Адреса магазинов и пунктов выдачи в Брянске</div>
                    <div class="contacts__desc">В нашем магазине вы можете приобрести весь спектр товаров для электромонтажа и монтажа отопления. В случае если вы на нашли интересующий Вас товар, позвоните по контактному телефону 8(4832) 400-200 и мы предложим Вам товар с аналогичными характеристиками.</div>

                    <div class="contacts__map" id="map"></div>

                    <div class="contacts__shops">
                        <? foreach ($arData as $city => $arStores) : ?>
                            <? foreach ($arStores as $keyStore => $arStore) : ?>
                                <div class="contacts__item-shop" data-city="<?= $city ?>" data-lat="<?= $arStore['GPS_N'] ?>" data-long="<?= $arStore['GPS_S'] ?>">
                                    <div class="contacts__shop-wrap-img">
                                        <img src="<?= $arStore['PICTURE']['SRC'] ?>" alt="<?= !$arStore['PICTURE']['ALT'] ? $arStore['TITLE'] . 'в городе' . $arStore['UF_CITY'] : $arStore['PICTURE']['ALT'] ?>">
                                    </div>
                                    <div class="contacts__shop-info">
                                        <div class="contacts__shop-title"><?= $arStore['TITLE'] ?></div>
                                        <div class="contacts__shop-adress"><?= $arStore['ADDRESS'] ?></div>
                                        <a class="contacts__shop-phone" href="tel:<?= $arStore['PHONE'] ? preg_replace('/[^0-9+]/', '', $arStore['PHONE']) : '+78007005480' ?>"><?= $arStore['PHONE'] ? $arStore['PHONE'] : '+78007005480' ?></a>
                                        <a class="contacts__shop-mail rel=" nofollow" href="mailto:<?= $arStore['EMAIL'] ? $arStore['EMAIL'] : 'info@faza.center' ?>"><?= $arStore['EMAIL'] ? $arStore['EMAIL'] : 'info@faza.center' ?></a>
                                        <div class="contacts__shop-clock">Время работы: <?= $arStore['SCHEDULE'] ?></div>
                                    </div>
                                </div>
                            <? endforeach ?>
                        <? endforeach ?>
                    </div>

                    <div class="contacts__feedback">
                        <div class="blue-button" data-popup-btn="feedback">Обратная связь</div>
                    </div>
                </div>
            </div>
    </section>
</main>


<script>
    ymaps.ready(function() {
        let arCities = JSON.parse('<?php echo json_encode($arCoords); ?>');

        var map = new ymaps.Map('map', {
            center: [54.04764188839026, 35.12689217194835],
            zoom: 7,
            controls: []
        });
        $.each(arCities, function(keyCity, arCoordinates) {
            var myGeoObjects = [];
            for (var i = 0; i < arCoordinates.length; i++) {
                myGeoObjects[i] = new ymaps.GeoObject({
                    geometry: {
                        type: "Point",
                        coordinates: [arCoordinates[i]['LAT'], arCoordinates[i]['LONG']]
                    },
                    properties: {
                        iconCaption: arCoordinates[i]['TITLE'],
                        balloonContentHeader: arCoordinates[i]['TITLE'],
                        balloonContentBody: arCoordinates[i]['ADDRESS'] + '</br>' + arCoordinates[i]['PHONE']
                    }
                });
            }
            var myClusterer = new ymaps.Clusterer();
            myClusterer.add(myGeoObjects);
            map.geoObjects.add(myClusterer);
        });

        $('.contacts__panel').on('click', '.js__switch-unit', function() {
            let tab = $(this);
            let city = tab.attr('data-city');
            let lat = tab.attr('data-lat');
            let long = tab.attr('data-long');

            $('.contacts__item-shop').each(function() {
                if ($(this).attr("data-city") !== city) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });

            map.setCenter([lat, long], '9');
        });

        $('.contacts__shops').on('click', '.contacts__item-shop', function() {
            let tab = $(this);
            let lat = tab.attr('data-lat');
            let long = tab.attr('data-long');

            map.setCenter([lat, long], '15');
        });
    });
</script>

<style>
    .contacts__map {
        height: 500px;
        width: 100%;
    }
</style>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>