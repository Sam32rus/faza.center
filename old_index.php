<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "продажа электрооборудования, низкие цены, лампы диодные, кабели и провода");
$APPLICATION->SetPageProperty("keywords", "Интернет-магазин, фаза, электротовары");
$APPLICATION->SetPageProperty("title", "Интернет-магазин электротоваров Фаза");
$APPLICATION->SetPageProperty("viewed_show", "Y");
$APPLICATION->SetTitle("Фаза");
?> 

            <section class="promo-sliders">
                <div class="content">
                    <div class="promo-sliders__inner">
                        <div class="promo-sliders__big-slider">
                            <div class="promo-big-slider js__promo-big-slider">

<?
$arSelectSliderleft = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*","SECTION_ID","PREVIEW_PICTURE");
$arFilterSliderleft = Array("IBLOCK_ID"=>9, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","SECTION_ID"=>1 );
$resSliderleft = CIBlockElement::GetList(Array(), $arFilterSliderleft, false, Array("nPageSize"=>50), $arSelectSliderleft);
while($obSliderleft = $resSliderleft->GetNextElement()){ 
 $arFieldsSliderleft = $obSliderleft->GetFields();  
 $arPropsSliderleft = $obSliderleft->GetProperties();
 $urlSliderleft = CFile::GetPath($arFieldsSliderleft["PREVIEW_PICTURE"]);
?>
 <div class="promo-big-slider__item-slid" style="background-image: url('<?=$urlSliderleft?>')"></div>
 <?
}
?>
                            </div>
                            <a href="<?=$arProps['ssilka']['VALUE']?>" class="white-button promo-big-slider__button">Подробнее</a>
                        </div>
                        <div class="promo-sliders__small-slider">
                            <div class="promo-small-slider js__promo-small-slider">
                                <?
$arSelectSliderRight = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*","SECTION_ID","PREVIEW_PICTURE");
$arFilterSliderRight = Array("IBLOCK_ID"=>9, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","SECTION_ID"=>2 );
$resSliderRight = CIBlockElement::GetList(Array(), $arFilterSliderRight, false, Array("nPageSize"=>50), $arSelectSliderRight);
while($obSliderRight = $resSliderRight->GetNextElement()){ 
 $arFieldsSliderRight = $obSliderRight->GetFields();  
 $arPropsSliderRight = $obSliderRight->GetProperties();
 $urlSliderRight = CFile::GetPath($arFieldsSliderRight["PREVIEW_PICTURE"]);
?>
 <div class="promo-small-slider__item-slid" style="background-image: url('<?=$urlSliderRight?>')"></div>
 <?
}
?>
                            
                            </div>
                            <div class="white-button promo-small-slider__button" data-popup-btn="registration">Зарегистрироваться</div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="advantages">
                <div class="content">
                    <div class="advantages__inner js__margin-scroll">

                        <a href="/info/ves-tovar-sertifitsirovan.php" class="advantages__item js__advantages-anim">
                            <span class="advantages__title">Весь товар сертифицирован</span>
                            <span class="link-arrow advantages__link js__active">Смотреть</span>
                            <span class="circles-pulse advantages__circle js__active">
                                <span class="circles-pulse__c1">
                                    <span class="circles-pulse__c2"></span>
                                </span>
                            </span>
                        </a>

                        <a href="/info/ves-tovar-sertifitsirovan2.php" class="advantages__item js__advantages-anim">
                            <span class="advantages__title">Весь товар сертифицирован</span>
                            <span class="link-arrow advantages__link js__active">Смотреть</span>
                            <span class="circles-pulse advantages__circle js__active">
                                <span class="circles-pulse__c1">
                                    <span class="circles-pulse__c2"></span>
                                </span>
                            </span>
                        </a>

                        <a href="/info/udobnaya-i-bystraya-dostavka.php" class="advantages__item js__advantages-anim">
                            <span class="advantages__title">Удобная и быстрая доставка</span>
                            <span class="link-arrow advantages__link js__active">Смотреть</span>
                            <span class="circles-pulse advantages__circle js__active">
                                <span class="circles-pulse__c1">
                                    <span class="circles-pulse__c2"></span>
                                </span>
                            </span>
                        </a>

                        <a href="/info/ves-tovar-sertifitsirovan3.php" class="advantages__item js__advantages-anim">
                            <span class="advantages__title">Весь товар сертифицирован</span>
                            <span class="link-arrow advantages__link js__active">Смотреть</span>
                            <span class="circles-pulse advantages__circle js__active">
                                <span class="circles-pulse__c1">
                                    <span class="circles-pulse__c2"></span>
                                </span>
                            </span>
                        </a>


                    </div>
                </div>
            </section>

            <section class="product-tabs">
                <div class="content">
                    <div class="product-tabs__inner js__margin-scroll">
                        <div class="tabs js__tabs">
                            <ul class="product-tabs__switch js__tabs-caption">
                                <li class="switch-tab active">Хиты продаж</li>
                                <li class="switch-tab">Новые поступления</li>
                                <li class="switch-tab">Скидка/акция</li>
                            </ul>

                            <div class="tabs__content product-tabs__content active">

                                <div class="product-slider js__product-slider">
                                        <?
                                        $APPLICATION->IncludeComponent(
	"bitrix:sale.bestsellers", 
	"templateIndex", 
	array(
		"LINE_ELEMENT_COUNT" => "30",
		"TEMPLATE_THEME" => "blue",
		"BY" => "QUANTITY",
		"PERIOD" => "100",
		"FILTER" => array(
			0 => "CANCELED",
			1 => "ALLOW_DELIVERY",
			2 => "PAYED",
			3 => "N",
			4 => "F",
		),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "86400",
		"AJAX_MODE" => "N",
		"DETAIL_URL" => "",
		"BASKET_URL" => "/basket/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "Y",
		"DISPLAY_COMPARE" => "Y",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"PRICE_CODE" => array(
			0 => "RUB",
		),
		"SHOW_PRICE_COUNT" => "1",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PRICE_VAT_INCLUDE" => "Y",
		"USE_PRODUCT_QUANTITY" => "N",
		"SHOW_NAME" => "Y",
		"SHOW_IMAGE" => "Y",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"PAGE_ELEMENT_COUNT" => "30",
		"SHOW_PRODUCTS_3" => "Y",
		"PROPERTY_CODE_3" => array(
			0 => "MANUFACTURER",
			1 => "MATERIAL",
		),
		"CART_PROPERTIES_3" => array(
			0 => "CORNER",
		),
		"ADDITIONAL_PICT_PROP_3" => "MORE_PHOTO",
		"LABEL_PROP_3" => "SPECIALOFFER",
		"PROPERTY_CODE_4" => array(
			0 => "COLOR",
		),
		"CART_PROPERTIES_4" => "",
		"OFFER_TREE_PROPS_4" => array(
			0 => "-",
		),
		"HIDE_NOT_AVAILABLE" => "N",
		"CONVERT_CURRENCY" => "Y",
		"CURRENCY_ID" => "RUB",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"COMPONENT_TEMPLATE" => "templateIndex",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SHOW_PRODUCTS_7" => "Y",
		"PROPERTY_CODE_7" => array(
			0 => "rating",
			1 => "",
		),
		"CART_PROPERTIES_7" => array(
			0 => "",
			1 => "",
		),
		"ADDITIONAL_PICT_PROP_7" => "",
		"LABEL_PROP_7" => "-"
	),
	false
);
?>
                                   

                                </div>

                            </div>

                            <div class="tabs__content product-tabs__content">

                                <div class="product-slider js__product-slider">
   <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"index_tovar_new", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/basket/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "timestamp_x",
		"ELEMENT_SORT_FIELD2" => "",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "",
		"ENLARGE_PRODUCT" => "STRICT",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => "",
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "25",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "18",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "RUB",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "rating",
			2 => "",
		),
		"SEF_MODE" => "Y",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "N",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "index_tovar_new",
		"SEF_RULE" => "",
		"SECTION_CODE_PATH" => ""
	),
	false
);?> 

                                    

                                </div>

                            </div>

                            <div class="tabs__content product-tabs__content">
<?
function GET_SALE_FILTER(){
  global $DB;

  $arDiscountElementID = array();
  $dbProductDiscounts = CCatalogDiscount::GetList(
     array("SORT" => "ASC"),
     array(),
     false,
     false,
     array(
        "ID", "SITE_ID", "ACTIVE", "ACTIVE_FROM", "ACTIVE_TO",
        "RENEWAL", "NAME", "SORT", "MAX_DISCOUNT", "VALUE_TYPE",
        "VALUE", "CURRENCY", "PRODUCT_ID"
     )
  );
  while ($arProductDiscounts = $dbProductDiscounts->Fetch())
  {
     if($res = CCatalogDiscount::GetDiscountProductsList(array(), array(">=DISCOUNT_ID" => $arProductDiscounts['ID']), false, false, array())){
        while($ob = $res->GetNext()){
           if(!in_array($ob["PRODUCT_ID"],$arDiscountElementID))
              $arDiscountElementID[] = $ob["PRODUCT_ID"];
        }}
  }

  return $arDiscountElementID;

}
?>

<?
$arElements = GET_SALE_FILTER();
// $GLOBALS['searchFilterskidka']  = array("=ID" => $arElements);
// var_dump($GLOBALS['searchFilterskidka']);

?>
                                <div class="product-slider js__product-slider">
<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "index_tovar_new", Array(
    "ACTION_VARIABLE" => "action",  // Название переменной, в которой передается действие
        "ADD_PICT_PROP" => "-",
        "ADD_PROPERTIES_TO_BASKET" => "Y",  // Добавлять в корзину свойства товаров и предложений
        "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
        "ADD_TO_BASKET_ACTION" => "ADD",
        "AJAX_MODE" => "N", // Включить режим AJAX
        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
        "AJAX_OPTION_HISTORY" => "N",   // Включить эмуляцию навигации браузера
        "AJAX_OPTION_JUMP" => "N",  // Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y", // Включить подгрузку стилей
        "BACKGROUND_IMAGE" => "-",  // Установить фоновую картинку для шаблона из свойства
        "BASKET_URL" => "/basket/", // URL, ведущий на страницу с корзиной покупателя
        "BROWSER_TITLE" => "-", // Установить заголовок окна браузера из свойства
        "CACHE_FILTER" => "N",  // Кешировать при установленном фильтре
        "CACHE_GROUPS" => "Y",  // Учитывать права доступа
        "CACHE_TIME" => "36000000", // Время кеширования (сек.)
        "CACHE_TYPE" => "A",    // Тип кеширования
        "COMPATIBLE_MODE" => "Y",   // Включить режим совместимости
        "CONVERT_CURRENCY" => "N",  // Показывать цены в одной валюте
        "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
        "DETAIL_URL" => "", // URL, ведущий на страницу с содержимым элемента раздела
        "DISABLE_INIT_JS_IN_COMPONENT" => "N",  // Не подключать js-библиотеки в компоненте
        "DISPLAY_BOTTOM_PAGER" => "N",  // Выводить под списком
        "DISPLAY_COMPARE" => "N",   // Разрешить сравнение товаров
        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
        "ELEMENT_SORT_FIELD" => "timestamp_x",  // По какому полю сортируем элементы
        "ELEMENT_SORT_FIELD2" => "id",  // Поле для второй сортировки элементов
        "ELEMENT_SORT_ORDER" => "asc",  // Порядок сортировки элементов
        "ELEMENT_SORT_ORDER2" => "desc",    // Порядок второй сортировки элементов
        "ENLARGE_PRODUCT" => "STRICT",
        "FILTER_NAME" => "searchFilterskidka",   // Имя массива со значениями фильтра для фильтрации элементов
        "HIDE_NOT_AVAILABLE" => "N",    // Недоступные товары
        "HIDE_NOT_AVAILABLE_OFFERS" => "N", // Недоступные торговые предложения
        "IBLOCK_ID" => "7", // Инфоблок
        "IBLOCK_TYPE" => "catalog", // Тип инфоблока
        "INCLUDE_SUBSECTIONS" => "Y",   // Показывать элементы подразделов раздела
        "LABEL_PROP" => "",
        "LAZY_LOAD" => "N",
        "LINE_ELEMENT_COUNT" => "3",    // Количество элементов выводимых в одной строке таблицы
        "LOAD_ON_SCROLL" => "N",
        "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_BTN_SUBSCRIBE" => "Подписаться",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "META_DESCRIPTION" => "-",  // Установить описание страницы из свойства
        "META_KEYWORDS" => "-", // Установить ключевые слова страницы из свойства
        "OFFERS_LIMIT" => "25", // Максимальное количество предложений для показа (0 - все)
        "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
        "PAGER_DESC_NUMBERING" => "N",  // Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",   // Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
        "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
        "PAGER_TEMPLATE" => ".default", // Шаблон постраничной навигации
        "PAGER_TITLE" => "Товары",  // Название категорий
        "PAGE_ELEMENT_COUNT" => "18",   // Количество элементов на странице
        "PARTIAL_PRODUCT_PROPERTIES" => "N",    // Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
        "PRICE_CODE" => array(  // Тип цены
            0 => "RUB",
        ),
        "PRICE_VAT_INCLUDE" => "Y", // Включать НДС в цену
        "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
        "PRODUCT_ID_VARIABLE" => "id",  // Название переменной, в которой передается код товара для покупки
        "PRODUCT_PROPS_VARIABLE" => "prop", // Название переменной, в которой передаются характеристики товара
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",  // Название переменной, в которой передается количество товара
        "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
        "PRODUCT_SUBSCRIPTION" => "Y",
        "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
        "RCM_TYPE" => "personal",
        "SECTION_CODE" => "",   // Код раздела
        "SECTION_ID" => $_REQUEST["SECTION_ID"],    // ID раздела
        "SECTION_ID_VARIABLE" => "SECTION_ID",  // Название переменной, в которой передается код группы
        "SECTION_URL" => "",    // URL, ведущий на страницу с содержимым раздела
        "SECTION_USER_FIELDS" => array( // Свойства раздела
            0 => "",
            1 => "",
        ),
        "SEF_MODE" => "N",  // Включить поддержку ЧПУ
        "SET_BROWSER_TITLE" => "N", // Устанавливать заголовок окна браузера
        "SET_LAST_MODIFIED" => "N", // Устанавливать в заголовках ответа время модификации страницы
        "SET_META_DESCRIPTION" => "N",  // Устанавливать описание страницы
        "SET_META_KEYWORDS" => "N", // Устанавливать ключевые слова страницы
        "SET_STATUS_404" => "N",    // Устанавливать статус 404
        "SET_TITLE" => "N", // Устанавливать заголовок страницы
        "SHOW_404" => "N",  // Показ специальной страницы
        "SHOW_ALL_WO_SECTION" => "Y",   // Показывать все элементы, если не указан раздел
        "SHOW_CLOSE_POPUP" => "N",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_FROM_SECTION" => "N",
        "SHOW_MAX_QUANTITY" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_PRICE_COUNT" => "1",  // Выводить цены для количества
        "SHOW_SLIDER" => "N",
        "SLIDER_INTERVAL" => "3000",
        "SLIDER_PROGRESS" => "N",
        "TEMPLATE_THEME" => "blue",
        "USE_ENHANCED_ECOMMERCE" => "N",
        "USE_MAIN_ELEMENT_SECTION" => "N",  // Использовать основной раздел для показа элемента
        "USE_PRICE_COUNT" => "N",   // Использовать вывод цен с диапазонами
        "USE_PRODUCT_QUANTITY" => "N",  // Разрешить указание количества товара
        "COMPONENT_TEMPLATE" => "index_tovar"
    ),
    false
);?> 
                                  

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </section>


            <section class="repairs">
                <div class="content">
                    <div class="repairs__inner">
                        <div class="repairs__head">
                            <div class="repairs__title">Делаешь ремонт</div>
                            <div class="repairs__link">
                                <a href="#" class="link-arrow-top">Выбрать свою комнату</a>
                            </div>

                        </div>
                        <div class="repairs__content">
                            <div class="choice">
                                <div class="choice__inner" style="background-image: url('/f/i/rooms/big-room1.jpg')">

                                    <div class="choice__point choice__point-living-1 tooltip">
                                        <span class="tooltip__content">
                                            <a href="#" class="choice__tooltip-link">Светильники</a>
                                        </span>
                                    </div>

                                    <div class="choice__point choice__point-living-2 tooltip">
                                        <span class="tooltip__content">
                                            <a href="#" class="choice__tooltip-link">Лампы накаливания</a>
                                        </span>
                                    </div>

                                </div>
                            </div>
                            <div class="repairs__rooms">
                                <a href="#" class="repairs__item-room" style="background-image: url('/f/i/rooms/room1.jpg')"></a>
                                <a href="#" class="repairs__item-room" style="background-image: url('/f/i/rooms/room2.jpg')"></a>
                                <a href="#" class="repairs__item-room" style="background-image: url('/f/i/rooms/room3.jpg')"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="partners-slider">
                <div class="content">
                    <div class="partners-slider__inner js__partners-slider">
                        <?
$arSelectbrand = Array("ID", "IBLOCK_ID", "NAME","PREVIEW_PICTURE","DETAIL_PAGE_URL");
$arFilterbrand = Array("IBLOCK_ID"=>IntVal("13"), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$bbrand = CIBlockElement::GetList(Array(), $arFilterbrand, false, Array("nPageSize"=>50), $arSelectbrand);
$i=0;$s=0;
while($bnbrand = $bbrand->GetNextElement()){
 $arFieldsbrand = $bnbrand->GetFields();  
 // $arPropsbrand = $bnbrand->GetProperties();
 $urlbrand = CFile::GetPath($arFieldsbrand["PREVIEW_PICTURE"]);

?><a href="<?=$arFieldsbrand["DETAIL_PAGE_URL"];?>"> 
                        <img src="<?=$urlbrand;?>" class="partners-slider__item"></a>
<?}?>                        
                    </div>
                </div>
            </section>

            <section class="product-list bgc_elis">
                
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"index_tovar", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/basket/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_PATH" => "/catalog/compare/",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[{\"CLASS_ID\":\"CondIBProp:7:9\",\"DATA\":{\"logic\":\"Equal\",\"value\":52}}]}",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"ENLARGE_PRODUCT" => "STRICT",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "L",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => "",
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "4",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "8",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "8",
		"PARTIAL_PRODUCT_PROPERTIES" => "Y",
		"PRICE_CODE" => array(
			0 => "RUB",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "N",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "index_tovar"
	),
	false
);?>
                        
            </section>


            <section class="news">
                <div class="content">
                    <div class="news__inner">
                        <div class="news__left-block">
                            <div class="news__title">Будь в курсе</div>
                            <div class="news-slider">
                                <div class="news-slider__inner js__news-slider">
<?
$arSelectbn = Array("ID", "IBLOCK_ID", "NAME","PROPERTY_*","SECTION_ID","PREVIEW_PICTURE","PREVIEW_TEXT","DETAIL_PAGE_URL");
$arFilterbn = Array("IBLOCK_ID"=>IntVal("10"), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","PROPERTY_big_el_value"=>'ДА');
$bnews = CIBlockElement::GetList(Array(), $arFilterbn, false, Array("nPageSize"=>5), $arSelectbn);
$i=0;$s=0;
while($bn = $bnews->GetNextElement()){
 $arFieldsbn = $bn->GetFields();  
 $arPropsbn = $bn->GetProperties();
 $urlbn = CFile::GetPath($arFieldsbn["PREVIEW_PICTURE"]);

?>
 <div class="news-slider__item-slid" style="background-image: url(<?=$urlbn;?>)">
                                        <a href="<?=$arFieldsbn["DETAIL_PAGE_URL"];?>" class="news-slider__title"><?=$arFieldsbn["NAME"];?></a>
                                        <a href="<?=$arFieldsbn["DETAIL_PAGE_URL"];?>" class="news-slider__description"><?=$arFieldsbn["PREVIEW_TEXT"];?></a>
                                    </div>
<?
}
?>
                                </div>
                            </div>
                        </div>

                        <div class="news__right-block">
                            <div class="news__wrap-title">
                                <div class="news__title">Новости</div>
                                <a href="/novosti/" class="news__link">Смотреть все новости</a>
                            </div>
                            <div class="news__wrap-articles">
                                <?$APPLICATION->IncludeComponent(
	"bitrix:news", 
	"Index_template", 
	array(
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_PAGER_SHOW_ALL" => "N",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"LIST_ACTIVE_DATE_FORMAT" => "j F Y",
		"LIST_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "2",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_REVIEW" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "Index_template",
		"SEF_FOLDER" => "",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "",
			"detail" => "",
		)
	),
	false
);?> 
                              

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="blogs">
                <div class="content">
                    <div class="blogs__inner">
                        <div class="blogs__title">Блоги</div>
                        <div class="blogs__content">
                            <div class="anim-num blogs__anim-num">
                                <div class="anim-num__inner">
<?
$arSelect = Array("ID", "NAME", "PREVIEW_TEXT","DETAIL_PAGE_URL");
$arFilter = Array("IBLOCK_ID"=>IntVal("12"), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5), $arSelect);
$i=0;$s=0;
?>
                                    <div class="anim-num__wrap-numbers">
                                        <div class="anim-num__line anim-num__line_black js__anim-num"></div>
                                        <?
while($ob = $res->GetNextElement()){
    $s++;
if ($s == 1) {
    $stats1 = "active";
 }else{$stats1 ='';}
    ?>  <div class="anim-num__item-number <?=$stats1;?>">0<?=$s;?></div><?
}
                                        ?>
                                        <!-- <div class="anim-num__item-number active">01</div>
                                        <div class="anim-num__item-number">02</div>
                                        <div class="anim-num__item-number">03</div>
                                        <div class="anim-num__item-number">04</div>
                                        <div class="anim-num__item-number">05</div> -->
                                    </div>
                                    <div class="anim-num__blue">
                                        <div class="anim-num__line anim-num__line_white js__anim-num"></div>
<?
$res2 = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5), $arSelect);

while($obs = $res2->GetNextElement())
{
    $i++;
 $arFields = $obs->GetFields();
 // print_r($arFields); 
 if ($i == 1) {
    $stats2 = "active";
 }else{$stats2 ='';}
 ?>
<div data-number="<?=$i;?>" class="anim-num__article <?=$stats2;?>">
                                            <a href="<?=$arFields['DETAIL_PAGE_URL']?>" class="anim-num__title"><?=$arFields['NAME']?></a>
                                            <a href="<?=$arFields['DETAIL_PAGE_URL']?>" class="anim-num__description"><?=$arFields['PREVIEW_TEXT']?></a>
                                        </div>
 <?
}
?> 

                                        <ul class="anim-num__tags">
                                            <li>#Свет</li>
                                            <li>#Кабель</li>
                                            <li>#Выключатель</li>
                                        </ul>
                                        <span class="link-arrow anim-num__link-arrow">Все статьи блога</span>
                                    </div>
                                </div>
                            </div>
                            <div class="subscribe blogs__subscribe">
                                <div class="subscribe__inner">
                                    <div class="subscribe__title">Подписаться на&nbsp;рассылку</div>

                                    <form action="/personal/subscribe/">
                                        <div class="form-data">
                                            <div class="form-data__title">Адрес электронной почты</div>
                                            <div class="form-data__wrap">
                                                <input class="form-data__input js__email" required name="email" type="text">
                                            </div>
                                            <div class="form-data__error">Заполните поле "Адрес электронной почты"</div>
                                        </div>
                                        <div class="form-data">
                                            <div class="form-data__wrap-submit">
                                                <button class="js__disabled-btn form-data__submit blue-button" type="submit">Подписаться</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>