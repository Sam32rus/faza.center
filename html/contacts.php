<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Корзина | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/breadcrumbs.php'; ?>


    <main class="main">
        <h1 class="title-page"><span class="content">Контакты</span></h1>

        <section class="basket">
            <div class="content">
                <div class="sorting">
                    <div class="sorting__inner">
                        <div class="sorting__found"><span>6</span> магазинов</div>
                    </div>
                </div>
                <div class="contacts__inner">

                    <div class="contacts__panel">

                        <a href="/" class="switch-unit js__switch-unit">
                            <span class="switch-unit__title">Брянск</span>
                            <span class="link-arrow switch-unit__link js__active">Перейти</span>
                            <img src="/f/i/icons/location-pointer.svg" class="switch-unit__point-img">
                        </a>

                        <a href="/" class="switch-unit js__switch-unit">
                            <span class="switch-unit__title">Калуга</span>
                            <span class="link-arrow switch-unit__link js__active">Перейти</span>
                            <img src="/f/i/icons/location-pointer.svg" class="switch-unit__point-img">
                        </a>

                        <a href="/" class="switch-unit js__switch-unit">
                            <span class="switch-unit__title">Орел</span>
                            <span class="link-arrow switch-unit__link js__active">Перейти</span>
                            <img src="/f/i/icons/location-pointer.svg" class="switch-unit__point-img">
                        </a>

                        <a href="/" class="switch-unit js__switch-unit">
                            <span class="switch-unit__title">Смоленске</span>
                            <span class="link-arrow switch-unit__link js__active">Перейти</span>
                            <img src="/f/i/icons/location-pointer.svg" class="switch-unit__point-img">
                        </a>

                        <a href="/" class="switch-unit js__switch-unit">
                            <span class="switch-unit__title">Тула</span>
                            <span class="link-arrow switch-unit__link js__active">Перейти</span>
                            <img src="/f/i/icons/location-pointer.svg" class="switch-unit__point-img">
                        </a>

                    </div>

                    <div class="contacts__main">
                        <div class="contacts__title">Адреса магазинов и пунктов выдачи в Брянске</div>

                        <div class="contacts__desc">В нашем магазине вы можете приобрести весь спектр товаров для электромонтажа и монтажа отопления. В случае если вы на нашли интересующий Вас товар, позвоните по контактному телефону 8(4832) 400-200 и мы предложим Вам товар с аналогичными характеристиками.</div>
                        <div class="contacts__map">
                            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ae05b7eb44829114511fb1ec523a79d2c73ad320bd3738df05076e9cd55f08d33&amp;width=100%25&amp;height=600&amp;lang=ru_RU&amp;scroll=true"></script>
                        </div>

                        <div class="contacts__shops">
                            <div class="contacts__item-shop">
                                <div class="contacts__shop-wrap-img">
                                    <img src="/f/i/store/faza1.jpg" alt="">
                                </div>
                                <div class="contacts__shop-info">
                                    <div class="contacts__shop-title">Фаза на объездной</div>
                                    <div class="contacts__shop-adress">Брянск, Брянская область д. Добрунь, ул. С.А. Халаева, 74</div>
                                    <a class="contacts__shop-phone" href="tel:+74832400-200">+7(4832)400-200</a>
                                    <a class="contacts__shop-mail rel="nofollow" href="mailto:info@faza.center">info@faza.center</a>
                                    <div class="contacts__shop-clock">Время работы: Пн-Пт: 9:00-20:00; Сб-Вск: 10:00-16:00</div>
                                </div>
                            </div>

                            <div class="contacts__item-shop">
                                <div class="contacts__shop-wrap-img">
                                    <img src="/f/i/store/faza2.jpg" alt="">
                                </div>
                                <div class="contacts__shop-info">
                                    <div class="contacts__shop-title">Фаза в Линии 3</div>
                                    <div class="contacts__shop-adress">Брянск, Россия, улица Ульянова, 3</div>
                                    <a class="contacts__shop-phone" href="tel:+74832400-200">+7(4832)400-200</a>
                                    <a class="contacts__shop-mail rel="nofollow" href="mailto:info@faza.center">info@faza.center</a>
                                    <div class="contacts__shop-clock">Время работы: Пн-Пт: 9:00-20:00; Сб-Вск: 10:00-16:00</div>
                                </div>
                            </div>
                        </div>

                        <div class="contacts__feedback">
                            <div class="blue-button" data-popup-btn="feedback">Обратная связь</div>
                        </div>



                    </div>
                </div>
        </section>



    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>