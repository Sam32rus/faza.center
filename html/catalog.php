<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/breadcrumbs.php'; ?>

    <main class="main">
        <h1 class="title-page"><span class="content">Лампы и электропатроны</span></h1>

        <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/catalog.php'; ?>

    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>