<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/breadcrumbs.php'; ?>


    <main class="main">
        <h1 class="title-page"><span class="content">Все акции</span></h1>

        <section class="promotions">
            <div class="content">
                <div class="sorting">
                    <div class="sorting__inner">
                        <div class="sorting__found"><span>128</span> акций</div>
                    </div>
                </div>

    

                <div class="promotions__inner">
                    <div class="promotions__title">Действующие акции</div>

                        <a href="#" class="promo-item">
                            <span class="promo-item__inner">
                                <span class="promo-item__img" style="background-image: url('/f/i/banners/product-price1.jpg')"></span>
                                <span class="promo-item__content">
                                    <span class="promo-item__title">Выгодное предложение на лампочки и люстры!</span>
                                    <span class="promo-item__date">с 22 июня по 11 июля 2021 года</span>
                                    <span class="promo-item__desc">Привлекательные цены на товары из списка! Ждём вас в сети магазинов электрики Faza! Привлекательные цены на товары из списка! Ждём вас в сети магазинов электрики Faza! Привлекательные цены на товары из списка! Ждём вас в сети магазинов электрики Faza!</span>
                                </span>
                                <span class="promo-item__time">
                                    <span class="promo-item__title-time">До конца акции осталось:</span>
                                    <span id="counter-1" class="js__counter promo-item__counter"></span>
                                    <div class="promo-item__all-product">Смотреть все товары акции</div>
                                </span>
                            </span>
                        </a>

                        <a href="#" class="promo-item">
                            <span class="promo-item__inner">
                                <span class="promo-item__img" style="background-image: url('/f/i/banners/product-price2.jpg')"></span>
                                <span class="promo-item__content">
                                    <span class="promo-item__title">Выгодное предложение на лампочки и люстры!</span>
                                    <span class="promo-item__date">с 22 июня по 11 июля 2021 года</span>
                                    <span class="promo-item__desc">Привлекательные цены на товары из списка! Ждём вас в сети магазинов электрики Faza! Привлекательные цены на товары из списка! Ждём вас в сети магазинов электрики Faza! Привлекательные цены на товары из списка! Ждём вас в сети магазинов электрики Faza!</span>
                                </span>
                                <span class="promo-item__time">
                                    <span class="promo-item__title-time">До конца акции осталось:</span>
                                    <span id="counter-2" class="js__counter promo-item__counter"></span>
                                    <div class="promo-item__all-product">Смотреть все товары акции</div>
                                </span>
                            </span>
                        </a>


                    <div class="promotions__title">Прошедшие акции</div>

                    <a href="#" class="promo-item">
                            <span class="promo-item__inner">
                                <span class="promo-item__img" style="background-image: url('/f/i/banners/product-price1.jpg')"></span>
                                <span class="promo-item__content">
                                    <span class="promo-item__title">Выгодное предложение на лампочки и люстры!</span>
                                    <span class="promo-item__date">с 22 июня по 11 июля 2021 года</span>
                                    <span class="promo-item__desc">Привлекательные цены на товары из списка! Ждём вас в сети магазинов электрики Faza! Привлекательные цены на товары из списка! Ждём вас в сети магазинов электрики Faza! Привлекательные цены на товары из списка! Ждём вас в сети магазинов электрики Faza!</span>
                                </span>
                                <span class="promo-item__time">
                                    <span class="promo-item__title-time">До конца акции осталось:</span>
                                    <span id="counter-3" class="js__counter promo-item__counter"></span>
                                    <div class="promo-item__all-product">Смотреть все товары акции</div>
                                </span>
                            </span>
                        </a>

            
                </div>
            </div>
        </section>



    </main>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addJs.php'; ?>


<script>
    $('#counter-1').timeTo(new Date('june 28 2021 00:00:00'));
    $('#counter-2').timeTo(new Date('july 20 2021 00:00:00'));
    $('#counter-3').timeTo(new Date('june 1 2021 00:00:00'));
</script>




</body>
</html>