<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper wrapper_card-product">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/breadcrumbs.php'; ?>


    <main class="main">

        <div class="wishlist">
            <div class="wishlist__inner">
                <div class="wishlist__btns">
                    <button class="blue-button blue-button_basket wishlist__btn-basket">В корзину</button>
                    <button class="transparent-button wishlist__btn-transparent">Купить в 1 клик</button>
                    <button class="like-button wishlist__like-button"></button>
                </div>

                <div class="user-menu">
                    <div class="user-menu__inner">
                        <a href="#" class="user-menu__item-point user-menu__item-point_catalog">Каталог</a>
                        <a href="#" class="user-menu__item-point user-menu__item-point_compare">Сравнить</a>
                        <a href="#" class="user-menu__item-point user-menu__item-point_heart">
                            <span class="user-menu__heart-sum">8</span>
                            Избранное
                        </a>
                        <a href="#" class="user-menu__item-point user-menu__item-point_user" data-popup-btn="login">Вход</a>
                        <a href="#" class="user-menu__item-point user-menu__item-point_basket">
                            <span class="user-menu__basket-sum">25</span>
                            Корзина
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <h1 class="title-page hide-tablet"><span class="content">Лампа LED 6Вт Е27 2400К ST64 Golden Gauss Filament</span></h1>
        <section class="info-product">
            <div class="content">
                <div class="info-product__inner">
                    <div class="rating info-product__rating">
                        <div class="rating__inner">
                            <div class="rating__star hide-tablet" data-rating="3.5">
                                <div class="rating__fill"></div>
                            </div>
                            <div class="rating__info hide-tablet"><span>15</span> отзывов</div>
                            <div class="rating__info rating__info_favorites hide-tablet">В избранное</div>
                            <div class="rating__info rating__info_compare">Сравнить</div>
                        </div>
                    </div>
                    <div class="share info-product__share" data-popup-btn="share">Поделиться</div>
                </div>
            </div>
        </section>

        <section class="card-product">
            <div class="content">
                <div class="card-product__inner">

                    <!--  Фото товара-->
                    <div class="card-product__image-product image-product">
                        <div class="image-product__inner">

                                <div class="image-product__slider_navigation js__image-product-nav">
                                    <div class="image-product__item-nav">
                                        <img class="image-product__img-nav" src="/f/i/product/4.jpg"/>
                                    </div>
                                    <div class="image-product__item-nav">
                                        <img class="image-product__img-nav" src="/f/i/product/1.png"/>
                                    </div>
                                    <div class="image-product__item-nav">
                                        <img class="image-product__img-nav" src="/f/i/product/2.png"/>
                                    </div>
                                </div>

                                <div class="image-product__slider js__image-product">
                                    <div class="image-product__item">
                                        <img class="image-product__img js__zoom" src="/f/i/product/4.jpg" data-zoom-image="/f/i/product/4.jpg"/>
                                    </div>
                                    <div class="image-product__item">
                                        <img class="image-product__img js__zoom" src="/f/i/product/1.png" data-zoom-image="/f/i/product/1.png"/>
                                    </div>
                                    <div class="item">
                                        <img class="image-product__img js__zoom" src="/f/i/product/2.png" data-zoom-image="/f/i/product/2.png"/>
                                    </div>
                                </div>

                        </div>
                    </div>
                    <!--  /Фото товара-->

                    <!--  Информация о продукте, для планшета и мобильного-->
                    <section class="info-product-tablet show-tablet">
                        <div class="title-page info-product-tablet__title-page">Лампа LED 6Вт Е27 2400К ST64 Golden Gauss Filament</div>
                        <div class="rating info-product-tablet__rating">
                            <div class="rating__inner">
                                <div class="rating__star" data-rating="3.5">
                                    <div class="rating__fill"></div>
                                </div>
                                <div class="rating__info"><span>15</span> отзывов</div>
                            </div>
                        </div>
                        <div class="info-product-tablet__code">Артикул: <span>1234567</span></div>
                        <div class="price-rub info-product-tablet__price-rub">
                            <div class="price-rub__inner">7000</div>
                        </div>
                    </section>
                    <!--  /Информация о продукте, для планшета и мобильного-->

                    <!-- О товаре-->
                    <div class="card-product__about-product hide-tablet">
                        <div class="card-product__about-list">
                            <div class="deploy">
                                <div class="card-product__about-title">О товаре</div>
                                <div class="card-product__item-point">Напряжение, В <span>220</span></div>
                                <div class="card-product__item-point">Цоколь <span>Е27</span></div>
                                <div class="card-product__item-point">Мощность, Вт <span>6</span></div>
                                <div class="card-product__item-point">Цветовая температура, К <span>2700</span></div>
                                <div class="card-product__item-point">Напряжение, В <span>220</span></div>
                                <div class="card-product__item-point">Цоколь <span>Е27</span></div>
                                <div class="card-product__item-point">Мощность, Вт <span>6</span></div>
                                <div class="deploy__content" style="display: none">
                                    <div class="card-product__item-point">Напряжение, В <span>220</span></div>
                                    <div class="card-product__item-point">Цоколь <span>Е27</span></div>
                                    <div class="card-product__item-point">Мощность, Вт <span>6</span></div>
                                    <div class="card-product__item-point">Цветовая температура, К <span>2700</span></div>
                                    <div class="card-product__item-point">Напряжение, В <span>220</span></div>
                                    <div class="card-product__item-point">Цоколь <span>Е27</span></div>
                                    <div class="card-product__item-point">Мощность, Вт <span>6</span></div>
                                </div>
                                <div class="catalog__add-brand catalog__add-brand_arrow js__down-feature">Все характеристики</div>
                            </div>
                        </div>
                        <div class="card-product__about-order">
                            <div class="order">
                                <div class="order__inner">
                                    <div class="order__code">Артикул: <span>1234567</span></div>
                                    <div class="price-rub order__price-rub">
                                        <div class="price-rub__inner">7000</div>
                                    </div>
                                    <button class="blue-button blue-button_basket order__blue-button">Добавить в корзину</button>
                                    <button class="transparent-button order__transparent-button">Купить в 1 клик</button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /О товаре-->


                </div>
            </div>
        </section>

        <section class="product-tabs">
            <div class="content">
                <div class="product-tabs__inner">
                    <div class="tabs js__tabs">
                        <ul class="product-tabs__switch js__tabs-caption">
                            <li class="switch-tab js__tab-feature">Характеристики</li>
                            <li class="switch-tab active">Отзывы</li>
                            <li class="switch-tab">Доставка и оплата</li>
                        </ul>

                        <div class="tabs__content product-tabs__content js__content-feature">

                            <div class="description-product">
                                <div class="description-product__inner">
                                    <div class="description-product__title">Характеристики</div>
                                    <div class="description-product__content">
                                        <div class="specifications description-product__specifications">
                                            <div class="specifications__inner">






                                                <div class="specifications__list">

                                                    <div class="specifications__item-point">
                                                        <div class="specifications__point-left">Технология</div>
                                                        <div class="specifications__line"></div>
                                                        <div class="specifications__point-right">Светодиодная</div>
                                                    </div>

                                                    <div class="specifications__item-point">
                                                        <div class="specifications__point-left">Условия хранения</div>
                                                        <div class="specifications__line"></div>
                                                        <div class="specifications__point-right">Хранить в сухом прохладном месте. Не бросать. Использовать лампу только на оборудовании, которому соответствует данный тип лампы. Перед установкой лампы отключить питание. В случае неисправности заменить лампу. При транспортировании упакованные лампы следует предохранять от механических повреждений. Запрещается эксплуатация ламп с поврежденной внешней колбой.</div>
                                                    </div>

                                                    <div class="specifications__item-point">
                                                        <div class="specifications__point-left">Производитель</div>
                                                        <div class="specifications__line"></div>
                                                        <div class="specifications__point-right">АТЛ Бизнес (Шэньчжэнь) ко., Лтд (адрес: 518054, КНР, Шэньчжэнь Наньшань дистрикт Наньхай роуд Чуанъе стрит Нос Баоличэн билдинг  рум  901)</div>
                                                    </div>

<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Тип цоколя</span> <span class="specifications__point-right">E27</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Цена за шт</span> <span class="specifications__point-right">84 ₽</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Технология</span> <span class="specifications__point-right">Светодиодная</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Тип цоколя</span> <span class="specifications__point-right">E27</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Цена за шт</span> <span class="specifications__point-right">84 ₽</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Технология</span> <span class="specifications__point-right">Светодиодная</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Тип цоколя</span> <span class="specifications__point-right">E27</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Цена за шт</span> <span class="specifications__point-right">84 ₽</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Технология</span> <span class="specifications__point-right">Светодиодная</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Тип цоколя</span> <span class="specifications__point-right">E27</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Цена за шт</span> <span class="specifications__point-right">84 ₽</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Технология</span> <span class="specifications__point-right">Светодиодная</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Тип цоколя</span> <span class="specifications__point-right">E27</span></li>-->
<!--                                                    <li class="specifications__item-point"><span class="specifications__point-left">Цена за шт</span> <span class="specifications__point-right">84 ₽</span></li>-->
                                                </div>
                                                <div class="specifications__text">Производитель оставляет за собой право изменять характеристики товара, его внешний вид и комплектность без предварительного уведомления продавца.</div>
                                                <div class="specifications__text">Предложение по продаже товара действительно в течение срока наличия этого товара на складе.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tabs__content product-tabs__content active">

                            <div class="description-product">
                                <div class="description-product__inner">
                                    <div class="description-product__title">Отзывы</div>

                                    <!-- Блок рейтинг-->
                                    <div class="appraisals description-product__appraisals">
                                        <div class="appraisals__inner">
                                            <div class="appraisals__title">Рейтинг</div>
                                            <div class="rating appraisals__rating">
                                                <div class="rating__inner">
                                                    <div class="rating__star" data-rating="1.5">
                                                        <div class="rating__fill"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="appraisals__item-data">
                                                <div class="appraisals__grade">5</div>
                                                <div class="appraisals__line">
                                                    <div style="width: 90%" class="appraisals__fill"></div>
                                                </div>
                                                <div class="appraisals__sum">26</div>
                                            </div>
                                            <div class="appraisals__item-data">
                                                <div class="appraisals__grade">4</div>
                                                <div class="appraisals__line">
                                                    <div style="width: 20%"class="appraisals__fill"></div>
                                                </div>
                                                <div class="appraisals__sum">18</div>
                                            </div>
                                            <div class="appraisals__item-data">
                                                <div class="appraisals__grade">4</div>
                                                <div class="appraisals__line">
                                                    <div style="width: 60%" class="appraisals__fill"></div>
                                                </div>
                                                <div class="appraisals__sum">23</div>
                                            </div>
                                            <div class="appraisals__item-data">
                                                <div class="appraisals__grade">2</div>
                                                <div class="appraisals__line">
                                                    <div style="width: 10%" class="appraisals__fill"></div>
                                                </div>
                                                <div class="appraisals__sum">3</div>
                                            </div>
                                            <div class="appraisals__item-data">
                                                <div class="appraisals__grade">1</div>
                                                <div class="appraisals__line">
                                                    <div style="width: 0%" class="appraisals__fill"></div>
                                                </div>
                                                <div class="appraisals__sum">0</div>
                                            </div>

                                            <div class="appraisals__wrap-btn">
                                                <div class="transparent-button appraisals__transparent-button show-tablet" data-popup-btn="all-reviews">Все отзывы</div>
                                                <div class="transparent-button appraisals__transparent-button" data-popup-btn="add-review">Оставить отзыв</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Блок рейтинг-->

                                    <div class="description-product__content">
                                        <div class="reviews description-product__reviews">
                                            <div class="reviews__inner">

                                                <!--Конкретный отзыв-->
                                                <div class="reviews__item" itemprop="review" itemscope itemtype="http://schema.org/Review">
                                                    <div class="reviews__author" itemprop="author">Игорь</div>
                                                    <div class="reviews__date" itemprop="datePublished">20.10.2020</div>
                                                    <div class="rating reviews__rating">
                                                        <div class="rating__inner">
                                                            <div class="rating__star" itemprop="reviewRating" data-rating="1.5">
                                                                <div class="rating__fill"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
                                                    <div class="merit">
                                                        <div class="merit__inner">
                                                            <div class="merit__item merit__item_like">Работает стабильно, светит ярко, есть гарантия</div>
                                                            <div class="merit__item merit__item_dislike">Нет</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/Конкретный отзыв-->

                                                <!--Конкретный отзыв-->
                                                <div class="reviews__item" itemprop="review" itemscope itemtype="http://schema.org/Review">
                                                    <div class="reviews__author" itemprop="author">Михаил</div>
                                                    <div class="reviews__date" itemprop="datePublished">20.10.2020</div>
                                                    <div class="rating reviews__rating">
                                                        <div class="rating__inner">
                                                            <div class="rating__star" itemprop="reviewRating" data-rating="1.5">
                                                                <div class="rating__fill"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
                                                    <div class="merit">
                                                        <div class="merit__inner">
                                                            <div class="merit__item merit__item_like">Рживучесть, яркость</div>
                                                            <div class="merit__item merit__item_dislike">не замечено</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/Конкретный отзыв-->

                                                <!--Конкретный отзыв-->
                                                <div class="reviews__item" itemprop="review" itemscope itemtype="http://schema.org/Review">
                                                    <div class="reviews__author" itemprop="author">Ирина</div>
                                                    <div class="reviews__date" itemprop="datePublished">20.10.2020</div>
                                                    <div class="rating reviews__rating">
                                                        <div class="rating__inner">
                                                            <div class="rating__star" itemprop="reviewRating" data-rating="1.5">
                                                                <div class="rating__fill"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
                                                    <div class="merit">
                                                        <div class="merit__inner">
                                                            <div class="merit__item merit__item_like">не замечено</div>
                                                            <div class="merit__item merit__item_dislike">Работает не стабильно, светит не ярко, нет гарантии</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/Конкретный отзыв-->


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tabs__content product-tabs__content">

                            <div class="description-product">
                                <div class="description-product__inner">
                                    <div class="description-product__title">Доставка и оплата</div>
                                    <div class="description-product__content">
                                        Уважаемые покупатели! Интернет-заказы доставляются ежедневно (временные интервалы с 10:00 до 16:00, с 14:00 до 18:00, с 18:00 до 22:00) по следующим условиям:
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="content">
                <div class="section__inner">
                    <div class="section__title">Рекомендуем</div>
                    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/product-slider.php'; ?>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="content">
                <div class="section__inner">
                    <div class="section__title">Лучшие бренды</div>
                    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/best-brands.php'; ?>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="content">
                <div class="section__inner">
                    <div class="section__title">Ранее просматривали</div>
                    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/viewed.php'; ?>
                </div>
            </div>
        </section>



    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>