<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/breadcrumbs.php'; ?>


    <main class="main">
        <h1 class="title-page"><span class="content">Все записи блога</span></h1>

        <section class="all-blogs">
            <div class="content">
                <div class="sorting">
                    <div class="sorting__inner">
                        <div class="sorting__found"><span>128</span> записей</div>
                    </div>
                </div>

                <div class="all-blogs__inner">

                <div class="item-blog">
                        <div class="item-blog__inner">
                            <a href="#" class="item-blog__image" style="background-image: url('/f/i/news/1.jpg')"></a>
                            <a href="#" class="item-blog__title">Приятные мелочи</a>
                            <div class="info-blog">
                                <div class="info-blog__inner">
                                    <div class="info-blog__author">
                                        <div class="info-blog__photo" style="background-image: url('/f/i/news/1.jpg')"></div>
                                        <div class="info-blog__name">Филипп С.</div>
                                    </div>
                                    <div class="info-blog__date">3 часа назад</div>
                                </div>
                            </div>
                            <a href="#" class="item-blog__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                            <div class="tags">
                                <div class="tags__inner">
                                    <a href="#" class="tags__item">Лампы</a>
                                    <a href="#" class="tags__item">Лайфак</a>
                                    <a href="#" class="tags__item">Ремонт</a>
                                    <a href="#" class="tags__item">Электрика</a>
                                    <a href="#" class="tags__item">Для дома</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item-blog">
                        <div class="item-blog__inner">
                            <a href="#" class="item-blog__image" style="background-image: url('/f/i/news/2.jpg')"></a>
                            <a href="#" class="item-blog__title">Приятные мелочи Приятные мелочи Приятные мелочи</a>
                            <div class="info-blog">
                                <div class="info-blog__inner">
                                    <div class="info-blog__author">
                                        <div class="info-blog__photo" style="background-image: url('/f/i/news/1.jpg')"></div>
                                        <div class="info-blog__name">Филипп С.</div>
                                    </div>
                                    <div class="info-blog__date">3 часа назад</div>
                                </div>
                            </div>
                            <a href="#" class="item-blog__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                            <div class="tags">
                                <div class="tags__inner">
                                    <a href="#" class="tags__item">Лампы</a>
                                    <a href="#" class="tags__item">Лайфак</a>
                                    <a href="#" class="tags__item">Ремонт</a>
                                    <a href="#" class="tags__item">Электрика</a>
                                    <a href="#" class="tags__item">Длч дома</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item-blog">
                        <div class="item-blog__inner">
                            <a href="#" class="item-blog__image" style="background-image: url('/f/i/news/1.jpg')"></a>
                            <a href="#" class="item-blog__title">Приятные мелочи</a>
                            <div class="info-blog">
                                <div class="info-blog__inner">
                                    <div class="info-blog__author">
                                        <div class="info-blog__photo"></div>
                                        <div class="info-blog__name">Филипп С.</div>
                                    </div>
                                    <div class="info-blog__date">3 часа назад</div>
                                </div>
                            </div>
                            <a href="#" class="item-blog__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                            <div class="tags">
                                <div class="tags__inner">
                                    <a href="#" class="tags__item">Лампы</a>
                                    <a href="#" class="tags__item">Лайфак</a>
                                    <a href="#" class="tags__item">Ремонт</a>
                                    <a href="#" class="tags__item">Электрика</a>
                                    <a href="#" class="tags__item">Длч дома</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item-blog">
                        <div class="item-blog__inner">
                            <a href="#" class="item-blog__image" style="background-image: url('/f/i/news/2.jpg')"></a>
                            <a href="#" class="item-blog__title">Приятные мелочи</a>
                            <div class="info-blog">
                                <div class="info-blog__inner">
                                    <div class="info-blog__author">
                                        <div class="info-blog__photo" style="background-image: url('/f/i/news/1.jpg')"></div>
                                        <div class="info-blog__name">Филипп С.</div>
                                    </div>
                                    <div class="info-blog__date">3 часа назад</div>
                                </div>
                            </div>
                            <a href="#" class="item-blog__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                            <div class="tags">
                                <div class="tags__inner">
                                    <a href="#" class="tags__item">Лампы</a>
                                    <a href="#" class="tags__item">Лайфак</a>
                                    <a href="#" class="tags__item">Ремонт</a>
                                    <a href="#" class="tags__item">Электрика</a>
                                    <a href="#" class="tags__item">Длч дома</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item-blog">
                        <div class="item-blog__inner">
                            <a href="#" class="item-blog__image" style="background-image: url('/f/i/news/1.jpg')"></a>
                            <a href="#" class="item-blog__title">Приятные мелочи Приятные мелочи Приятные мелочи</a>
                            <div class="info-blog">
                                <div class="info-blog__inner">
                                    <div class="info-blog__author">
                                        <div class="info-blog__photo"></div>
                                        <div class="info-blog__name">Филипп С.</div>
                                    </div>
                                    <div class="info-blog__date">3 часа назад</div>
                                </div>
                            </div>
                            <a href="#" class="item-blog__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                            <div class="tags">
                                <div class="tags__inner">
                                    <a href="#" class="tags__item">Лампы</a>
                                    <a href="#" class="tags__item">Лайфак</a>
                                    <a href="#" class="tags__item">Ремонт</a>
                                    <a href="#" class="tags__item">Электрика</a>
                                    <a href="#" class="tags__item">Длч дома</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item-blog">
                        <div class="item-blog__inner">
                            <a href="#" class="item-blog__image" style="background-image: url('/f/i/news/2.jpg')"></a>
                            <a href="#" class="item-blog__title">Приятные мелочи</a>
                            <div class="info-blog">
                                <div class="info-blog__inner">
                                    <div class="info-blog__author">
                                        <div class="info-blog__photo" style="background-image: url('/f/i/news/2.jpg')"></div>
                                        <div class="info-blog__name">Филипп С.</div>
                                    </div>
                                    <div class="info-blog__date">3 часа назад</div>
                                </div>
                            </div>
                            <a href="#" class="item-blog__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                            <div class="tags">
                                <div class="tags__inner">
                                    <a href="#" class="tags__item">Лампы</a>
                                    <a href="#" class="tags__item">Лайфак</a>
                                    <a href="#" class="tags__item">Ремонт</a>
                                    <a href="#" class="tags__item">Электрика</a>
                                    <a href="#" class="tags__item">Длч дома</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                </div>
            </div>
        </section>



    </main>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addJs.php'; ?>


</body>
</html>