<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Главная | Faza</title>
</head>
<body>
    <div class="wrapper">

        <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

        <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

        <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>


        <main class="main">

            <section class="promo-sliders">
                <div class="content">
                    <div class="promo-sliders__inner">
                        <div class="promo-sliders__big-slider">
                            <div class="promo-big-slider js__promo-big-slider">
                                <div class="promo-big-slider__item-slid" style="background-image: url('/f/i/promo-slider/big-1.jpg')"></div>
                                <div class="promo-big-slider__item-slid" style="background-image: url('/f/i/promo-slider/big-1.jpg')"></div>
                                <div class="promo-big-slider__item-slid" style="background-image: url('/f/i/promo-slider/big-1.jpg')"></div>
                                <div class="promo-big-slider__item-slid" style="background-image: url('/f/i/promo-slider/big-1.jpg')"></div>
                            </div>
                            <a href="#" class="white-button promo-big-slider__button">Подробнее</a>
                        </div>
                        <div class="promo-sliders__small-slider">
                            <div class="promo-small-slider js__promo-small-slider">
                                <div class="promo-small-slider__item-slid" style="background-image: url('/f/i/promo-slider/small-1.jpg')"></div>
                                <div class="promo-small-slider__item-slid" style="background-image: url('/f/i/promo-slider/small-1.jpg')"></div>
                                <div class="promo-small-slider__item-slid" style="background-image: url('/f/i/promo-slider/small-1.jpg')"></div>
                            </div>
                            <div class="white-button promo-small-slider__button" data-popup-btn="registration">Зарегистрироваться</div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="advantages">
                <div class="content">
                    <div class="advantages__inner js__margin-scroll">

                        <a href="#" class="advantages__item js__advantages-anim">
                            <span class="advantages__title">Весь товар сертифицирован</span>
                            <span class="link-arrow advantages__link js__active">Смотреть</span>
                            <span class="circles-pulse advantages__circle js__active">
                                <span class="circles-pulse__c1">
                                    <span class="circles-pulse__c2"></span>
                                </span>
                            </span>
                        </a>

                        <a href="#" class="advantages__item js__advantages-anim">
                            <span class="advantages__title">Весь товар сертифицирован</span>
                            <span class="link-arrow advantages__link js__active">Смотреть</span>
                            <span class="circles-pulse advantages__circle js__active">
                                <span class="circles-pulse__c1">
                                    <span class="circles-pulse__c2"></span>
                                </span>
                            </span>
                        </a>

                        <a href="#" class="advantages__item js__advantages-anim">
                            <span class="advantages__title">Удобная и быстрая доставка</span>
                            <span class="link-arrow advantages__link js__active">Смотреть</span>
                            <span class="circles-pulse advantages__circle js__active">
                                <span class="circles-pulse__c1">
                                    <span class="circles-pulse__c2"></span>
                                </span>
                            </span>
                        </a>

                        <a href="#" class="advantages__item js__advantages-anim">
                            <span class="advantages__title">Весь товар сертифицирован</span>
                            <span class="link-arrow advantages__link js__active">Смотреть</span>
                            <span class="circles-pulse advantages__circle js__active">
                                <span class="circles-pulse__c1">
                                    <span class="circles-pulse__c2"></span>
                                </span>
                            </span>
                        </a>


                    </div>
                </div>
            </section>

            <section class="product-tabs">
                <div class="content">
                    <div class="product-tabs__inner js__margin-scroll">
                        <div class="tabs js__tabs">
                            <ul class="product-tabs__switch js__tabs-caption">
                                <li class="switch-tab active">Хиты продаж</li>
                                <li class="switch-tab">Новые поступления</li>
                                <li class="switch-tab">Скидка/акция</li>
                            </ul>

                            <div class="tabs__content product-tabs__content active">

                                <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/product-slider.php'; ?>

                            </div>

                            <div class="tabs__content product-tabs__content">

                                <div class="product-slider js__product-slider">

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="3.5">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">990</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="2">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">990</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="1">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">990</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="3.5">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">990</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="3.5">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">990</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="1">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">990</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="5">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">990</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="3.5">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="2" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">990</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="3">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">990</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->



                                </div>

                            </div>

                            <div class="tabs__content product-tabs__content">

                                <div class="product-slider js__product-slider">

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/3.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="1.5">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">800</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/3.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="2.5">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">800</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/3.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="3">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">800</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/3.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="3.5">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">800</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/3.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="1.5">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">800</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/3.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="1.5">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">800</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid">
                                        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/3.png')"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                                <div class="rating__star product-slider__star" data-rating="1.5">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27
                                            IP54</a>
                                        <div class="product-slider__buy-price">
                                            <button class="buy-button js__show-count"></button>
                                            <div class="count no-active">
                                                <div class="count__inner js__count">
                                                    <input class="js__count-minus count__minus" type="button" value="-" />
                                                    <input class="js__count-sum count__sum" type="text" size="3" value="0"/>
                                                    <input class="js__count-plus count__plus" type="button" value="+" />
                                                </div>
                                            </div>
                                            <div class="price-rub product-slider__price">
                                                <div class="price-rub__inner">800</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/calculator.php'; ?>

            <section class="partners-slider">
                <div class="content">
                    <div class="partners-slider__inner js__partners-slider">
                        <a href="#"><img src="/f/i/partners/1.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/2.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/3.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/4.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/1.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/2.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/3.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/4.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/1.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/2.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/3.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/4.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/1.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/2.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/3.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/4.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/1.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/2.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/3.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/4.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/1.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/2.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/3.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/4.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/1.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/2.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/3.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/4.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/1.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/2.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/3.png" class="partners-slider__item"></a>
                        <a href="#"><img src="/f/i/partners/4.png" class="partners-slider__item"></a>
                    </div>
                </div>
            </section>

            <section class="product-list bgc_elis">
                <div class="content">
                    <div class="product-list__inner">
                        <!--Банер в в начале-->
                        <a href="#" class="product-list__banner" style="background-image: url('/f/i/banners/product-price1.jpg')"></a>
                        <!-- / Банер в в начале-->

                        <!--Конкретный товар-->
                        <div class="product-price product-list__product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                        <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price product-list__product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price product-list__product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price product-list__product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price product-list__product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price product-list__product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price product-list__product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price product-list__product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Банер в конце-->
                        <a href="#" class="product-list__banner" style="background-image: url('/f/i/banners/product-price2.jpg')"></a>
                        <!--/ Банер в конце-->
                    </div>
                </div>
            </section>

            <section class="news">
                <div class="content">
                    <div class="news__inner">

                        <div class="news__left-block">
                            <div class="news__title">Будь в курсе</div>
                            <div class="news-slider">
                                <div class="news-slider__inner js__news-slider">

                                    <div class="news-slider__item-slid" style="background-image: url(/f/i/news/slider-1.jpg)">
                                        <a href="#" class="news-slider__title">Обзор новых инструментов для высотных работ</a>
                                        <a href="#" class="news-slider__description">Что делать, если хочется хорошего, мужского инструмента, а бюджет ограничен? Хватило только на шуруповерт, а остальное? Не переживайте, есть отличный способ пополнить ассортимент домашнего инструмента!</a>
                                    </div>

                                    <div class="news-slider__item-slid" style="background-image: url(/f/i/news/1.jpg)">
                                        <a href="#" class="news-slider__title">Обзор новых инструментов для высотных работ</a>
                                        <a href="#" class="news-slider__description">Что делать, если хочется хорошего, мужского инструмента, а бюджет ограничен? Хватило только на шуруповерт, а остальное? Не переживайте, есть отличный способ пополнить ассортимент домашнего инструмента!</a>
                                    </div>

                                    <div class="news-slider__item-slid" style="background-image: url(/f/i/news/2.jpg)">
                                        <a href="#" class="news-slider__title">Обзор новых инструментов для высотных работ</a>
                                        <a href="#" class="news-slider__description">Что делать, если хочется хорошего, мужского инструмента, а бюджет ограничен? Хватило только на шуруповерт, а остальное? Не переживайте, есть отличный способ пополнить ассортимент домашнего инструмента!</a>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="news__right-block">
                            <div class="news__wrap-title">
                                <div class="news__title">Новости</div>
                                <a href="#" class="news__link">Смотреть все новости</a>
                            </div>
                            <div class="news__wrap-articles">

                                <div class="item-article news__item-article">
                                    <div class="item-article__inner">
                                        <a href="#" class="item-article__image" style="background-image: url('/f/i/news/1.jpg')"></a>
                                        <div class="item-article__date">10 Января 2020</div>
                                        <a href="#" class="item-article__title">Приятные мелочи</a>
                                        <a href="#" class="item-article__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                                    </div>
                                </div>

                                <div class="item-article news__item-article">
                                    <div class="item-article__inner">
                                        <a href="#" class="item-article__image" style="background-image: url('/f/i/news/2.jpg')"></a>
                                        <div class="item-article__date">12 Января 2020</div>
                                        <a href="#" class="item-article__title">Обзор новых инструментов для высотных работ</a>
                                        <a href="" class="item-article__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="blogs">
                <div class="content">
                    <div class="blogs__inner">
                        <div class="blogs__title">Блоги</div>
                        <div class="blogs__content">
                            <div class="anim-num blogs__anim-num">
                                <div class="anim-num__inner">
                                    <div class="anim-num__wrap-numbers">
                                        <div class="anim-num__line anim-num__line_black js__anim-num"></div>
                                        <div class="anim-num__item-number active">01</div>
                                        <div class="anim-num__item-number">02</div>
                                        <div class="anim-num__item-number">03</div>
                                        <div class="anim-num__item-number">04</div>
                                        <div class="anim-num__item-number">05</div>
                                    </div>
                                    <div class="anim-num__blue">
                                        <div class="anim-num__line anim-num__line_white js__anim-num"></div>

                                        <div data-number="1" class="anim-num__article active">
                                            <a href="#" class="anim-num__title">Правильная установка дифференциального автомата</a>
                                            <a href="#" class="anim-num__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                                        </div>
                                        <div data-number="2" class="anim-num__article">
                                            <a href="#" class="anim-num__title">Правильная установка дифференциального автомата 2</a>
                                            <a href="#" class="anim-num__description">Светодиодные системы, а вернее их первые образцы2</a>
                                        </div>
                                        <div data-number="3" class="anim-num__article">
                                            <a href="#" class="anim-num__title">Правильная установка дифференциального автомата 3</a>
                                            <a href="#" class="anim-num__description">Светодиодные системы, а вернее их первые образцы3</a>
                                        </div>
                                        <div data-number="4" class="anim-num__article">
                                            <a href="#" class="anim-num__title">Правильная установка дифференциального автомата 4</a>
                                            <a href="#" class="anim-num__description">Светодиодные системы, а вернее их первые образцы4</a>
                                        </div>
                                        <div data-number="5" class="anim-num__article">
                                            <a href="#" class="anim-num__title">Правильная установка дифференциального автомата 5</a>
                                            <a href="#" class="anim-num__description">Светодиодные системы, а вернее их первые образцы6</a>
                                        </div>


                                        <ul class="anim-num__tags">
                                            <li>#Свет</li>
                                            <li>#Кабель</li>
                                            <li>#Выключатель</li>
                                        </ul>
                                        <span class="link-arrow anim-num__link-arrow">Все статьи блога</span>
                                    </div>
                                </div>
                            </div>
                            <div class="subscribe blogs__subscribe">
                                <div class="subscribe__inner">
                                    <div class="subscribe__title">Подписаться на&nbsp;рассылку</div>
                                    <form action="">
                                        <div class="form-data">
                                            <div class="form-data__title">Адрес электронной почты</div>
                                            <div class="form-data__wrap">
                                                <input class="form-data__input js__email" name="email" type="text">
                                            </div>
                                            <div class="form-data__error">Заполните поле "Адрес электронной почты"</div>
                                        </div>
                                        <div class="form-data">
                                            <div class="form-data__wrap-submit">
                                                <button class="js__disabled-btn form-data__submit blue-button" type="submit">Подписаться</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </main>

        <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

    </div>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>