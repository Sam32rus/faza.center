<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>



    <main class="main">
        <div class="content">

            <section class="cabinet">
                <div class="cabinet__inner">
                    <div class="cabinet__menu js__cabinet-menu">
                        <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/cab-menu.php'; ?>
                    </div>
                    <div class="cabinet__main">
                        <div class="cabinet__title js__cabinet-menu-open">Подписка</div>
                        <div class="cabinet__content">

                            <form action="" class="cabinet__form">
                                <div class="form-data">
                                    <div class="form-data__wrap">
                                        <input class="form-data__input js__email" placeholder="Адрес электронной почты" name="email" type="text">
                                    </div>
                                    <div class="form-data__error">Заполните поле "Адрес электронной почты"</div>
                                </div>
                                <div class="cabinet__gray-text">
                                    После добавления или изменения адреса подписки вам будет выслан код подтверждения. Подписка будет не активной до ввода кода подтверждения.
                                </div>

                                <div class="cabinet__sub-title">Рубрика подписки</div>

                                <div class="form-data form-data_small-indent">
                                    <input id="checkbox-1" class="form-data__checkbox js__checkbox" type="checkbox" name="agreement">
                                    <label for="checkbox-1" class="form-data__label"><b>Новости магазина</b></label>
                                </div>

                                <div class="form-data form-data_small-indent">
                                    <input id="checkbox-2" class="form-data__checkbox js__checkbox" type="checkbox" name="agreement">
                                    <label for="checkbox-2" class="form-data__label"><b>Акции и спецпредложения</b></label>
                                </div>

                                <div class="form-data form-data_big-indent">
                                    <input id="checkbox-3" class="form-data__checkbox js__checkbox" type="checkbox" name="agreement">
                                    <label for="checkbox-3" class="form-data__label"><b>Новые споступления</b></label>
                                </div>
                                <div class="cabinet__wrap-btn"><div class="blue-button">Подписаться</div></div>
                            </form>

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>
