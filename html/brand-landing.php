<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Главная | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>


    <main class="main">

        <article class="article">
            <div class="article__inner">
                <div class="article__wrap-head-img">
                    <img src="/f/i/partners/big-ecola.jpg" alt="Ecola" class="article__item-head-img">
                </div>
                <h1 class="article__center">Широкие ассортимент компании Ecola</h1>
            </div>
        </article>

        <section class="scroll-menu">
            <div class="content">
                <div class="scroll-menu__inner">
                    <a href="" class="scroll-menu__item-point">Лампы</a>
                    <a href="" class="scroll-menu__item-point">Усилители</a>
                    <a href="" class="scroll-menu__item-point">Конроллеры</a>
                    <a href="" class="scroll-menu__item-point">Ленты</a>
                    <a href="" class="scroll-menu__item-point">Питание</a>
                    <a href="" class="scroll-menu__item-point">Световые модули</a>
                </div>
            </div>
        </section>

        <section class="promo-sliders">
            <div class="content">
                <div class="promo-sliders__inner">

                    <div class="promo-sliders__l-partners-slider">
                        <div class="l-partners-slider js__l-partners-slider">
                            <div class="l-partners-slider__item-slid" style="background-image: url('/f/i/partners/era-lend-big.jpg')"></div>
                            <div class="l-partners-slider__item-slid" style="background-image: url('/f/i/partners/era-lend-big.jpg')"></div>
                            <div class="l-partners-slider__item-slid" style="background-image: url('/f/i/partners/era-lend-big.jpg')"></div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


        <section class="l-partners-blocks">
            <div class="content">
                <div class="l-partners-blocks__inner">
                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Лампы</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Уселители Уселители Уселители Уселители</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Контроллеры</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Ленты Ленты Ленты Ленты Ленты</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Питание</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">ПитаниеПитание  Световые модули</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Вся продукция Вся продукция Вся продукция Вся продукция</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Лампы</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Уселители Уселители Уселители Уселители</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Контроллеры</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Ленты Ленты Ленты Ленты Ленты</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Питание</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">ПитаниеПитание  Световые модули</div>
                    </a>

                    <a href="#" class="l-partners-blocks__item" style="background-image: url('/f/i/promo-slider/small-1.jpg">
                        <div class="l-partners-blocks__title">Вся продукция Вся продукция Вся продукция Вся продукция</div>
                    </a>
                </div>
            </div>
        </section>


        <section class="section">
            <div class="content">
                <div class="section__inner">
                    <div class="section__title">Популярные товары</div>
                    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/product-slider.php'; ?>
                </div>
            </div>
        </section>

        <section class="product-tabs">
            <div class="content">
                <div class="product-tabs__inner">
                    <div class="tabs js__tabs">
                        <ul class="product-tabs__switch js__tabs-caption">
                            <li class="switch-tab active">О бренде</li>
                            <li class="switch-tab">Отзывы</li>
                        </ul>

                        <div class="tabs__content product-tabs__content active">

                            <div class="description-product">
                                <div class="description-product__inner">
                                    <div class="description-product__title">О бренде</div>
                                    <div class="description-product__content">
                                        <div class="article">
                                            <p>Ecola – один из ведущих китайских производителей энергосберегающих осветительных приборов. Компания функционирует на рынке светотехники с 2006 года. Ее конкурентными преимуществами являются внушительный ассортимент выпускаемой продукции, оптимальное соотношение цена/качество, экологичность и долговечность.</p>
                                            <p>Энергосберегающие лампы Ecola имеют компактные размеры, низкое энергопотребление и высокое КПД. Такие источники света позволяют сэкономить в 15 раз больше электрической энергии, чем при использовании обычных ламп накаливания. То же самое касается и эксплуатационного срока.</p>
                                            <img src="/f/i/promo-slider/big-1.jpg" alt="">
                                            <p>Продукция Ecola представлена на рынке:</p>
                                            <ul>
                                                <li>потолочными светильниками;</li>
                                                <li>энергосберегающими лампами;</li>
                                                <li>светодиодными лентами и панелями;</li>
                                                <li>прожекторами;</li>
                                                <li>переходниками;</li>
                                                <li>патронами;</li>
                                                <li>аксессуарами.</li>
                                            </ul>
                                            <p>Чтобы удерживать лидерство и высокие конкурентные позиции, специалисты Ecola не только строго следят за тенденциями изменения спроса потребителей, но и создают новые тренды в светотехнической отрасли.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tabs__content product-tabs__content">

                            <div class="description-product">
                                <div class="description-product__inner">
                                    <div class="description-product__title">Отзывы</div>

                                    <!-- Блок рейтинг-->
                                    <div class="appraisals description-product__appraisals">
                                        <div class="appraisals__inner">
                                            <div class="appraisals__title">Рейтинг</div>
                                            <div class="rating appraisals__rating">
                                                <div class="rating__inner">
                                                    <div class="rating__star" data-rating="1.5">
                                                        <div class="rating__fill"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="appraisals__item-data">
                                                <div class="appraisals__grade">5</div>
                                                <div class="appraisals__line">
                                                    <div style="width: 90%" class="appraisals__fill"></div>
                                                </div>
                                                <div class="appraisals__sum">26</div>
                                            </div>
                                            <div class="appraisals__item-data">
                                                <div class="appraisals__grade">4</div>
                                                <div class="appraisals__line">
                                                    <div style="width: 20%"class="appraisals__fill"></div>
                                                </div>
                                                <div class="appraisals__sum">18</div>
                                            </div>
                                            <div class="appraisals__item-data">
                                                <div class="appraisals__grade">4</div>
                                                <div class="appraisals__line">
                                                    <div style="width: 60%" class="appraisals__fill"></div>
                                                </div>
                                                <div class="appraisals__sum">23</div>
                                            </div>
                                            <div class="appraisals__item-data">
                                                <div class="appraisals__grade">2</div>
                                                <div class="appraisals__line">
                                                    <div style="width: 10%" class="appraisals__fill"></div>
                                                </div>
                                                <div class="appraisals__sum">3</div>
                                            </div>
                                            <div class="appraisals__item-data">
                                                <div class="appraisals__grade">1</div>
                                                <div class="appraisals__line">
                                                    <div style="width: 0%" class="appraisals__fill"></div>
                                                </div>
                                                <div class="appraisals__sum">0</div>
                                            </div>

                                            <div class="appraisals__wrap-btn">
                                                <div class="transparent-button appraisals__transparent-button show-tablet" data-popup-btn="all-reviews">Все отзывы</div>
                                                <div class="transparent-button appraisals__transparent-button" data-popup-btn="add-review">Оставить отзыв</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Блок рейтинг-->

                                    <div class="description-product__content">
                                        <div class="reviews description-product__reviews">
                                            <div class="reviews__inner">

                                                <!--Конкретный отзыв-->
                                                <div class="reviews__item" itemprop="review" itemscope itemtype="http://schema.org/Review">
                                                    <div class="reviews__author" itemprop="author">Игорь</div>
                                                    <div class="reviews__date" itemprop="datePublished">20.10.2020</div>
                                                    <div class="rating reviews__rating">
                                                        <div class="rating__inner">
                                                            <div class="rating__star" itemprop="reviewRating" data-rating="1.5">
                                                                <div class="rating__fill"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
                                                    <div class="merit">
                                                        <div class="merit__inner">
                                                            <div class="merit__item merit__item_like">Работает стабильно, светит ярко, есть гарантия</div>
                                                            <div class="merit__item merit__item_dislike">Нет</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/Конкретный отзыв-->

                                                <!--Конкретный отзыв-->
                                                <div class="reviews__item" itemprop="review" itemscope itemtype="http://schema.org/Review">
                                                    <div class="reviews__author" itemprop="author">Михаил</div>
                                                    <div class="reviews__date" itemprop="datePublished">20.10.2020</div>
                                                    <div class="rating reviews__rating">
                                                        <div class="rating__inner">
                                                            <div class="rating__star" itemprop="reviewRating" data-rating="1.5">
                                                                <div class="rating__fill"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
                                                    <div class="merit">
                                                        <div class="merit__inner">
                                                            <div class="merit__item merit__item_like">Рживучесть, яркость</div>
                                                            <div class="merit__item merit__item_dislike">не замечено</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/Конкретный отзыв-->

                                                <!--Конкретный отзыв-->
                                                <div class="reviews__item" itemprop="review" itemscope itemtype="http://schema.org/Review">
                                                    <div class="reviews__author" itemprop="author">Ирина</div>
                                                    <div class="reviews__date" itemprop="datePublished">20.10.2020</div>
                                                    <div class="rating reviews__rating">
                                                        <div class="rating__inner">
                                                            <div class="rating__star" itemprop="reviewRating" data-rating="1.5">
                                                                <div class="rating__fill"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
                                                    <div class="merit">
                                                        <div class="merit__inner">
                                                            <div class="merit__item merit__item_like">не замечено</div>
                                                            <div class="merit__item merit__item_dislike">Работает не стабильно, светит не ярко, нет гарантии</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/Конкретный отзыв-->


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </section>



    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>