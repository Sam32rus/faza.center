<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Корзина | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/breadcrumbs.php'; ?>


    <main class="main">
        <h1 class="title-page"><span class="content">Выберете продукт</span></h1>

        <section class="repairs">
                <div class="content">
                    <div class="repairs__inner">
                        <div class="repairs__content">
                            <div class="choice hide-phone">
                                <div class="choice__inner" style="background-image: url('/f/i/rooms/big-room1.jpg')">

                                    <div class="choice__point choice__point-living-1 tooltip">
                                        <span class="tooltip__content">
                                            <a href="#" class="choice__tooltip-link">Светильники</a>
                                        </span>
                                    </div>

                                    <div class="choice__point choice__point-living-2 tooltip">
                                        <span class="tooltip__content">
                                            <a href="#" class="choice__tooltip-link">Лампы накаливания</a>
                                        </span>
                                    </div>

                                </div>
                            </div>
                            <div class="repairs__other">
                                <div class="repairs__other-head">Вам также может быть интересно:</div>

                                <ul class="repairs__other-ul">
                                    <li class="repairs__other-li">
                                        <a href="#" class="repairs__item-other">Источники света (лампы)</a>
                                        <ul class="repairs__other-ul repairs__other-ul_child">
                                            <li class="repairs__other-li repairs__other-li_child"><a href="#" class="repairs__item-other">Автомобильные лампы</a></li>
                                            <li class="repairs__other-li repairs__other-li_child"><a href="#" class="repairs__item-other">Светодиодная лента</a></li>
                                            <li class="repairs__other-li repairs__other-li_child"><a href="#" class="repairs__item-other">Новогодние</a></li>
                                        </ul>
                                    </li>
                                    <li class="repairs__other-li"><a href="#" class="repairs__item-other">Молниезащита</a></li>
                                    <li class="repairs__other-li"><a href="#" class="repairs__item-other">Электроустановочные изделия</a></li>
                                    <li class="repairs__other-li">
                                        <a href="#" class="repairs__item-other">Провода и кабели</a>
                                        <ul class="repairs__other-ul repairs__other-ul_child">
                                            <li class="repairs__other-li repairs__other-li_child"><a href="#" class="repairs__item-other">Силовые</a></li>
                                            <li class="repairs__other-li repairs__other-li_child"><a href="#" class="repairs__item-other">Контрольные</a></li>
                                            <li class="repairs__other-li repairs__other-li_child"><a href="#" class="repairs__item-other">Медные</a></li>
                                        </ul>
                                    </li>
                                    <li class="repairs__other-li"><a href="#" class="repairs__item-other">Молниезащита</a></li>
                                    <li class="repairs__other-li"><a href="#" class="repairs__item-other">Умный дом</a></li>
                                    <li class="repairs__other-li"><a href="#" class="repairs__item-other">Инструмент</a></li>
                                    <li class="repairs__other-li">
                                        <a href="#" class="repairs__item-other">Провода и кабели</a>
                                        <ul class="repairs__other-ul repairs__other-ul_child">
                                            <li class="repairs__other-li repairs__other-li_child"><a href="#" class="repairs__item-other">Силовые</a></li>
                                            <li class="repairs__other-li repairs__other-li_child"><a href="#" class="repairs__item-other">Контрольные</a></li>
                                            <li class="repairs__other-li repairs__other-li_child"><a href="#" class="repairs__item-other">Медные</a></li>
                                        </ul>
                                    </li>
                                    <li class="repairs__other-li"><a href="#" class="repairs__item-other">Молниезащита</a></li>
                                    <li class="repairs__other-li"><a href="#" class="repairs__item-other">Умный дом</a></li>
                                    <li class="repairs__other-li"><a href="#" class="repairs__item-other">Инструмент</a></li>
                                </ul>


                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/catalog-calculat.php'; ?>

    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>