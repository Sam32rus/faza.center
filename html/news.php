<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/breadcrumbs.php'; ?>


    <main class="main">
        <h1 class="title-page"><span class="content">Все новости</span></h1>

        <section class="all-news">
            <div class="content">
                <div class="sorting">
                    <div class="sorting__inner">
                        <div class="sorting__found"><span>128</span> записей</div>
                    </div>
                </div>

                <div class="all-news__inner">

                    <div class="item-article all-news__item-article">
                        <div class="item-article__inner">
                            <a href="#" class="item-article__image" style="background-image: url('/f/i/news/1.jpg')"></a>
                            <div class="item-article__date">10 Января 2020</div>
                            <a href="#" class="item-article__title">Приятные мелочи</a>
                            <a href="#" class="item-article__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                        </div>
                    </div>

                    <div class="item-article all-news__item-article">
                        <div class="item-article__inner">
                            <a href="#" class="item-article__image" style="background-image: url('/f/i/news/2.jpg')"></a>
                            <div class="item-article__date">12 Января 2020</div>
                            <a href="#" class="item-article__title">Обзор новых инструментов для высотных работ</a>
                            <a href="" class="item-article__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                        </div>
                    </div>

                    <div class="item-article all-news__item-article">
                        <div class="item-article__inner">
                            <a href="#" class="item-article__image" style="background-image: url('/f/i/news/1.jpg')"></a>
                            <div class="item-article__date">10 Января 2020</div>
                            <a href="#" class="item-article__title">Приятные мелочи</a>
                            <a href="#" class="item-article__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                        </div>
                    </div>

                    <div class="item-article all-news__item-article">
                        <div class="item-article__inner">
                            <a href="#" class="item-article__image" style="background-image: url('/f/i/news/2.jpg')"></a>
                            <div class="item-article__date">12 Января 2020</div>
                            <a href="#" class="item-article__title">Обзор новых инструментов для высотных работ</a>
                            <a href="" class="item-article__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                        </div>
                    </div>

                    <div class="item-article all-news__item-article">
                        <div class="item-article__inner">
                            <a href="#" class="item-article__image" style="background-image: url('/f/i/news/1.jpg')"></a>
                            <div class="item-article__date">10 Января 2020</div>
                            <a href="#" class="item-article__title">Приятные мелочи</a>
                            <a href="#" class="item-article__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                        </div>
                    </div>

                    <div class="item-article all-news__item-article">
                        <div class="item-article__inner">
                            <a href="#" class="item-article__image" style="background-image: url('/f/i/news/2.jpg')"></a>
                            <div class="item-article__date">12 Января 2020</div>
                            <a href="#" class="item-article__title">Обзор новых инструментов для высотных работ</a>
                            <a href="" class="item-article__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                        </div>
                    </div>

                    <div class="item-article all-news__item-article">
                        <div class="item-article__inner">
                            <a href="#" class="item-article__image" style="background-image: url('/f/i/news/1.jpg')"></a>
                            <div class="item-article__date">10 Января 2020</div>
                            <a href="#" class="item-article__title">Приятные мелочи</a>
                            <a href="#" class="item-article__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                        </div>
                    </div>

                    <div class="item-article all-news__item-article">
                        <div class="item-article__inner">
                            <a href="#" class="item-article__image" style="background-image: url('/f/i/news/2.jpg')"></a>
                            <div class="item-article__date">12 Января 2020</div>
                            <a href="#" class="item-article__title">Обзор новых инструментов для высотных работ</a>
                            <a href="" class="item-article__description">Светодиодные системы, а вернее их первые образцы, были созданы еще в середине прошлого</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>



    </main>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addJs.php'; ?>


</body>
</html>