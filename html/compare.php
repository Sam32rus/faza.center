<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/breadcrumbs.php'; ?>


    <main class="main">
        <h1 class="title-page"><span class="content">Розетки и выключатели</span></h1>


        <div class="compare">
            <div class="content">
                <div class="compare__inner js__compare">

                    <div class="compare__item">

                        <div class="product-price compare__product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                <div class="rating compare__rating">
                                    <div class="rating__inner">
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">7000</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Артикул</div>
                            <div class="compare__property-desc">Б0030042</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Цена</div>
                            <div class="compare__property-desc">4 800 ₽ Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Артикул</div>
                            <div class="compare__property-desc">Б0030042</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Цена</div>
                            <div class="compare__property-desc">4 800 ₽</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Артикул</div>
                            <div class="compare__property-desc">Б0030042</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Цена</div>
                            <div class="compare__property-desc">4 800 ₽</div>
                        </div>
                    </div>

                    <div class="compare__item">

                        <div class="product-price compare__product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                <div class="rating compare__rating">
                                    <div class="rating__inner">
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">7000</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Артикул</div>
                            <div class="compare__property-desc">Б0030042</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Цена</div>
                            <div class="compare__property-desc">4 800 ₽</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Артикул</div>
                            <div class="compare__property-desc">Б0030042</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Цена</div>
                            <div class="compare__property-desc">4 800 ₽</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Артикул</div>
                            <div class="compare__property-desc">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт Б0030042 Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт Б0030042</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Цена</div>
                            <div class="compare__property-desc">4 800 ₽</div>
                        </div>
                    </div>

                    <div class="compare__item">

                        <div class="product-price compare__product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                <div class="rating compare__rating">
                                    <div class="rating__inner">
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">7000</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Артикул</div>
                            <div class="compare__property-desc">Б0030042</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Цена</div>
                            <div class="compare__property-desc">4 800 ₽</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Артикул</div>
                            <div class="compare__property-desc">Б0030042</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Цена</div>
                            <div class="compare__property-desc">4 800 ₽</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Артикул</div>
                            <div class="compare__property-desc">Б0030042</div>
                        </div>
                        <div class="compare__property">
                            <div class="compare__property-title">Цена</div>
                            <div class="compare__property-desc">4 800 ₽</div>
                        </div>
                    </div>




                </div>
            </div>
        </div>



    </main>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addJs.php'; ?>


</body>
</html>