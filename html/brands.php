<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/breadcrumbs.php'; ?>


    <main class="main">
        <h1 class="title-page"><span class="content">Бренды</span></h1>


        <div class="general">
            <div class="content">
                <div class="general__inner">

                    <div class="brands brands_big">
                        <a href="" class="brands__inner">
                            <span class="brands__img" style="background-image: url('/f/i/partners/1.png')"></span>
                            <span class="brands__title">Калужский кабельный завод</span>
                        </a>
                    </div>

                    <div class="brands brands_big">
                        <a href="" class="brands__inner">
                            <span class="brands__img" style="background-image: url('/f/i/partners/2.png')"></span>
                            <span class="brands__title">Energizer</span>
                        </a>
                    </div>

                    <div class="brands brands_big">
                        <a href="" class="brands__inner">
                            <span class="brands__img" style="background-image: url('/f/i/partners/3.png')"></span>
                            <span class="brands__title">Ezetek</span>
                        </a>
                    </div>

                    <div class="brands">
                        <a href="" class="brands__inner">
                            <span class="brands__img" style="background-image: url('/f/i/partners/4.png')"></span>
                            <span class="brands__title">Ritter</span>
                        </a>
                    </div>

                    <div class="brands">
                        <a href="" class="brands__inner">
                            <span class="brands__img" style="background-image: url('/f/i/partners/big-ecola.jpg')"></span>
                            <span class="brands__title">БАРС-ЭЛЕКТРО</span>
                        </a>
                    </div>

                    <div class="brands">
                        <a href="" class="brands__inner">
                            <span class="brands__img" style="background-image: url('/f/i/partners/big-mazo.png')"></span>
                            <span class="brands__title">ГАРНИЗОН</span>
                        </a>
                    </div>

                    <div class="brands">
                        <a href="" class="brands__inner">
                            <span class="brands__img" style="background-image: url('/f/i/partners/n1.png')"></span>
                            <span class="brands__title">МЗЭМИ</span>
                        </a>
                    </div>

                    <div class="brands">
                        <a href="" class="brands__inner">
                            <span class="brands__img" style="background-image: url('/f/i/partners/n2.png')"></span>
                            <span class="brands__title">Людиновокабель</span>
                        </a>
                    </div>

                    <div class="brands">
                        <a href="" class="brands__inner">
                            <span class="brands__img" style="background-image: url('/f/i/partners/n3.png')"></span>
                            <span class="brands__title">Энергомера</span>
                        </a>
                    </div>

                </div>
            </div>
        </div>



    </main>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addJs.php'; ?>


</body>
</html>