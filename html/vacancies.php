<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addCss.php'; ?>
    <title>Вакансии | Faza</title>
</head>
<body>
    
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/breadcrumbs.php'; ?>

    <main class="main">
        <h1 class="title-page"><span class="content">Все вакансии</span></h1>

        <section class="vacancies">
            <div class="content">
                <div class="sorting">
                    <div class="sorting__inner">
                        <div class="sorting__found"><span>128</span> вакансий</div>
                    </div>
                </div>

                <div class="vacancies__inner">
                    <div class="vacancies__title">Вакансии:</div>

                        <a href="/html/article-promo.php" class="vacancy-item">
                            <span class="vacancy-item__inner">
                                <span class="vacancy-item__img" style="background-image: url('/f/i/user.png')"></span>
                                <span class="vacancy-item__content">
                                    <span class="vacancy-item__title">Оператор Контактного центра на дому</span>
                                    <span class="vacancy-item__date">Статус: Закрыта</span>
                                    <span class="vacancy-item__desc">Обработка входящих звонков. Работа на дому, подработка, свободный график, работа для студентов.</span>
                                    <span class="vacancy-item__details">Подробнее</span>
                                </span>
                            </span>
                        </a>

                        <a href="/html/article-promo.php" class="vacancy-item">
                            <span class="vacancy-item__inner">
                                <span class="vacancy-item__img" style="background-image: url('/f/i/user.png')"></span>
                                <span class="vacancy-item__content">
                                    <span class="vacancy-item__title">Продавец-консультант</span>
                                    <span class="vacancy-item__date">Статус: Закрыта</span>
                                    <span class="vacancy-item__desc">Мы ждём тебя, если ты интересуешься техникой, любишь общаться с людьми, у тебя есть желание развиваться в продажах и зарабатывать. Этого достаточно, а остальному мы научим!</span>
                                    <span class="vacancy-item__details">Подробнее</span>
                                </span>
                            </span>
                        </a>

                        <a href="/html/article-promo.php" class="vacancy-item">
                            <span class="vacancy-item__inner">
                                <span class="vacancy-item__img" style="background-image: url('/f/i/user.png')"></span>
                                <span class="vacancy-item__content">
                                    <span class="vacancy-item__title">Оператор Контактного центра на дому</span>
                                    <span class="vacancy-item__date">Статус: Закрыта</span>
                                    <span class="vacancy-item__desc">Обработка входящих звонков. Работа на дому, подработка, свободный график, работа для студентов.</span>
                                    <span class="vacancy-item__details">Подробнее</span>
                                </span>
                            </span>
                        </a>

                        <a href="/html/article-promo.php" class="vacancy-item">
                            <span class="vacancy-item__inner">
                                <span class="vacancy-item__img" style="background-image: url('/f/i/user.png')"></span>
                                <span class="vacancy-item__content">
                                    <span class="vacancy-item__title">Продавец-консультант</span>
                                    <span class="vacancy-item__date">Статус: Закрыта</span>
                                    <span class="vacancy-item__desc">Мы ждём тебя, если ты интересуешься техникой, любишь общаться с людьми, у тебя есть желание развиваться в продажах и зарабатывать. Этого достаточно, а остальному мы научим!</span>
                                    <span class="vacancy-item__details">Подробнее</span>
                                </span>
                            </span>
                        </a>

                </div>
            </div>
        </section>

    </main>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addJs.php'; ?>

<script>
    $('#counter-1').timeTo(new Date('june 28 2021 00:00:00'));
    $('#counter-2').timeTo(new Date('july 20 2021 00:00:00'));
    $('#counter-3').timeTo(new Date('june 1 2021 00:00:00'));
</script>

</body>
</html>