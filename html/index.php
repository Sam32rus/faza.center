<!doctype html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
        <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
        <title>Главная | Faza</title>
        <style>
            h2{
                font-size: 28px;
            }
            .index-list{
                padding-top: 20px;
                list-style: revert;
            }
            .index-list li{
                padding-bottom: 15px;
                font-size: 18px;
                font-weight: bold;
            }
            .index-list li{
                border-bottom: 2px solid transparent;
            }
            .index-list li a:hover, .no-link:hover{
                border-bottom: 2px solid #000;
                cursor: pointer;
            }
        </style>
    </head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>
        <div class="content">

            <h2>Сайт</h2>
            <ol class="index-list" style="margin-bottom: 40px">
                <li><a href="/html/home.php">Главная</a></li>
                <li><a href="/html/catalog.php">Каталог</a></li>
                <li><a href="/html/catalog-general.php">Каталог (Общая)</a></li>
                <li><a href="/html/search.php">Результаты поиска</a></li>
                <li><a href="/html/shine.php">Светотехнические изделия</a></li>
                <li><a href="/html/article.php">О компании</a></li>
                <li><a href="/html/brand.php">О Бреде</a></li>
                <li><a href="/html/brand-landing.php">О Бреде (Лендинг)</a></li>
                <li><a href="/html/brands.php">Страница брендов</a></li>
                <li><a href="/html/basket.php">Корзина</a></li>
                <li><a href="/html/empty-cart.php">Пустая корзина</a></li>
                <li><a href="/html/empty-compare.php">Пустая страница сравнение</a></li>
                <li><a href="/html/404.php">404</a></li>
                <li><a href="/html/compare.php">Сравнить</a></li>
                <li><a href="/html/contacts.php">Контакты</a></li>
                <li><a href="/html/calculator.php">Калькулятор (Внутренняя)</a></li>
                <li><a href="/html/news.php">Все новости, записи блога</a></li>
                <li><a href="/html/promotions.php">Акции (Общая)</a></li>
                <li><a href="/html/article-promo.php">Акция (Внутренняя)</a></li>
                <li><a href="/html/card-product.php">Карточка товара</a></li>
                <li><a href="/html/mail.php">Email письмо</a></li>
                <li><a href="/html/gift-cards.php">Подарочные карты</a></li>
                <li><a href="/html/blog.php">Блог (Общая)</a></li>
                <li><a href="/html/article-blog.php">Блог (Внутренняя)</a></li>
            </ol>

            <h2>Попапы (Сайт)</h2>
            <ol class="index-list" style="margin-bottom: 40px">
                <li><span data-popup-btn="specify" class="no-link">Попап (Уточнить цену у производителя)</span></li>
                <li><span data-popup-btn="login" class="no-link">Попап (Авторизация)</span></li>
                <li><span data-popup-btn="registration" class="no-link">Попап (Регистрация)</span></li>
                <li><span data-popup-btn="recovery" class="no-link">Попап (Востановление пароля)</span></li>
                <li><span data-popup-btn="recovery-ok" class="no-link">Попап (Пароль востановлен)</span></li>
                <li><span data-popup-btn="registration-ok" class="no-link">Попап (Вы успешно зарегистрированы)</span></li>
                <li><span data-popup-btn="mail-ok" class="no-link">Попап (Вы успешно подписаны на нашу e-mail рассылку)</span></li>
                <li><span data-popup-btn="add-review" class="no-link">Попап (Добавить отзыв)</span></li>
                <li><span data-popup-btn="all-reviews" class="no-link">Попап (Все отзывы)</span></li>
                <li><span data-popup-btn="share" class="no-link">Попап (Поделиться)</span></li>
                <li><span data-popup-btn="ok-basket" class="no-link">Попап (Товар добавлен в корзину)</span></li>
                <li><span data-popup-btn="quick-request" class="no-link">Попап (Купить в один клик)</span></li>
                <li><span data-popup-btn="gift-cards" class="no-link">Попап (Подарочные карты)</span></li>
            </ol>

            <h2>Личный кабинет</h2>
            <ol class="index-list" style="margin-bottom: 80px">
                <li><a href="/html/cabinet-chosen.php">Избранные товары</a></li>
                <li><a href="/html/cabinet-viewed.php">Просмотренные</a></li>
                <li><a href="/html/cabinet-order.php">Мои заказы</a></li>
                <li><a href="/html/cabinet-order-empty.php">Мои заказы (Пустая страница)</a></li>
                <li><a href="/html/cabinet-card.php">Мои карты</a></li>
                <li><a href="/html/cabinet-card-empty.php">Мои карты (Пустая страница)</a></li>
                <li><a href="/html/cabinet-subscription.php">Подписки</a></li>
                <li><a href="/html/cabinet-cash.php">Мой счет</a></li>
                <li><a href="/html/cabinet-personal-empty.php">Персональные данные (Не заполненная)</a></li>
                <li><a href="/html/cabinet-personal.php">Персональные данные (Заполненная)</a></li>
            </ol>

            <h2>Попапы (Личный кабинет)</h2>
            <ol class="index-list" style="margin-bottom: 80px">
                <li><span data-popup-btn="card-activ" class="no-link">Попап (Карта отправлена на активацию)</span></li>
                <li><span data-popup-btn="add-card" class="no-link">Попап (Добавление новой скидочной карты)</span></li>
                <li><span data-popup-btn="add-phone" class="no-link">Попап (Добавление мобильного телефона)</span></li>
                <li><span data-popup-btn="add-address" class="no-link">Попап (Добавление адреса)</span></li>
                <li><span data-popup-btn="add-legal" class="no-link">Попап (Добавление юридических реквизитов)</span></li>
            </ol>



        </div>
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>