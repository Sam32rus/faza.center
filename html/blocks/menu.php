<menu class="menu js__menu">
    <div class="menu__inner js__menu-inner">
        <div class="menu__tabs js__tabs">
            <ul class="menu__list-tab js__tabs-caption">
                <li class="menu__item-tab active"><a class="menu__link-tab" href="#">Светотехнические изделия</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Розетки и выключатели</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Провода и кабели</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Узо, дифавтоматы, автоматы</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Арматура для СИП</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Щитовое оборудование</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Счетчики электроэнергии</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Гофра, кабель-канал, лотки, муфты</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Коробки монтажные, подрозетники</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Клеммы, изолента, крепеж</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Инструмент и средства индивидуальной защиты</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Фонари и элементы питания</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Удлинители, сетевые фильтры, стабилизаторы</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Домофоны и дверные звонки</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Обогрев и вентиляция</a></li>
                <li class="menu__item-tab"><a class="menu__link-tab" href="#">Генераторы и электродвигатели</a></li>
            </ul>
            <div class="tabs__content menu__tabs-content active">
                <ul class="menu__list-content">
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Светотехнические изделия</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Светотехнические изделия</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Светотехнические изделия</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Светотехнические изделия</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Светотехнические изделия</a></li>
                </ul>
            </div>
            <div class="tabs__content menu__tabs-content">
                <ul class="menu__list-content">
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Розетки и выключатели</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Розетки и выключатели</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Розетки и выключатели</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Розетки и выключатели</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Розетки и выключатели</a></li>
                </ul>
            </div>
            <div class="tabs__content menu__tabs-content">
                <ul class="menu__list-content">
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Провода и кабели</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Провода и кабели</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Провода и кабели</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Провода и кабели</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Провода и кабели</a></li>
                </ul>
            </div>
            <div class="tabs__content menu__tabs-content">
                <ul class="menu__list-content">
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Узо, дифавтоматы, автоматы</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Узо, дифавтоматы, автоматы</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Узо, дифавтоматы, автоматы</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Узо, дифавтоматы, автоматы</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Узо, дифавтоматы, автоматы</a></li>
                </ul>
            </div>
            <div class="tabs__content menu__tabs-content">
                <ul class="menu__list-content">
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Арматура для СИП</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Арматура для СИП</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Арматура для СИП</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Арматура для СИП</a></li>
                    <li class="menu__item-tab"><a class="menu__link-tab" href="#">Арматура для СИП</a></li>
                </ul>
            </div>
        </div>
        <div class="menu__partners">
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/1.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/2.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/3.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/4.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/1.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/2.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/3.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/4.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/1.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/2.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/3.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/4.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/1.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/2.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/3.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/4.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/1.png" class="menu__partners-img"></a>
            <a href="#" class="menu__partners-item"><img src="/f/i/partners/2.png" class="menu__partners-img"></a>
        </div>
    </div>
</menu>


<menu class="mobile-menu js__mobile-menu">
    <div class="mobile-menu__inner">
        <div class="user-menu mobile-menu__user-menu">
            <div class="user-menu__inner">
                <a href="#" class="user-menu__item-point user-menu__item-point_catalog">Каталог</a>
                <a href="#" class="user-menu__item-point user-menu__item-point_compare">Сравнить</a>
                <a href="#" class="user-menu__item-point user-menu__item-point_heart">
                    <span class="user-menu__heart-sum">8</span>
                    Избранное
                </a>
                <a href="#" class="user-menu__item-point user-menu__item-point_user" data-popup-btn="login">Вход</a>
                <a href="#" class="user-menu__item-point user-menu__item-point_basket">
                    <span class="user-menu__basket-sum">25</span>
                    Корзина
                </a>
            </div>
        </div>
        <div class="mobile-menu__close js__close-mobile-menu"></div>
        <div class="mobile-menu__wrap-list js__wrap-mobile-menu">
            <ul class="mobile-menu__main-list js__mobile-menu-main-list active">
                <li class="mobile-menu__item">Светотехнические издели</li>
                <li class="mobile-menu__item">Розетки и выключател</li>
                <li class="mobile-menu__item">Провода и кабел</li>
                <li class="mobile-menu__item">Узо, дифавтоматы, автомат</li>
                <li class="mobile-menu__item">Арматура для СИ</li>
                <li class="mobile-menu__item">Щитовое оборудовани</li>
                <li class="mobile-menu__item">Счетчики электроэнерги</li>
                <li class="mobile-menu__item">Гофра, кабель-канал, лотки, муфт</li>
                <li class="mobile-menu__item">Коробки монтажные, подрозетник</li>
                <li class="mobile-menu__item">Клеммы, изолента, крепе</li>
                <li class="mobile-menu__item">Инструмент и средства индивидуальной защит</li>
                <li class="mobile-menu__item">Фонари и элементы питани</li>
                <li class="mobile-menu__item">Удлинители, сетевые фильтры, стабилизатор</li>
                <li class="mobile-menu__item">Домофоны и дверные звонк</li>
                <li class="mobile-menu__item">Обогрев и вентиляци</li>
                <li class="mobile-menu__item">Генераторы и электродвигател</li>
            </ul>
            <ul class="mobile-menu__second-list js__mobile-menu-second-list">
                <li class="mobile-menu__item mobile-menu__item_back js__mobile-menu-back">Светотехнические издели</li>
                <li class="mobile-menu__item"><a href="#">Светотехнические издели</a></li>
                <li class="mobile-menu__item"><a href="#">Светотехнические издели</a></li>
                <li class="mobile-menu__item"><a href="#">Светотехнические издели</a></li>
                <li class="mobile-menu__item"><a href="#">Светотехнические издели</a></li>
            </ul>
            <ul class="mobile-menu__second-list js__mobile-menu-second-list">
                <li class="mobile-menu__item mobile-menu__item_back js__mobile-menu-back">Розетки и выключател</li>
                <li class="mobile-menu__item"><a href="#">Розетки и выключател</a></li>
                <li class="mobile-menu__item"><a href="#">Розетки и выключател</a></li>
                <li class="mobile-menu__item"><a href="#">Розетки и выключател</a></li>
                <li class="mobile-menu__item"><a href="#">Розетки и выключател</a></li>
            </ul>
            <ul class="mobile-menu__second-list js__mobile-menu-second-list">
                <li class="mobile-menu__item mobile-menu__item_back js__mobile-menu-back">Провода и кабел</li>
                <li class="mobile-menu__item"><a href="#">Провода и кабел</a></li>
                <li class="mobile-menu__item"><a href="#">Провода и кабел</a></li>
                <li class="mobile-menu__item"><a href="#">Провода и кабел</a></li>
                <li class="mobile-menu__item"><a href="#">Провода и кабел</a></li>
            </ul>
            <ul class="mobile-menu__second-list js__mobile-menu-second-list">
                <li class="mobile-menu__item mobile-menu__item_back js__mobile-menu-back">Узо, дифавтоматы, автомат</li>
                <li class="mobile-menu__item"><a href="#">Узо, дифавтоматы, автомат</a></li>
                <li class="mobile-menu__item"><a href="#">Узо, дифавтоматы, автомат</a></li>
                <li class="mobile-menu__item"><a href="#">Узо, дифавтоматы, автомат</a></li>
                <li class="mobile-menu__item"><a href="#">Узо, дифавтоматы, автомат</a></li>
            </ul>
        </div>
    </div>
</menu>