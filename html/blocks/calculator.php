<div class="calculator">
    <div class="content">

        <div class="calculator__inner">

            <div class="hide-phone">
                <img src="/f/i/calculator/calculator.png" class="calculator__img">
                <a href="#" class="calculator__button calculator__button_bathroom">Ванная&nbsp;комната</a>
                <div class="calculator__point calculator__point_bathroom"></div>

                <a href="#" class="calculator__button calculator__button_garage">Гараж</a>
                <div class="calculator__point calculator__point_garage"></div>

                <a href="#" class="calculator__button calculator__button_hallway">Прихожая</a>
                <div class="calculator__point calculator__point_hallway"></div>

                <a href="#" class="calculator__button calculator__button_children">Детская</a>
                <div class="calculator__point calculator__point_children"></div>

                <a href="#" class="calculator__button calculator__button_garden">Сад</a>
                <div class="calculator__point calculator__point_garden"></div>

                <a href="#" class="calculator__button calculator__button_kitchen">Кухня</a>
                <div class="calculator__point calculator__point_kitchen"></div>

                <a href="#" class="calculator__button calculator__button_sauna">Сауна</a>
                <div class="calculator__point calculator__point_sauna"></div>

                <a href="#" class="calculator__button calculator__button_bedroom">Спальная</a>
                <div class="calculator__point calculator__point_bedroom"></div>
            </div>
            <div class="show-phone">
                <div class="calculator__wrap-img-button">
                    <a href="#" class="calculator__img-button" style="background-image: url('/f/i/calculator/bathroom.webp')">
                        <span class="calculator__text-img-button">Ванная комната</span> 
                    </a>
                    <a href="#" class="calculator__img-button" style="background-image: url('/f/i/calculator/garage.webp')">
                        <span class="calculator__text-img-button">Гараж</span>
                    </a>
                    <a href="#" class="calculator__img-button" style="background-image: url('/f/i/calculator/hallway.webp')">
                        <span class="calculator__text-img-button">Прихожая</span>
                    </a>
                    <a href="#" class="calculator__img-button" style="background-image: url('/f/i/calculator/children.webp')">
                        <span class="calculator__text-img-button">Детская</span>
                    </a>
                    <a href="#" class="calculator__img-button" style="background-image: url('/f/i/calculator/garden.webp')">
                        <span class="calculator__text-img-button">Сад</span>
                    </a>
                    <a href="#" class="calculator__img-button" style="background-image: url('/f/i/calculator/kitchen.webp')">
                        <span class="calculator__text-img-button">Кухня</span>
                    </a>
                    <a href="#" class="calculator__img-button" style="background-image: url('/f/i/calculator/sauna.webp')">
                        <span class="calculator__text-img-button">Сауна</span>
                    </a>
                    <a href="#" class="calculator__img-button" style="background-image: url('/f/i/calculator/bedroom.webp')">
                        <span class="calculator__text-img-button">Спальная</span>
                    </a>
                </div>
            </div>
        </div>

    </div> 
</div> 