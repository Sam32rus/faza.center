<footer class="footer">
    <div class="content">
        <div class="footer__inner">
            <div class="footer__coll">
                <div class="footer__title">О компании</div>
                <a href="#" class="footer__link">Производители</a>
                <a href="#" class="footer__link">Контакты</a>
                <a href="#" class="footer__link">Магазины</a>
                <a href="#" class="footer__link">Калькулятор</a>
                <a href="#" class="footer__link">Стать поставщиком</a>
                <a href="#" class="footer__link">Вакансии</a>
                <a href="#" class="footer__link">Подарочные карты</a>
            </div>
            <div class="footer__coll">
                <div class="footer__title">Помощь</div>
                <a href="#" class="footer__link">Возврат</a>
                <a href="#" class="footer__link">Доставка</a>
                <a href="#" class="footer__link">Оплата</a>
                <a href="#" class="footer__link">Как оформить заказ</a>
                <a href="#" class="footer__link">Политика конфидициальности</a>
                <a href="#" class="footer__link">Бригады</a>
            </div>

            <div class="footer__coll">
                <div class="footer__title">Информация</div>
                <a href="#" class="footer__link">Новости</a>
                <a href="#" class="footer__link">Блог</a>
                <a href="#" class="footer__link">Акции и купоны</a>
            </div>
            <div class="footer__coll">
                <div class="footer__title">Кабинет</div>
                <span data-popup-btn="login" class="footer__link">Вход</span>
                <span data-popup-btn="registration" class="footer__link">Регистрация</span>
                <a href="#" class="footer__link">Сравнение</a>
                <a href="#" class="footer__link">Избранное</a>
                <a href="#" class="footer__link">Корзина</a>
            </div>
            <div class="footer__coll footer__coll_contacts">
                <div class="footer__social-list">
                    <div class="footer__title footer__title_no-line">Мы в соц. сетях</div>
                    <a href="#" target="_blank" class="footer__social-item footer__social-item_fb"></a>
                    <a href="#" target="_blank" class="footer__social-item footer__social-item_in"></a>
                    <a href="#" target="_blank" class="footer__social-item footer__social-item_yt"></a>
                    <a href="#" target="_blank" class="footer__social-item footer__social-item_vk"></a>
                </div>
                <div class="footer__wrap-phones">
                    <a href="tel:+7(4832)400-200" class="footer__phone">+7(4832)400-200</a>
                    <a href="tel:+7(4832)302-752" class="footer__phone">+7(4832)302-752</a>
                </div>

            </div>
        </div>
    </div>
</footer>