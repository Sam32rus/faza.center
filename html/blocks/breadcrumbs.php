<div class="breadcrumbs">
    <div class="content">
        <ul class="breadcrumbs__list" itemscope="http://schema.org/BreadcrumbList">
            <li class="breadcrumbs__point" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a itemprop="item" href="http://faza.center/"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumbs__point" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a itemprop="item" href="http://faza.center/item-page"><span itemprop="name">Светохнические изделия</span></a>
                <meta itemprop="position" content="2" />
            </li>
            <li class="breadcrumbs__point" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <span itemprop="item"><span itemprop="name">Лампы и электропатроны</span></span>
                <meta itemprop="position" content="3" />
            </li>
        </ul>
    </div>
</div>
