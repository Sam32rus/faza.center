<div class="product-slider js__product-slider">


    <!--Конкретный товар-->
    <div class="product-slider__item-slid">
        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/1.png')"></a>
        <div class="rating product-slider__rating">
            <div class="rating__inner">
                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__star product-slider__star" data-rating="3.5">
                    <div class="rating__fill"></div>
                </div>
            </div>
        </div>
        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27IP54</a>
        <div class="product-slider__buy-price">
            <button class="buy-button"></button>
            <div class="price-rub product-slider__price">
                <div class="price-rub__inner">1090</div>
            </div>
        </div>
    </div>
    <!--/ Конкретный товар-->

    <!--Конкретный товар-->
    <div class="product-slider__item-slid">
        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
        <div class="rating product-slider__rating">
            <div class="rating__inner">
                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__star product-slider__star" data-rating="2.5">
                    <div class="rating__fill"></div>
                </div>
            </div>
        </div>
        <a href="#" class="product-slider__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
        <div class="product-slider__buy-price">
            <button class="buy-button"></button>
            <div class="price-rub product-slider__price">
                <div class="price-rub__inner">1000</div>
            </div>
        </div>
    </div>
    <!--/ Конкретный товар-->

    <!--Конкретный товар-->
    <div class="product-slider__item-slid">
        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/3.png')"></a>
        <div class="rating product-slider__rating">
            <div class="rating__inner">
                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__star product-slider__star" data-rating="3">
                    <div class="rating__fill"></div>
                </div>
            </div>
        </div>
        <a href="#" class="product-slider__title">Дрель-шуруповерт WORTEX DR 1314 в кор. (Сетевая дрел...</a>
        <div class="product-slider__buy-price">
            <button class="buy-button"></button>
            <div class="price-rub product-slider__price">
                <div class="price-rub__inner">990</div>
            </div>
        </div>
    </div>
    <!--/ Конкретный товар-->

    <!--Конкретный товар-->
    <div class="product-slider__item-slid">
        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/1.png')"></a>
        <div class="rating product-slider__rating">
            <div class="rating__inner">
                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__star product-slider__star" data-rating="3.5">
                    <div class="rating__fill"></div>
                </div>
            </div>
        </div>
        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27IP54</a>
        <div class="product-slider__buy-price">
            <button class="buy-button"></button>
            <div class="price-rub product-slider__price">
                <div class="price-rub__inner">1090</div>
            </div>
        </div>
    </div>
    <!--/ Конкретный товар-->

    <!--Конкретный товар-->
    <div class="product-slider__item-slid">
        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
        <div class="rating product-slider__rating">
            <div class="rating__inner">
                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__star product-slider__star" data-rating="2.5">
                    <div class="rating__fill"></div>
                </div>
            </div>
        </div>
        <a href="#" class="product-slider__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
        <div class="product-slider__buy-price">
            <button class="buy-button"></button>
            <div class="price-rub product-slider__price">
                <div class="price-rub__inner">1000</div>
            </div>
        </div>
    </div>
    <!--/ Конкретный товар-->

    <!--Конкретный товар-->
    <div class="product-slider__item-slid">
        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/3.png')"></a>
        <div class="rating product-slider__rating">
            <div class="rating__inner">
                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__star product-slider__star" data-rating="3">
                    <div class="rating__fill"></div>
                </div>
            </div>
        </div>
        <a href="#" class="product-slider__title">Дрель-шуруповерт WORTEX DR 1314 в кор. (Сетевая дрел...</a>
        <div class="product-slider__buy-price">
            <button class="buy-button"></button>
            <div class="price-rub product-slider__price">
                <div class="price-rub__inner">990</div>
            </div>
        </div>
    </div>
    <!--/ Конкретный товар-->

    <!--Конкретный товар-->
    <div class="product-slider__item-slid">
        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/1.png')"></a>
        <div class="rating product-slider__rating">
            <div class="rating__inner">
                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__star product-slider__star" data-rating="3.5">
                    <div class="rating__fill"></div>
                </div>
            </div>
        </div>
        <a href="#" class="product-slider__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27IP54</a>
        <div class="product-slider__buy-price">
            <button class="buy-button"></button>
            <div class="price-rub product-slider__price">
                <div class="price-rub__inner">1090</div>
            </div>
        </div>
    </div>
    <!--/ Конкретный товар-->

    <!--Конкретный товар-->
    <div class="product-slider__item-slid">
        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/2.png')"></a>
        <div class="rating product-slider__rating">
            <div class="rating__inner">
                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__star product-slider__star" data-rating="2.5">
                    <div class="rating__fill"></div>
                </div>
            </div>
        </div>
        <a href="#" class="product-slider__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
        <div class="product-slider__buy-price">
            <button class="buy-button"></button>
            <div class="price-rub product-slider__price">
                <div class="price-rub__inner">1000</div>
            </div>
        </div>
    </div>
    <!--/ Конкретный товар-->

    <!--Конкретный товар-->
    <div class="product-slider__item-slid">
        <a href="#" class="product-slider__image" style="background-image: url('/f/i/product/3.png')"></a>
        <div class="rating product-slider__rating">
            <div class="rating__inner">
                <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                <div class="rating__star product-slider__star" data-rating="3">
                    <div class="rating__fill"></div>
                </div>
            </div>
        </div>
        <a href="#" class="product-slider__title">Дрель-шуруповерт WORTEX DR 1314 в кор. (Сетевая дрел...</a>
        <div class="product-slider__buy-price">
            <button class="buy-button"></button>
            <div class="price-rub product-slider__price">
                <div class="price-rub__inner">990</div>
            </div>
        </div>
    </div>
    <!--/ Конкретный товар-->

</div>
