<header class="header">
    <div class="content">
        <div class="header__inner">
            <div class="header__left-part">
                <div class="top-menu header__top-menu">
                    <div class="top-menu__inner">
                        <a href="#" class="top-menu__item-point top-menu__item-point_city">Брянск</a>
                        <a href="#" class="top-menu__item-point">О компании</a>
                        <a href="#" class="top-menu__item-point">Калькулятор</a>
                        <a href="#" class="top-menu__item-point">Бренды</a>
                        <span class="top-menu__item-point top-menu__item-point_arrow js__top-menu-arrow">
                            Помощь
                            <ul class="top-menu__arrow-list">
                                <li class="top-menu__arrow-item"><a href="#">Оплата</a></li>
                                <li class="top-menu__arrow-item"><a href="#">Доставка</a></li>
                                <li class="top-menu__arrow-item"><a href="#">Возврат</a></li>
                                <li class="top-menu__arrow-item"><a href="#">Техподдержка</a></li>
                            </ul>
                        </span>
                        <a href="#" class="top-menu__item-point">Контакты</a>
                    </div>
                </div>
                <div class="header__main-top main-top">
                    <div class="main-top__inner">
                        <a href="/" class="main-top__logo"></a>
                        <div class="button-catalog main-top__button-catalog js__close-open-menu">Каталог</div>
                        <div class="search main-top__search">
                            <form class="search__inner">
                                <input type="text" class="search__input">
                                <button type="submit" class="search__submit"></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header__right-part js__open-menu-mobile">
                <div class="user-menu header__menu">
                    <div class="user-menu__inner">
                        <div class="user-menu__item-point user-menu__item-point_compare">Сравнить</div>
                        <div class="user-menu__item-point user-menu__item-point_heart">
                            <div class="user-menu__heart-sum">8</div>
                            Избранное
                        </div>
                        <div class="user-menu__item-point user-menu__item-point_user" data-popup-btn="login">Вход</div>
                        <div class="user-menu__item-point user-menu__item-point_basket">
                            <div class="user-menu__basket-sum">25</div>
                            Корзина
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>