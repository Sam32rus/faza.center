<!--Попап регистрации-->
<div class="popup" data-popup-id="registration">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="registration"></div>
        <div class="popup__content">
            <div class="popup__title">Регистрация</div>
            <form action="">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__name" placeholder="Имя" name="name" type="text">
                    </div>
                    <div class="form-data__error">Заполните поле "Имя"</div>
                </div>
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__email" placeholder="Адрес электронной почты" name="email" type="text">
                    </div>
                    <div class="form-data__error">Заполните поле "Адрес электронной почты"</div>
                </div>
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__default" placeholder="Пароль" name="password" type="password">
                    </div>
                    <div class="form-data__error">Введите "Ваш пароль"</div>
                </div>
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__default" placeholder="Повторите пароль" name="password" type="confirm_password">
                    </div>
                    <div class="form-data__error">Заполните поле "Повторите пароль"</div>
                </div>
                <div class="form-data">
                    <input id="agreement" class="form-data__checkbox js__checkbox" type="checkbox" name="agreement">
                    <label for="agreement" class="form-data__label">Я даю согласие на обработку своих <a href="/info/policy/">персональных данных</a>.</label>
                </div>
                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <button class="js__disabled-btn form-data__submit blue-button" type="submit">Зарегистрироваться</button>
                    </div>
                </div>
                <div class="popup__change-popup">
                    <div class="popup__link" data-popup-btn="login">Войти</div>
                </div>

            </form>
        </div>
    </div>
</div>
<!--/Попап регистрации-->

<!--Попап авторизации-->
<div class="popup" data-popup-id="login">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="login"></div>
        <div class="popup__content">
            <div class="popup__title">Вход</div>
            <form action="">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__email"  placeholder="Адрес электронной почты" name="email" type="text">
                    </div>
                    <div class="form-data__error">Заполните поле "Адрес электронной почты"</div>
                </div>
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__default" placeholder="Пароль" name="password" type="password">
                    </div>
                    <div class="form-data__error">Введите "Пароль"</div>
                </div>
                <div class="popup__link popup__link_bottom" data-popup-btn="recovery">Забыли пароль?</div>
                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <button class="js__disabled-btn form-data__submit blue-button" type="submit">Войти</button>
                    </div>
                </div>
                <div class="popup__change-popup">
                    <div class="popup__link" data-popup-btn="registration">Зарегистрироваться</div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--/Попап авторизации-->

<!--Попап восстановления пароля-->
<div class="popup" data-popup-id="recovery">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="recovery"></div>
        <div class="popup__content">
            <div class="popup__title">Восстановление пароля</div>
            <div class="popup__text">Ссылка для восстановления пароля будет отправлена на указанный адрес электронной почты</div>
            <form action="">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__email" placeholder="Адрес электронной почты" name="email" type="text">
                    </div>
                    <div class="form-data__error">Заполните поле "Адрес электронной почты"</div>
                </div>
                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <button class="js__disabled-btn form-data__submit blue-button" type="submit">Отправить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--/Попап восстановления пароля-->

<!--Попап пароль восстановлен-->
<div class="popup" data-popup-id="recovery-ok">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="recovery-ok"></div>
        <div class="popup__content">
            <div class="popup__title">Восстановление пароля</div>
            <div class="popup__text">Ссылка для восстановления пароля отправлена на электронную почту указанную при регитрации</div>
        </div>
    </div>
</div>
<!--/Попап пароль восстановлен-->

<!--Попап Вы зарегистрированы-->
<div class="popup" data-popup-id="registration-ok">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="registration-ok"></div>
        <div class="popup__content">
            <div class="popup__title">Регистрация</div>
            <div class="popup__text">Вы успешно зарегистрированы!</div>
        </div>
    </div>
</div>
<!--/Попап Вы зарегистрированы-->

<!--Попап Вы успешно подписаны на нашу e-mail рассылку-->
<div class="popup" data-popup-id="mail-ok">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="mail-ok"></div>
        <div class="popup__content">
            <div class="popup__title">Подписаться на рассылку</div>
            <div class="popup__text">Вы успешно подписаны на нашу e-mail рассылку!</div>
        </div>
    </div>
</div>
<!--/Попап Вы успешно подписаны на нашу e-mail рассылку-->

<!--Попап Добавить отзыв-->
<div class="popup" data-popup-id="add-review">
    <div class="popup__inner w-800">
        <div class="popup__close" data-popup-close="add-review"></div>
        <div class="popup__content">
            <div class="popup__title">Ваш отзыв</div>
            <div class="popup__text">
                <form action="">

                    <div class="form-data">
                        <div class="form-data__wrap">
                            <input class="form-data__input js__default" placeholder="Ваше имя" type="phone">
                        </div>
                        <div class="form-data__error">Заполните поле "Ваше имя"</div>
                    </div>

                    <div class="form-data">
                        <div class="form-data__wrap">
                            <textarea class="form-data__textarea js__default" rows="7" placeholder="Отзыв" name=""></textarea>
                        </div>
                        <div class="form-data__error">Заполните поле "Отзыв"</div>
                    </div>

                    <div class="popup__cols-2">

                        <div class="form-data popup__col-1">
                            <div class="form-data__wrap">
                                <textarea class="form-data__textarea js__default" rows="4" placeholder="Дстоинства" name=""></textarea>
                            </div>
                            <div class="form-data__error">Заполните поле "Дстоинства"</div>
                        </div>

                        <div class="form-data popup__col-1">
                            <div class="form-data__wrap">
                                <textarea class="form-data__textarea js__default" rows="4" placeholder="Недостатки" name=""></textarea>
                            </div>
                            <div class="form-data__error">Заполните поле "Недостатки"</div>
                        </div>
                    </div>

                    <div class="form-data">
                        <div class="form-data__stars">
                            <div class="form-data__stars-title">Ваша оценка</div>
                            <div class="rating-stars">
                                <div class="rating-stars__inner">
                                    <input type="radio" name="rating" value="5" id="5"><label for="5"></label>
                                    <input type="radio" name="rating" value="4" id="4"><label for="4"></label>
                                    <input type="radio" name="rating" value="3" id="3"><label for="3"></label>
                                    <input type="radio" name="rating" value="2" id="2"><label for="2"></label>
                                    <input type="radio" name="rating" value="1" id="1"><label for="1"></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-data__wrap-submit">
                        <button class="js__disabled-btn form-data__submit blue-button" type="submit">Опубликовать отзыв</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--/Попап Добавить отзыв-->

<!--Попап Все отзывы-->
<div class="popup" data-popup-id="all-reviews">
    <div class="popup__inner w-800">
        <div class="popup__close" data-popup-close="all-reviews"></div>
        <div class="popup__content">
            <div class="popup__title">Все отзывы</div>
            <div class="popup__text">
                <div class="reviews">
                    <div class="reviews__inner">

                        <!--Конкретный отзыв-->
                        <div class="reviews__item" itemprop="review" itemscope="" itemtype="http://schema.org/Review">
                            <div class="reviews__author" itemprop="author">Игорь</div>
                            <div class="reviews__date" itemprop="datePublished">20.10.2020</div>
                            <div class="rating reviews__rating">
                                <div class="rating__inner">
                                    <div class="rating__star" itemprop="reviewRating" data-rating="1.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
                            <div class="merit">
                                <div class="merit__inner">
                                    <div class="merit__item merit__item_like">Работает стабильно, светит ярко, есть гарантия</div>
                                    <div class="merit__item merit__item_dislike">Нет</div>
                                </div>
                            </div>
                        </div>
                        <!--/Конкретный отзыв-->

                        <!--Конкретный отзыв-->
                        <div class="reviews__item" itemprop="review" itemscope="" itemtype="http://schema.org/Review">
                            <div class="reviews__author" itemprop="author">Михаил</div>
                            <div class="reviews__date" itemprop="datePublished">20.10.2020</div>
                            <div class="rating reviews__rating">
                                <div class="rating__inner">
                                    <div class="rating__star" itemprop="reviewRating" data-rating="1.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
                            <div class="merit">
                                <div class="merit__inner">
                                    <div class="merit__item merit__item_like">Рживучесть, яркость</div>
                                    <div class="merit__item merit__item_dislike">не замечено</div>
                                </div>
                            </div>
                        </div>
                        <!--/Конкретный отзыв-->

                        <!--Конкретный отзыв-->
                        <div class="reviews__item" itemprop="review" itemscope="" itemtype="http://schema.org/Review">
                            <div class="reviews__author" itemprop="author">Ирина</div>
                            <div class="reviews__date" itemprop="datePublished">20.10.2020</div>
                            <div class="rating reviews__rating">
                                <div class="rating__inner">
                                    <div class="rating__star" itemprop="reviewRating" data-rating="1.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
                            <div class="merit">
                                <div class="merit__inner">
                                    <div class="merit__item merit__item_like">не замечено</div>
                                    <div class="merit__item merit__item_dislike">Работает не стабильно, светит не ярко, нет гарантии</div>
                                </div>
                            </div>
                        </div>
                        <!--/Конкретный отзыв-->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/Попап Все отзывы-->

<!--Попап поделиться-->
<div class="popup" data-popup-id="share">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="share"></div>
        <div class="popup__content">
            <div class="popup__title">Поделиться</div>
            <div class="popup__text popup__text_center"><div class="ya-share2" data-curtain data-size="l" data-services="twitter,vkontakte,facebook,odnoklassniki"></div></div>
        </div>
    </div>
</div>
<!--/Попап поделиться-->

<!--Попап Карта отправлена на активацию-->
<div class="popup" data-popup-id="card-activ">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="card-activ"></div>
        <div class="popup__content">
            <div class="popup__title">Поделиться</div>
            <div class="popup__text">Карта отправлена на активацию. После успешной модерации она станет активной.</div>
        </div>
    </div>
</div>
<!--/Попап Карта отправлена на активацию-->

<!--Попап Добавление новой скидочной карты-->
<div class="popup" data-popup-id="add-card">
    <div class="popup__inner w-600">
        <div class="popup__close" data-popup-close="add-card"></div>
        <div class="popup__content">
            <form class="discount-card popup__discount-card">
                <div class="discount-card__inner">
                    <div class="discount-card__title">Введите номер скидочной&nbsp;карты</div>
                    <input type="text" class="discount-card__input js__discount-card">
                    <button class="discount-card__button blue-button" type="submit">Активировать</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--/Попап Добавление новой скидочной карты-->

<!--Попап добавления моб. телефона -->
<div class="popup" data-popup-id="add-phone">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="add-phone"></div>
        <div class="popup__content">
            <div class="popup__title">Добавление телефона</div>

            <form action="" class="js__form-phone">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__phone required" placeholder="Телефон" type="phone">
                    </div>
                    <div class="form-data__error">Заполните поле "Телефон"</div>
                </div>
                <div class="form-data">
                    <input id="make-main-phone" class="form-data__checkbox js__make-main-phone" type="checkbox">
                    <label for="make-main-phone" class="form-data__label">Сделать основным номером для связи</label>
                </div>
                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <div class="js__add-phone-personal form-data__submit blue-button">Готово</div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<!--/Попап добавления моб. телефона -->

<!--Попап добавления адреса -->
<div class="popup" data-popup-id="add-address">
    <div class="popup__inner w-640">
        <div class="popup__close" data-popup-close="add-address"></div>
        <div class="popup__content">
            <div class="popup__title">Добавление адреса</div>

            <form action="" class="js__form-address">

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__city required" placeholder="Населенный пункт" type="text">
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="flex-wrap flex-wrap_jc-sb">

                    <div class="form-data flex-wrap__item-73">
                        <div class="form-data__wrap">
                            <input class="form-data__input js__street required" placeholder="Улица" type="text">
                        </div>
                        <div class="form-data__error">Заполните поле</div>
                    </div>

                    <div class="form-data  flex-wrap__item-22">
                        <div class="form-data__wrap">
                            <input class="form-data__input js__house-number required" placeholder="Дом" type="text">
                        </div>
                        <div class="form-data__error">Заполните поле</div>
                    </div>

                </div>

                <div class="flex-wrap flex-wrap_jc-sb">

                    <div class="form-data flex-wrap__item-30">
                        <div class="form-data__wrap">
                            <input class="form-data__input js__entrance" placeholder="Подъезд" type="text">
                        </div>
                        <div class="form-data__error">Заполните поле</div>
                    </div>

                    <div class="form-data flex-wrap__item-30">
                        <div class="form-data__wrap">
                            <input class="form-data__input js__floor" placeholder="Этаж" type="number">
                        </div>
                        <div class="form-data__error">Заполните поле</div>
                    </div>

                    <div class="form-data flex-wrap__item-30">
                        <div class="form-data__wrap">
                            <input class="form-data__input js__apartment-number" placeholder="Кв/офис" type="number">
                        </div>
                        <div class="form-data__error">Заполните поле</div>
                    </div>

                </div>

                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <div class="js__add-address-personal form-data__submit blue-button">Готово</div>
                    </div>
                </div>

                <div class="popup__gray-text">
                    Нажимая кнопку вы соглашаетесь с условиями <a href="/info/polzovatelskoe-soglashenie/">пользовательского&nbsp;соглашения</a>
                </div>

            </form>
        </div>
    </div>
</div>
<!--/Попап добавления адреса -->

<!-- Попап добавления юридического адреса или ИП -->
<div class="popup" data-popup-id="add-legal">
    <div class="popup__inner w-640">
        <div class="popup__close" data-popup-close="add-legal"></div>
        <div class="popup__content">
            <div class="popup__title">Добавление юридического адреса</div>

            <div class="form-data-radio">
                <div class="form-data-radio__title">Вид предпринимательской деятельности:</div>
                <div class="form-data-radio__inner">

                    <div class="form-data-radio__item">
                        <input id="radio-1" type="radio" name="radio" value="ИП" checked>
                        <label for="radio-1" class="form-data-radio__label js__label-ip">ИП</label>
                    </div>

                    <div class="form-data-radio__item">
                        <input id="radio-2" type="radio" name="radio" value="OOO">
                        <label for="radio-2" class="form-data-radio__label js__label-ooo">OOO</label>
                    </div>

                </div>
            </div>

            <!-- Форма ИП-->
            <form action="" class="js__form-address-ip">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__fio-ip required" rows="1" placeholder="ФИО"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__ogrn-ip required" rows="1" placeholder="ОГРН ИП"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__inn-ip required" rows="1" placeholder="ИНН"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__date-ip required" placeholder="Дата регистрации" type="date">
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__point-reg-ip required" rows="2" placeholder="Место регистрации"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__invoice-ip required" rows="1" placeholder="Расчетный счет"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea required js__bank-ip" rows="2" placeholder="Учреждение Банка"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__bik-bank-ip required" rows="1" placeholder="БИК Банка"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__ks-bank-ip required" rows="1" placeholder="К/с Банка"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>


                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <div class="js__add-requisites-personal-ip form-data__submit blue-button">Готово</div>
                    </div>
                </div>

            </form>
            <!-- /Форма ИП-->

            <!-- Форма ООО-->
            <form action="" class="js__form-address-ooo" style="display: none;">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__name-ooo required" rows="2" placeholder="Наименование организации"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__address-ooo required" rows="3" placeholder="Юридический адрес"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__inn-ooo required" rows="1" placeholder="ИНН/КПП"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__ogrn-ooo required" rows="1" placeholder="ОГРН"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__invoice-ooo required" rows="1" placeholder="Расчетный счет"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__bank-ooo required" rows="2" placeholder="Учреждение Банка"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__bik-bank-ooo required" rows="1" placeholder="БИК Банка"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__ks-bank-ooo required" rows="1" placeholder="К/с Банка"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__fio-ooo required" rows="2" placeholder="ФИО Директора"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <div class="js__add-requisites-personal-ooo form-data__submit blue-button">Готово</div>
                    </div>
                </div>

            </form>
            <!-- /Форма ООО-->

            <div class="popup__gray-text">
                Нажимая кнопку вы соглашаетесь с условиями <a href="/info/polzovatelskoe-soglashenie/">пользовательского&nbsp;соглашения</a>
            </div>
        </div>
    </div>
</div>
<!--/Попап добавления юридического адреса или ИП -->

<!--Попап "Товар добавлен в корзину" -->
<div class="popup" data-popup-id="ok-basket">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="ok-basket"></div>
        <div class="popup__content">
            <div class="popup__title">Товар добавлен в корзину</div>
                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <div class="js__add-phone-personal form-data__submit blue-button">Перейти в корзину</div>
                    </div>
                </div>
            <div class="popup__change-popup">
                <div class="popup__link" data-popup-close="ok-basket">Продолжить покупки</div>
            </div>
        </div>
    </div>
</div>
<!--/Попап "Товар добавлен в корзину" -->

<!--Попап "Купить в один клик" -->
<div class="popup" data-popup-id="quick-request">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="quick-request"></div>
        <div class="popup__content">
            <div class="popup__title">Купить в 1 клик</div>
               <form action="" class="js__form-address">

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__name" placeholder="Имя" type="text">
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__phone" placeholder="Телефон" type="text">
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__mail" placeholder="E-Mail" type="text">
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <div class="form-data__submit blue-button">Купить</div>
                    </div>
                </div>

                <div class="popup__gray-text">
                    Нажимая кнопку вы соглашаетесь с условиями <a href="/info/polzovatelskoe-soglashenie/">пользовательского&nbsp;соглашения</a>
                </div>

            </form>

        </div>
    </div>
</div>
<!--/Попап "Купить в один клик" -->

<!--Попап "обратная связь" в контактах -->
<div class="popup" data-popup-id="feedback">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="feedback"></div>
        <div class="popup__content">
            <div class="popup__title">Обратная связь</div>
            <form action="">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__default" placeholder="Ваше имя" type="phone">
                    </div>
                    <div class="form-data__error">Заполните поле "Ваше имя"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__mail" placeholder="E-Mail" type="text">
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__phone" placeholder="Телефон" type="text">
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__default" rows="7" placeholder="Сообщение" name=""></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле "Сообщение"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <button class="js__disabled-btn form-data__submit blue-button" type="submit">Отправить</button>
                    </div>
                </div>

                <div class="popup__gray-text">
                    Нажимая кнопку вы соглашаетесь с условиями <a href="/info/polzovatelskoe-soglashenie/">пользовательского&nbsp;соглашения</a>
                </div>
            </form>

        </div>
    </div>
</div>
<!--/Попап "обратная связь" в контактах -->

<!--Попап "Уточнить цену у производителя" -->
<div class="popup" data-popup-id="specify">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="specify"></div>
        <div class="popup__content">
            <div class="popup__title">Уточнение цены</div>
            <div class="popup__text">Укажите свои данные, чтобы наш менеджер связался с Вами для решения данного вопроса.</div>
            <form action="">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__default" placeholder="Ваше имя" type="phone">
                    </div>
                    <div class="form-data__error">Заполните поле "Ваше имя"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__phone" placeholder="Телефон" type="text">
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__default" rows="7" placeholder="Примечание" name=""></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле "Примечание"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <button class="js__disabled-btn form-data__submit blue-button" type="submit">Уточнить</button>
                    </div>
                </div>

                <div class="popup__gray-text">
                    Нажимая кнопку вы соглашаетесь с условиями <a href="/info/polzovatelskoe-soglashenie/">пользовательского&nbsp;соглашения</a>
                </div>
            </form>

        </div>
    </div>
</div>
<!--/Попап "Уточнить цену у производителя" -->





