<section class="catalog">
    <div class="content">
        <div class="sorting">
            <div class="sorting__inner">
                <div class="sorting__found">Найдено <span>128</span> товаров</div>
                <div class="sorting__filter">
                    <div class="filter-button sorting__filter-button js__mobile-filter-open">Фильтр</div>
                    <a href="#" class="sorting__button active descending" rel="nofollow">по цене</a>
                    <a href="#" class="sorting__button active аscending" rel="nofollow">по названию</a>
                    <a href="#" class="sorting__button" rel="nofollow">по рейтингу</a>
                    <a href="#" class="sorting__button" rel="nofollow">по отзывам</a>
                </div>
            </div>
        </div>
        <div class="catalog__inner">

            <div class="catalog__sorting js__mobile-filter">

                <form action="" class="catalog__form">

                    <div class="catalog__mobile-head">
                        <div class="catalog__mobile-head-title">Фильтр</div>
                        <button class="transparent-button js__mobile-filter-close">Отмена</button>
                    </div>

                    <div class="catalog__extra-btn hide-tablet"><button class="clear-button">Очистить фильтры</button></div>

                    <div class="form-data">
                        <input id="price-card" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                        <label for="price-card" class="form-data__label">Цена по карте</label>
                    </div>

                    <!--Пункт сортировки каталога -->
                    <div class="deploy catalog__item-sort">
                        <div class="catalog__sort-title js__deploy-toggle open">Цена</div>

                        <div class="deploy__content active catalog__sort-content">
                            <div class="form-data">
                                <div class="form-data__range">
                                    <div class="form-data__range-inputs">
                                        <div class="form-data form-data_range-from">
                                            <div class="form-data__wrap form-data__wrap_small form-data__wrap_price">
                                                <div class="form-data__price-pl">от</div>
                                                <input class="form-data__input js__range-price-min" name="" type="number" value="0">
                                            </div>
                                        </div>
                                        <div class="form-data__dash"></div>
                                        <div class="form-data form-data_range-to">
                                            <div class="form-data__wrap form-data__wrap_small form-data__wrap_price">
                                                <div class="form-data__price-pl">до</div>
                                                <input class="form-data__input js__range-price-max" name="" type="number" value="9999">
                                            </div>
                                        </div>
                                    </div>

                                    <input class="js__range-price" type="text" name="" value="" />
                                </div>
                            </div>
                            <pre class="demo__output js-output__d1"></pre>
                        </div>
                    </div>
                    <!-- /Пункт сортировки каталога -->

                    <!--Пункт сортировки каталога -->
                    <div class="deploy catalog__item-sort">
                        <div class="catalog__sort-title js__deploy-toggle open">Производитель</div>
                        <div class="deploy__content catalog__sort-content">
                            <div class="form-data">
                                <div class="form-data__wrap form-data__wrap_small form-data__wrap_logo-right-small">
                                    <input class="form-data__input" name="" type="text">
                                    <div class="form-data__logo-right-small form-data__logo-right-small_search"></div>
                                </div>
                            </div>
                            <div class="form-data">
                                <input id="brand-1" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                <label for="brand-1" class="form-data__label">Бренд 1 <span class="form-data__label-comment">(12)</span></label>
                            </div>
                            <div class="form-data">
                                <input id="brand-2" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                <label for="brand-2" class="form-data__label">Бренд 2 <span class="form-data__label-comment">(12)</span></label>
                            </div>
                            <div class="form-data">
                                <input id="brand-3" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                <label for="brand-3" class="form-data__label">Бренд 3 <span class="form-data__label-comment">(12)</span></label>
                            </div>
                            <div class="form-data">
                                <input id="brand-4" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                <label for="brand-4" class="form-data__label">Бренд 4 <span class="form-data__label-comment">(12)</span></label>
                            </div>
                            <div class="deploy">
                                <div class="deploy__content" style="display: none;">
                                    <div class="form-data">
                                        <input id="brand-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="brand-5" class="form-data__label">Бренд 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="brand-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="brand-6" class="form-data__label">Бренд 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="brand-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="brand-7" class="form-data__label">Бренд 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="brand-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="brand-8" class="form-data__label">Бренд 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                </div>
                                <div class="catalog__add-brand js__deploy-toggle" data-deploy-open="Скрыть" data-deploy-close="Еще бренды">Еще бренды</div>
                            </div>
                        </div>
                    </div>
                    <!-- /Пункт сортировки каталога -->

                    <!--Пункт сортировки каталога -->
                    <div class="deploy catalog__item-sort">
                        <div class="catalog__sort-title js__deploy-toggle">Мощность, Вт</div>
                        <div class="deploy__content catalog__sort-content"  style="display: none;">
                            <div class="form-data">
                                <div class="form-data__wrap form-data__wrap_small form-data__wrap_logo-right-small">
                                    <input class="form-data__input" name="" type="text">
                                    <div class="form-data__logo-right-small form-data__logo-right-small_search"></div>
                                </div>
                            </div>
                            <div class="form-data">
                                <input id="voltage-1" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                <label for="voltage-1" class="form-data__label">Вольтаж 1 <span class="form-data__label-comment">(12)</span></label>
                            </div>
                            <div class="form-data">
                                <input id="voltage-2" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                <label for="voltage-2" class="form-data__label">Вольтаж 2 <span class="form-data__label-comment">(12)</span></label>
                            </div>
                            <div class="form-data">
                                <input id="voltage-3" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                <label for="voltage-3" class="form-data__label">Вольтаж 3 <span class="form-data__label-comment">(12)</span></label>
                            </div>
                            <div class="form-data">
                                <input id="voltage-4" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                <label for="voltage-4" class="form-data__label">Вольтаж 4 <span class="form-data__label-comment">(12)</span></label>
                            </div>
                            <div class="deploy">
                                <div class="deploy__content" style="display: none;">
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-5" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-5" class="form-data__label">Вольтаж 5 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-6" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-6" class="form-data__label">Вольтаж 6 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-7" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-7" class="form-data__label">Вольтаж 7 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                    <div class="form-data">
                                        <input id="voltage-8" class="form-data__checkbox js__checkbox" type="checkbox" name="">
                                        <label for="voltage-8" class="form-data__label">Вольтаж 8 <span class="form-data__label-comment">(12)</span></label>
                                    </div>
                                </div>
                                <div class="catalog__add-brand js__deploy-toggle" data-deploy-open="Скрыть" data-deploy-close="Еще варианты">Еще варианты</div>
                            </div>
                        </div>
                    </div>
                    <!-- /Пункт сортировки каталога -->

                    <div class="catalog__wrap-found-btn show-tablet">
                        <div class="blue-button catalog__blue-button js__mobile-filter-close">Посмотреть <span>328</span> товаров</div>
                    </div>

                </form>
            </div>

            <div class="catalog__product">

                <ul class="catalog__sorting-show">
                    <li>Бренд 1 <span class="catalog__close-item"></span></li>
                    <li>Бренд 3 <span class="catalog__close-item"></span></li>
                    <li>Вольтаж 4  <span class="catalog__close-item"></span></li>
                </ul>
                <div class="catalog__wrap-product">
                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">7000</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Конкретный товар-->

                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">800</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/Конкретный товар-->

                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">88000</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/Конкретный товар-->

                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">1000</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/Конкретный товар-->

                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">7000</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Конкретный товар-->

                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">800</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/Конкретный товар-->

                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">88000</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/Конкретный товар-->

                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">1000</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/Конкретный товар-->

                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">7000</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Конкретный товар-->

                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">800</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/Конкретный товар-->

                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">88000</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/Конкретный товар-->

                    <!--Конкретный товар-->
                    <div class="product-price catalog__item-product">
                        <div class="product-price__inner">
                            <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                            <div class="rating catalog__rating">
                                <div class="rating__inner">
                                    <div class="rating__chart">
                                        <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                        <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__heart">
                                        <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                        <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                    </div>
                                    <div class="rating__star" data-rating="3.5">
                                        <div class="rating__fill"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                            <div class="product-price__price">
                                <button class="buy-button product-price__buy-button"></button>
                                <div class="price-rub">
                                    <div class="price-rub__inner">1000</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/Конкретный товар-->
                </div>
            </div>

            <!-- Ранее просматривали-->
            <section class="section">
                <div class="section__inner">
                    <div class="section__title">Ранее просматривали</div>
                    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/viewed.php'; ?>
                </div>
            </section>
            <!-- /Ранее просматривали-->


        </div>
    </div>
</section>