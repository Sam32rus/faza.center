<div class="cab-menu">
    <div class="cab-menu__inner">
        <div class="cab-menu__wrap-logo"><img src="/f/i/cabinet/logo.svg" class="class="cab-menu__logo"></div>
        <ul class="cab-menu__list-point">
            <li class="cab-menu__item-point"><a href="/html/cabinet-chosen.php" class="cab-menu__item-link cab-menu__item-link_chosen active">Избранные товары</a><span class="cab-menu__item-sum">8</span></li>
            <li class="cab-menu__item-point"><a href="/html/cabinet-viewed.php" class="cab-menu__item-link cab-menu__item-link_viewed">Просмотренные</a><span class="cab-menu__item-sum">123</span></li>
            <li class="cab-menu__item-point"><a href="/html/cabinet-comparisons.php" class="cab-menu__item-link cab-menu__item-link_comparisons">Сравнения</a></li>
            <li class="cab-menu__item-point"><a href="/html/cabinet-order.php" class="cab-menu__item-link cab-menu__item-link_order">Мои заказы</a><span class="cab-menu__item-sum">48</span></li>
            <li class="cab-menu__list-line"></li>
            <li class="cab-menu__item-point"><a href="/html/cabinet-cash.php" class="cab-menu__item-link cab-menu__item-link_cash">Мой счет</a> <span class="cab-menu__item-cash">10 000</span></li>
            <li class="cab-menu__item-point"><a href="/html/cabinet-card.php" class="cab-menu__item-link cab-menu__item-link_card">Мои карты</a><span class="cab-menu__item-sum">3</span></li>
            <li class="cab-menu__item-point"><a href="/html/cabinet-personal.php" class="cab-menu__item-link cab-menu__item-link_personal">Личные данные</a></li>
            <li class="cab-menu__item-point"><a href="/html/cabinet-subscription.php" class="cab-menu__item-link cab-menu__item-link_subscription">Подписка</a></li>
            <li class="cab-menu__list-line"></li>
            <li class="cab-menu__item-point"><a href="/html/cabinet-contractor.php" class="cab-menu__item-link no-active cab-menu__item-link_contractor">Подрядчикам</a></li>
            <li class="cab-menu__list-line"></li>
            <li class="cab-menu__item-point"><a href="" class="cab-menu__item-link cab-menu__item-link_exit">Выйти</a></li>
        </ul>
        <div class="user-menu cab-menu__user-menu js__cabinet-menu-user-menu">
            <div class="content">
                <div class="user-menu__inner">
                    <a href="#" class="user-menu__item-point user-menu__item-point_catalog">Каталог</a>
                    <a href="#" class="user-menu__item-point user-menu__item-point_compare">Сравнить</a>
                    <a href="#" class="user-menu__item-point user-menu__item-point_heart">Избранное</a>
                    <a href="#" class="user-menu__item-point user-menu__item-point_user" data-popup-btn="login">Вход</a>
                    <div class="user-menu__item-point user-menu__item-point_basket">
                        <div class="user-menu__basket-sum">25</div>
                        Корзина
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
