<div class="viewed">
    <div class="viewed__inner">
        <div class="viewed__products">

            <!--Конкретный товар-->
            <div class="product-price viewed__item-product">
                <div class="product-price__inner">
                    <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                    <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                    <div class="product-price__price">
                        <button class="buy-button product-price__buy-button"></button>
                        <div class="price-rub">
                            <div class="price-rub__inner">88000</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Конкретный товар-->

            <!--Конкретный товар-->
            <div class="product-price viewed__item-product">
                <div class="product-price__inner">
                    <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                    <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                    <div class="product-price__price">
                        <button class="buy-button product-price__buy-button"></button>
                        <div class="price-rub">
                            <div class="price-rub__inner">1000</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Конкретный товар-->

            <!--Конкретный товар-->
            <div class="product-price viewed__item-product">
                <div class="product-price__inner">
                    <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                    <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                    <div class="product-price__price">
                        <button class="buy-button product-price__buy-button"></button>
                        <div class="price-rub">
                            <div class="price-rub__inner">1000</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Конкретный товар-->

            <!--Конкретный товар-->
            <div class="product-price viewed__item-product">
                <div class="product-price__inner">
                    <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                    <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                    <div class="product-price__price">
                        <button class="buy-button product-price__buy-button"></button>
                        <div class="price-rub">
                            <div class="price-rub__inner">1000</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Конкретный товар-->

            <!--Конкретный товар-->
            <div class="product-price viewed__item-product">
                <div class="product-price__inner">
                    <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                    <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                    <div class="product-price__price">
                        <button class="buy-button product-price__buy-button"></button>
                        <div class="price-rub">
                            <div class="price-rub__inner">88000</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Конкретный товар-->

            <!--Конкретный товар-->
            <div class="product-price viewed__item-product">
                <div class="product-price__inner">
                    <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                    <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                    <div class="product-price__price">
                        <button class="buy-button product-price__buy-button"></button>
                        <div class="price-rub">
                            <div class="price-rub__inner">1000</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Конкретный товар-->

            <!--Конкретный товар-->
            <div class="product-price viewed__item-product">
                <div class="product-price__inner">
                    <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                    <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                    <div class="product-price__price">
                        <button class="buy-button product-price__buy-button"></button>
                        <div class="price-rub">
                            <div class="price-rub__inner">1000</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Конкретный товар-->

            <!--Конкретный товар-->
            <div class="product-price viewed__item-product">
                <div class="product-price__inner">
                    <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                    <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                    <div class="product-price__price">
                        <button class="buy-button product-price__buy-button"></button>
                        <div class="price-rub">
                            <div class="price-rub__inner">1000</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Конкретный товар-->

        </div>
    </div>
</div>
