<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>



    <main class="main">
        <div class="content">

            <section class="cabinet">
                <div class="cabinet__inner">
                    <div class="cabinet__menu js__cabinet-menu">
                        <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/cab-menu.php'; ?>
                    </div>
                    <div class="cabinet__main">
                        <div class="cabinet__title js__cabinet-menu-open">Личные данные</div>
                        <div class="cabinet__content">

                            <form action="" class="personal">
                                <div class="personal__inner">
                                    <div class="form-data">
                                        <div class="form-data__wrap">
                                            <input class="form-data__input js__default name required" placeholder="ФИО" value="" type="text" name="">
                                        </div>
                                        <div class="form-data__error">Заполните поле</div>
                                    </div>
                                    <div class="personal__wrap-two">
                                        <div class="form-data personal__date">
                                            <div class="form-data__wrap">
                                                <input class="form-data__input js__date required" value="" onfocus="(this.type='date')" placeholder="Дата рождения" name="" type="text">
                                            </div>
                                            <div class="form-data__error">Заполните поле</div>
                                        </div>
                                        <div class="form-data personal__email">
                                            <div class="form-data__wrap">
                                                <input class="form-data__input js__email required" value="" placeholder="E-mail1" name="" type="email">
                                            </div>
                                            <div class="form-data__error">Заполните поле</div>
                                        </div>
                                    </div>

                                    <div class="personal__title">Телефон</div>
                                    <div class="js__personal-phone-field"></div>
                                    <div class="button-add" data-popup-btn="add-phone">Добавить телефон</div>

                                    <div class="personal__title">Адреса</div>
                                    <div class="js__personal-address-field"></div>
                                    <div class="button-add" data-popup-btn="add-address">Добавить адрес</div>

                                    <div class="personal__title">Юридические реквизиты</div>
                                    <div class="js__personal-requisites-field"></div>
                                    <div class="button-add" data-popup-btn="add-legal">Добавить реквизиты</div>


                                    <div class="personal__wrap-btn">
                                        <button class="js__disabled-btn form-data__submit blue-button" type="submit">Сохранить</button>
                                    </div>


                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>
