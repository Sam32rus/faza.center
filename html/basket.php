<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Корзина | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/breadcrumbs.php'; ?>


    <main class="main">
        <h1 class="title-page"><span class="content">Корзина</span></h1>

        <section class="basket">
            <div class="content">
                <div class="sorting">
                    <div class="sorting__inner">
                        <div class="sorting__found"><span>8</span> товаров</div>
                    </div>
                </div>
                <div class="basket__inner">

                    <div class="basket__panel">

                        <div class="price-rub basket__price">
                            <div class="price-rub__title">Итого:</div>
                            <div class="price-rub__inner">17000</div>
                        </div>

                        <div class="price-rub basket__nds">
                            <div class="price-rub__title">Сумма с НДС:</div>
                            <div class="price-rub__inner">21000</div>
                        </div>

                        <div class="basket__wrap-bt-panel">
                            <buttun class="blue-button basket__blue-button">Оформить заказ</buttun>
                            <buttun class="blue-button basket__blue-button">Быстрый заказ</buttun>
                        </div>

                        <div class="basket__brands-panel">
                            <div class="basket__brands-title">Бренды в корзине:</div>
                            <a href="#" class="basket__item-brand" style="background-image: url('/f/i/partners/n1.png')"></a>
                            <a href="#" class="basket__item-brand" style="background-image: url('/f/i/partners/n2.png')"></a>
                            <a href="#" class="basket__item-brand" style="background-image: url('/f/i/partners/n3.png')"></a>
                        </div>


                    </div>

                    <div class="basket__product">


                        <!--Конкретный товар-->
                        <div class="basket-product basket__item-product">
                            <div class="basket-product__inner">
                                <button class="basket-product__close"></button>
                                <div class="basket-product__img" style="background-image: url('/f/i/product/product-price2.jpg')"></div>
                                <div class="basket-product__name">
                                    <div class="basket-product__title">Клеммник 6/16мм 12 пар 60А ЗВИ</div>
                                    <div class="basket-product__desc">
                                        <div class="basket-product__desc-title">Бренд:</div>
                                        <a href="#" class="basket-product__brand" style="background-image: url('/f/i/partners/n1.png')"></a>
                                    </div>
                                </div>
                                <div class="basket-product__wrap-price">

                                    <div class="basket-product__price">
                                        <div class="basket-product__price-desc">Цена за 1 шт.:</div>
                                        <div class="price-rub basket-product__price-rub">
                                            <div class="price-rub__inner">21000</div>
                                        </div>
                                    </div>

                                    <div class="count basket-product__count">
                                        <div class="count__inner js__count">
                                            <input class="js__count-minus count__minus" type="button" value="-">
                                            <input class="js__count-sum count__sum" type="text" size="3" value="1">
                                            <input class="js__count-plus count__plus" type="button"  value="+">
                                        </div>
                                    </div>

                                    <div class="basket-product__price">
                                        <div class="basket-product__price-desc">Итого:</div>
                                        <div class="price-rub basket-product__price-rub">
                                            <div class="price-rub__inner">21000</div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="basket-product basket__item-product">
                            <div class="basket-product__inner">
                                <button class="basket-product__close"></button>


                                <div class="basket-product__img" style="background-image: url('/f/i/product/product-price1.jpg')"></div>
                                <div class="basket-product__name">
                                    <div class="basket-product__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27IP54</div>
                                    <div class="basket-product__desc">
                                        <div class="basket-product__desc-title">Бренд:</div>
                                        <a href="#" class="basket-product__brand" style="background-image: url('/f/i/partners/n2.png')"></a>
                                    </div>
                                </div>

                                <div class="basket-product__wrap-price">

                                    <div class="basket-product__price">
                                        <div class="basket-product__price-desc">Цена за 1 шт.:</div>
                                        <div class="price-rub basket-product__price-rub">
                                            <div class="price-rub__inner">21000</div>
                                        </div>
                                    </div>

                                    <div class="count basket-product__count">
                                        <div class="count__inner js__count">
                                            <input class="js__count-minus count__minus" type="button" value="-">
                                            <input class="js__count-sum count__sum" type="text" size="3" value="1">
                                            <input class="js__count-plus count__plus" type="button"  value="+">
                                        </div>
                                    </div>

                                    <div class="basket-product__price">
                                        <div class="basket-product__price-desc">Итого:</div>
                                        <div class="price-rub basket-product__price-rub">
                                            <div class="price-rub__inner">21000</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <!--/ Конкретный товар-->




                </div>
            </div>
        </section>



    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>