<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addCss.php'; ?>
    <title>Бренд | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/breadcrumbs.php'; ?>

    <main class="main">

        <article class="article">
            <div class="article__inner">
                <div class="article__wrap-head-img">
                    <img src="/f/i/partners/big-ecola.jpg" alt="Ecola" class="article__item-head-img">
                </div>
                <h1>Ecola</h1>
                <p>Ecola – один из ведущих китайских производителей энергосберегающих осветительных приборов. Компания функционирует на рынке светотехники с 2006 года. Ее конкурентными преимуществами являются внушительный ассортимент выпускаемой продукции, оптимальное соотношение цена/качество, экологичность и долговечность.</p>
                <p>Энергосберегающие лампы Ecola имеют компактные размеры, низкое энергопотребление и высокое КПД. Такие источники света позволяют сэкономить в 15 раз больше электрической энергии, чем при использовании обычных ламп накаливания. То же самое касается и эксплуатационного срока.</p>
                <img src="/f/i/promo-slider/big-1.jpg" alt="">
                <p>Продукция Ecola представлена на рынке:</p>
                <ul>
                    <li>потолочными светильниками;</li>
                    <li>энергосберегающими лампами;</li>
                    <li>светодиодными лентами и панелями;</li>
                    <li>прожекторами;</li>
                    <li>переходниками;</li>
                    <li>патронами;</li>
                    <li>аксессуарами.</li>
                </ul>
                <p>Чтобы удерживать лидерство и высокие конкурентные позиции, специалисты Ecola не только строго следят за тенденциями изменения спроса потребителей, но и создают новые тренды в светотехнической отрасли.</p>
            </div>
        </article>

    </main>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addJs.php'; ?>

</body>
</html>
