<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>



    <main class="main">
        <div class="content">

            <section class="cabinet">
                <div class="cabinet__inner">
                    <div class="cabinet__menu js__cabinet-menu">
                        <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/cab-menu.php'; ?>
                    </div>
                    <div class="cabinet__main">
                        <div class="cabinet__title js__cabinet-menu-open">Избранные товары</div>
                        <div class="cabinet__content">

                            <div class="cabinet__products">

                                <div class="product-price cabinet__item-product">
                                    <div class="product-price__inner">
                                        <div class="product-price__remove"></div>
                                        <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                        <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                        <div class="product-price__price">
                                            <button class="buy-button product-price__buy-button"></button>
                                            <div class="price-rub">
                                                <div class="price-rub__inner">1000</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-price cabinet__item-product">
                                    <div class="product-price__inner">
                                        <div class="product-price__remove"></div>
                                        <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                        <a href="#" class="product-price__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27 IP54</a>
                                        <div class="product-price__price">
                                            <button class="buy-button product-price__buy-button"></button>
                                            <div class="price-rub">
                                                <div class="price-rub__inner">1000</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-price cabinet__item-product">
                                    <div class="product-price__inner">
                                        <div class="product-price__remove"></div>
                                        <a href="#" class="product-price__img" style="background-image: url('/f/i/product/3.png')"></a>
                                        <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                                        <div class="product-price__price">
                                            <button class="buy-button product-price__buy-button"></button>
                                            <div class="price-rub">
                                                <div class="price-rub__inner">1000</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-price cabinet__item-product">
                                    <div class="product-price__inner">
                                        <div class="product-price__remove"></div>
                                        <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                        <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                        <div class="product-price__price">
                                            <button class="buy-button product-price__buy-button"></button>
                                            <div class="price-rub">
                                                <div class="price-rub__inner">1000</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-price cabinet__item-product">
                                    <div class="product-price__inner">
                                        <div class="product-price__remove"></div>
                                        <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                        <a href="#" class="product-price__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27 IP54</a>
                                        <div class="product-price__price">
                                            <button class="buy-button product-price__buy-button"></button>
                                            <div class="price-rub">
                                                <div class="price-rub__inner">1000</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-price cabinet__item-product">
                                    <div class="product-price__inner">
                                        <div class="product-price__remove"></div>
                                        <a href="#" class="product-price__img" style="background-image: url('/f/i/product/3.png')"></a>
                                        <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                                        <div class="product-price__price">
                                            <button class="buy-button product-price__buy-button"></button>
                                            <div class="price-rub">
                                                <div class="price-rub__inner">1000</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-price cabinet__item-product">
                                    <div class="product-price__inner">
                                        <div class="product-price__remove"></div>
                                        <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                        <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                        <div class="product-price__price">
                                            <button class="buy-button product-price__buy-button"></button>
                                            <div class="price-rub">
                                                <div class="price-rub__inner">1000</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-price cabinet__item-product">
                                    <div class="product-price__inner">
                                        <div class="product-price__remove"></div>
                                        <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                        <a href="#" class="product-price__title">Светильник НПБ 100вт бел.круг реш. ЛОН Е27 IP54</a>
                                        <div class="product-price__price">
                                            <button class="buy-button product-price__buy-button"></button>
                                            <div class="price-rub">
                                                <div class="price-rub__inner">1000</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </section>

        </div>
    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>
