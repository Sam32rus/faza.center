<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/breadcrumbs.php'; ?>


    <main class="main">
        <h1 class="title-page"><span class="content">Результаты по запросу: <span>Лампы</span></span></h1>

        <section class="result">

            <div class="content">
                <div class="sorting">
                    <div class="sorting__inner">
                        <div class="sorting__found">Найдено <span>44</span></div>
                        <div class="sorting__filter">
                            <div class="filter-button sorting__filter-button js__mobile-filter-open">Фильтр</div>
                            <a href="#" class="sorting__button active descending" rel="nofollow">по цене</a>
                            <a href="#" class="sorting__button active аscending" rel="nofollow">по названию</a>
                            <a href="#" class="sorting__button" rel="nofollow">по рейтингу</a>
                            <a href="#" class="sorting__button" rel="nofollow">по отзывам</a>
                        </div>
                    </div>
                </div>
                <div class="result__inner">

                    <div class="result__product">

                        <!--Конкретный товар-->
                        <div class="product-price result__item-product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                <div class="rating result__rating">
                                    <div class="rating__inner">
                                        <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">7000</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price result__item-product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                <div class="rating result__rating">
                                    <div class="rating__inner">
                                        <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price result__item-product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                <div class="rating result__rating">
                                    <div class="rating__inner">
                                        <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">7000</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price result__item-product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                <div class="rating result__rating">
                                    <div class="rating__inner">
                                        <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price result__item-product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                <div class="rating result__rating">
                                    <div class="rating__inner">
                                        <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price result__item-product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                <div class="rating result__rating">
                                    <div class="rating__inner">
                                        <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">7000</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price result__item-product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                <div class="rating result__rating">
                                    <div class="rating__inner">
                                        <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price result__item-product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price2.jpg')"></a>
                                <div class="rating result__rating">
                                    <div class="rating__inner">
                                        <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Лампочка Philips ESS LEDCandle-3,  E14, 5.5 Вт</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">7000</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price result__item-product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                <div class="rating result__rating">
                                    <div class="rating__inner">
                                        <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Конкретный товар-->

                        <!--Конкретный товар-->
                        <div class="product-price result__item-product">
                            <div class="product-price__inner">
                                <a href="#" class="product-price__img" style="background-image: url('/f/i/product/product-price1.jpg')"></a>
                                <div class="rating result__rating">
                                    <div class="rating__inner">
                                        <div class="rating__chart">
                                            <input id="id123" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
                                            <label for="id123" class="rating__chart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__heart">
                                            <input id="id231" class="rating__heart-input" type="checkbox" data-entity="compare-checkbox"">
                                            <label for="id231" class="rating__heart-label" data-entity="compare-title"></label>
                                        </div>
                                        <div class="rating__star" data-rating="3.5">
                                            <div class="rating__fill"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="product-price__title">Диф.авт. 2п 10А С 100мА АД12 5шт/уп</a>
                                <div class="product-price__price">
                                    <button class="buy-button product-price__buy-button"></button>
                                    <div class="price-rub">
                                        <div class="price-rub__inner">800</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/Конкретный товар-->
                    </div>

                    
                    <!-- Ранее просматривали-->
                    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/viewed.php'; ?>
                    <!-- /Ранее просматривали-->


                </div>
            </div>
        </section>



    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>