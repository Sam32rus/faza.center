<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/breadcrumbs.php'; ?>


    <main class="main">
        <h1 class="title-page"><span class="content">Розетки и выключатели</span></h1>


        <div class="general">
            <div class="content">
                <div class="general__inner">

                    <div class="category general__category">
                        <a href="" class="category__inner">
                            <span class="category__img" style="background-image: url('/f/i/category/1.jpg')"></span>
                            <span class="category__title">Продукция Ретро</span>
                        </a>
                    </div>

                    <div class="category general__category">
                        <a href="" class="category__inner">
                            <span class="category__img" style="background-image: url('/f/i/category/2.jpg')"></span>
                            <span class="category__title">Розетки и выключатели открытой установки</span>
                        </a>
                    </div>

                    <div class="category general__category">
                        <a href="" class="category__inner">
                            <span class="category__img" style="background-image: url('/f/i/category/3.jpg')"></span>
                            <span class="category__title">Розетки и выключатели скрытой установки</span>
                        </a>
                    </div>

                    <div class="category general__category">
                        <a href="" class="category__inner">
                            <span class="category__img" style="background-image: url('/f/i/category/1.jpg')"></span>
                            <span class="category__title">Продукция Ретро</span>
                        </a>
                    </div>

                    <div class="category general__category">
                        <a href="" class="category__inner">
                            <span class="category__img" style="background-image: url('/f/i/category/2.jpg')"></span>
                            <span class="category__title">Розетки и выключатели открытой установки</span>
                        </a>
                    </div>

                    <div class="category general__category">
                        <a href="" class="category__inner">
                            <span class="category__img" style="background-image: url('/f/i/category/3.jpg')"></span>
                            <span class="category__title">Розетки и выключатели скрытой установки</span>
                        </a>
                    </div>

                    <div class="category general__category">
                        <a href="" class="category__inner">
                            <span class="category__img" style="background-image: url('/f/i/category/1.jpg')"></span>
                            <span class="category__title">Продукция Ретро</span>
                        </a>
                    </div>

                    <div class="category general__category">
                        <a href="" class="category__inner">
                            <span class="category__img" style="background-image: url('/f/i/category/2.jpg')"></span>
                            <span class="category__title">Розетки и выключатели открытой установки</span>
                        </a>
                    </div>

                    <div class="category general__category">
                        <a href="" class="category__inner">
                            <span class="category__img" style="background-image: url('/f/i/category/3.jpg')"></span>
                            <span class="category__title">Розетки и выключатели скрытой установки</span>
                        </a>
                    </div>

                </div>
            </div>
        </div>



    </main>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addJs.php'; ?>


</body>
</html>