<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>



    <main class="main">
        <div class="content">

            <section class="cabinet">
                <div class="cabinet__inner">
                    <div class="cabinet__menu js__cabinet-menu">
                        <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/cab-menu.php'; ?>
                    </div>
                    <div class="cabinet__main">
                        <div class="cabinet__title js__cabinet-menu-open">Личные данные</div>
                        <div class="cabinet__content">

                            <form action="" class="personal">
                                <div class="personal__inner">
                                    <div class="form-data">
                                        <div class="form-data__wrap">
                                            <input class="form-data__input required" placeholder="ФИО" value="" type="text" name="">
                                        </div>
                                        <div class="form-data__error">Заполните поле</div>
                                    </div>
                                    <div class="personal__wrap-two">
                                        <div class="form-data personal__date">
                                            <div class="form-data__wrap">
                                                <input class="form-data__input js__date required" value="" onfocus="(this.type='date')" placeholder="Дата рождения" name="" type="text">
                                            </div>
                                            <div class="form-data__error">Заполните поле</div>
                                        </div>
                                        <div class="form-data personal__email">
                                            <div class="form-data__wrap">
                                                <input class="form-data__input js__email required" value="" placeholder="E-mail" name="" type="email">
                                            </div>
                                            <div class="form-data__error">Заполните поле</div>
                                        </div>
                                    </div>

                                    <div class="personal__title">Телефон</div>
                                    <div class="js__personal-phone-field"><div class="form-data form-data_small-indent">
                                            <div class="form-data__wrap form-data__wrap_editing">
                                                <input class="form-data__input js__phone main-phone" disabled="disabled" value="+7 952 296 78 46" name="phone-1" type="phone">
                                                <div class="form-data__editing js__editing"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button-add" data-popup-btn="add-phone">Добавить телефон</div>

                                    <div class="personal__title">Адреса</div>
                                    <div class="js__personal-address-field"><div class="form-data form-data_small-indent">
                                            <div class="form-data__wrap form-data__wrap_editing">
                                                <input class="form-data__input" disabled="disabled" value="г. Брянск, ул. Дуки, д. 22, под. 2, эт. 3, кв. 12" name="address-1" type="text">
                                                <div class="form-data__editing js__editing"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button-add" data-popup-btn="add-address">Добавить адрес</div>

                                    <div class="personal__title">Юридические реквизиты</div>
                                    <div class="js__personal-requisites-field">
                                        <div class="personal__row">
                                            <div class="personal__coll-title">Вид предпринимательской деятельности:</div>
                                            <div class="personal__coll-data">
                                                <div class="form-data">
                                                    <div class="form-data__wrap form-data__wrap_editing">
                                                        <textarea class="form-data__textarea" disabled="disabled" name="type-business" rows="1">ООО</textarea>
                                                        <div class="form-data__editing js__editing"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="personal__row">
                                            <div class="personal__coll-title">Наименование организации</div>
                                            <div class="personal__coll-data">
                                                <div class="form-data">
                                                    <div class="form-data__wrap form-data__wrap_editing">
                                                        <textarea class="form-data__textarea" disabled="disabled" name="inn-ooo" rows="2">Общество с ограниченной ответственностью «Ньютон-Электро»</textarea>
                                                        <div class="form-data__editing js__editing"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="personal__row">
                                            <div class="personal__coll-title">Юридический адрес</div>
                                            <div class="personal__coll-data">
                                                <div class="form-data">
                                                    <div class="form-data__wrap form-data__wrap_editing">
                                                        <textarea class="form-data__textarea" disabled="disabled" name="inn-ooo" rows="2">241520, Брянская обл., Брянский р-он, с. Супонево,   ул. Шоссейная, д.3</textarea>
                                                        <div class="form-data__editing js__editing"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="personal__row">
                                            <div class="personal__coll-title">ИНН/КПП</div>
                                            <div class="personal__coll-data">
                                                <div class="form-data">
                                                    <div class="form-data__wrap form-data__wrap_editing">
                                                        <textarea class="form-data__textarea" disabled="disabled" name="inn-ooo" rows="1">3245514792/324501001</textarea>
                                                        <div class="form-data__editing js__editing"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="personal__row">
                                            <div class="personal__coll-title">ОГРН</div>
                                            <div class="personal__coll-data">
                                                <div class="form-data">
                                                    <div class="form-data__wrap form-data__wrap_editing">
                                                        <textarea class="form-data__textarea" disabled="disabled" name="ogrn-ooo" rows="1">1143256006497</textarea>
                                                        <div class="form-data__editing js__editing"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="personal__row">
                                            <div class="personal__coll-title">Расчетный счет</div>
                                            <div class="personal__coll-data">
                                                <div class="form-data">
                                                    <div class="form-data__wrap form-data__wrap_editing">
                                                        <textarea class="form-data__textarea" disabled="disabled" name="invoice-ooo" rows="1">40702810902000008941</textarea>
                                                        <div class="form-data__editing js__editing"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="personal__row">
                                            <div class="personal__coll-title">Учреждение Банка</div>
                                            <div class="personal__coll-data">
                                                <div class="form-data">
                                                    <div class="form-data__wrap form-data__wrap_editing">
                                                        <textarea class="form-data__textarea" disabled="disabled" name="bank-ooo" rows="1">ПАО «Промсвязьбанк»  г. Ярославль</textarea>
                                                        <div class="form-data__editing js__editing"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="personal__row">
                                            <div class="personal__coll-title">Бик Банка</div>
                                            <div class="personal__coll-data">
                                                <div class="form-data">
                                                    <div class="form-data__wrap form-data__wrap_editing">
                                                        <textarea class="form-data__textarea" disabled="disabled" name="bik-bank-ooo" rows="1">047888760</textarea>
                                                        <div class="form-data__editing js__editing"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="personal__row">
                                            <div class="personal__coll-title">К/С Банка</div>
                                            <div class="personal__coll-data">
                                                <div class="form-data">
                                                    <div class="form-data__wrap form-data__wrap_editing">
                                                        <textarea class="form-data__textarea" disabled="disabled" name="ks-bank-ooo" rows="1">30101810300000000760</textarea>
                                                        <div class="form-data__editing js__editing"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="personal__row">
                                            <div class="personal__coll-title">ФИО Директора</div>
                                            <div class="personal__coll-data">
                                                <div class="form-data">
                                                    <div class="form-data__wrap form-data__wrap_editing">
                                                        <textarea class="form-data__textarea" disabled="disabled" name="fio-ooo" rows="1">Шестаков Андрей Анатольевич</textarea>
                                                        <div class="form-data__editing js__editing"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button-add" data-popup-btn="add-legal">Добавить реквизиты</div>


                                    <div class="personal__wrap-btn">
                                        <button class="js__disabled-btn form-data__submit blue-button" type="submit">Сохранить</button>
                                    </div>


                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>
