<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>
<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/menu.php'; ?>



    <main class="main">
        <div class="content">

            <section class="cabinet">
                <div class="cabinet__inner">
                    <div class="cabinet__menu js__cabinet-menu">
                        <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/cab-menu.php'; ?>
                    </div>
                    <div class="cabinet__main">
                        <div class="cabinet__title js__cabinet-menu-open">Мои карты</div>
                        <div class="cabinet__content">

                            <div class="cabinet__cards">

                                <div class="card cabinet__card">
                                    <div class="card__inner">
                                        <div class="card__number">1234 5678 9123 4567</div>
                                        <div class="card__discount">25 %</div>
                                    </div>
                                </div>

                                <div class="card cabinet__card no-active">
                                    <div class="card__inner">
                                        <div class="card__number">4234 5678 9423 4567</div>
                                        <div class="card__discount">5 %</div>
                                        <div class="card__button">Активировать</div>
                                    </div>
                                </div>

                                <div class="card cabinet__card no-active">
                                    <div class="card__inner">
                                        <div class="card__number">1234 5678 9123 4567</div>
                                        <div class="card__discount">10 %</div>
                                        <div class="card__button">Активировать</div>
                                    </div>
                                </div>

                            </div>

                            <div class="cabinet__wrap-btn"><div class="blue-button" data-popup-btn="add-card">Добавить карту</div></div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </main>

    <? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/html/blocks/addJs.php'; ?>


</body>
</html>
