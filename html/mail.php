<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="https://faza.center/f/i/favicon.png" type="image/png">
    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addCss.php'; ?>
    <title>Каталог | Faza</title>
</head>
<body>


<div class="wrapper">

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/popups.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/header.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/menu.php'; ?>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/breadcrumbs.php'; ?>

    <main class="main">

        <table border="0" cellpadding="0" cellspacing="0" style="background:#fff; border-collapse:collapse; color:#000; font-family:'arial','verdana',sans-serif; max-width:600px; width:600px">
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px;"></td>
                        </tr>
                        <tr>
                            <td style="height:44px; width:231px;"><img src="https://faza.center/f/i/mail/px.png" style="width:231px; height:44px;"></td>
                            <td><a href="https://faza.center/"><img src="https://faza.center/f/i/icons/faza.svg" style="width:138px; height:44px;"></a></td>
                            <td style="height:44px; width:231px;"><img src="https://faza.center/f/i/mail/px.png" style="width:231px; height:44px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td><img src="https://faza.center/f/i/mail/head.jpg" style="width:600px; height:300px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" style="max-width:600px; width:600px">
                        <tr>
                            <td colspan="3" style="height:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:50px;"></td>
                        </tr>
                        <tr>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="width:50px;"></td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="color:#000; font-family:'arial','verdana',sans-serif; font-size:30px; font-weight: bold; line-height: 34px;">Киберпонедельник – скидки не заканчиваются!</td>
                                    </tr>
                                    
                                </table>
                            </td>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="width:50px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:20px;"></td>
                        </tr>
                        <tr>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="width:50px;"></td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width:500px; max-width:500px;">
                                    <tr>
                                        <td style="width:300px; max-width:300px; color:#000; font-family:'arial','verdana',sans-serif; font-size:16px; line-height: 22px;">Приглашаем окунуться в распродажу со скидками до 40% только на один день! Сегодня товары всех категорий сайта можно купить дешевле!</td>
                                        <td style="width:40px; max-width:40px;"><img src="https://faza.center/f/i/mail/px.png" style="width:40px;"></td>
                                        <td style="width:160px; max-width:160px;">
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:160px; max-width:160px;">
                                                <tr>
                                                    <td style="color:#000; font-family:'arial','verdana',sans-serif; font-size:14px; line-height: 18px;">Только сегодня используйте промокод:</td>
                                                </tr>
                                                <tr>
                                                    <td style="height:10px"><img src="https://faza.center/f/i/mail/px.png" style="height:10px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="color: #233893; border: 3px solid #233893; text-align: center; font-size:22px; line-height: 38px; font-weight: bold;">
                                                        FAZA148
                                                    </td>
                                                </tr>
                                            </table>
                                        
                                        </td>
                                    </tr>
                                </table> 
                            </td>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="width:50px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:50px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" >
                                <table border="0" cellpadding="0" cellspacing="0" style="width:600px; max-width:600px;">
                                    <tr>
                                        <td style="width:200px;"><img src="https://faza.center/f/i/mail/px.png" style="width:200px; height:1px;"></td>
                                        <td><a href="#" style="width:200px; max-width:200px; color: #ffffff; background-color: #233893; text-align:center; display:block; padding: 20px 0;">Что можно купить</a></td>
                                        <td style="width:200px;"><img src="https://faza.center/f/i/mail/px.png" style="width:200px; height:1px;"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:60px;"><img src="https://faza.center/f/i/mail/px.png" style="height:60px; width:1px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td style="background-color: #F3F3F3">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:600px; max-width:600px;">
                        <tr>
                            <td colspan="5" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px; width:1px;"></td>
                        </tr>
                        <tr>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width:240px; max-width:240px; background-color: #fff;">
                                    <tr>
                                        <td colspan="3" style="width:240px;"><img src="https://faza.center/f/i/mail/head.jpg" style="width:240px; height:120px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                        <td style="width:200px; color:#000; font-family:'arial','verdana',sans-serif; font-size:14px; line-height: 22px; font-weight: bold;"> Провода<br/> и кабели</td>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                        <td style="width:200px; color:#000; font-family:'arial','verdana',sans-serif; font-size:16px; line-height: 24px; font-weight: bold;">Скидки до 40%</td>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px; border-top: 2px solid #F3F3F3;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr> 
                                        <td colspan="3">
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:240px; max-width:240px; background-color: #fff;">
                                                <tr>
                                                    <td style="width:40px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:40px;"></td>
                                                    <td style="width:160px;"><a href="" style="width:160px; font-size:14px; color: #ffffff; background-color: #233893; text-align:center; display:block; padding: 10px 0;">Купить со скидкой</a></td>
                                                    <td style="width:40px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:40px;"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width:240px; max-width:240px; background-color: #fff;">
                                    <tr>
                                        <td colspan="3" style="width:240px;"><img src="https://faza.center/f/i/mail/head.jpg" style="width:240px; height:120px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                        <td style="width:200px; color:#000; font-family:'arial','verdana',sans-serif; font-size:14px; line-height: 22px; font-weight: bold;"> Провода<br/> и кабели</td>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                        <td style="width:200px; color:#000; font-family:'arial','verdana',sans-serif; font-size:16px; line-height: 24px; font-weight: bold;">Скидки до 40%</td>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px; border-top: 2px solid #F3F3F3;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr> 
                                        <td colspan="3">
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:240px; max-width:240px; background-color: #fff;">
                                                <tr>
                                                    <td style="width:40px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:40px;"></td>
                                                    <td style="width:160px;"><a href="" style="width:160px; font-size:14px; color: #ffffff; background-color: #233893; text-align:center; display:block; padding: 10px 0;">Купить со скидкой</a></td>
                                                    <td style="width:40px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:40px;"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                        </tr>

                        <tr>
                            <td colspan="5" style="height:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:20px; width:1px;"></td>
                        </tr>

                        <tr>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width:240px; max-width:240px; background-color: #fff;">
                                    <tr>
                                        <td colspan="3" style="width:240px;"><img src="https://faza.center/f/i/mail/head.jpg" style="width:240px; height:120px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                        <td style="width:200px; color:#000; font-family:'arial','verdana',sans-serif; font-size:14px; line-height: 22px; font-weight: bold;"> Провода<br/> и кабели</td>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                        <td style="width:200px; color:#000; font-family:'arial','verdana',sans-serif; font-size:16px; line-height: 24px; font-weight: bold;">Скидки до 40%</td>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px; border-top: 2px solid #F3F3F3;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr> 
                                        <td colspan="3">
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:240px; max-width:240px; background-color: #fff;">
                                                <tr>
                                                    <td style="width:40px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:40px;"></td>
                                                    <td style="width:160px;"><a href="" style="width:160px; font-size:14px; color: #ffffff; background-color: #233893; text-align:center; display:block; padding: 10px 0;">Купить со скидкой</a></td>
                                                    <td style="width:40px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:40px;"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width:240px; max-width:240px; background-color: #fff;">
                                    <tr>
                                        <td colspan="3" style="width:240px;"><img src="https://faza.center/f/i/mail/head.jpg" style="width:240px; height:120px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                        <td style="width:200px; color:#000; font-family:'arial','verdana',sans-serif; font-size:14px; line-height: 22px; font-weight: bold;"> Провода<br/> и кабели</td>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                        <td style="width:200px; color:#000; font-family:'arial','verdana',sans-serif; font-size:16px; line-height: 24px; font-weight: bold;">Скидки до 40%</td>
                                        <td style="width:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:20px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px; border-top: 2px solid #F3F3F3;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                    <tr> 
                                        <td colspan="3">
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:240px; max-width:240px; background-color: #fff;">
                                                <tr>
                                                    <td style="width:40px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:40px;"></td>
                                                    <td style="width:160px;"><a href="" style="width:160px; font-size:14px; color: #ffffff; background-color: #233893; text-align:center; display:block; padding: 10px 0;">Купить со скидкой</a></td>
                                                    <td style="width:40px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:40px;"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="height:5px;"><img src="https://faza.center/f/i/mail/px.png" style="height:5px; width:1px;"></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                        </tr>

                        <tr>
                            <td colspan="5" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px; width:1px;"></td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
            <tr>
                <td colspan="3" >
                    <table border="0" cellpadding="0" cellspacing="0" style="width:600px; max-width:600px;">
                        <tr>
                            <td colspan="3" style="height:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:50px;"></td>
                        </tr>
                        <tr>
                            <td style="width:200px;"><img src="https://faza.center/f/i/mail/px.png" style="width:200px; height:1px;"></td>
                            <td><a href="#" style="width:200px; max-width:200px; color: #ffffff; background-color: #233893; text-align:center; display:block; padding: 20px 0;">Все товары каталога</a></td>
                            <td style="width:200px;"><img src="https://faza.center/f/i/mail/px.png" style="width:200px; height:1px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:50px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="background-color: #F3F3F3;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:600px; max-width:600px;">
                        <tr>
                            <td colspan="3" style="height:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:50px;"></td>
                        </tr>
                        <tr>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width:500px; max-width:500px; background-color: #fff;">
                                    <tr>
                                        <td><img src="https://faza.center/f/i/mail/head.jpg" style="width:500px; height:250px;"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:500px; max-width:500px; background-color: #fff;">
                                                <tr>
                                                    <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px; width:1px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                    <td style="width:440px; color:#000; font-family:'arial','verdana',sans-serif; font-size:20px; line-height: 22px; font-weight: bold;">Узо, дифавтоматы, автоматы</td>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:500px; max-width:500px; background-color: #fff;">
                                                <tr>
                                                    <td colspan="3" style="height:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:20px; width:1px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                    <td style="width:440px; color:#000; font-family:'arial','verdana',sans-serif; font-size:16px; line-height: 22px;">Приглашаем окунуться в распродажу со скидками до 40% только на один день! Сегодня товары всех категорий сайта можно купить дешевле!</td>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px; width:1px;"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px;"></td>
                        </tr>
                        <tr>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width:500px; max-width:500px; background-color: #fff;">
                                    <tr>
                                        <td><img src="https://faza.center/f/i/mail/head.jpg" style="width:500px; height:250px;"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:500px; max-width:500px; background-color: #fff;">
                                                <tr>
                                                    <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px; width:1px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                    <td style="width:440px; color:#000; font-family:'arial','verdana',sans-serif; font-size:20px; line-height: 22px; font-weight: bold;">Узо, дифавтоматы, автоматы</td>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:500px; max-width:500px; background-color: #fff;">
                                                <tr>
                                                    <td colspan="3" style="height:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:20px; width:1px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                    <td style="width:440px; color:#000; font-family:'arial','verdana',sans-serif; font-size:16px; line-height: 22px;">Приглашаем окунуться в распродажу со скидками до 40% только на один день! Сегодня товары всех категорий сайта можно купить дешевле!</td>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px; width:1px;"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px;"></td>
                        </tr>
                        <tr>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width:500px; max-width:500px; background-color: #fff;">
                                    <tr>
                                        <td><img src="https://faza.center/f/i/mail/head.jpg" style="width:500px; height:250px;"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:500px; max-width:500px; background-color: #fff;">
                                                <tr>
                                                    <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px; width:1px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                    <td style="width:440px; color:#000; font-family:'arial','verdana',sans-serif; font-size:20px; line-height: 22px; font-weight: bold;">Узо, дифавтоматы, автоматы</td>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" style="width:500px; max-width:500px; background-color: #fff;">
                                                <tr>
                                                    <td colspan="3" style="height:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:20px; width:1px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                    <td style="width:440px; color:#000; font-family:'arial','verdana',sans-serif; font-size:16px; line-height: 22px;">Приглашаем окунуться в распродажу со скидками до 40% только на один день! Сегодня товары всех категорий сайта можно купить дешевле!</td>
                                                    <td style="width:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:30px;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px; width:1px;"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:50px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="background-color: #000000;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:600px; max-width:600px;">
                        <tr>
                            <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px;"></td>
                        </tr>
                        <tr>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" style="width:500px; max-width:500px;">
                                    <tr>
                                        <td style="width:146px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:146px;"></td>
                                        <td style="width:36px;"><a href="#"><img src="https://faza.center/f/i/mail/vk.png" style="height:36px; width:36px;"></a></td>
                                        <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                                        <td style="width:36px;"><a href="#"><img src="https://faza.center/f/i/mail/fb.png" style="height:36px; width:36px;"></a></td>
                                        <td style="width:96px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                                        <td style="width:36px;"><a href="#"><img src="https://faza.center/f/i/mail/yt.png" style="height:36px; width:36px;"></a></td>
                                        <td style="width:146px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:146px;"></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:20px;"></td>
                        </tr>
                        <tr>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                            <td style="color:#fff; line-height:20px; font-family:'arial','verdana',sans-serif;">
                                Вы получили это письмо, потому что подписались на получение новостей и акций инернет-магазина faza.center
                            </td>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:20px;"></td>
                        </tr>
                        <tr>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                            <td style="color:#fff; line-height:20px; font-family:'arial','verdana',sans-serif;">
                                Данное письмо не требует ответа.
                            </td>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:20px;"><img src="https://faza.center/f/i/mail/px.png" style="height:20px;"></td>
                        </tr>
                        <tr>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                            <td style="color:#fff; line-height:20px; font-family:'arial','verdana',sans-serif;">
                                Если вы больше не хотите получать информацию об акциях, распродажах и спецпредложениях faza.center, то вы можете  <a href="#" style="color:#233893;">отписаться от рассылки</a>.
                            </td>
                            <td style="width:50px;"><img src="https://faza.center/f/i/mail/px.png" style="height:1px; width:50px;"></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:30px;"><img src="https://faza.center/f/i/mail/px.png" style="height:30px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>


        </table>

    </main>

    <? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/footer.php'; ?>

</div>

<? include $_SERVER['DOCUMENT_ROOT'] . '/html/blocks/addJs.php'; ?>

</body>
</html>
