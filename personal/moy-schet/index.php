<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>
    <main class="main">
    <div class="content">
<section class="cabinet">
                <div class="cabinet__inner">
                    <div class="cabinet__menu js__cabinet-menu">
                        <?$APPLICATION->IncludeComponent("bitrix:menu", "template_personal", Array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
			0 => "",
		),
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "N",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	),
	false
);?>
                    </div>
                    <div class="cabinet__main">
                        <div class="cabinet__title js__cabinet-menu-open">Пополнение счета</div>
                        <div class="cabinet__content">

                            <div class="cabinet__wrap-cash">

                                <form class="discount-card cabinet__discount-card">
                                    <div class="discount-card__inner">
                                        <div class="discount-card__title">Введите номер подарочной&nbsp;карты</div>
                                        <input type="text" class="discount-card__input js__discount-card">
                                        <button class="discount-card__button blue-button" type="submit">Активировать</button>
                                    </div>
                                </form>

                                <div class="balance cabinet__balance">
                                    <div class="balance__title">Текущий баланс вашего счета</div>
                                    <div class="balance__cash">0</div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </section>
    </div></main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>