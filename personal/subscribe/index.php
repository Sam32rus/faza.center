<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Подписка на новости");
?>
<main class="main">
	<div class="content">
		<section class="cabinet">
			<div class="cabinet__inner">
				<div class="cabinet__menu js__cabinet-menu">
					<? $APPLICATION->IncludeComponent(
						"bitrix:menu",
						"template_personal",
						array(
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "N",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
							"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						),
						false
					); ?>

				</div>
				<div class="cabinet__main">
					<div class="cabinet__title js__cabinet-menu-open">Подписка</div>
					<div class="cabinet__content">
						<? $APPLICATION->IncludeComponent(
							"bitrix:subscribe.edit",
							"personal",
							array(
								"AJAX_MODE" => "Y",
								"SHOW_HIDDEN" => "Y",
								"ALLOW_ANONYMOUS" => "Y",
								"SHOW_AUTH_LINKS" => "Y",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "36000000",
								"SET_TITLE" => "Y",
								"AJAX_OPTION_SHADOW" => "Y",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"COMPONENT_TEMPLATE" => "personal",
								"AJAX_OPTION_ADDITIONAL" => ""
							),
							false
						); ?>
					</div>
				</div>
			</div>
		</section>
	</div>
</main>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>