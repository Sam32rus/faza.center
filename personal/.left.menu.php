<?
$aMenuLinks = Array(
	Array(
		"Избранные товары", 
		"/personal/favorites/", 
		Array(), 
		Array("class"=>"cab-menu__item-link_chosen"), 
		"" 
	),
	Array(
		"Просмотренные", 
		"/personal/viewed/", 
		Array(), 
		Array("class"=>"cab-menu__item-link_viewed"), 
		"" 
	),
	Array(
		"Сравнения", 
		"/catalog/compare/", 
		Array(), 
		Array("class"=>"cab-menu__item-link_comparisons"), 
		"" 
	),
	Array(
		"Мои заказы", 
		"/personal/orders/", 
		Array(), 
		Array("class"=>"cab-menu__item-link_order"), 
		"" 
	),
	Array(
		"Мой счет", 
		"/personal/moy-schet/", 
		Array(), 
		Array("class"=>"cab-menu__item-link_cash no-active"), 
		"" 
	),
	Array(
		"Мои карты", 
		"/personal/moi-karty/", 
		Array(), 
		Array("class"=>"cab-menu__item-link_card no-active"), 
		"" 
	),
	Array(
		"Личные данные", 
		"/personal/", 
		Array(), 
		Array("class"=>"cab-menu__item-link_personal"), 
		"" 
	),
	Array(
		"Подписка", 
		"/personal/subscribe/", 
		Array(), 
		Array("class"=>"cab-menu__item-link_subscription"), 
		"" 
	),
	Array(
		"Подрядчикам", 
		"/personal/", 
		Array(), 
		Array("class"=>"cab-menu__item-link_contractor no-active"), 
		"" 
	),
	Array(
		"Выйти", 
		"?logout=yes&login=yes", 
		Array(), 
		Array("class"=>"cab-menu__item-link_exit"), 
		"\$GLOBALS[\"USER\"]->isAuthorized()" 
	)
);
?>