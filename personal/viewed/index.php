<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Title");
?>
<main class="main">
	<div class="content">
		<section class="cabinet">
			<div class="cabinet__inner">
				<div class="cabinet__menu js__cabinet-menu">
					<? $APPLICATION->IncludeComponent(
						"bitrix:menu",
						"template_personal",
						array(
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "N",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
							"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						),
						false
					); ?>
				</div>
				<div class="cabinet__main">
					<div class="cabinet__title js__cabinet-menu-open">Ранее просматривали</div>
					<div class="cabinet__content">

						<div class="cabinet__products">

							<? $APPLICATION->IncludeComponent(
								"bitrix:catalog.products.viewed",
								"personal__viewed",
								array(
									"ACTION_VARIABLE" => "action_cpv",	// Название переменной, в которой передается действие
									"ADDITIONAL_PICT_PROP_7" => "-",	// Дополнительная картинка
									"ADD_PROPERTIES_TO_BASKET" => "Y",	// Добавлять в корзину свойства товаров и предложений
									"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
									"BASKET_URL" => "/basket/",	// URL, ведущий на страницу с корзиной покупателя
									"CACHE_GROUPS" => "Y",	// Учитывать права доступа
									"CACHE_TIME" => "3600",	// Время кеширования (сек.)
									"CACHE_TYPE" => "A",	// Тип кеширования
									"COMPARE_NAME" => "CATALOG_COMPARE_LIST",	// Уникальное имя для списка сравнения
									"COMPARE_PATH" => "/catalog/compare/",	// Путь к странице сравнения
									"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
									"DEPTH" => "2",	// Максимальная отображаемая глубина разделов
									"DISPLAY_COMPARE" => "Y",	// Разрешить сравнение товаров
									"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
									"HIDE_NOT_AVAILABLE" => "L",	// Недоступные товары
									"HIDE_NOT_AVAILABLE_OFFERS" => "N",	// Недоступные торговые предложения
									"IBLOCK_ID" => "7",	// Инфоблок
									"IBLOCK_MODE" => "single",	// Показывать товары из
									"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
									"LABEL_PROP_7" => "",	// Свойство меток товара
									"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
									"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
									"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
									"MESS_BTN_COMPARE" => "Сравнить",	// Текст кнопки "Сравнить"
									"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
									"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
									"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
									"PAGE_ELEMENT_COUNT" => "9",	// Количество элементов на странице
									"PARTIAL_PRODUCT_PROPERTIES" => "Y",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
									"PRICE_CODE" => array(	// Тип цены
										0 => "RUB",
									),
									"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
									"PRODUCT_BLOCKS_ORDER" => "buttons,price,props,sku,quantityLimit,quantity",	// Порядок отображения блоков товара
									"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
									"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
									"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
									"PRODUCT_ROW_VARIANTS" => "[]",	// Вариант отображения товаров
									"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
									"SECTION_CODE" => "",	// Код раздела
									"SECTION_ELEMENT_CODE" => "",	// Символьный код элемента, для которого будет выбран раздел
									"SECTION_ELEMENT_ID" => $GLOBALS["CATALOG_CURRENT_ELEMENT_ID"],	// ID элемента, для которого будет выбран раздел
									"SECTION_ID" => $GLOBALS["CATALOG_CURRENT_SECTION_ID"],	// ID раздела
									"SHOW_CLOSE_POPUP" => "N",	// Показывать кнопку продолжения покупок во всплывающих окнах
									"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
									"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
									"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
									"SHOW_OLD_PRICE" => "N",	// Показывать старую цену
									"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
									"SHOW_SLIDER" => "N",	// Показывать слайдер для товаров
									"SLIDER_INTERVAL" => "3000",
									"SLIDER_PROGRESS" => "N",
									"TEMPLATE_THEME" => "",	// Цветовая тема
									"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
									"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
									"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
								),
								false
							); ?>

						</div>
					</div>
				</div>

			</div>
		</section>
	</div>
</main>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>