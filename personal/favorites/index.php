<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

use Bitrix\Main\Web\Cookie;
use Bitrix\Main\Application;

global $APPLICATION;
$APPLICATION->SetTitle("Избранные товары");


if (!$USER->IsAuthorized()) // Для неавторизованного
{
    $favorites = unserialize(Application::getInstance()->getContext()->getRequest()->getCookie("favorites"));
} else {
    $idUser = $USER->GetID();
    $rsUser = CUser::GetByID($idUser);
    $arUser = $rsUser->Fetch();
    $favorites = $arUser['UF_FAVORITES'];
}

$GLOBALS['arrFilter'] = array("ID" => $favorites);
// if(count($favorites) > 0 && is_array($favorites)){
// var_dump($GLOBALS['arrFilter']);

if ($favorites) :
?>
    <main class="main">
        <div class="content">
            <section class="cabinet">
                <div class="cabinet__inner">
                    <div class="cabinet__menu js__cabinet-menu">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "template_personal",
                            array(
                                "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                                "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
                                "DELAY" => "N",    // Откладывать выполнение шаблона меню
                                "MAX_LEVEL" => "1",    // Уровень вложенности меню
                                "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                                    0 => "",
                                ),
                                "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                                "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                                "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                                "ROOT_MENU_TYPE" => "left",    // Тип меню для первого уровня
                                "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                            ),
                            false
                        ); ?>
                    </div>
                    <div class="cabinet__main">
                        <div class="cabinet__title js__cabinet-menu-open">Избранные товары</div>
                        <div class="cabinet__content">

                            <? $APPLICATION->IncludeComponent(
                                "bitrix:catalog.section",
                                "personal__favorites",
                                array(
                                    "COMPONENT_TEMPLATE" => "personal__favorites",
                                    "IBLOCK_TYPE" => "catalog",
                                    "IBLOCK_ID" => "7",
                                    "SECTION_USER_FIELDS" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "FILTER_NAME" => "arrFilter",
                                    "INCLUDE_SUBSECTIONS" => "Y",
                                    "SHOW_ALL_WO_SECTION" => "N",
                                    "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
                                    "HIDE_NOT_AVAILABLE" => "N",
                                    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                                    "ELEMENT_SORT_FIELD" => "sort",
                                    "ELEMENT_SORT_ORDER" => "asc",
                                    "ELEMENT_SORT_FIELD2" => "id",
                                    "ELEMENT_SORT_ORDER2" => "desc",
                                    "PAGE_ELEMENT_COUNT" => "30",
                                    "LINE_ELEMENT_COUNT" => "4",
                                    "OFFERS_LIMIT" => "5",
                                    "BACKGROUND_IMAGE" => "-",
                                    "SECTION_URL" => "",
                                    "DETAIL_URL" => "",
                                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                                    "PRODUCT_BLOCKS_ORDER" => "buttons,price,props,sku,quantityLimit,quantity",
                                    "SEF_MODE" => "Y",
                                    "SEF_RULE" => "",
                                    "SECTION_ID" => "",
                                    "SECTION_CODE" => "",
                                    "SECTION_CODE_PATH" => "",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_GROUPS" => "Y",
                                    "SET_TITLE" => "Y",
                                    "SET_BROWSER_TITLE" => "Y",
                                    "BROWSER_TITLE" => "-",
                                    "SET_META_KEYWORDS" => "Y",
                                    "META_KEYWORDS" => "-",
                                    "SET_META_DESCRIPTION" => "Y",
                                    "META_DESCRIPTION" => "-",
                                    "SET_LAST_MODIFIED" => "N",
                                    "USE_MAIN_ELEMENT_SECTION" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "CACHE_FILTER" => "N",
                                    "ACTION_VARIABLE" => "action",
                                    "PRODUCT_ID_VARIABLE" => "id",
                                    "PRICE_CODE" => array(
                                        0 => "RUB",
                                    ),
                                    "USE_PRICE_COUNT" => "N",
                                    "SHOW_PRICE_COUNT" => "1",
                                    "PRICE_VAT_INCLUDE" => "Y",
                                    "CONVERT_CURRENCY" => "N",
                                    "BASKET_URL" => BASKET_URL,
                                    "USE_PRODUCT_QUANTITY" => "N",
                                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                                    "PRODUCT_PROPS_VARIABLE" => "prop",
                                    "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                                    "DISPLAY_COMPARE" => "Y",
                                    "COMPARE_PATH" => "",
                                    "PAGER_TEMPLATE" => ".default",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "Товары",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "SHOW_404" => "N",
                                    "MESSAGE_404" => "",
                                    "COMPATIBLE_MODE" => "Y",
                                    "DISABLE_INIT_JS_IN_COMPONENT" => "N"
                                ),
                                $component,
                                array(
                                    "HIDE_ICONS" => "N"
                                )
                            ); ?>

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
<? else : ?>
    <main class="main">
        <div class="content">
            <section class="cabinet">
                <div class="cabinet__inner">
                    <div class="cabinet__menu js__cabinet-menu">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "template_personal",
                            array(
                                "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                                "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
                                "DELAY" => "N",    // Откладывать выполнение шаблона меню
                                "MAX_LEVEL" => "1",    // Уровень вложенности меню
                                "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                                    0 => "",
                                ),
                                "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                                "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                                "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                                "ROOT_MENU_TYPE" => "left",    // Тип меню для первого уровня
                                "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                            ),
                            false
                        ); ?>
                    </div>
                    <div class="cabinet__main">
                        <div class="cabinet__title js__cabinet-menu-open">Избранные товары</div>
                        <div class="cabinet__content">
                            <div class="empty__result">
                                Список товаров пуст
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
<? endif; ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>