<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
?>
<?
global $USER;
if (!$USER->isAdmin()) {
    // LocalRedirect(SITE_DIR . 'personal/orders/');
}
if (!$USER->isAuthorized()) {
    LocalRedirect(SITE_DIR . 'auth/?backurl=' . SITE_DIR . 'personal/');
}?>
<?$APPLICATION->IncludeComponent(
    "iis:profile",
    ".default",
    array(),
    false
);?>

<?// } ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>