const gulp = require('gulp');
const babel = require('gulp-babel');
const plumber = require('gulp-plumber');
const concat = require("gulp-concat");
const minify = require('gulp-uglify');
const log = require('gulplog');
var stylus = require('gulp-stylus');
var nib = require('nib');

const buildStyles = function (done) {
	return gulp.src('./source/stylus/styles.styl')
		.pipe(plumber())
		.pipe(stylus({
			use: [nib()]
		}))
		.pipe(gulp.dest('./f/css'));

	done();
};

const buildJs = function (done) {
	return gulp.src('./source/js/blocks/*.js')
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(concat('app.js'))
		.pipe(minify())
		.pipe(gulp.dest('./f/js'));

	done();
};

const vendorJs = function (done) {
	return gulp.src('./source/js/vendor/*.js')
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(concat('vendor.js'))
		.pipe(minify())
		.pipe(gulp.dest('./f/js'));

	done();
};

const watch = function (done) {
	gulp.watch(['./source/stylus/*.*', './source/stylus/*/*.*'], gulp.series(buildStyles));
	gulp.watch(['./source/js/*.*', './source/js/*/*.*'], gulp.series(buildJs));
};

gulp.task('default', gulp.series(buildStyles, buildJs, vendorJs, watch));

