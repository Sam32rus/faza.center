<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

// Bitrix\Main\Loader::registerAutoLoadClasses('iis.exchange', array(
//     'Iis\Exchange\CExchange' => 'lib/CExchange.php',
// ));

CModule::AddAutoloadClasses(
	'iis.exchange',
	array(
		'\Iis\Exchange\Api' => "lib/api.php",
	)
);