<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$MESS['NETI_SIMPLEDRAW_COMPONENT_PARTNER_NAME'] = 'Neti Ltd.';
$MESS['NETI_SIMPLEDRAW_COMPONENT_PATH_NAME'] = 'Рисование на Canvas';

$MESS['NETI_SIMPLEDRAW_COMPONENT_NAME'] = 'Компонент для рисования';
$MESS['NETI_SIMPLEDRAW_COMPONENT_DESCRIPTION'] = 'Комплексный компонент для работы с Canvas и сохранения рисунков';