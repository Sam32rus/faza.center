<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
?>

<?
$APPLICATION->includeComponent(
        'neti.simpledraw:simpledraw.detail',
        '',
        array(
            
            'PICTURE_ID'        => $arResult['VARIABLES']['ID'],
            'MODE'              => $arResult['DETAIL_MODE'],
            
            'LIST_URL'      => $arResult['SEF_FOLDER'].$arResult['URL_TEMPLATES']['list'],
            'NEW_URL'       => $arResult['SEF_FOLDER'].$arResult['URL_TEMPLATES']['new'],
            'DETAIL_URL'    => $arResult['SEF_FOLDER'].$arResult['URL_TEMPLATES']['detail'],
            'EDIT_URL'      => $arResult['SEF_FOLDER'].$arResult['URL_TEMPLATES']['edit'],
            'DELETE_URL'    => $arResult['SEF_FOLDER'].$arResult['URL_TEMPLATES']['delete'],
            
            'PICTURE_WIDTH'     => $arParams['PICTURE_WIDTH'],
            'PICTURE_HEIGHT'    => $arParams['PICTURE_HEIGHT'],
            
            'CACHE_TYPE'        => $arParams['CACHE_TYPE'],
            'CACHE_TIME'        => $arParams['CACHE_TIME'],
            'MAX_FILE_SIZE'     => $arParams['MAX_FILE_SIZE'],
            'SET_TITLE'         => $arParams['SET_TITLE'],
        ),
        $component
    );
?>