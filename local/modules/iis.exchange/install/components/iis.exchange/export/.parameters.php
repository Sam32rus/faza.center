<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

//------------------------------------------------------------------------------
$arComponentParameters = array(
    'GROUPS' => array(
        'IMAGE' => array(
            'NAME' => Loc::getMessage('GROUP_IMAGE'),
            'SORT' => 150,
        ),
        'PAGER' => array(
            'NAME' => Loc::getMessage('GROUP_PAGER'),
            'SORT' => 600,
        ),
    ),
    'PARAMETERS' => array(
        'VARIABLE_ALIASES' => Array(
            'ID' => Array('NAME' => Loc::GetMessage('SIMPLEDRAW_ELEMENT_ID_DESC')),
        ),
        'SEF_MODE' => Array(
            'list' => array(
                'NAME' => Loc::GetMessage('SIMPLEDRAW_SEF_LIST'),
                'DEFAULT' => '',
                'VARIABLES' => array(),
            ),
            'new' => array(
                'NAME' => Loc::GetMessage('SIMPLEDRAW_SEF_NEW'),
                'DEFAULT' => 'new/',
                'VARIABLES' => array(),
            ),
            'detail' => array(
                'NAME' => Loc::GetMessage('SIMPLEDRAW_SEF_DETAIL'),
                'DEFAULT' => '#ID#/',
                'VARIABLES' => array('ID'),
            ),
            'edit' => array(
                'NAME' => Loc::GetMessage('SIMPLEDRAW_SEF_EDIT'),
                'DEFAULT' => '#ID#/edit/',
                'VARIABLES' => array('ID'),
            ),
            'delete' => array(
                'NAME' => Loc::GetMessage('SIMPLEDRAW_SEF_DELETE'),
                'DEFAULT' => '#ID#/del/',
                'VARIABLES' => array('ID'),
            ),
        ),
        'PICTURE_WIDTH' => array(
            'PARENT' => 'IMAGE',
            'NAME' => Loc::getMessage('PICTURE_WIDTH'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 500,
        ),
        'PICTURE_HEIGHT' => array(
            'PARENT' => 'IMAGE',
            'NAME' => Loc::getMessage('PICTURE_HEIGHT'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 500,
        ),
        'PREVIEW_WIDTH' => array(
            'PARENT' => 'IMAGE',
            'NAME' => Loc::getMessage('PREVIEW_WIDTH'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 150,
        ),
        'PREVIEW_HEIGHT' => array(
            'PARENT' => 'IMAGE',
            'NAME' => Loc::getMessage('PREVIEW_HEIGHT'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 150,
        ),     
        'MAX_FILE_SIZE' => array(
            'PARENT' => 'IMAGE',
            'NAME' => Loc::getMessage('MAX_FILE_SIZE'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 20*1024*1024,
        ),
        
        'PICTURES_COUNT' => Array(
            'PARENT' => 'PAGER',
            'NAME' => GetMessage('PICTURES_COUNT'),
            'TYPE' => 'STRING',
            'DEFAULT' => '6',
        ),
        'DISPLAY_TOP_PAGER' => array(
            'PARENT' => 'PAGER',
            'NAME' => Loc::getMessage('DISPLAY_TOP_PAGER'),
            'TYPE' => 'CHECKBOX',
            'MULTIPLE' => 'N',
            'DEFAULT' => 'N',
        ),
        'DISPLAY_BOTTOM_PAGER' => array(
            'PARENT' => 'PAGER',
            'NAME' => Loc::getMessage('DISPLAY_BOTTOM_PAGER'),
            'TYPE' => 'CHECKBOX',
            'MULTIPLE' => 'N',
            'DEFAULT' => 'Y',
        ),
        
        'CACHE_TIME' => array('DEFAULT' => 3600000),
        'SET_TITLE' => array(),
    )
);