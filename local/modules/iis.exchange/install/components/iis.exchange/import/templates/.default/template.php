<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>

<? if (!empty($arResult['SUCCESS'])) { ?>
<div class="simpledraw-alert success">
    <p><?=$arResult['SUCCESS']?></p>
</div>
<? } ?>

<div class="simpledraw">
    <!--<h1><?=$arResult['DATA']['UF_NAME']?></h1>-->
    <div class="btn-row">
        <a href="<?=$arResult['URLS']['EDIT']?>" class="simpledraw-btn green"><?=Loc::getMessage('LINK_EDIT')?></a>
        <a href="<?=$arResult['URLS']['DELETE']?>" class="simpledraw-btn red"><?=Loc::getMessage('LINK_DELETE')?></a>
    </div>

    <div class="image">
        <img src="<?=$arResult['DATA']['UF_FILE_SRC']?>" alt="<?=$arResult['DATA']['UF_NAME']?>"/>
    </div>
    
    <div class="date"><?=(!empty($arResult['DATA']['UF_DATETIME']) ? $arResult['DATA']['UF_DATETIME']->format('d.m.Y') : '')?></div>
</div>