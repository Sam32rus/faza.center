<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$MESS['HELLO_MESSAGE'] = 'Вы хотите удалить рисунок из базы данных. Это действие необратимо. Продолжить?';
$MESS['TEXT_WARNING'] = 'Удалить рисунок?';
$MESS['BTN_SUBMIT'] = 'Удалить';
$MESS['BTN_CANCEL'] = 'Отмена';