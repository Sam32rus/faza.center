<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$MESS['ERROR_NO_IMAGE'] = 'Не удалось определить данные изображения.';
$MESS['ERROR_NO_NAME'] = 'Необходимо указать название рисунка.';
$MESS['ERROR_NO_PASSWORD'] = 'Не указан пароль.';
$MESS['ERROR_PASSWORD_CONFIRM'] = 'Пароль не совпадает с его подтверждением.';
$MESS['ERROR_IMAGE_STRUCTURE'] = 'Ошибка в формате данных рисунка.';
$MESS['ERROR_MAX_FILE_SIZE'] = 'Превышен максимальный размер сохраняемого файла.';
$MESS['ERROR_FILE_SAVE'] = 'Не удалось произвести сохранение файла.';

$MESS['ERROR_PASSWORD_WRONG'] = 'Вы указали неверный пароль. Доступ запрещен.';

$MESS['SUCCESS_SAVE'] = 'Ваш рисунок сохранен.';
$MESS['SUCCESS_DELETE'] = 'Рисунок удален.';

$MESS['CHAIN_ITEM_NEW'] = 'Создание рисунка';
$MESS['CHAIN_ITEM_EDIT'] = 'Редактирование рисунка UF_NAME';
$MESS['CHAIN_ITEM_DELETE'] = 'Удаление рисунка UF_NAME';

$MESS['PAGE_TITLE_NEW'] = 'Создание рисунка';
$MESS['PAGE_TITLE_EDIT'] = 'UF_NAME (редактирование)';
$MESS['PAGE_TITLE_DELETE'] = 'UF_NAME (удаление)';