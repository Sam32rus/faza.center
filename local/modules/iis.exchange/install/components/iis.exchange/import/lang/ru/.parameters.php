<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$MESS['GROUP_IMAGE'] = 'Настройки изображений и холста';
$MESS['PICTURE_WIDTH'] = 'Ширина холста';
$MESS['PICTURE_HEIGHT'] = 'Высота холста';
$MESS['MAX_FILE_SIZE'] = 'Максимальный размер файла (в байтах)';

$MESS['MODE'] = 'Текущий режим компонента (view, new, edit, delete)';
$MESS['PICTURE_ID'] = 'Идентификатор картинки';