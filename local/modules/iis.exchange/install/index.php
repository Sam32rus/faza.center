<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\EventManager;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);

class iis_exchange extends CModule
{
    public $MODULE_ID = 'iis.exchange';
    public $MODULE_VERSION = '1.0';
    public $MODULE_VERSION_DATE = '2021-10-29 00:00:00';
    public $PARTNER_NAME = 'iis';

    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS;
    
    public $errors, $module_path;

    public function __construct()
    {
        $this->MODULE_NAME =  Loc::getMessage('MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        
        $this->errors = array();
        
        if (file_exists($_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->MODULE_ID.'/')) {
            $this->module_path = $_SERVER['DOCUMENT_ROOT'].'/local/modules/'.$this->MODULE_ID;
        } else {
            $this->module_path = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID;
        }
    }

    public function doInstall()
    {
        global $APPLICATION;
        
        if (!ModuleManager::isModuleInstalled($this->MODULE_ID)) {
            ModuleManager::registerModule($this->MODULE_ID);
            if (CModule::IncludeModule($this->MODULE_ID)) {
                
                // создает или обновляет структуру HL блока
                $this->doInstallDB();
                if (!empty($this->errors)) {
                    ModuleManager::unregisterModule($this->MODULE_ID);
                    $APPLICATION->ThrowException(implode('<br>', $this->errors));
                    return FALSE;
                }
                // копирование файлов компонентов
                $this->doInstallFiles();
                
            } else {
                $APPLICATION->ThrowException(Loc::getMessage('ERROR_MODULE_REGISTER'));
                return FALSE;
            }
        }
    }
    
    public function doUninstall()
    {
        global $APPLICATION, $step;
        
        if (CModule::IncludeModule($this->MODULE_ID)) {
            $step = IntVal($step);
            if($step < 2)
            {
                $APPLICATION->IncludeAdminFile(Loc::GetMessage('REPORT_UNINSTALL_TITLE'), $this->module_path.'/install/unstep1.php');
            }
            elseif($step == 2)
            {
                if ($_REQUEST['savedata'] != 'Y') {
                   $this->doUnInstallDB(); 
                }
                $this->doUnInstallFiles();
                ModuleManager::unregisterModule($this->MODULE_ID);
            }
        }
    }
    
    public function doInstallDB()
    {
        if (!empty($this->errors)) { return; }
    }
    
    public function doUnInstallDB()
    {

    }
    
    public function doInstallFiles() {
        if ($_ENV['COMPUTERNAME'] != 'BX') {
            CopyDirFiles($this->module_path.'/install/components', $_SERVER['DOCUMENT_ROOT'].'/local/components', TRUE, TRUE);
        }
    }

    public function doUnInstallFiles() {
        if ($_ENV['COMPUTERNAME'] != 'BX') {
            DeleteDirFilesEx('/local/components/' . $this->MODULE_ID);
        }
    }
}