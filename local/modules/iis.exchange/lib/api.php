<?php
namespace Iis\Exchange;
use Bitrix\Main\Application, 
    Bitrix\Main\Context, 
    Bitrix\Main\Request, 
    Bitrix\Main\Server;

class Api
{
    static function start()
    {
        $request = Context::getCurrent()->getRequest();
        if (!$request->isPost())
        {
            die();
        }

        $brandXmlId = $request->getPost('export_brand');
        $brandSavePath = $request->getPost('export_path_brand');
        // $brandLimitProducts = $request->getPost('export_limit_per_file_brand');
        // $pageNum = $request->getPost('export_file_num');


        // $filter = [
        //     'IBLOCK_ID' => '7',
        //     'ACTIVE' => 'Y',
        //     'ACTIVE_DATE' => 'Y',
        //     'PROPERTY_brand' => $brandXmlId,
        //     '!IBLOCK_SECTION_ID' => false
        // ];

        // $countElements = \CIBlockElement::GetList([], $filter, false, ['checkOutOfRange' => true], ['ID'])->SelectedRowsCount();
        // $countFiles = ceil($countElements / $brandLimitProducts);

        if (!empty($brandXmlId) && !empty($brandSavePath)/* && !empty($brandLimitProducts)*/)
        {
        self::startBrandExport($brandXmlId, $brandSavePath/*, $brandLimitProducts, $pageNum*/);
        }
        // switch ($params['tabControl_active_tab']) {
        //     case 'export':
        //         self::startExport();
        //         break;
        //     case 'import':
        //         self::startImport();
        //         break;
        //     case 'brandExport':
        //         self::startBrandExport();
        //         break;
        // }
    }

    static function startExport()
    {
        file_put_contents($_SERVER["DOCUMENT_ROOT"]."/upload/test.txt", print_r(file_get_contents("php://input"),true));
    }

    static function startImport()
    {
        file_put_contents($_SERVER["DOCUMENT_ROOT"]."/upload/test.txt", print_r(file_get_contents("php://input"),true));
    }
    
    static function startBrandExport($brandXmlId, $brandSavePath/*, $brandLimitProducts, $pageNum = 1*/)
    {
        include($_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/include/catalog_export/exchangeBrandProduct1c_run.php');
    }
}