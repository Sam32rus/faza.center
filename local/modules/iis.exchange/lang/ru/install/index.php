<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['MODULE_NAME'] = 'Обмен с 1С';
$MESS['MODULE_DESCRIPTION'] = 'Модуль для обмена с 1С';

$MESS['ERROR_MODULE_REGISTER'] = 'Не удалось произвести регистрацию модуля';
$MESS['ERROR_MODULE_HIGHLOADBLOCK'] = 'Для работы модуля необходимо установить модуль управления HighLoadBlock';
$MESS['ERROR_FIELD_CREATE'] = 'Не удалось создать поле highload блока: NAME';