<?php

// namespace Iis\Exchange;
$module_id = 'iis.exchange';

\Bitrix\Main\Loader::includeModule('main');
\Bitrix\Main\Loader::includeModule('highloadblock');
\Bitrix\Main\Loader::includeModule($module_id);
use \Iis\Exchange\Api;


use Bitrix\Highloadblock as HL;

if (!$USER->IsAdmin())
    return;




$hlblock = HL\HighloadBlockTable::getById(2)->fetch(); 
$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
$entity_data_class = $entity->getDataClass(); 

$rsData = $entity_data_class::getList(array(
    "select" => array("*"),
    "order" => array("ID" => "ASC"),
));
$arBrands = array('-' => 'Выберите бренд');

while($arData = $rsData->Fetch()){
    $arBrands[$arData['UF_XML_ID']] = $arData['UF_NAME'] . ' [' . $arData['ID'] . ']';
}


/* ВКЛАДКИ */
$arTabs = [
    [
        'DIV' => 'export',
        'TAB' => 'Настройки экспорта',
        'ICON' => 'ib_settings',
        'TITLE' => 'Настройки экспорта',
        'OPTIONS' => [
            [
                'export_type', 
                'Тип экспорта',
                'full',
                [
                    'selectbox', 
                    [
                        'full'  => 'Полная выгрузка',
                        'changed'  => 'Выгрузка изменений'
                    ]
                ]
            ],
            ['export_path', 'Путь хранения файлов экспорта', '', ['text', 120]],
            ['export_limit_per_file', 'Количество записей в файле', '', ['text', 10]]
        ]
    ],
    [
        'DIV' => 'import',
        'TAB' => 'Настройки импорта',
        'ICON' => 'ib_settings',
        'TITLE' => 'Настройки импорта',
        'OPTIONS' => [
            [
                'import_type', 
                'Тип импорта',
                'full',
                [
                    'selectbox', 
                    [
                        'full'  => 'Полный импорт',
                        'changed'  => 'Импорт изменений'
                    ]
                ]
            ],
        ]
    ],
    [
        'DIV' => 'brandExport',
        'TAB' => 'Экспорт товаров бренда',
        'ICON' => 'ib_settings',
        'TITLE' => 'Экспорт товаров бренда',
        'OPTIONS' => [
            [
                'export_brand', 
                'Выгружаемый бренд',
                '',
                [
                    'selectbox', 
                    $arBrands
                ]
            ],
            ['export_path_brand', 'Путь хранения файлов экспорта', '/upload/export1C/brands/', ['text', 120]],
            // ['export_limit_per_file_brand', 'Количество записей в файле', '10000', ['text', 10]],
            // ['export_file_num', 'Номер файла (из рассчета 1 файл на 10000 товаров)', '1', ['text', 10]]
        ]
    ],
];
$tabControl = new CAdminTabControl("tabControl", $arTabs);

if ($REQUEST_METHOD == "POST" && $apply && check_bitrix_sessid()) {
    foreach ($arAllOptions as $arOption) {
        $name = $arOption[0];
        if (empty($name)) {
            continue;
        }
        $val = $_REQUEST[$name];
        if ($arOption[3][0] == 'checkbox' && $val != 'Y')
            $val = 'N';
        if ($arOption[3][0] == 'selectboxMulti') {
            $val = json_encode($val);
        }
        if ($arOption[3][0] == 'bxMuliElementLink') {
            $val = array_values(array_filter($val));
            $val = json_encode($val);
        }
        COption::SetOptionString($module_id, $name, $val, $arOption[1]);
    }
    
    if($apply){
        Api::start();
    }
}
//-------------------------------------------------------------------------------


$tabControl->Begin();
?>
<form action="<? echo($APPLICATION->GetCurPage()); ?>?mid=<? echo($module_id); ?>&lang=<? echo(LANGUAGE_ID); ?>" method="post">
    <?
    foreach($arTabs as $arTab)
    {
        if($arTab["OPTIONS"])
        {
            $tabControl->BeginNextTab();
            __AdmSettingsDrawList($module_id, $arTab["OPTIONS"]);
        }
    }
    $tabControl->Buttons();
    ?>
    <input type="submit" name="apply" value="Запустить" class="adm-btn-save" />
    <?
    echo(bitrix_sessid_post());
    ?>
</form>
<?
$tabControl->End();
?>