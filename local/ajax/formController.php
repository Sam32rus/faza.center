<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Mail\Event;
use Bitrix\Main\Loader;

Loader::includeModule("iblock");

if ($_POST['SiteVerify'] == 'Y') {
    /*if(empty($_POST['agreements']) || $_POST['agreements'] !== 'on'){
        echo json_encode(array(
            'status' => 'error',
            'errors'    => ['agreements']
        ));
        exit();
    }*/
    if (!empty($_POST['subject'])) {
        $eventID = "";
        $eventName = "";
        $errors = [];
        switch ($_POST['subject']) {
            case 'Добавить отзыв':
                $required = ['NAME', 'TEXT_REVIEW', 'rating', 'productID'];
                $eventID = 90;
                $eventName = "NEW_REVIEW";
                $subject = $_POST['subject'];
                $name = $_POST['NAME'] ? $_POST['NAME'] : "Гость";
                $message = $_POST['TEXT_REVIEW'] ? $_POST['TEXT_REVIEW'] : "Отзыв не заполнен";
                $advantagers = $_POST['ADVANTAGERS'] ? $_POST['ADVANTAGERS'] : "Отсутствуют";
                $disadvantagers = $_POST['DISADVANTAGERS'] ? $_POST['DISADVANTAGERS'] : "Отсутствуют";
                $rating = $_POST['rating'] ? $_POST['rating'] : "0";
                $productID = $_POST['productID'];
                $productName = $_POST['productName'];

                $variable_bool = false;
                $tmp_err = [];
                foreach ($_POST as $key => $value) {
                    if (in_array($key, $required) && empty($value)) {
                        $errors[] = $key;
                    }
                    if (in_array($key, $variable)) {
                        if (!empty($value))
                            $variable_bool = true;
                        else if (empty($value))
                            $tmp_err[] = $key;
                    }
                }
                if (!$variable_bool) {
                    foreach ($tmp_err as $key => $value) {
                        $errors[] = $value;
                    }
                }
                if ($productID) :
                    $el = new CIBlockElement;

                    $properties = array();
                    $properties['NAME'] = $name;
                    $properties['DATE'] = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time());
                    $properties['TEXT_REVIEW'] = ["VALUE" => ["TEXT" => $message, "TYPE" => "text"]];
                    $properties['ADVANTAGERS'] = ["VALUE" => ["TEXT" => $advantagers, "TYPE" => "text"]];
                    $properties['DISADVANTAGERS'] = ["VALUE" => ["TEXT" => $disadvantagers, "TYPE" => "text"]];
                    $properties['RATING'] = $rating;
                    $properties['PRODUCT']['VALUE'] = $productID;

                    $arLoadProductArray = array(
                        "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
                        "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
                        "IBLOCK_ID"      => REVIEWS_IBLOCK,
                        "PROPERTY_VALUES" => $properties,
                        "NAME"           => "Отзыв от " . date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time()),
                        "ACTIVE"         => "N",            // активен
                    );

                    if (!$el->Add($arLoadProductArray))
                        $errors[] = 'Отзыв не был добавлен';

                endif;

                $result = 'review';
                break;
            case 'Уточнените цену':
                $required = ['NAME', 'PHONE'];
                $variable = [];
                $eventID = 89;
                $eventName = "SPECIFY_FORM";
                $subject = $_POST['subject'];
                $name = $_POST['NAME'] ? $_POST['NAME'] : "Не заполнено";
                $phone = $_POST['PHONE'] ? $_POST['PHONE'] : "Не заполнено";
                $message = $_POST['MESSAGE'] ? $_POST['MESSAGE'] : "Не заполнено";
                $productName = $_POST['productName'] ? $_POST['productName'] : "Не заполнено";
                $productID = $_POST['productID'] ? $_POST['productID'] : "Не заполнено";
                $variable_bool = false;
                foreach ($_POST as $key => $value) {
                    if (in_array($key, $required) && empty($value)) {
                        $errors[] = $key;
                    }
                    if (in_array($key, $variable)) {
                        if (!empty($value))
                            $variable_bool = true;
                        else if (empty($value))
                            $tmp_err[] = $key;
                    }
                }
                if (!$variable_bool) {
                    foreach ($tmp_err as $key => $value) {
                        $errors[] = $value;
                    }
                }
                $result = 'default';
                break;
            case 'Обратная связь':
                $required = ['NAME'];
                $variable = ['PHONE', 'EMAIL'];
                $eventID = 88;
                $eventName = "FEEDBACK_FORM";

                $subject = $_POST['subject'];
                $name = $_POST['NAME'] ? $_POST['NAME'] : "Не заполнено";
                $phone = $_POST['PHONE'] ? $_POST['PHONE'] : "Не заполнено";
                $email = $_POST['EMAIL'] ? $_POST['EMAIL'] : "Не заполнено";
                $message = $_POST['MESSAGE'] ? $_POST['MESSAGE'] : "Не заполнено";

                $variable_bool = false;
                foreach ($_POST as $key => $value) {
                    if (in_array($key, $required) && empty($value)) {
                        $errors[] = $key;
                    }
                    if (in_array($key, $variable)) {
                        if (!empty($value))
                            $variable_bool = true;
                        else if (empty($value))
                            $tmp_err[] = $key;
                    }
                }
                if (!$variable_bool) {
                    foreach ($tmp_err as $key => $value) {
                        $errors[] = $value;
                    }
                }
                $result = 'default';
                break;
        }
        switch ($result) {
            case 'review':
                $files = array();
                foreach ($_FILES as $file) {
                    if (!empty($file['tmp_name'])) {
                        $files[] = CFile::SaveFile($file, "");
                    }
                }
                $arEventFields = array(
                    "SUBJECT"       => "Заполнена форма '" . $subject . "'",
                    "USER_NAME"     => $name,
                    "USER_MESSAGE"  => $message ? $message : "",
                    "PRODUCT_NAME"  => $productName ? $productName : "",
                    "PAGE"          => $_POST["page"],
                );

                $mail = CEvent::SendImmediate($eventName, SITE_ID, $arEventFields, 'Y', '', $files);

                foreach ($files as $file) {
                    CFile::Delete($file); // удаляем файл и.к. на сервере он нам не неужен
                }
                echo json_encode(array(
                    'status' => 'ok',
                    'message' => 'Ваш отзыв скоро появится на сайте!',
                    'formTitle' => "Ваш отзыв принят!",
                    'popupID' => 'add-review'
                ));
                exit;
                break;
            case 'default':
                if (!empty($eventName) && empty($errors)) {
                    $files = array();
                    foreach ($_FILES as $file) {
                        if (!empty($file['tmp_name'])) {
                            $files[] = CFile::SaveFile($file, "");
                        }
                    }
                    $arEventFields = array(
                        "SUBJECT"       => "Заполнена форма '" . $subject . "'",
                        "USER_NAME"     => $name,
                        "USER_PHONE"    => $phone ? $phone : "",
                        "USER_EMAIL"    => $email ? $email : "",
                        "USER_MESSAGE"  => $message ? $message : "",
                        "PRODUCT_NAME"  => $productName ? $productName : "",
                        "PRODUCT_ID"    => $productID ? $productID : "",
                        "PAGE"          => $_POST["page"],
                    );

                    $mail = CEvent::SendImmediate($eventName, SITE_ID, $arEventFields, 'Y', '', $files);

                    foreach ($files as $file) {
                        CFile::Delete($file); // удаляем файл и.к. на сервере он нам не неужен
                    }
                } elseif (!empty($errors)) {
                    echo json_encode(array(
                        'status' => 'error',
                        'errors' => $errors
                    ));
                    exit;
                }

                if ($mail) :
                    echo json_encode(array(
                        'status' => 'ok',
                        'message' => 'Наш менеджер свяжется с Вами<br>в ближайшее время!',
                        'formTitle' => "Соообщение отправлено!"
                    ));
                    exit;
                else :
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Заполнены не все поля!',
                        'formTitle' => "Ошибка"
                    ));
                endif;
                break;
        }
    }
}
