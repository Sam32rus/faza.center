<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//there must be at least one newsletter category
global $APPLICATION, $USER;
use Bitrix\Main\Loader;
Loader::includeModule("subscribe");

$errors = [];
$required = ['email'];
foreach ($_POST as $key => $value) {
    if (in_array($key, $required) && empty($value)) {
        $errors[] = $key;
    }
}

if(empty($errors)){
    $arFields = array(
        "USER_ID" => ($USER->IsAuthorized() ? $USER->GetID() : false),
        "FORMAT" => "html",
        "EMAIL" => $_POST['email'],
        "ACTIVE" => "Y",
        "CONFIRMED" => 'Y',
        "RUB_ID" => ['1', '2', '3']
    );
    $subscr = new CSubscription;
    
    //can add without authorization
    $ID = $subscr->Add($arFields);
    if ($ID > 0){
        echo json_encode(array(
            'status' => 'success',
            'popupID' => 'mail-ok',
        ));
    } else {
        echo json_encode(array(
            'status' => 'failure',
            'formTitle' => 'Ошибка подписки',
            'message' => $subscr->LAST_ERROR,
        ));
    }
    
}else{
    $res['status'] = 'error';
    $res['errors'] = $errors;
    echo json_encode($res);
}



