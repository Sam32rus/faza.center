<?
if (!defined('PUBLIC_AJAX_MODE')) {
    define('PUBLIC_AJAX_MODE', true);
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION, $USER;
use Bitrix\Main\Loader;
Loader::includeModule("main");

switch ($_REQUEST['TYPE']) {
    case "SEND_PWD": {
            //Компонент с шаблоном errors выводит только ошибки
            $APPLICATION->IncludeComponent("bitrix:system.auth.forgotpasswd", "", array('API_SEND' => 'Y'));
        }
        break;

    case "REGISTRATION": {
        $errors = [];
        $required = ['name', 'email', 'password', 'confirm_password'];
        foreach ($_POST as $key => $value) {
            if(in_array($key, $required) && empty($value)){
                $errors[] = $key;
            }
        }
        if($_POST['password'] !== $_POST['confirm_password'] || strlen($_POST['password']) < 6){
            $errors[] = 'password';
            $errors[] = 'confirm_password';
        }

        if($_POST['email']){
            $user = \Bitrix\Main\UserTable::getList(array(
                'filter' => array(
                    '=EMAIL' => $_POST['email'],
                ),
                'limit'=>1,
                'select'=>array('ID'),
            ))->fetch();
            if($user){
                $errors[] = 'email_is_used';
            }
        }
        if(empty($errors)){
            $arResult = $USER->Register(
                $_POST['email'],
                $_POST['name'],
                '',
                $_POST['password'],
                $_POST['confirm_password'],
                $_POST['email']
            );
            echo json_encode(array(
                'status' => 'success',
                'popupID' => 'registration-ok',
            ));
        }else{
            $res['status'] = 'error';
            $res['errors'] = $errors;
            echo json_encode($res);
        }
            
        }
        break;

    default: {
        $errors = [];
        $required = ['email', 'password'];
        foreach ($_POST as $key => $value) {
            if(in_array($key, $required) && empty($value)){
                $errors[] = $key;
            }
        }

        if (!is_object($USER)) $USER = new CUser;

        $arAuthResult = $USER->Login($_POST['email'], $_POST['password'], "Y");

        if($arAuthResult['TYPE'] !== 'ERROR'){
            echo json_encode(array(
                'status' => 'success',
                'popupID' => 'authorization-ok',
            ));
        }else{
            $errors[] = 'incorrect';
            $res['status'] = 'error';
            $res['errors'] = $errors;
            echo json_encode($res);
        }
    }
}
