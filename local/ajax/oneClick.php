<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

define('DEFAULT_PAY_SYSTEM_ID', 2); // Наличные
define('DEFAULT_LOCATION_ID', '0000073738'); // Москва

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('sale');
\Bitrix\Main\Loader::includeModule('catalog');

$errors = [];
if($_POST['page'] == '/personal/cart/'){
    $type = 'cart';
}else{
    $type = 'element';
    $required = ['NAME', 'productID'];
    foreach ($_POST as $key => $value) {
        if(in_array($key, $required) && empty($value)){
            $errors[] = $key;
        }
    }
}

if(empty($errors)){
    switch($type){
        case 'element':
            // Получаем данные о товаре по его ID
            $dbItems = CIBlockElement::GetList(
                array(),
                array('ID' => IntVal($_REQUEST["productID"])),
                false,
                false,
                array('ID', 'IBLOCK_ID', 'NAME')
            );
            if ($arItem = $dbItems->GetNext()) {
                $arItem['PRICE'] = CCatalogProduct::GetOptimalPrice($arItem['ID'], 1);
                // Создаем корзину и добавляем туда товар, 1шт
                $basket = \Bitrix\Sale\Basket::create(SITE_ID);
                $basketItem = $basket->createItem("catalog", $arItem['ID']);
                $basketItem->setFields(
                    array(
                        'PRODUCT_ID' => $arItem['ID'],
                        'NAME' => $arItem['NAME'],
                        // "BASE_PRICE" =>$arItem['PRICE']['RESULT_PRICE']["BASE_PRICE"],
                        // 'PRICE' => $arItem['PRICE']['RESULT_PRICE']["DISCOUNT_PRICE"],
                        'CURRENCY' => $arItem['PRICE']['RESULT_PRICE']['CURRENCY'],
                        'QUANTITY' => 1,
                        'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                    )
                );

                // Создаем заказ и привязываем корзину, перерасчет происходит автоматически
                $order = \Bitrix\Sale\Order::create(SITE_ID, ($USER->IsAuthorized()) ? $USER->GetID() : \CSaleUser::GetAnonymousUserID());
                $order->setPersonTypeId(1); // Физ. лицо
                $order->setBasket($basket);

                // Создаём одну отгрузку и устанавливаем способ доставки - "Без доставки" (он служебный)
                $shipmentCollection = $order->getShipmentCollection();
                $shipment = $shipmentCollection->createItem();
                $service = \Bitrix\Sale\Delivery\Services\Manager::getById(\Bitrix\Sale\Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
                $shipment->setFields(array(
                    'DELIVERY_ID' => $service['ID'],
                    'DELIVERY_NAME' => $service['NAME'],
                ));
                $shipmentItemCollection = $shipment->getShipmentItemCollection();
                $arResult['basket'] = $basket;
                foreach ($basket as $item) {
                    $shipmentItem = $shipmentItemCollection->createItem($item);
                    $shipmentItem->setQuantity($item->getQuantity());
                }

                // Создаём оплату
                $paymentCollection = $order->getPaymentCollection();
                $payment = $paymentCollection->createItem();
                $paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById(DEFAULT_PAY_SYSTEM_ID);
                $payment->setFields(array(
                    'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
                    'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
                ));

                // Устанавливаем свойства
                $propertyCollection = $order->getPropertyCollection();
                $nameProp = $propertyCollection->getPayerName();
                $nameProp->setValue(htmlspecialcharsbx($_REQUEST['NAME']));
                $emailProp = $propertyCollection->getUserEmail();
                $emailProp->setValue(htmlspecialcharsbx($_REQUEST['EMAIL']));
                $phoneProp = $propertyCollection->getPhone();
                $phoneProp->setValue(htmlspecialcharsbx($_REQUEST['PHONE']));
                $locProp = $propertyCollection->getDeliveryLocation();
                $locProp->setValue(DEFAULT_LOCATION_ID);

                // Сохраняем
                $order->doFinalAction(true);
                $order->save();
                if ($order->getId()) :
                    echo json_encode(array(
                        'status' => 'ok',
                        'message' => 'Ваш заказ №' . $order->getId() . ' был успешно оформлен!<br>Наш менеджер свяжется с Вами в ближайшее время!',
                        'formTitle' => "Заказ оформлен!"
                    ));
                else :
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Заполнены не все поля!',
                        'formTitle' => "Ошибка"
                    ));
                endif;
            }
            break;
        case 'cart':
            $arID = array();
            $arBasketItems = array();
            $dbBasketItems = CSaleBasket::GetList(
                array(
                            "NAME" => "ASC",
                            "ID" => "ASC"
                        ),
                array(
                            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                            "LID" => SITE_ID,
                            "ORDER_ID" => "NULL"
                        ),
                false,
                false,
                array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS")
            );
            while ($arItems = $dbBasketItems->Fetch())
            {
                if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"])
                {
                    CSaleBasket::UpdatePrice($arItems["ID"],
                                            $arItems["CALLBACK_FUNC"],
                                            $arItems["MODULE"],
                                            $arItems["PRODUCT_ID"],
                                            $arItems["QUANTITY"],
                                            "N",
                                            $arItems["PRODUCT_PROVIDER_CLASS"]
                                            );
                    $arID[] = $arItems["ID"];
                }
            }
            if (!empty($arID))
            {
                $dbBasketItems = CSaleBasket::GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                    ),
                array(
                    "ID" => $arID,
                    "ORDER_ID" => "NULL"
                    ),
                    false,
                    false,
                    array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "WEIGHT", "PRODUCT_PROVIDER_CLASS", "NAME")
                );
                while ($arItems = $dbBasketItems->Fetch())
                {
                    $arBasketItems[] = $arItems;
                }
            }

            
            if (!empty($arBasketItems)) {
                $basket = \Bitrix\Sale\Basket::create(SITE_ID);

                foreach($arBasketItems as $keyItem => $arItem){
                    if($arItem['CAN_BUY'] == 'Y'){
                        $priceItem = CCatalogProduct::GetOptimalPrice($arItem['PRODUCT_ID'], 1);
                        $basketItem = $basket->createItem("catalog", $arItem['PRODUCT_ID']);
                        $basketItem->setFields(
                            array(
                                'PRODUCT_ID' => $arItem['PRODUCT_ID'],
                                'NAME' => $arItem['NAME'],
                                // "BASE_PRICE" =>$arItem['PRICE']['RESULT_PRICE']["BASE_PRICE"],
                                // 'PRICE' => $arItem['PRICE']['RESULT_PRICE']["DISCOUNT_PRICE"],
                                'CURRENCY' => $priceItem['RESULT_PRICE']['CURRENCY'],
                                'QUANTITY' => $arItem['QUANTITY'],
                                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                            )
                        );
                    }
                    
                }

                // Создаем заказ и привязываем корзину, перерасчет происходит автоматически
                $order = \Bitrix\Sale\Order::create(SITE_ID, ($USER->IsAuthorized()) ? $USER->GetID() : \CSaleUser::GetAnonymousUserID());
                $order->setPersonTypeId(1); // Физ. лицо
                $order->setBasket($basket);

                // Создаём одну отгрузку и устанавливаем способ доставки - "Без доставки" (он служебный)
                $shipmentCollection = $order->getShipmentCollection();
                $shipment = $shipmentCollection->createItem();
                $service = \Bitrix\Sale\Delivery\Services\Manager::getById(\Bitrix\Sale\Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
                $shipment->setFields(array(
                    'DELIVERY_ID' => $service['ID'],
                    'DELIVERY_NAME' => $service['NAME'],
                ));
                $shipmentItemCollection = $shipment->getShipmentItemCollection();
                $arResult['basket'] = $basket;
                foreach ($basket as $item) {
                    $shipmentItem = $shipmentItemCollection->createItem($item);
                    $shipmentItem->setQuantity($item->getQuantity());
                }

                // Создаём оплату
                $paymentCollection = $order->getPaymentCollection();
                $payment = $paymentCollection->createItem();
                $paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById(DEFAULT_PAY_SYSTEM_ID);
                $payment->setFields(array(
                    'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
                    'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
                ));

                // Устанавливаем свойства
                $propertyCollection = $order->getPropertyCollection();
                $nameProp = $propertyCollection->getPayerName();
                $nameProp->setValue(htmlspecialcharsbx($_REQUEST['NAME']));
                $emailProp = $propertyCollection->getUserEmail();
                $emailProp->setValue(htmlspecialcharsbx($_REQUEST['EMAIL']));
                $phoneProp = $propertyCollection->getPhone();
                $phoneProp->setValue(htmlspecialcharsbx($_REQUEST['PHONE']));
                $locProp = $propertyCollection->getDeliveryLocation();
                $locProp->setValue(DEFAULT_LOCATION_ID);

                // Сохраняем
                $order->doFinalAction(true);
                $order->save();
                if ($order->getId()) :
                    echo json_encode(array(
                        'status' => 'ok',
                        'message' => 'Ваш заказ №' . $order->getId() . ' был успешно оформлен!<br>Наш менеджер свяжется с Вами в ближайшее время!',
                        'formTitle' => "Заказ оформлен!"
                    ));
                else :
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Заполнены не все поля!',
                        'formTitle' => "Ошибка"
                    ));
                endif;
            }
            break;
    }
    
}else{
    echo json_encode(array(
        'status' => 'error',
        'errors' => $errors
    ));
    exit;
}
