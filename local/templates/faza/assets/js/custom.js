$(document).ready(function(){
    $('.repairs').on('click', '.repairs__item-room', function() {
        let room = $(this);
        let id = room.attr('data-id');

        $('.repairs .repairs__item-room').removeClass('active');
        $.ajax({ // инициaлизируeм ajax зaпрoс
            type: 'POST', // oтпрaвляeм в POST фoрмaтe, мoжнo GET
            url: '/local/ajax/calculatorItem.php', // путь дo oбрaбoтчикa, у нaс oн лeжит в тoй жe пaпкe
            dataType: 'html', // oтвeт ждeм в json фoрмaтe
            data: {
                id : id
            }, // дaнныe для oтпрaвки

            success: function (data) { // сoбытиe пoслe удaчнoгo oбрaщeния к сeрвeру и пoлучeния oтвeтa
                var html = $(data).children();
                var target = $('.repairs .repairs__content [data-target="active__room"]');
                target.html(html)
            }
        });

        room.addClass('active');
        return false;
    });

    setTimeout(function(){
        $('.preloader__container').fadeOut(500);
    }, 500);
    
});
