$(function(){
    //Обработчик формы авторизации
    $('#modal-login').on('submit','form',function(){

        var form = $(this);

        $.ajax({
            type: "POST",
            url: '/local/ajax/auth.php',
            data: form.serialize(),
            dataType: 'json',
            timeout: 3000,
            error: function(request,error) {
                if (error == "timeout") {
                    alert('timeout');
                }
                else {
                    alert('Error! Please try again!');
                }
            },
            success: function(data) {
                if(data.status == 'success'){
                    $.popup('closeAll');
                    $.popup('open', data.popupID); // Открыть
                    window.setTimeout('TSRedirectUser()',2000);
                }else{
                    $.each( data.errors, function( key, value ) {
                        form.find('[data-error="' + value + '"]').show();
                    });
                }
            }
        });

        return false;
    });

    //Обработчик формы регистрации
    $('#modal-register').on('submit','form',function(){
        var form = $(this);
        $.ajax({
            type: "POST",
            url: '/local/ajax/auth.php',
            data: form.serialize(),
            dataType: 'json',
            timeout: 3000,
            error: function(request,error) {
                if (error == "timeout") {
                    alert('timeout');
                }
                else {
                    alert('Error! Please try again!');
                }
            },
            success: function(data) {
                if(data.status == 'success'){
                    $.popup('closeAll');
                    $.popup('open', data.popupID); // Открыть
                    window.setTimeout('TSRedirectUser()',2000);
                }else{
                    $.each( data.errors, function( key, value ) {
                        form.find('[data-error="' + value + '"]').show();
                    });
                }
                
            }
        });

        return false;
    });

    //Обработчик формы восстановления пароля
    $('#modal-forgot-password').on('submit','form',function(){

        var form = $(this);
        $.ajax({
            type: "POST",
            url: '/local/ajax/auth.php',
            data: $(this).serialize(),
            dataType: 'json',
            timeout: 3000,
            error: function(request,error) {
                if (error == "timeout") {
                    alert('timeout');
                }
                else {
                    alert('Error! Please try again!');
                }
            },
            success: function(data) {
                if(data.status == 'success'){
                    $.popup('closeAll');
                    $.popup('open', data.popupID); // Открыть
                    // window.setTimeout('TSRedirectUser()',2000);
                }
            }
        });

        return false;
    });

});

function TSRedirectUser(){
    window.location.reload();
}
