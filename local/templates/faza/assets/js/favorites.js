$(document).ready(function () {
    $('body').on('click', '.favorite__btn', function (e) {
        var favorID = $(this).attr('data-item');
        if ($(this).hasClass('active'))
            var doAction = 'delete';
        else
            var doAction = 'add';

        addFavorite(favorID, doAction);
    });
    $('body').on('click', '[data-target="favoriteRemove"]', function (e) {
        var favorID = $(this).attr('data-item');
        addFavorite(favorID, 'delete');
        $(this).parents('[data-entity="item"]').hide();
    });
});
/* Избранное */
function addFavorite(id, action) {
    var param = 'id=' + id + "&action=" + action;
    $.ajax({
        url: '/local/ajax/favorites.php', // URL отправки запроса
        type: "GET",
        dataType: "html",
        data: param,
        success: function (response) { // Если Данные отправлены успешно
            var result = $.parseJSON(response);

            if (result == 1) { // Если всё чётко, то выполняем действия, которые показывают, что данные отправлены
                $('.favorite__btn[data-item="' + id + '"]').addClass('active');
                var wishCount = parseInt($('[data-id="want"] .user-menu__heart-sum').html()) + 1;
                $('[data-id="want"] .user-menu__heart-sum').html(wishCount); // Визуально меняем количество у иконки
            }
            if (result == 2) {
                $('.favorite__btn[data-item="' + id + '"]').removeClass('active');
                var wishCount = parseInt($('[data-id="want"] .user-menu__heart-sum').html()) - 1;
                $('[data-id="want"] .user-menu__heart-sum').html(wishCount); // Визуально меняем количество у иконки
            }
        },
        error: function (jqXHR, textStatus, errorThrown) { // Ошибка
            console.log('Error: ' + errorThrown);
        }
    });
}
