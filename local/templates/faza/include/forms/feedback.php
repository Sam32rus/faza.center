<div class="popup" data-popup-id="feedback">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="feedback"></div>
        <div class="popup__content">
            <div class="popup__title">Обратная связь</div>
            <form method="POST" id="new-review" data-block="formController">
                <input type="hidden" name="SiteVerify" value="N">
                <input type="hidden" name="subject" value="Обратная связь">
                <input type="hidden" name="page" value="<?= $APPLICATION->GetCurPage(false); ?>">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__default" placeholder="Ваше имя" name="NAME" type="text" required="">
                    </div>
                    <div class="form-data__error" data-error="NAME">Заполните поле "Ваше имя"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__mail" placeholder="E-Mail" type="text" name="EMAIL">
                    </div>
                    <div class="form-data__error" data-error="EMAIL">Заполните поле "E-Mail"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__phone" placeholder="+7 ___ ___ __ __" type="text" maxlength="16" autocomplete="off" name="PHONE">
                    </div>
                    <div class="form-data__error" data-error="PHONE">Заполните поле "Телефон"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__default" rows="7" placeholder="Сообщение" name="MESSAGE"></textarea>
                    </div>
                    <div class="form-data__error" data-error="MESSAGE">Заполните поле "Сообщение"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <button class="js__disabled-btn form-data__submit blue-button" type="submit">Отправить</button>
                    </div>
                </div>

                <div class="popup__gray-text">
                    Нажимая кнопку вы соглашаетесь с условиями пользовательского&nbsp;соглашения
                </div>
            </form>

        </div>
    </div>
</div>