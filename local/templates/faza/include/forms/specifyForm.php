<div class="popup" data-popup-id="specify">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="specify"></div>
        <div class="popup__content">
            <div class="popup__title">Уточнените цену</div>
            <div class="popup__text">Укажите свои данные, чтобы наш менеджер связался с Вами для решения данного вопроса.</div>
            <form id="specifyForm" data-block="formController">
                <input type="hidden" name="SiteVerify" value="N">
                <input type="hidden" name="subject" value="Уточнените цену">
                <input type="hidden" name="page" value="<?= $APPLICATION->GetCurPage(false); ?>">
                <input type="hidden" name="productID" value="">
                <input type="hidden" name="productName" value="">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__default" placeholder="Ваше имя" type="text" name="NAME" required="">
                    </div>
                    <div class="form-data__error" data-error="NAME">Заполните поле "Ваше имя"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__phone" placeholder="Телефон" type="phone" name="PHONE" required="">
                    </div>
                    <div class="form-data__error" data-error="PHONE">Заполните поле "Телефон"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__default" rows="7" placeholder="Примечание" name="MESSAGE"></textarea>
                    </div>
                    <div class="form-data__error" data-error="MESSAGE">Заполните поле "Примечание"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <button class="js__disabled-btn form-data__submit blue-button" type="submit">Уточнить</button>
                    </div>
                </div>

                <div class="popup__gray-text">
                    Нажимая кнопку вы соглашаетесь с условиями пользовательского&nbsp;соглашения
                </div>
            </form>

        </div>
    </div>
</div>