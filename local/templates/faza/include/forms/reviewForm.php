<div class="popup" data-popup-id="add-review">
	<div class="popup__inner w-800">
		<div class="popup__close" data-popup-close="add-review"></div>
		<div class="popup__content">
			<div class="popup__title">Ваш отзыв</div>
			<div class="popup__text">
				<form method="POST" id="new-review" data-block="formController">
					<input type="hidden" name="SiteVerify" value="N">
					<input type="hidden" name="subject" value="Добавить отзыв">
					<input type="hidden" name="page" value="<?= $APPLICATION->GetCurPage(false); ?>">
					<input type="hidden" name="productID" value="<?= $arResult['ID']; ?>">
					<input type="hidden" name="productName" value="<?= $arResult['NAME']; ?>">

					<div class="form-data">
						<div class="form-data__wrap">
							<input class="form-data__input js__default" placeholder="Ваше имя" type="text" name="NAME" required="">
						</div>
						<div class="form-data__error">Заполните поле "Ваше имя"</div>
					</div>

					<div class="form-data">
						<div class="form-data__wrap">
							<textarea class="form-data__textarea js__default" rows="7" placeholder="Отзыв" name="TEXT_REVIEW" required=""></textarea>
						</div>
						<div class="form-data__error">Заполните поле "Отзыв"</div>
					</div>

					<div class="popup__cols-2">

						<div class="form-data popup__col-1">
							<div class="form-data__wrap">
								<textarea class="form-data__textarea js__default" rows="4" placeholder="Дстоинства" name="ADVANTAGERS"></textarea>
							</div>
							<div class="form-data__error">Заполните поле "Дстоинства"</div>
						</div>

						<div class="form-data popup__col-1">
							<div class="form-data__wrap">
								<textarea class="form-data__textarea js__default" rows="4" placeholder="Недостатки" name="DISADVANTAGERS"></textarea>
							</div>
							<div class="form-data__error">Заполните поле "Недостатки"</div>
						</div>
					</div>

					<div class="form-data">
						<div class="form-data__stars">
							<div class="form-data__stars-title">Ваша оценка</div>
							<div class="rating-stars">
								<div class="rating-stars__inner">
									<input type="radio" name="rating" value="5" id="5"><label for="5"></label>
									<input type="radio" name="rating" value="4" id="4"><label for="4"></label>
									<input type="radio" name="rating" value="3" id="3"><label for="3"></label>
									<input type="radio" name="rating" value="2" id="2"><label for="2"></label>
									<input type="radio" name="rating" value="1" id="1"><label for="1"></label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-data__wrap-submit">
						<button class="js__disabled-btn form-data__submit blue-button" type="submit">Опубликовать отзыв</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>