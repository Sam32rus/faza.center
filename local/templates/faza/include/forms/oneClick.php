<!--Попап "Купить в один клик" -->
<div class="popup" data-popup-id="quick-request">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="quick-request"></div>
        <div class="popup__content">
            <div class="popup__title">Купить в 1 клик</div>
               <form class="js__form-address" id="oneClickForm" data-block="oneClick">
                <input type="hidden" name="SiteVerify" value="N">
                <input type="hidden" name="subject" value="Купить в 1 клик">
                <input type="hidden" name="page" value="<?= $APPLICATION->GetCurPage(false); ?>">
                <input type="hidden" name="productID" value="">

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__name" placeholder="Имя" type="text" name="NAME" required="">
                    </div>
                    <div class="form-data__error" data-error="NAME">Заполните поле "Имя"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__phone" placeholder="Телефон" type="text" name="PHONE" required="">
                    </div>
                    <div class="form-data__error" data-error="PHONE">Заполните поле "Телефон"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__mail" placeholder="E-Mail" type="text" name="EMAIL" required="">
                    </div>
                    <div class="form-data__error" data-error="EMAIL">Заполните поле "E-Mail"</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <button class="js__disabled-btn form-data__submit blue-button" type="submit">Купить</button>
                    </div>
                </div>

                <div class="popup__gray-text">
                    Нажимая кнопку вы соглашаетесь с условиями пользовательского&nbsp;соглашения
                </div>

            </form>

        </div>
    </div>
</div>
<!--/Попап "Купить в один клик" -->