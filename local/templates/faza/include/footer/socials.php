<div class="footer__social-list">
    <div class="footer__title footer__title_no-line">Мы в соц. сетях</div>
    <a href="#" target="_blank" class="footer__social-item footer__social-item_fb"></a>
    <a href="#" target="_blank" class="footer__social-item footer__social-item_in"></a>
    <a href="#" target="_blank" class="footer__social-item footer__social-item_yt"></a>
    <a href="#" target="_blank" class="footer__social-item footer__social-item_vk"></a>
</div>
