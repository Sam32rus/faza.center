<?
$arResult['SECTIONS'] = [];

$rsSections = \Bitrix\Iblock\SectionTable::getList([
    'order' => ['LEFT_MARGIN' => 'ASC'],
    'filter' => [
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => 7,
        '<=DEPTH_LEVEL' => 2,
    ],
    'select' => ['ID', 'IBLOCK_ID', 'NAME', 'CODE', 'IBLOCK_SECTION_ID', 'DEPTH_LEVEL', 'SECTION_PAGE_URL' => 'IBLOCK.SECTION_PAGE_URL'],
    'cache' => ['ttl' => 3600]
]);

while ($arSection = $rsSections->fetch()) {
    switch ($arSection['DEPTH_LEVEL']){
        case '1':
            $arSection['SECTION_PAGE_URL'] = CIBlock::ReplaceDetailUrl($arSection['SECTION_PAGE_URL'], $arSection, false, 'S');
            break;
        case '2':
            $arSection['SECTION_PAGE_URL'] = $arResult['SECTIONS'][$arSection['IBLOCK_SECTION_ID']]['SECTION_PAGE_URL'] . $arSection['CODE'] . '/';
            break;
    }

    $arResult['SECTIONS'][$arSection['ID']] = $arSection;
}


switch ($arParams['TYPE']) {
    case 'mobile':
        ?>
        <div class="mobile-menu__close js__close-mobile-menu"></div>
            <div class="mobile-menu__wrap-list js__wrap-mobile-menu">
                <ul class="mobile-menu__main-list js__mobile-menu-main-list active">
                    <?foreach ($arResult['SECTIONS'] as $key => $arSection) :
                        switch ($arSection['DEPTH_LEVEL']) {
                            case '1':
                                echo '<li class="mobile-menu__item">' . $arSection['NAME'] . '</li>';
                                break;
                            case '2':
                                continue;
                                break;
                        }
                    endforeach;?>
                </ul>
                <?foreach ($arResult['SECTIONS'] as $key => $arSection) :
                    switch ($arSection['DEPTH_LEVEL']) {
                        case '1':
                            if($arSection !== current($arResult['SECTIONS']))
                                echo '</ul>';

                            echo '<ul class="mobile-menu__second-list js__mobile-menu-second-list">
                            <li class="mobile-menu__item mobile-menu__item_back js__mobile-menu-back">' . $arSection['NAME'] . '</li>';
                            continue;
                            break;
                        case '2':
                            echo '<li class="mobile-menu__item"><a href="' . $arSection['SECTION_PAGE_URL'] . '">' . $arSection['NAME'] . '</a></li>';
                            break;
                    }
                    if($arSection == end($arResult['SECTIONS']))
                        echo '</ul>';
                endforeach;?>
            </div>
        <?
        break;

    default://pc?>
        <menu class="menu js__menu">
            <div class="menu__inner js__menu-inner">
                <div class="menu__tabs js__tabs">
                    <ul class="menu__list-tab js__tabs-caption">
                        <?foreach ($arResult['SECTIONS'] as $key => $arSection) :
                            switch ($arSection['DEPTH_LEVEL']) {
                                case '1':
                                    echo '<li class="menu__item-tab"><a class="menu__link-tab" href="' . $arSection['SECTION_PAGE_URL'] . '">' . $arSection['NAME'] . '</a></li>';
                                    break;
                                case '2':
                                    continue;
                                    break;
                            }
                        endforeach;?>
                    </ul>
                    <?foreach ($arResult['SECTIONS'] as $key => $arSection) :
                        switch ($arSection['DEPTH_LEVEL']) {
                            case '1':
                                if($arSection !== current($arResult['SECTIONS']))
                                    echo '</div></ul>';

                                echo '<div class="tabs__content menu__tabs-content"><ul class="menu__list-content">';
                                continue;
                                break;
                            case '2':
                                echo '<li class="menu__item-tab"><a class="menu__link-tab" href="' . $arSection['SECTION_PAGE_URL'] . '">' . $arSection['NAME'] . '</a></li>';
                                break;
                        }
                        if($arSection == end($arResult['SECTIONS']))
                            echo '</div></ul>';
                    endforeach;?>
                </div>
                <!--BRANDS-->
                <?$APPLICATION->IncludeComponent(
                    "iis:highloadblock.list",
                    "brands__list-menu",
                    Array(
                        "BLOCK_ID" => "2",
                        "CHECK_PERMISSIONS" => "N",
                        "DETAIL_URL" => "/catalog/brands/#CODE#/",
                        "FILTER_NAME" => "",
                        "PAGEN_ID" => "page",
                        "ROWS_PER_PAGE" => "",
                        "SORT_FIELD" => "ID",
                        "SORT_ORDER" => "ASC"
                    )
                );?>
                <!--/BRANDS-->

            </div>
        </menu>

    <?break;
}

?>
