<div class="user-menu__wrap-point">
    <a class="user-menu__item-point user-menu__item-point_compare" href="<?= COMPARE_URL ?>" class="user-menu__item-point user-menu__item-point_compare">
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.compare.list",
            "header__compare",
            array(
                "IBLOCK_TYPE" => 'catalog',
                "IBLOCK_ID" => '7',
                "NAME" => 'CATALOG_COMPARE_LIST',
                "DETAIL_URL" => '/product/#ELEMENT_CODE#/',
                "COMPARE_URL" => '/catalog/compare/',
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => 'id',
                'POSITION_FIXED' => 'N',
                'POSITION' => 'top left'
            ),
            $component,
            array("HIDE_ICONS" => "Y")
        );
        ?>
        Сравнить
    </a>
</div>

<div data-id="want" class="user-menu__wrap-point">
    <a href="<?= FAVORITE_URL ?>" class="user-menu__item-point user-menu__item-point_heart">
        <? if (!$USER->IsAuthorized()) :
            $arElements = unserialize($APPLICATION->get_cookie('favorites'));
            if (is_array($arElements)) : ?>
                <div class="user-menu__heart-sum col"><? echo count($arElements); ?></div>
            <? else : ?>
                <div class="user-menu__heart-sum col">0</div>
            <? endif;
        else : ?>
            <div class="user-menu__heart-sum col">
                <?
                $idUser = $USER->GetID();
                $rsUser = CUser::GetByID($idUser);
                $arUser = $rsUser->Fetch();
                $arElements = $arUser['UF_FAVORITES'];
                ?>

                <?= count($arElements); ?>
            </div>
        <? endif; ?>
        Избранное
    </a>
</div>

<div class="user-menu__wrap-point">
    <? if ($USER->IsAuthorized()) :
        $name = trim($USER->GetFullName());
        if (!$name)
            $name = trim($USER->GetLogin());
        if (mb_strlen($name) > 15)
            $name = mb_substr($name, 0, 12) . '...';
    ?>
        <a href="<?= PERSONAL_URL ?>" class="user-menu__item-point user-menu__item-point_user"><?= $name; ?></a>
        <!--data-popup-btn="login"-->
    <? else : ?>
        <div class="user-menu__item-point user-menu__item-point_user" data-popup-btn="login">Вход</div>
    <? endif; ?>
</div>