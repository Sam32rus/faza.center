<a href="<?=CATALOG_URL?>" class="user-menu__item-point user-menu__item-point_catalog">Каталог</a>
<a href="<?=COMPARE_URL?>" class="user-menu__item-point user-menu__item-point_compare">Сравнить</a>
<a href="<?=FAVORITE_URL?>" class="user-menu__item-point user-menu__item-point_heart" data-id="want">
    <?if(!$USER->IsAuthorized()):
        $arElements = unserialize($APPLICATION->get_cookie('favorites'));
        if(is_array($arElements)):?>
            <span class="user-menu__heart-sum"><?echo count($arElements);?></span>
        <?else:?>
            <span class="user-menu__heart-sum">0</span>
        <?endif;
    else:?>
        <span class="user-menu__heart-sum">
            <?
            $idUser = $USER->GetID();
            $rsUser = CUser::GetByID($idUser);
            $arUser = $rsUser->Fetch();
            $arElements = $arUser['UF_FAVORITES'];
            ?>

            <?=count($arElements);?>
        </span>
    <?endif;?>
    Избранное
</a>
<?if ($USER->IsAuthorized()):
    $name = trim($USER->GetFullName());
    if (!$name)
        $name = trim($USER->GetLogin());
    if (mb_strlen($name) > 15)
        $name = mb_substr($name, 0, 12).'...';
    ?>
    <a href="<?=PERSONAL_URL?>" class="user-menu__item-point user-menu__item-point_user"><?=$name;?></a>
<?else:?>
    <a href="javascript:void(0);" class="user-menu__item-point user-menu__item-point_user" data-popup-btn="login">Вход</a>
<?endif;?>


<?$APPLICATION->IncludeComponent(
    "bitrix:sale.basket.basket.line",
    "header__mobile_cart",
    array(
        "HIDE_ON_BASKET_PAGES" => "N",
        "PATH_TO_AUTHORIZE" => SITE_DIR."auth/",
        "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
        "PATH_TO_ORDER" => SITE_DIR."personal/order/",
        "PATH_TO_PERSONAL" => SITE_DIR."personal/",
        "PATH_TO_PROFILE" => SITE_DIR."personal/",
        "PATH_TO_REGISTER" => SITE_DIR."login/",
        "POSITION_FIXED" => "N",
        "SHOW_AUTHOR" => "Y",
        "SHOW_EMPTY_VALUES" => "Y",
        "SHOW_NUM_PRODUCTS" => "Y",
        "SHOW_PERSONAL_LINK" => "N",
        "SHOW_PRODUCTS" => "N",
        "SHOW_REGISTRATION" => "N",
        "SHOW_TOTAL_PRICE" => "N",
        "COMPONENT_TEMPLATE" => "header__cart"
    ),
    false
);?>
