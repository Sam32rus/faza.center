			<? if ($APPLICATION->GetCurPage(false) !== '/' || $APPLICATION->GetCurPage(false) !== '/catalog/' || $APPLICATION->GetCurPage(false) !== '/personal/'){?>
						</div>
					</section>
				</main>
			<?}else{?>
				</main>
			<?}?>


			<footer class="footer">
				<div class="content">
					<div class="footer__inner">
						<div class="footer__coll">
							<div class="footer__title">О компании</div>
							<?$APPLICATION->IncludeComponent("bitrix:menu", "treeBotom", 
								Array(
									"ROOT_MENU_TYPE" => "",	// Тип меню для первого уровня
									"MENU_CACHE_TYPE" => "A",	// Тип кеширования
									"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
									"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
									"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
									"CACHE_SELECTED_ITEMS" => "N",
									"CHILD_MENU_TYPE" => "botoom",	// Тип меню для остальных уровней
									"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
									"DELAY" => "N",	// Откладывать выполнение шаблона меню
									"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
									"COMPONENT_TEMPLATE" => "tree",
									"MAX_LEVEL" => "1",	// Уровень вложенности меню
								),
								false
							);?>         
						</div>
						<div class="footer__coll">
							<div class="footer__title">Помощь</div>
							<?$APPLICATION->IncludeComponent(
								"bitrix:menu", 
								"treeBotom", 
								array(
									"ROOT_MENU_TYPE" => "botoom2",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_TIME" => "3600000",
									"MENU_CACHE_USE_GROUPS" => "N",
									"MENU_CACHE_GET_VARS" => array(
									),
									"CACHE_SELECTED_ITEMS" => "N",
									"CHILD_MENU_TYPE" => "botoom2",
									"USE_EXT" => "Y",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N",
									"COMPONENT_TEMPLATE" => "tree",
									"MAX_LEVEL" => "1"
								),
								false
							);?>
						</div>

						<div class="footer__coll">
							<div class="footer__title">Информация</div>
							<?$APPLICATION->IncludeComponent(
								"bitrix:menu", 
								"treeBotom", 
								array(
									"ROOT_MENU_TYPE" => "botoom3",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_TIME" => "3600000",
									"MENU_CACHE_USE_GROUPS" => "N",
									"MENU_CACHE_GET_VARS" => array(
									),
									"CACHE_SELECTED_ITEMS" => "N",
									"CHILD_MENU_TYPE" => "botoom3",
									"USE_EXT" => "Y",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N",
									"COMPONENT_TEMPLATE" => "tree",
									"MAX_LEVEL" => "1"
								),
								false
							);?>
						</div>
						<div class="footer__coll">
							<div class="footer__title">Кабинет</div>
							<?$APPLICATION->IncludeComponent(
								"bitrix:menu", 
								"treeBotom", 
								array(
									"ROOT_MENU_TYPE" => "botoom4",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_TIME" => "3600000",
									"MENU_CACHE_USE_GROUPS" => "N",
									"MENU_CACHE_GET_VARS" => array(
									),
									"CACHE_SELECTED_ITEMS" => "N",
									"CHILD_MENU_TYPE" => "botoom4",
									"USE_EXT" => "Y",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N",
									"COMPONENT_TEMPLATE" => "tree",
									"MAX_LEVEL" => "1"
								),
								false
							);?>
						</div>
						<div class="footer__coll footer__coll_contacts">
							<div class="footer__social-list">
								<div class="footer__title footer__title_no-line">Мы в соц. сетях</div>
								<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
									array(
										"COMPONENT_TEMPLATE" => ".default",
										"PATH" => SITE_DIR."include/boot_social.php",
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "",
										"AREA_FILE_RECURSIVE" => "Y",
										"EDIT_TEMPLATE" => "standard.php"
									),
									false
								);?>
							</div>
							<div class="footer__wrap-phones">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
									array(
										"COMPONENT_TEMPLATE" => ".default",
										"PATH" => SITE_DIR."include/boot_phone.php",
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "",
										"AREA_FILE_RECURSIVE" => "Y",
										"EDIT_TEMPLATE" => "standard.php"
									),
									false
								);?>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<?
		use Bitrix\Main\Page\Asset;
		Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.min.js");
		Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.plugin.js");
		Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.modulecreator.min.js");
		Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/vendor.js");
		Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/app.js");
		?>
		<script type="text/javascript">
			$(document).ready(function() {
				/* Favorites */
				$('.rating__heart').on('click', function(e) {
					var favorID = $(this).attr('data-item');
					if($(this).hasClass('active'))
						var doAction = 'delete';
					else
						var doAction = 'add';

					addFavorite(favorID, doAction);
				});
				$('.rating__info_favorites').on('click', function(e) {
					var favorID = $(this).attr('data-item');
					if($(this).hasClass('active'))
						var doAction = 'delete';
					else
						var doAction = 'add';

					addFavorite(favorID, doAction); 
				});

				/* Favorites */
			});
			/* Избранное */
			function addFavorite(id, action)
			{
				var param = 'id='+id+"&action="+action;
				$.ajax({
		            url:     '/include/ajax/favorite.php', // URL отправки запроса
		            type:     "GET",
		            dataType: "html",
		            data: param,
		            success: function(response) { // Если Данные отправлены успешно
		            	var result = $.parseJSON(response);
		            	console.log(result);

		                if(result == 1){ // Если всё чётко, то выполняем действия, которые показывают, что данные отправлены
		                	$('.favor[data-item="'+id+'"]').addClass('active');
		                	var wishCount = parseInt($('#want .col').html()) + 1;
		                     $('#want .col').html(wishCount); // Визуально меняем количество у иконки
		                 }
		                 if(result == 2){
		                 	$('.favor[data-item="'+id+'"]').removeClass('active');
		                 	var wishCount = parseInt($('#want .col').html()) - 1;
		                     $('#want .col').html(wishCount); // Визуально меняем количество у иконки
		                 }
		             },
		            error: function(jqXHR, textStatus, errorThrown){ // Ошибка
		            	console.log('Error: '+ errorThrown);
		            }
		        });
			}
			/* Избранное */
		</script>
	</body>
</html>