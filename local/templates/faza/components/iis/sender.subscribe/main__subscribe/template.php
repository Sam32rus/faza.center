<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
$buttonId = $this->randString();
?>
<?if($arResult['STATUS'] == 'success'):?>
	<script>
		$.popup('closeAll');
		$.popup('open', 'mail-ok'); // Открыть
	</script>
<?else:?>
<div class="subscribe blogs__subscribe" id="sender-subscribe">
	<?$frame = $this->createFrame("sender-subscribe", false)->begin();?>
	<div class="subscribe__inner">
		<div class="subscribe__title">Подписаться на&nbsp;рассылку</div>
		<form id="bx_subscribe_subform_<?=$buttonId?>" role="form" method="post" action="<?=$arResult["FORM_ACTION"]?>">
			<?=bitrix_sessid_post()?>
			<input type="hidden" name="sender_subscription" value="add">

			<div class="form-data">
				<div class="form-data__title"><?=Loc::getMessage("subscr_form_email_title")?></div>
				<div class="form-data__wrap">
					<input class="form-data__input js__email" type="email" name="SENDER_SUBSCRIBE_EMAIL" value="<?=$arResult["EMAIL"]?>">
				</div>
				<div class="form-data__error">Заполните поле "<?=Loc::getMessage("subscr_form_email_title")?>"</div>
			</div>
			<div class="form-data">
				<div class="form-data__wrap-submit">
					<button class="js__disabled-btn form-data__submit blue-button" id="bx_subscribe_btn_<?=$buttonId?>" type="submit"><?=GetMessage("subscr_form_button")?></button>
				</div>
			</div>
		</form>
	</div>

	<?$frame->beginStub();?>

	<div class="subscribe__inner">
		<div class="subscribe__title">Подписаться на&nbsp;рассылку</div>
		<form id="bx_subscribe_subform_<?=$buttonId?>" role="form" method="post" action="<?=$arResult["FORM_ACTION"]?>">
			<?=bitrix_sessid_post()?>
			<input type="hidden" name="sender_subscription" value="add">

			<div class="form-data">
				<div class="form-data__title"><?=Loc::getMessage("subscr_form_email_title")?></div>
				<div class="form-data__wrap">
					<input class="form-data__input js__email" type="email" name="SENDER_SUBSCRIBE_EMAIL" value="<?=$arResult["EMAIL"]?>">
				</div>
				<div class="form-data__error">Заполните поле "<?=Loc::getMessage("subscr_form_email_title")?>"</div>
			</div>
			<div class="form-data">
				<div class="form-data__wrap-submit">
					<button class="js__disabled-btn form-data__submit blue-button" id="bx_subscribe_btn_<?=$buttonId?>" type="submit"><?=GetMessage("subscr_form_button")?></button>
				</div>
			</div>
		</form>
	</div>

	<?$frame->end();?>
</div>
<?endif;?>