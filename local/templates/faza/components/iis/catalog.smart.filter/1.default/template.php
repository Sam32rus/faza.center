<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
use Bitrix\Main\Localization\Loc;
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx-'.$arParams['TEMPLATE_THEME']
);

if (isset($templateData['TEMPLATE_THEME'])){
	$this->addExternalCss($templateData['TEMPLATE_THEME']);
}
?>
<div class="catalog__sorting js__mobile-filter" id="filter">
    <form class="catalog__form" name="<?= $arResult["FILTER_NAME"]."_form"?>" action="<?= $arResult["FORM_ACTION"]?>" method="get">
        <div class="catalog__mobile-head">
            <div class="catalog__mobile-head-title">Фильтр</div>
            <button class="transparent-button js__mobile-filter-close">Отмена</button>
        </div>

        <div class="catalog__extra-btn hide-tablet"><button class="clear-button">Очистить фильтры</button></div>

        <div class="form-data">
            <input id="price-card" class="form-data__checkbox js__checkbox" type="checkbox" name="">
            <label for="price-card" class="form-data__label">Цена по карте</label>
        </div>

        <?foreach($arResult["HIDDEN"] as $arItem):?>
            <input type="hidden" name="<?= $arItem["CONTROL_NAME"]?>" id="<?= $arItem["CONTROL_ID"]?>" value="<?= $arItem["HTML_VALUE"]?>" />
        <?endforeach;?>

        <?
        //prices
        foreach($arResult["ITEMS"] as $key => $arItem)
        {

            $key = $arItem["ENCODED_ID"];
            if(isset($arItem["PRICE"])):
                if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                    continue;

                $step_num = 4;
                $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
                $prices = array();
                if (Bitrix\Main\Loader::includeModule("currency"))
                {
                    for ($i = 0; $i < $step_num; $i++)
                    {
                        $prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
                    }
                    $prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
                }
                else
                {
                    $precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
                    for ($i = 0; $i < $step_num; $i++)
                    {
                        $prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $precision, ".", "");
                    }
                    $prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                }
                ?>
                <!-- filter-item -->
                <div class="deploy catalog__item-sort filter-item bx-filter-parameters-box bx-active" data-role="bx_filter_block">
                    <div class="catalog__sort-title js__deploy-toggle open">Цена</div>

                    <span class="bx-filter-container-modef"></span>

                    <div class="deploy__content active catalog__sort-content">
                        <div class="form-data">
                            <div class="form-data__range">
                                <div class="form-data__range-inputs">
                                    <div class="form-data form-data_range-from">
                                        <div class="form-data__wrap form-data__wrap_small form-data__wrap_price">
                                            <div class="form-data__price-pl">от</div>
                                            <input class="form-data__input js__range-price-min"
                                                    type="text"
                                                    data-index="0"
                                                    value="<?=$arItem['VALUES']['MIN']['HTML_VALUE']?>"
                                                    name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                                    id="<?= $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                                    placeholder="<?=$arItem['VALUES']['MIN']['VALUE']?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="form-data__dash"></div>
                                    <div class="form-data form-data_range-to">
                                        <div class="form-data__wrap form-data__wrap_small form-data__wrap_price">
                                            <div class="form-data__price-pl">до</div>
                                            <input  data-index="1"
                                                    value="<?=$arItem['VALUES']['MAX']['HTML_VALUE']?>"
                                                    class="form-data__input js__range-price-max"
                                                    name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                                    id="<?= $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                                    placeholder="<?=$arItem['VALUES']['MAX']['VALUE']?>"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <input class="js__range-price" type="text" name="" value="" />
                            </div>
                            <pre class="demo__output js-output__d1"></pre>
                        </div>
                    </div>
                </div>


                <div style="display: none" class="col-xs-10 col-xs-offset-1 bx-ui-slider-track-container">
                    <div class="bx-ui-slider-track" id="drag_track_<?=$key?>">
                        <?for($i = 0; $i <= $step_num; $i++):?>
                            <div class="bx-ui-slider-part p<?=$i+1?>"><span><?=$prices[$i]?></span></div>
                        <?endfor;?>

                        <div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
                        <div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
                        <div class="bx-ui-slider-pricebar-v"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
                        <div class="bx-ui-slider-range" id="drag_tracker_<?=$key?>"  style="left: 0%; right: 0%;">
                            <a class="bx-ui-slider-handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
                            <a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
                        </div>
                    </div>
                </div>
                <? $arJsParams = array(
                    "leftSlider" => 'left_slider_'.$key,
                    "rightSlider" => 'right_slider_'.$key,
                    "tracker" => "drag_tracker_".$key,
                    "trackerWrap" => "drag_track_".$key,
                    "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                    "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                    "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
                    "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
                    "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                    "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                    "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
                    "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                    "precision" => $precision,
                    "colorUnavailableActive" => 'colorUnavailableActive_'.$key,
                    "colorAvailableActive" => 'colorAvailableActive_'.$key,
                    "colorAvailableInactive" => 'colorAvailableInactive_'.$key,
                ); ?>
                <script type="text/javascript">
                    BX.ready(function(){
                        window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                    });
                </script>
                <!-- / filter-item -->
            <?endif;
        }

        //not prices
        foreach($arResult["ITEMS"] as $key=>$arItem)
        {
            if(
                empty($arItem["VALUES"])
                || isset($arItem["PRICE"])
            )
                continue;

            if (
                $arItem["DISPLAY_TYPE"] == "A"
                && (
                    $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                )
            )
                continue;
            ?>
            <div class="deploy catalog__item-sort">
                <div class="catalog__sort-title js__deploy-toggle <?= $arItem["DISPLAY_EXPANDED"]== "Y" ? '' : 'open' ;?>"><?=$arItem['NAME']?></div>
                <div class="deploy__content catalog__sort-content">
                    <span class="bx-filter-container-modef"></span>
                    <div class="form-data">
                        <div class="form-data__wrap form-data__wrap_small form-data__wrap_logo-right-small">
                            <input class="form-data__input" name="" type="text">
                            <div class="form-data__logo-right-small form-data__logo-right-small_search"></div>
                        </div>
                    </div>
                    <?php
                    $arCur = current($arItem["VALUES"]);
                    switch ($arItem["DISPLAY_TYPE"])
                    {
                        case "A"://NUMBERS_WITH_SLIDER
                            ?>
                            <!-- filter-item -->
                                <div class="collapse <?= $arItem["DISPLAY_EXPANDED"]== "Y" ? 'show' : '' ;?>" id="f-collapse-<?=$key?>">
                                    <div class="card card-body">
                                        <div class="range-inputs">
                                            <div class="form-group">
                                                <input type="text"
                                                       data-index="0"
                                                       value="<?=$arItem['VALUES']['MIN']['HTML_VALUE']?>"
                                                       class="range-txt<?=$key?> min-price"
                                                       name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                                       id="<?= $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                                       placeholder="<?=$arItem['VALUES']['MIN']['VALUE']?>"
                                                >
                                            </div>
                                            <span class="dash">-</span>
                                            <div class="form-group">
                                                <input type="text"
                                                       data-index="1"
                                                       value="<?=$arItem['VALUES']['MAX']['HTML_VALUE']?>"
                                                       class="range-txt<?=$key?> max-price"
                                                       name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                                       id="<?= $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                                       placeholder="<?=$arItem['VALUES']['MAX']['VALUE']?>"
                                                >
                                            </div>
                                        </div>
                                        <div class="range">
                                            <div id="slider<?=$key?>"></div>
                                        </div>
                                        <div class="info-range">
                                            <span><?=$arItem['VALUES']['MIN']['VALUE']?></span>
                                            <span><?=$arItem['VALUES']['MAX']['VALUE']?></span>
                                        </div>
                                    </div>
                                </div>

                            <script>
                                $("#slider<?=$key?>").slider({
                                    range: true,
                                    min: <?=$arItem['VALUES']['MIN']['VALUE']?>,
                                    max: <?=$arItem['VALUES']['MAX']['VALUE']?>,
                                    step: 1,
                                    values: [
                                        <?=$arItem['VALUES']['MIN']['HTML_VALUE'] ? $arItem['VALUES']['MIN']['HTML_VALUE'] : $arItem['VALUES']['MIN']['VALUE']?>,
                                        <?=$arItem['VALUES']['MAX']['HTML_VALUE'] ? $arItem['VALUES']['MAX']['HTML_VALUE'] : $arItem['VALUES']['MAX']['VALUE']?>
                                    ],
                                    slide: function(event, ui) {
                                        for (var i = 0; i < ui.values.length; ++i) {
                                            $("input.range-txt<?=$key?>[data-index=" + i + "]").val(ui.values[i]);
                                        }
                                    },
                                    change: function(event, ui) {
                                        smartFilter.keyup(this);
                                    }
                                });
                                $("input.range-txt<?=$key?>").change(function() {
                                    var $this = $(this);
                                    $("#slider<?=$key?>").slider("values", $this.data("index"), $this.val());
                                });
                            </script>
                            <?
                            $arJsParams = array(
                                "leftSlider" => 'left_slider_'.$key,
                                "rightSlider" => 'right_slider_'.$key,
                                "tracker" => "drag_tracker_".$key,
                                "trackerWrap" => "drag_track_".$key,
                                "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                                "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                                "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
                                "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
                                "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                                "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                                "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
                                "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                                "precision" => $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0,
                                "colorUnavailableActive" => 'colorUnavailableActive_'.$key,
                                "colorAvailableActive" => 'colorAvailableActive_'.$key,
                                "colorAvailableInactive" => 'colorAvailableInactive_'.$key,
                            );
                            ?>
                            <script type="text/javascript">
                                BX.ready(function(){
                                    window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                                });
                            </script>
                            <?
                            break;
                        case "B"://NUMBERS
                            ?>
                            <div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
                                <i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
                                <div class="bx-filter-input-container">
                                    <input
                                        class="min-price"
                                        type="text"
                                        name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                        id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                        value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
                                        size="5"
                                        onkeyup="smartFilter.keyup(this)"
                                        />
                                </div>
                            </div>
                            <div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
                                <i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
                                <div class="bx-filter-input-container">
                                    <input
                                        class="max-price"
                                        type="text"
                                        name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                        id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                        value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
                                        size="5"
                                        onkeyup="smartFilter.keyup(this)"
                                        />
                                </div>
                            </div>
                            <?
                            break;
                        case "G"://CHECKBOXES_WITH_PICTURES
                            ?>
                            <div class="col-xs-12">
                                <div class="bx-filter-param-btn-inline">
                                <?foreach ($arItem["VALUES"] as $val => $ar):?>
                                    <input
                                        style="display: none"
                                        type="checkbox"
                                        name="<?=$ar["CONTROL_NAME"]?>"
                                        id="<?=$ar["CONTROL_ID"]?>"
                                        value="<?=$ar["HTML_VALUE"]?>"
                                        <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                    />
                                    <?
                                    $class = "";
                                    if ($ar["CHECKED"])
                                        $class.= " bx-active";
                                    if ($ar["DISABLED"])
                                        $class.= " disabled";
                                    ?>
                                    <label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
                                        <span class="bx-filter-param-btn bx-color-sl">
                                            <?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
                                            <span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
                                            <?endif?>
                                        </span>
                                    </label>
                                <?endforeach?>
                                </div>
                            </div>
                            <?
                            break;
                        case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
                            ?>
                            <div class="col-xs-12">
                                <div class="bx-filter-param-btn-block">
                                <?foreach ($arItem["VALUES"] as $val => $ar):?>
                                    <input
                                        style="display: none"
                                        type="checkbox"
                                        name="<?=$ar["CONTROL_NAME"]?>"
                                        id="<?=$ar["CONTROL_ID"]?>"
                                        value="<?=$ar["HTML_VALUE"]?>"
                                        <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                    />
                                    <?
                                    $class = "";
                                    if ($ar["CHECKED"])
                                        $class.= " bx-active";
                                    if ($ar["DISABLED"])
                                        $class.= " disabled";
                                    ?>
                                    <label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
                                        <span class="bx-filter-param-btn bx-color-sl">
                                            <?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
                                                <span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
                                            <?endif?>
                                        </span>
                                        <span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
                                        if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
                                            ?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
                                        endif;?></span>
                                    </label>
                                <?endforeach?>
                                </div>
                            </div>
                            <?
                            break;
                        case "P"://DROPDOWN
                            $checkedItemExist = false;
                            ?>
                            <div class="col-xs-12">
                                <div class="bx-filter-select-container">
                                    <div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
                                        <div class="bx-filter-select-text" data-role="currentOption">
                                            <?
                                            foreach ($arItem["VALUES"] as $val => $ar)
                                            {
                                                if ($ar["CHECKED"])
                                                {
                                                    echo $ar["VALUE"];
                                                    $checkedItemExist = true;
                                                }
                                            }
                                            if (!$checkedItemExist)
                                            {
                                                echo GetMessage("CT_BCSF_FILTER_ALL");
                                            }
                                            ?>
                                        </div>
                                        <div class="bx-filter-select-arrow"></div>
                                        <input
                                            style="display: none"
                                            type="radio"
                                            name="<?=$arCur["CONTROL_NAME_ALT"]?>"
                                            id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
                                            value=""
                                        />
                                        <?foreach ($arItem["VALUES"] as $val => $ar):?>
                                            <input
                                                style="display: none"
                                                type="radio"
                                                name="<?=$ar["CONTROL_NAME_ALT"]?>"
                                                id="<?=$ar["CONTROL_ID"]?>"
                                                value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                                                <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                            />
                                        <?endforeach?>
                                        <div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none;">
                                            <ul>
                                                <li>
                                                    <label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
                                                        <? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
                                                    </label>
                                                </li>
                                            <?
                                            foreach ($arItem["VALUES"] as $val => $ar):
                                                $class = "";
                                                if ($ar["CHECKED"])
                                                    $class.= " selected";
                                                if ($ar["DISABLED"])
                                                    $class.= " disabled";
                                            ?>
                                                <li>
                                                    <label for="<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')"><?=$ar["VALUE"]?></label>
                                                </li>
                                            <?endforeach?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?
                            break;
                        case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
                            ?>
                            <div class="col-xs-12">
                                <div class="bx-filter-select-container">
                                    <div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
                                        <div class="bx-filter-select-text fix" data-role="currentOption">
                                            <?
                                            $checkedItemExist = false;
                                            foreach ($arItem["VALUES"] as $val => $ar):
                                                if ($ar["CHECKED"])
                                                {
                                                ?>
                                                    <?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
                                                        <span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
                                                    <?endif?>
                                                    <span class="bx-filter-param-text">
                                                        <?=$ar["VALUE"]?>
                                                    </span>
                                                <?
                                                    $checkedItemExist = true;
                                                }
                                            endforeach;
                                            if (!$checkedItemExist)
                                            {
                                                ?><span class="bx-filter-btn-color-icon all"></span> <?
                                                echo GetMessage("CT_BCSF_FILTER_ALL");
                                            }
                                            ?>
                                        </div>
                                        <div class="bx-filter-select-arrow"></div>
                                        <input
                                            style="display: none"
                                            type="radio"
                                            name="<?=$arCur["CONTROL_NAME_ALT"]?>"
                                            id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
                                            value=""
                                        />
                                        <?foreach ($arItem["VALUES"] as $val => $ar):?>
                                            <input
                                                style="display: none"
                                                type="radio"
                                                name="<?=$ar["CONTROL_NAME_ALT"]?>"
                                                id="<?=$ar["CONTROL_ID"]?>"
                                                value="<?=$ar["HTML_VALUE_ALT"]?>"
                                                <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                            />
                                        <?endforeach?>
                                        <div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none">
                                            <ul>
                                                <li style="border-bottom: 1px solid #e5e5e5;padding-bottom: 5px;margin-bottom: 5px;">
                                                    <label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
                                                        <span class="bx-filter-btn-color-icon all"></span>
                                                        <? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
                                                    </label>
                                                </li>
                                            <?
                                            foreach ($arItem["VALUES"] as $val => $ar):
                                                $class = "";
                                                if ($ar["CHECKED"])
                                                    $class.= " selected";
                                                if ($ar["DISABLED"])
                                                    $class.= " disabled";
                                            ?>
                                                <li>
                                                    <label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')">
                                                        <?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
                                                            <span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
                                                        <?endif?>
                                                        <span class="bx-filter-param-text">
                                                            <?=$ar["VALUE"]?>
                                                        </span>
                                                    </label>
                                                </li>
                                            <?endforeach?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?
                            break;
                        case "K"://RADIO_BUTTONS
                            ?>
                            <div class="col-xs-12">
                                <div class="radio">
                                    <label class="bx-filter-param-label" for="<? echo "all_".$arCur["CONTROL_ID"] ?>">
                                        <span class="bx-filter-input-checkbox">
                                            <input
                                                type="radio"
                                                value=""
                                                name="<? echo $arCur["CONTROL_NAME_ALT"] ?>"
                                                id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
                                                onclick="smartFilter.click(this)"
                                            />
                                            <span class="bx-filter-param-text"><? echo GetMessage("CT_BCSF_FILTER_ALL"); ?></span>
                                        </span>
                                    </label>
                                </div>
                                <?foreach($arItem["VALUES"] as $val => $ar):?>
                                    <div class="radio">
                                        <label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
                                            <span class="bx-filter-input-checkbox <? echo $ar["DISABLED"] ? 'disabled': '' ?>">
                                                <input
                                                    type="radio"
                                                    value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                                                    name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
                                                    id="<? echo $ar["CONTROL_ID"] ?>"
                                                    <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                    onclick="smartFilter.click(this)"
                                                />
                                                <span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
                                                if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
                                                    ?>&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
                                                endif;?></span>
                                            </span>
                                        </label>
                                    </div>
                                <?endforeach;?>
                            </div>
                            <?
                            break;
                        case "U"://CALENDAR
                            ?>
                            <div class="col-xs-12">
                                <div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
                                    <?$APPLICATION->IncludeComponent(
                                        'bitrix:main.calendar',
                                        '',
                                        array(
                                            'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
                                            'SHOW_INPUT' => 'Y',
                                            'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MIN"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
                                            'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
                                            'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                                            'SHOW_TIME' => 'N',
                                            'HIDE_TIMEBAR' => 'Y',
                                        ),
                                        null,
                                        array('HIDE_ICONS' => 'Y')
                                    );?>
                                </div></div>
                                <div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
                                    <?$APPLICATION->IncludeComponent(
                                        'bitrix:main.calendar',
                                        '',
                                        array(
                                            'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
                                            'SHOW_INPUT' => 'Y',
                                            'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MAX"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
                                            'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
                                            'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                                            'SHOW_TIME' => 'N',
                                            'HIDE_TIMEBAR' => 'Y',
                                        ),
                                        null,
                                        array('HIDE_ICONS' => 'Y')
                                    );?>
                                </div></div>
                            </div>
                            <?
                            break;
                        default://CHECKBOXES
                            $count = 0;
                            foreach($arItem["VALUES"] as $val => $ar):
                                $class = "";
                                if ($ar["CHECKED"])
                                    $class.= " bx-active";
                                if ($ar["DISABLED"])
                                    $class.= " disabled";
                                ?>
                                <?if($count == 5):?>
                                    <div class="deploy">
                                        <div class="deploy__content" style="display: none;">
                                <?endif;?>
                                <div class="form-data">
                                    <input type="checkbox"
                                            class="form-data__checkbox js__checkbox"
                                           value="<?= $ar["HTML_VALUE"] ?>"
                                           name="<?= $ar["CONTROL_NAME"] ?>"
                                           id="<?= $ar["CONTROL_ID"] ?>"
                                           <?= $ar["CHECKED"]? 'checked="checked"': '' ?>
                                           onclick="smartFilter.click(this)"
                                    >
                                    <label title="<?=$ar["VALUE"];?>" for="<?= $ar["CONTROL_ID"] ?>" class="form-data__label">
                                        <?=$ar["VALUE"];?>
                                        <?php if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):?>
                                            <span class="form-data__label-comment" data-role="count_<?=$ar["CONTROL_ID"]?>">(<?= $ar["ELEMENT_COUNT"]; ?>)</span>
                                        <?php endif;?>
                                    </label>
                                </div>
                                <?if($count >= 5 && end($arItem["VALUES"]) == $ar):?>
                                        </div>
                                        <div class="catalog__add-brand js__deploy-toggle" data-deploy-open="Скрыть" data-deploy-close="<?=Loc::getMessage('SHOW_MORE_PROPERTY');?>"><?=Loc::getMessage('SHOW_MORE_PROPERTY');?></div>
                                    </div>
                                <?endif;?>
                                <?$count++;?>
                            <?endforeach;
                    } ?>
                </div>
            </div>
        <?
        }
        ?>


        <input
                style="display: none"
                class="btn btn-themes"
                type="submit"
                id="set_filter"
                name="set_filter"
                value="<?=GetMessage("CT_BCSF_SET_FILTER")?>"
        />

        <?//echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<div class="catalog__wrap-found-btn show-tablet"><div class="blue-button catalog__blue-button js__mobile-filter-close">Посмотреть <span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span> товаров</div></div>'));?>


        <div class="bx-filter-popup-result <?if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"]?>" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">

            <span class="arrow"></span>
            <br/>
            <a href="<?echo $arResult["FILTER_URL"]?>" target=""><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
        </div>
        <div class="clb"></div>

        <div class="catalog__wrap-found-btn show-tablet" style="display: none" class="modef_num_container filter-info">
            <div class="blue-button catalog__blue-button js__mobile-filter-close">Посмотреть <span id="modef_num_sec">328</span> товаров</div>
        </div>

    </form>
</div>
<script type="text/javascript">
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>
