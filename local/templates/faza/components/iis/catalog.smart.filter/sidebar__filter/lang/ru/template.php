<?
$MESS ['CT_BCSF_FILTER_TITLE'] = "Подбор параметров";
$MESS ['CT_BCSF_FILTER_FROM'] = "От";
$MESS ['CT_BCSF_FILTER_TO'] = "До";
$MESS ['CT_BCSF_SET_FILTER'] = "Показать";
$MESS ['CT_BCSF_DEL_FILTER'] = "Сбросить";
$MESS ['CT_BCSF_FILTER_COUNT'] = "Найдено #ELEMENT_COUNT# товаров";
$MESS ['CT_BCSF_FILTER_SHOW'] = "Показать";
$MESS ['CT_BCSF_FILTER_ALL'] = "Все";

$MESS ['FILTER_TITLE'] = "Фильтр";
$MESS ['FILTER_CANCEL'] = "Отмена";
$MESS ['FILTER_CLEAR'] = "Очистить фильтры";

$MESS ['SHOW_MORE_PROPERTY'] = "Показать больше";
?>
