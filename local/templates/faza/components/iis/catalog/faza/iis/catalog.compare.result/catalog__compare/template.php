<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$isAjax = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$isAjax = (
		(isset($_POST['ajax_action']) && $_POST['ajax_action'] == 'Y')
		|| (isset($_POST['compare_result_reload']) && $_POST['compare_result_reload'] == 'Y'));
}

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder() . '/themes/' . $arParams['TEMPLATE_THEME'] . '/style.css',
	'TEMPLATE_CLASS' => 'bx_' . $arParams['TEMPLATE_THEME']
); ?>



<div class="compare__inner js__compare <? echo $templateData['TEMPLATE_CLASS']; ?>" id="bx_catalog_compare_block">
	<? if ($isAjax) {
		$APPLICATION->RestartBuffer();
	} ?>
	<? foreach ($arResult["ITEMS"] as $keyElement => $arElement) : ?>
		<div class="compare__item">
			<? if (!empty($arResult["SHOW_FIELDS"])) { ?>
				<div class="product-price compare__product">
					<div class="product-price__inner">
						<a class="basket-product__close" onclick="CatalogCompareObj.delete('<?= CUtil::JSEscape($arElement['~DELETE_URL']) ?>');" href="javascript:void(0)"></a>
						<? foreach ($arResult["SHOW_FIELDS"] as $code => $arProp) {
							switch ($code) {
								case "NAME": ?>
									<a href="<?= $arElement["DETAIL_PAGE_URL"] ?>" class="product-price__title"><?= $arElement[$code] ?></a>
									<? if ($arElement["CAN_BUY"] && (!empty($arResult["PRICES"]) || is_array($arElement["PRICE_MATRIX"]))) { ?>
										<div class="product-price__price">
											<a class="buy-button product-price__buy-button" href="<?= $arElement["BUY_URL"] ?>" rel="nofollow"></a>


											<? if (isset($arElement['MIN_PRICE']) && is_array($arElement['MIN_PRICE'])) { ?>

												<div class="price-rub">
													<div class="price-rub__inner"><?= $arElement['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?></div>
												</div>

												<? } elseif (!empty($arElement['PRICE_MATRIX']) && is_array($arElement['PRICE_MATRIX'])) {
												$matrix = $arElement['PRICE_MATRIX'];
												$rows = $matrix['ROWS'];
												$rowsCount = count($rows);
												if ($rowsCount > 0) {
													if (count($rows) > 1) {
														foreach ($rows as $index => $rowData) {
															if (empty($matrix['MIN_PRICES'][$index]))
																continue;
															if ($rowData['QUANTITY_FROM'] == 0)
																$rowTitle = GetMessage('CP_TPL_CCR_RANGE_TO', array('#TO#' => $rowData['QUANTITY_TO']));
															elseif ($rowData['QUANTITY_TO'] == 0)
																$rowTitle = GetMessage('CP_TPL_CCR_RANGE_FROM', array('#FROM#' => $rowData['QUANTITY_FROM']));
															else
																$rowTitle = GetMessage(
																	'CP_TPL_CCR_RANGE_FULL',
																	array('#FROM#' => $rowData['QUANTITY_FROM'], '#TO#' => $rowData['QUANTITY_TO'])
																);
															echo '<tr><td>' . $rowTitle . '</td><td>';
															echo \CCurrencyLang::CurrencyFormat($matrix['MIN_PRICES'][$index]['PRICE'], $matrix['MIN_PRICES'][$index]['CURRENCY']);
															echo '</td></tr>';
															unset($rowTitle);
														}
														unset($index, $rowData);
													} else {
														$currentPrice = current($matrix['MIN_PRICES']);
												?>
														<div class="price-rub">
															<div class="price-rub__inner"><?= $currentPrice['PRICE']; ?></div>
														</div>
											<?
														unset($currentPrice);
													}
												}
												unset($rowsCount, $rows, $matrix);
											} ?>

										</div>
									<? }
									break;
								case "PREVIEW_PICTURE":
								case "DETAIL_PICTURE":
									if (!empty($arElement["FIELDS"][$code]) && is_array($arElement["FIELDS"][$code])) : ?>
										<a href="<?= $arElement["DETAIL_PAGE_URL"] ?>" class="product-price__img" style="background-image: url('<?= $arElement["FIELDS"][$code]["SRC"] ?>')" title="<?= $arElement["FIELDS"][$code]["TITLE"] ?>"></a>
										<div class="rating compare__rating">
											<div class="rating__inner">
												<div class="rating__heart">
													<input id="favorite__<?= $arElement['ID'] ?>" class="rating__heart-input" type="checkbox">
													<label id="favorite__<?= $arElement['ID'] ?>" class="rating__heart-label favorite__btn" data-item="<?=$arElement['ID']?>"></label>
												</div>
												<? $APPLICATION->IncludeComponent(
													'iis:iblock.vote',
													'stars__list',
													array(
														'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
														'IBLOCK_TYPE' => $arElement['IBLOCK_TYPE'],
														'IBLOCK_ID' => $arElement['IBLOCK_ID'],
														'ELEMENT_ID' => $arElement['ID'],
														'ELEMENT_CODE' => '',
														'MAX_VOTE' => '5',
														'VOTE_NAMES' => array('1', '2', '3', '4', '5'),
														'SET_STATUS_404' => 'N',
														'DISPLAY_AS_RATING' => 'vote_avg',
														'CACHE_TYPE' => 'A',
														'CACHE_TIME' => '3600'
													),
													$component,
													array('HIDE_ICONS' => 'Y')
												); ?>
											</div>
										</div>
						<? endif;
									break;

								default:
									echo $arElement["FIELDS"][$code];
									break;
							}
						} ?>
					</div>
				</div>
			<? } ?>

			<? if (!empty($arResult["SHOW_PROPERTIES"])) {
				foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty) {
					$showRow = true;
					if ($arResult['DIFFERENT']) {
						$arCompare = array();
						foreach ($arResult["ITEMS"] as $arElement) {
							$arPropertyValue = $arElement["DISPLAY_PROPERTIES"][$code]["VALUE"];
							if (is_array($arPropertyValue)) {
								sort($arPropertyValue);
								$arPropertyValue = implode(" / ", $arPropertyValue);
							}
							$arCompare[] = $arPropertyValue;
						}
						unset($arElement);
						$showRow = (count(array_unique($arCompare)) > 1);
					}

					if ($showRow) { ?>
						<div class="compare__property">
							<div class="compare__property-title"><?= $arProperty["NAME"] ?></div>
							<div class="compare__property-desc"><?= (is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) ? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) : $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]) ?></div>
						</div>
			<? }
				}
			} ?>
		</div>
	<? endforeach; ?>
	<? if ($isAjax) {
		die();
	} ?>
</div>




<script type="text/javascript">
	var CatalogCompareObj = new BX.Iblock.Catalog.CompareClass("bx_catalog_compare_block", '<?= CUtil::JSEscape($arResult['~COMPARE_URL_TEMPLATE']); ?>');
</script>