BX.namespace("BX.Iblock.Catalog");

BX.Iblock.Catalog.CompareClass = (function () {
	var CompareClass = function (wrapObjId, reloadUrl) {
		this.wrapObjId = wrapObjId;
		this.reloadUrl = reloadUrl;
		BX.addCustomEvent(window, 'onCatalogDeleteCompare', BX.proxy(this.reload, this));
	};

	CompareClass.prototype.MakeAjaxAction = function (url) {
		BX.showWait(BX(this.wrapObjId));
		BX.ajax.post(
			url,
			{
				ajax_action: 'Y'
			},
			BX.proxy(this.reloadResult, this)
		);
	};

	CompareClass.prototype.reload = function () {
		BX.showWait(BX(this.wrapObjId));
		BX.ajax.post(
			this.reloadUrl,
			{
				compare_result_reload: 'Y'
			},
			BX.proxy(this.reloadResult, this)
		);
	};

	CompareClass.prototype.reloadResult = function (result) {
		BX.closeWait();
		BX(this.wrapObjId).innerHTML = result;
	};

	CompareClass.prototype.delete = function (url) {
		// BX.showWait(BX(this.wrapObjId));
		BX.ajax.post(
			url,
			{
				ajax_action: 'Y'
			},
			BX.proxy(this.deleteResult, this)
		);
	};

	CompareClass.prototype.deleteResult = function (result) {
		// BX.closeWait();
		BX.onCustomEvent('OnCompareChange');

		$('.js__compare').slick('unslick');
		BX(this.wrapObjId).innerHTML = result;
		let qtyChildren = $('.js__compare').children('.compare__item').length;
		if( $(window).width() >= 1319 && qtyChildren > 4){
			$('.js__compare').slick(getSliderSettings());
		}
		else if( $(window).width() < 1319 && qtyChildren > 3){
			$('.js__compare').slick(getSliderSettings());
		}
		else if( $(window).width() < 1064 && qtyChildren > 2){
			$('.js__compare').slick(getSliderSettings());
		}
		else if( $(window).width() < 767 && qtyChildren > 1){
			$('.js__compare').slick(getSliderSettings());
		}
		else {
			$('.js__compare').children('.compare__item').css({
				width: 100/qtyChildren + "%"
			});
		}
	};

	return CompareClass;
})();


function getSliderSettings() {
	return {
		dots: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: false,
		arrows: true,
		nextArrow: '<div class="compare__arrow compare__arrow_right"></div>',
		prevArrow: '<div class="compare__arrow compare__arrow_left"></div>',
		responsive: [
			{
				breakpoint: 1319,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 1064,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	}
}