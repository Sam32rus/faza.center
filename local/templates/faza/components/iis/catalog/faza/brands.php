<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\ModuleManager;
$APPLICATION->SetPageProperty("keywords", "");

$hlblock = HL\HighloadBlockTable::getById(2)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$arBrandData = $entity_data_class::getList(array(
    'filter' => ['!UF_FILE' => false],
    "select" => array("*"),
    "order" => array("UF_SORT" => "ASC"),
))->fetchAll();

foreach($arBrandData as $key => $brand){
    if($brand['UF_FILE']){
        $arBrandData[$key]['PICTURE'] = CFile::ResizeImageGet($brand['UF_FILE'], array('width'=>150, 'height'=>100), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    }
}


if(!empty($arBrandData))://страница бренда
    $APPLICATION->AddChainItem('Бренды', '/catalog/brands/');
    $APPLICATION->SetPageProperty("title", "Бренды магазина Фаза");
    $APPLICATION->SetPageProperty("description", "Бренды магазина Фаза");
    $APPLICATION->SetTitle("Бренды");
    ?>
    <main class="main">
        <h1 class="title-page"><span class="content"><?=$APPLICATION->ShowTitle(false);?></span></h1>
        <div class="general">
            <div class="content">
                <div class="general__inner">
                    <?foreach($arBrandData as $key => $brand):?>
                        <div class="brands<?=$brand['UF_IS_LANDING'] ? ' brands_big' : ''?>">
                            <a href="/catalog/brands/<?=$brand['UF_XML_ID']?>/" class="brands__inner">
                                <span class="brands__img" style="background-image: url('<?=$brand['PICTURE']['src'] ? $brand['PICTURE']['src'] : '/local/templates/faza/assets/i/brand_no_photo.png'?>')"></span>
                                <span class="brands__title"><?=$brand['UF_NAME']?></span>
                            </a>
                        </div>
                    <?endforeach;?>
                </div>
            </div>
        </div>
    </main>
<?endif;?>
