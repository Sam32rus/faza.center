<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$imageWidth = 120;
$imageHeight = 96;


foreach ($arResult['rows'] as $key => $arItem)
{
    if ($arItem['UF_FILE'])
    {
        $arResult["rows"][$key]['RESIZE_PICTURE'] = CFile::ResizeImageGet($arItem["UF_FILE"], Array("width" => $imageWidth, "height" => $imageHeight));
    }
}
?>