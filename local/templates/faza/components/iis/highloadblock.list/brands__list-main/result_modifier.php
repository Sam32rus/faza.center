<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$imageWidth = 108;
$imageHeight = 70;


foreach ($arResult['rows'] as $key => $arItem)
{
    if ($arItem['UF_FILE'])
    {
        $arResult["rows"][$key]['RESIZE_PICTURE'] = CFile::ResizeImageGet($arItem["UF_FILE"], Array("width" => $imageWidth, "height" => $imageHeight));
    }
}
?>