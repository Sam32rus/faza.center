<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
{
	echo $arResult['ERROR'];
	return false;
}

//$GLOBALS['APPLICATION']->SetTitle('Highloadblock List');

if ($_GET['bitrix_include_areas'] == 'Y')
    return;

?>

<?php if (isset($arResult['rows']) && is_array($arResult['rows']) && count($arResult['rows'])) :?>
    <section class="section">
        <div class="content">
            <div class="section__inner">
                <div class="section__title">Лучшие бренды</div>
                <div class="best-brands">
                    <div class="best-brands__inner">
                        <?php foreach ($arResult['rows'] as $row):
                            $url = str_replace(
                                array('#CODE#'),
                                array($row['UF_XML_ID']),
                                $arParams['DETAIL_URL']
                            );
                            $url = str_replace(
                                array('#UF_NAME#'),
                                array($row['UF_NAME']),
                                $url
                            );
                            ?>
                            <a href="<?=$url?>" class="best-brands__link-item">
                                <img src="<?=$row['RESIZE_PICTURE']['src']?>" alt="<?=$row['UF_NAME']?>">
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
