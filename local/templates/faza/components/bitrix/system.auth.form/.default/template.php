<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CJSCore::Init();
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>
<form action="">
	<div class="form-data">
		<div class="form-data__error" data-error="incorrect">Неверный E-mail или пароль</div>
		<div class="form-data__wrap">
			<input class="form-data__input js__email" placeholder="Адрес электронной почты" name="email" type="text">
		</div>
		<div class="form-data__error" data-error="email">Заполните поле "Адрес электронной почты"</div>
	</div>
	<div class="form-data">
		<div class="form-data__wrap">
			<input class="form-data__input js__default" placeholder="Пароль" name="password" type="password">
		</div>
		<div class="form-data__error" data-error="password">Введите "Пароль"</div>
	</div>
	<div class="popup__link popup__link_bottom" data-popup-btn="recovery">Забыли пароль?</div>
	<div class="form-data">
		<div class="form-data__wrap-submit">
			<button class="js__disabled-btn form-data__submit blue-button" type="submit">Войти</button>
		</div>
	</div>
	<div class="popup__change-popup">
		<div class="popup__link" data-popup-btn="registration">Зарегистрироваться</div>
	</div>
</form>