<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CJSCore::Init();
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>
<div class="popup__content">
	<div class="popup__title">Вход</div>
	<form name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
		<? if ($arResult["BACKURL"] <> '') : ?>
			<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
		<? endif ?>
		<? foreach ($arResult["POST"] as $key => $value) : ?>
			<input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
		<? endforeach ?>
		<input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="AUTH" />

		<div class="form-data">
			<div class="form-data__wrap">
				<input class="form-data__input js__email" placeholder="<?= GetMessage("AUTH_LOGIN") ?>" name="USER_LOGIN" type="text">
				<script>
					BX.ready(function() {
						var loginCookie = BX.getCookie("<?= CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"]) ?>");
						if (loginCookie) {
							var form = document.forms["system_auth_form<?= $arResult["RND"] ?>"];
							var loginInput = form.elements["USER_LOGIN"];
							loginInput.value = loginCookie;
						}
					});
				</script>
			</div>
			<div class="form-data__error">Заполните поле "<?= GetMessage("AUTH_LOGIN") ?>"</div>
		</div>
		<div class="form-data">
			<div class="form-data__wrap">
				<input class="form-data__input js__default" placeholder="<?= GetMessage("AUTH_PASSWORD") ?>" name="USER_PASSWORD" type="password">
			</div>
			<div class="form-data__error">Введите "<?= GetMessage("AUTH_PASSWORD") ?>"</div>
		</div>
		<div class="popup__link popup__link_bottom" data-popup-btn="recovery">Забыли пароль?</div>
		<div class="form-data">
			<div class="form-data__wrap-submit">
				<input class="js__disabled-btn form-data__submit blue-button" type="submit" name="Login" value="<?= GetMessage("AUTH_LOGIN_BUTTON") ?>" />
			</div>
		</div>
		<div class="form-data">
			<div class="popup__link" data-popup-btn="registration">Зарегистрироваться</div>
		</div>
	</form>

	<? if ($arResult["AUTH_SERVICES"]) : ?>
		<? $APPLICATION->IncludeComponent(
			"bitrix:socserv.auth.form",
			"",
			array(
				"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
				"AUTH_URL" => $arResult["AUTH_URL"],
				"POST" => $arResult["POST"],
				"POPUP" => "Y",
				"SUFFIX" => "form",
			),
			$component,
			array("HIDE_ICONS" => "Y")
		); ?>
	<? endif ?>
</div>