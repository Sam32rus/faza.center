<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$this->setFrameMode(true);

	foreach ($arResult['ITEMS'] as $key => $arItem)
	{

		$strMainID = $this->GetEditAreaId($arItem['ID'] . $key);

		$arItemIDs = array(
			'ID' => $strMainID,
			'PICT' => $strMainID . '_pict',
			'SECOND_PICT' => $strMainID . '_secondpict',
			'MAIN_PROPS' => $strMainID . '_main_props',

			'QUANTITY' => $strMainID . '_quantity',
			'QUANTITY_DOWN' => $strMainID . '_quant_down',
			'QUANTITY_UP' => $strMainID . '_quant_up',
			'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
			'BUY_LINK' => $strMainID . '_buy_link',
			'SUBSCRIBE_LINK' => $strMainID . '_subscribe',

			'PRICE' => $strMainID . '_price',
			'DSC_PERC' => $strMainID . '_dsc_perc',
			'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',

			'PROP_DIV' => $strMainID . '_sku_tree',
			'PROP' => $strMainID . '_prop_',
			'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
			'BASKET_PROP_DIV' => $strMainID . '_basket_prop'
		);

		$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

		$strTitle = (
			isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]) && '' != isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])
			? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]
			: $arItem['NAME']
		);
		$showImgClass = $arParams['SHOW_IMAGE'] != "Y" ? "no-imgs" : "";
    // echo"<pre>";var_dump($arItem['PROPERTIES']['vote_sum']);echo"</pre>";

		?>


                                    <!--Конкретный товар-->
                                    <div class="product-slider__item-slid" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                        <a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="product-slider__image" style="background-image: url(<? echo($arParams['SHOW_IMAGE'] == "Y" ? $arItem['PREVIEW_PICTURE']['SRC'] : ""); ?>)"></a>
                                        <div class="rating product-slider__rating">
                                            <div class="rating__inner">
                                                <div class="rating__chart" data-compareurl="<? echo $arItem['COMPARE_URL']; ?>"></div>
                                                <div class="rating__heart" data-item='<?=$arItem['ID']?>'></div>
                                                <div class="rating__star product-slider__star" data-rating="<?=$arItem['PROPERTIES']['vote_sum']['VALUE']?>">
                                                    <div class="rating__fill"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="product-slider__title"><? echo $arItem['NAME']; ?></a>
                                      <!--   <?
			if ($arItem['CAN_BUY'])
			{
				if ('Y' == $arParams['USE_PRODUCT_QUANTITY'])
				{
					?>
					<div class="bx_catalog_item_controls_blockone">
						<div style="display: inline-block;position: relative;">
							<a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>" href="javascript:void(0)" class="bx_bt_button_type_2 bx_small" rel="nofollow">-</a>
							<input type="text" class="bx_col_input" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
							<a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0)" class="bx_bt_button_type_2 bx_small" rel="nofollow">+</a>
							<span id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>" class="bx_cnt_desc"><? echo $arItem['CATALOG_MEASURE_NAME']; ?></span>
						</div>
					</div>
				<?
				}
				?>
					<a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="bx_bt_button bx_medium" href="javascript:void(0)" rel="nofollow"><?
						echo('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCS_TPL_MESS_BTN_BUY'));
						?></a>
				
			<?
			}
			else
			{
				?>
				<div class="bx_catalog_item_controls_blockone">
				<a class="bx_medium bx_bt_button_type_2" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" rel="nofollow">
					<? echo('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('SB_TPL_MESS_BTN_DETAIL')); ?>
				</a>
				</div><?
				if ('Y' == $arParams['PRODUCT_SUBSCRIPTION'] && 'Y' == $arItem['CATALOG_SUBSCRIPTION'])
				{
					?>
					<a id="<? echo $arItemIDs['SUBSCRIBE_LINK']; ?>" class="bx_bt_button_type_2 bx_medium" href="javascript:void(0)"><?
						echo('' != $arParams['MESS_BTN_SUBSCRIBE'] ? $arParams['MESS_BTN_SUBSCRIBE'] : GetMessage('SB_TPL_MESS_BTN_SUBSCRIBE'));
						?>
					</a>
					<?
				}
			}
			?> -->
                                        <div class="product-slider__buy-price">  
                                            <button class="buy-button js__show-count addToBasketButton" data-addurl="<? echo $arItem['ADD_URL']; ?>" ></button>
                                           <div class="count no-active">
                                               <div class="count__inner js__count">
                                                   <input class="js__count-minus count__minus" type="button"  value="-" />
                                                   <input class="js__count-sum count__sum" type="text" size="3" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>"/>
                                                   <input class="js__count-plus count__plus" type="button"  data-addurl="<? echo $arItem['ADD_URL']; ?>" value="+" />
                                               </div>
                                           </div>
                                            <div class="price-rub product-slider__price">
                                            	<div class="price-rub__inner"><?
			if (!empty($arItem['MIN_PRICE']))
			{
				if (isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
				{
					echo GetMessage(
						'SB_TPL_MESS_PRICE_SIMPLE_MODE',
						array(
							'#PRICE#' => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
							'#MEASURE#' => GetMessage(
								'SB_TPL_MESS_MEASURE_SIMPLE_MODE',
								array(
									'#VALUE#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_RATIO'],
									'#UNIT#' => $arItem['MIN_PRICE']['CATALOG_MEASURE_NAME']
								)
							)
						)
					);
				}
				else
				{
					echo $arItem['MIN_PRICE']['VALUE'];
				}
				if ('Y' == $arParams['SHOW_OLD_PRICE'] && $arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE'])
				{
					?> <span style="color: #a5a5a5;font-size: 12px;font-weight: normal;white-space: nowrap;text-decoration: line-through;"><? echo $arItem['MIN_PRICE']['PRINT_VALUE']; ?></span><?
				}
			}
			?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ Конкретный товар-->
     <?
	}
	?>
