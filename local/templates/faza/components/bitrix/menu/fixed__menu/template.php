<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):
	$previousLevel = 0;
	echo "<div class='footer__coll'>";
	foreach($arResult as $arItem):
		if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
			<?=str_repeat("</div><div class='footer__coll'>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
		<?endif?>

		<?if ($arItem["IS_PARENT"]):?>
			<div class="footer__title"><?=$arItem["TEXT"]?></div>
		<?else:?>
			<?if ($arItem["PERMISSION"] > "D"):?>
				<?if ($arItem['PARAMS']['auth'] == 'Y') {?>
					<span data-popup-btn='login' class="footer__link"><?=$arItem["TEXT"]?></span>
				<?}elseif($arItem['PARAMS']['reg'] == 'Y'){?>
					<span data-popup-btn='registration' class="footer__link"><?=$arItem["TEXT"]?></span>
				<?}else{?>
					<a href="<?=$arItem["LINK"]?>" class="footer__link"><?=$arItem["TEXT"]?></a>
				<?}?>
			<?endif?>
		<?endif?>

		<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

	<?endforeach?>

	<?if ($previousLevel > 1):?>
		<?=str_repeat("</div>", ($previousLevel-1) );?>
	<?endif?>
<?endif?>
