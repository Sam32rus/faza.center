<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="cab-menu">
    <div class="cab-menu__inner">
        <?if($arParams['CONFIRMED']):?>
            <div class="cab-menu__wrap-logo"><img src="/f/i/cabinet/logo.svg" class="cab-menu__logo"></div>
        <?endif;?>
        <ul class="cab-menu__list-point">

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li class="cab-menu__item-point"><a href="<?=$arItem["LINK"]?>" class="cab-menu__item-link <?=$arItem["PARAMS"]["class"]?>"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li class="cab-menu__item-point"><a href="<?=$arItem["LINK"]?>" class="cab-menu__item-link <?=$arItem["PARAMS"]["class"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	<?if($arItem["PARAMS"]["class"]=="cab-menu__item-link_order"){?>
            <li class="cab-menu__list-line"></li>
	<?}?>
	<?if($arItem["PARAMS"]["class"]=="cab-menu__item-link_subscription"){?>
            <li class="cab-menu__list-line"></li>
	<?}?>
<?endforeach?>

</ul> 
<div class="user-menu cab-menu__user-menu js__cabinet-menu-user-menu">
            <div class="content">
                <div class="user-menu__inner">
                    <a href="/catalog/" class="user-menu__item-point user-menu__item-point_catalog">Каталог</a>
                    <a href="/catalog/compare/" class="user-menu__item-point user-menu__item-point_compare">Сравнить</a>
                    <a href="#" class="user-menu__item-point user-menu__item-point_heart">Избранное</a>
                    <a href="#" class="user-menu__item-point user-menu__item-point_user" data-popup-btn="login">Вход</a>
                    <div class="user-menu__item-point user-menu__item-point_basket">
                        <div class="user-menu__basket-sum">25</div>
                        Корзина
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?endif?>