<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)) : ?>
  <div class="top-menu header__top-menu">
    <div class="top-menu__inner">
    <?/*<? $APPLICATION->IncludeComponent(
        "bitrix:sale.location.selector.steps",
        "templateloc",
        array(
          "CACHE_TIME" => "36000000",  // Время кеширования (сек.)
          "CACHE_TYPE" => "A",  // Тип кеширования
          "CODE" => "",  // Символьный код местоположения
          "FILTER_BY_SITE" => "N",  // Фильтровать по сайту
          "ID" => "",  // ID местоположения
          "INPUT_NAME" => "LOCATION",  // Имя поля ввода
          "JS_CALLBACK" => "",  // Javascript-функция обратного вызова
          "JS_CONTROL_GLOBAL_ID" => "",  // Идентификатор javascript-контрола
          "PRECACHE_LAST_LEVEL" => "N",  // Предварительно загружать последний выбранный уровень
          "PRESELECT_TREE_TRUNK" => "N",  // Отображать статичный ствол дерева
          "PROVIDE_LINK_BY" => "id",  // Сохранять связь через
          "SHOW_DEFAULT_LOCATIONS" => "N",  // Отображать местоположения по-умолчанию
        ),
        false
      ); ?>
      <div class="top-menu__item-point top-menu__wrap-point-city">
        <a href="#" class="top-menu__item-point top-menu__item-point_city">Брянск</a>
      </div>*/?>

      <div class="top-menu__item-point top-menu__wrap-point-link">
        <?
        $previousLevel = 0;
        foreach ($arResult as $arItem) : ?>

          <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) : ?>
            <?= str_repeat("</ul></span>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
          <? endif ?>

          <? if ($arItem["IS_PARENT"]) : ?>

            <? if ($arItem["DEPTH_LEVEL"] == 1) : ?>
              <span class="top-menu__item-point top-menu__item-point_arrow js__top-menu-arrow"><?= $arItem["TEXT"] ?>
                <ul class="top-menu__arrow-list">
                <? else : ?>
                  <span class="top-menu__item-point top-menu__item-point_arrow js__top-menu-arrow child__parent"><?= $arItem["TEXT"] ?>
                    <ul class="top-menu__arrow-list">
                    <? endif ?>

                  <? else : ?>

                    <? if ($arItem["PERMISSION"] > "D") : ?>

                      <? if ($arItem["DEPTH_LEVEL"] == 1) : ?>
                        <a href="<?= $arItem["LINK"] ?>" class="top-menu__item-point"><?= $arItem["TEXT"] ?></a>
                      <? else : ?>
                        <li class="top-menu__arrow-item"><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
                      <? endif ?>

                    <? else : ?>

                      <? if ($arItem["DEPTH_LEVEL"] == 1) : ?>
                        <a href="<?= $arItem["LINK"] ?>" class="top-menu__item-point"><?= $arItem["TEXT"] ?></a>
                      <? else : ?>
                        <li class="top-menu__arrow-item"><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
                      <? endif ?>

                    <? endif ?>

                  <? endif ?>

                  <? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>

                <? endforeach ?>

                <? if ($previousLevel > 1) : //close last item tags
                ?>
                  <?= str_repeat("</ul></span>", ($previousLevel - 1)); ?>
                <? endif ?>

      </div>
      <div class="top-menu__item-point top-menu__wrap-point-phone">
        <a href="tel:+78007002074" class="top-menu__item-point top-menu__item-point_phone">8 800 700 2074</a>
      </div>

    </div>
  </div>
<? endif ?>