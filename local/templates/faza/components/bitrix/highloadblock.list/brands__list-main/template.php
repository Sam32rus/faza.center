<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
{
	echo $arResult['ERROR'];
	return false;
}

//$GLOBALS['APPLICATION']->SetTitle('Highloadblock List');

if ($_GET['bitrix_include_areas'] == 'Y')
    return;

?>

<?php if (isset($arResult['rows']) && is_array($arResult['rows']) && count($arResult['rows'])) :?>
    <section class="partners-slider">
        <div class="content">
            <div class="partners-slider__inner js__partners-slider">
                <?php foreach ($arResult['rows'] as $row):
                    $url = str_replace(
                        array('#CODE#'),
                        array($row['UF_XML_ID']),
                        $arParams['DETAIL_URL']
                    );
                    $url = str_replace(
                        array('#UF_NAME#'),
                        array($row['UF_NAME']),
                        $url
                    );
                    ?>
                    <a href="<?=$url?>" class="partners-slider__link">
                        <?=html_remove_attributes($row['UF_FILE'], ['src'])?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
