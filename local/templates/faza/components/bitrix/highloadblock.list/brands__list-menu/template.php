<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
{
	echo $arResult['ERROR'];
	return false;
}

//$GLOBALS['APPLICATION']->SetTitle('Highloadblock List');

if ($_GET['bitrix_include_areas'] == 'Y')
    return;

?>

<?php if (isset($arResult['rows']) && is_array($arResult['rows']) && count($arResult['rows'])) :?>
    <div class="menu__partners">
        <?php foreach ($arResult['rows'] as $row):
            $url = str_replace(
                array('#CODE#'),
                array($row['UF_XML_ID']),
                $arParams['DETAIL_URL']
            );
            $url = str_replace(
                array('#UF_NAME#'),
                array($row['UF_NAME']),
                $url
            );
            ?>
            <a href="<?=$url?>" class="menu__partners-item">
                <?=html_remove_attributes($row['UF_FILE'], ['src'])?>
            </a>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
