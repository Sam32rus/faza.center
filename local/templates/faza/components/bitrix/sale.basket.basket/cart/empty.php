<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
?>

<section class="empty">
	<div class="content">
		<div class="empty__inner">
			<img src="/f/i/icons/shopping-cart.svg" class="empty__center-img">
			<div class="empty__center-text">Ваша корзина пуста. <a href="/catalog/" class="link-blue">Нажмите&nbsp;здесь</a>, чтобы продолжить покупки.</div>
		</div>
	</div>
</section>