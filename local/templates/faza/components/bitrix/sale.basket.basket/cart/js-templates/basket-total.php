<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-count-product-block" type="text/html">
	1111
	{{#PRODUCT_COUNT}}
	<div class="sorting__inner">
        <div class="sorting__found"><?=Loc::getMessage('TOTAL_BASKET_COUNT', ['COUNT' => PRODUCT_COUNT]);?></div>
    </div>
    {{/PRODUCT_COUNT}}
</script>

<script id="basket-total-template" type="text/html">
	<div class="price-rub basket__price">
        <div class="price-rub__title"><?=Loc::getMessage('SBB_TOTAL')?>:</div>
        <div class="price-rub__inner" data-entity="basket-total-price">
			{{{PRICE_FORMATED}}}
		</div>
    </div>


    <div class="price-rub basket__nds">
		{{#SHOW_VAT}}
			<div class="price-rub__title"><?=Loc::getMessage('SBB_VAT')?>:</div>
			<div class="price-rub__inner">{{{VAT_SUM_FORMATED}}}</div>
		{{/SHOW_VAT}}
    </div>

    <div class="basket__wrap-bt-panel">
    	<button class="blue-button basket__blue-button" data-entity="basket-checkout-button">
			<?=Loc::getMessage('SBB_ORDER')?>
		</button>
        <button class="blue-button basket__blue-button" data-popup-btn="quick-request" data-type="cart"><?=Loc::getMessage('SBB_FAST_ORDER')?></button>
    </div>
    {{#BRANDS_HTML}}
	    <div class="basket__brands-panel">
	        <div class="basket__brands-title"><?=Loc::getMessage('BRANDS_IN_BASKET');?></div>
	        {{{BRANDS_HTML}}}
	    </div>
    {{/BRANDS_HTML}}
</script>
