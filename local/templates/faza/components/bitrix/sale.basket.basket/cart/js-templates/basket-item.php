<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $mobileColumns
 * @var array $arParams
 * @var string $templateFolder
 */

$usePriceInAdditionalColumn = in_array('PRICE', $arParams['COLUMNS_LIST']) && $arParams['PRICE_DISPLAY_MODE'] === 'Y';
$useSumColumn = in_array('SUM', $arParams['COLUMNS_LIST']);
$useActionColumn = in_array('DELETE', $arParams['COLUMNS_LIST']);

$restoreColSpan = 2 + $usePriceInAdditionalColumn + $useSumColumn + $useActionColumn;

$positionClassMap = array(
	'left' => 'basket-item-label-left',
	'center' => 'basket-item-label-center',
	'right' => 'basket-item-label-right',
	'bottom' => 'basket-item-label-bottom',
	'middle' => 'basket-item-label-middle',
	'top' => 'basket-item-label-top'
);

$discountPositionClass = '';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}
?>
<script id="basket-item-template" type="text/html">
	<div class="basket-product basket__item-product" id="basket-item-{{ID}}" data-entity="basket-item" data-id="{{ID}}">
        <div class="basket-product__inner">
            <button class="basket-product__close" data-entity="basket-item-delete"></button>

            <div class="basket-product__img" title="{{NAME}}" style="background-image: url('{{{IMAGE_URL}}}{{^IMAGE_URL}}<?=$templateFolder?>/images/no_photo.png{{/IMAGE_URL}}')"></div>
            <div class="basket-product__name">
                <div class="basket-product__title">
                	{{#DETAIL_PAGE_URL}}
						<a href="{{DETAIL_PAGE_URL}}">
					{{/DETAIL_PAGE_URL}}
						{{NAME}}
					{{#DETAIL_PAGE_URL}}
						</a>
					{{/DETAIL_PAGE_URL}}
				</div>
				<?if (!empty($arParams['PRODUCT_BLOCKS_ORDER'])){
					foreach ($arParams['PRODUCT_BLOCKS_ORDER'] as $blockName){
						switch (trim((string)$blockName)){
							case 'columns':
								?>
								{{#COLUMN_LIST}}
									{{#IS_TEXT}}
										<div class="basket-product__desc{{#HIDE_MOBILE}} hidden-xs{{/HIDE_MOBILE}}" data-entity="basket-item-property">
						                    <div class="basket-product__desc-title">{{NAME}}:</div>
						                    {{#BRAND_LOGO}}
						                    	<a href="/catalog/brands/{{VALUE}}/" class="basket-product__brand" style="background-image: url('{{BRAND_LOGO}}')" title="{{BRAND_TITLE}}" data-column-property-code="{{CODE}}" data-entity="basket-item-property-column-value"></a>
						                    {{/BRAND_LOGO}}
						                    {{^BRAND_LOGO}}
												<a href="/catalog/brands/{{VALUE}}/" class="basket-product__brand empty__photo" title="{{BRAND_TITLE}}" data-column-property-code="{{CODE}}" data-entity="basket-item-property-column-value">{{BRAND_TITLE}}</a>
											{{/BRAND_LOGO}}
						                </div>
									{{/IS_TEXT}}
								{{/COLUMN_LIST}}
							<?
							break;
						}
					}
				}?>

            </div>

            <div class="basket-product__wrap-price">
                <div class="basket-product__price">
                    <div class="basket-product__price-desc"><?=Loc::getMessage('SBB_BASKET_ITEM_PRICE_FOR')?> {{MEASURE_RATIO}} {{MEASURE_TEXT}}:</div>
                    <div class="price-rub basket-product__price-rub">
                        <div class="price-rub__inner" id="basket-item-price-{{ID}}">{{{PRICE_FORMATED}}}</div>
                    </div>
                </div>

				<div class="count basket-product__count">
					<div class="count__inner js__count basket-item-block-amount{{#NOT_AVAILABLE}} disabled{{/NOT_AVAILABLE}}" data-entity="basket-item-quantity-block">
                        <input class="count__minus" type="button" value="-" data-entity="basket-item-quantity-minus">
						<input type="text" class="count__sum" value="{{QUANTITY}}"
							{{#NOT_AVAILABLE}} disabled="disabled"{{/NOT_AVAILABLE}}
							data-value="{{QUANTITY}}" data-entity="basket-item-quantity-field"
							id="basket-item-quantity-{{ID}}">
                        <input class="count__plus" type="button" value="+" data-entity="basket-item-quantity-plus">
                    </div>
				</div>

                <div class="basket-product__price">
                    <div class="basket-product__price-desc"><?=Loc::getMessage('SBB_TOTAL')?>:</div>
                    <div class="price-rub basket-product__price-rub">
                        <div class="price-rub__inner">{{{SUM_PRICE_FORMATED}}}</div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</script>
