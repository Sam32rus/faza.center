<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["PHONE_REGISTRATION"])
{
	CJSCore::Init('phone_auth');
}
?>

<div class="bx-auth">

<?
ShowMessage($arParams["~AUTH_RESULT"]);
?>

<?if($arResult["SHOW_FORM"]):?>

<form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform">
	<?if ($arResult["BACKURL"] <> ''): ?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<? endif ?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="CHANGE_PWD">

	<div class="popup__content">
		<div class="popup__title"><?=GetMessage("AUTH_CHANGE_PASSWORD")?></div>

	</div>


<?if($arResult["PHONE_REGISTRATION"]):?>
		<table>
			<tr>
				<td>1<?echo GetMessage("sys_auth_chpass_phone_number")?></td>
				<td>
					1<input type="text" value="<?=htmlspecialcharsbx($arResult["USER_PHONE_NUMBER"])?>" class="bx-auth-input" disabled="disabled" />
					<input type="hidden" name="USER_PHONE_NUMBER" value="<?=htmlspecialcharsbx($arResult["USER_PHONE_NUMBER"])?>" />
				</td>
			</tr>
			<tr>
				<td>1<span class="starrequired">*</span><?echo GetMessage("sys_auth_chpass_code")?></td>
				<td>1<input type="text" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" class="bx-auth-input" autocomplete="off" /></td>
			</tr>
		</table>
<?else:?>
			
			<div class="form-data">
				<div class="form-data__wrap">
					<input type="text" name="USER_LOGIN" maxlength="50" placeholder="<?=GetMessage("AUTH_LOGIN")?>" value="<?=$arResult["LAST_LOGIN"]?>" class="form-data__input js__default" />
				</div>
				<div class="form-data__error">Заполните поле</div>
			</div>
<?
	if($arResult["USE_PASSWORD"]):
?>
			<div class="form-data">
				<div class="form-data__wrap">
					<input type="password" name="USER_CURRENT_PASSWORD" maxlength="255" value="<?=$arResult["USER_CURRENT_PASSWORD"]?>" placeholder="<?echo GetMessage("sys_auth_changr_pass_current_pass")?>" class="form-data__input js__default" autocomplete="new-password" />
				</div>
				<div class="form-data__error">Заполните поле</div>
			</div>
<?
	else:
?>

			<div class="form-data">
				<div class="form-data__wrap">
					<input type="text" name="USER_CHECKWORD" maxlength="50" placeholder="<?=GetMessage("AUTH_CHECKWORD")?>" value="<?=$arResult["USER_CHECKWORD"]?>" class="form-data__input js__default" autocomplete="off" />
				</div>
				<div class="form-data__error">Заполните поле</div>
			</div>

<?
	endif
?>
<?endif?>

			<div class="form-data">
				<div class="form-data__wrap">
					<input type="password" name="USER_PASSWORD" maxlength="255" placeholder="<?=GetMessage("AUTH_NEW_PASSWORD_REQ")?>" value="<?=$arResult["USER_PASSWORD"]?>" class="form-data__input js__default" autocomplete="new-password" />
				</div>
				<div class="form-data__error">Заполните поле</div>
			</div>

<?if($arResult["SECURE_AUTH"]):?>
				<span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
				<noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
				</noscript>
<script type="text/javascript">
document.getElementById('bx_auth_secure').style.display = 'inline-block';
</script>
<?endif?>

		<div class="form-data">
			<div class="form-data__wrap">
				<input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" placeholder="<?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?>" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" class="form-data__input js__default" autocomplete="new-password" />
			</div>
			<div class="form-data__error">Заполните поле</div>
		</div>

		<?if($arResult["USE_CAPTCHA"]):?>
		
		<table>
			<tr>
				<td></td>
				<td>
					<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
				</td>
			</tr>
			<tr>
				<td><span class="starrequired">*</span><?echo GetMessage("system_auth_captcha")?></td>
				<td><input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" /></td>
			</tr>
		</table>

		<?endif?>

	<div class="form-data">
		<div class="form-data__wrap-submit">
			<input class="js__disabled-btn form-data__submit blue-button" type="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>">
		</div>
	</div>
</form>

<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
<p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>

<?if($arResult["PHONE_REGISTRATION"]):?>

<script type="text/javascript">
new BX.PhoneAuth({
	containerId: 'bx_chpass_resend',
	errorContainerId: 'bx_chpass_error',
	interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
	data:
		<?=CUtil::PhpToJSObject([
			'signedData' => $arResult["SIGNED_DATA"]
		])?>,
	onError:
		function(response)
		{
			var errorDiv = BX('bx_chpass_error');
			var errorNode = BX.findChildByClassName(errorDiv, 'errortext');
			errorNode.innerHTML = '';
			for(var i = 0; i < response.errors.length; i++)
			{
				errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br>';
			}
			errorDiv.style.display = '';
		}
});
</script>

<div id="bx_chpass_error" style="display:none"><?ShowError("error")?></div>

<div id="bx_chpass_resend"></div>

<?endif?>

<?endif?>

<div class="form-data">
	<a href="<?=$arResult["AUTH_AUTH_URL"]?>" class="popup__link"><?=GetMessage("AUTH_AUTH")?></div>
</div>

</div>