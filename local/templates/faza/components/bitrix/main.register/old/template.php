<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Localization\Loc;
if ($arResult["SHOW_SMS_FIELD"] == true) {
	CJSCore::Init('phone_auth');
}
?>

<div class="popup" data-popup-id="registration">
	<div class="popup__inner">
		<div class="popup__close" data-popup-close="registration"></div>
		<div class="popup__content">
			<? if ($USER->IsAuthorized()) : ?>
				<p><? echo GetMessage("MAIN_REGISTER_AUTH") ?></p>
			<? else : 
				if (count($arResult["ERRORS"]) > 0) :
					foreach ($arResult["ERRORS"] as $key => $error)
						if (intval($key) == 0 && $key !== 0)
							$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);

					ShowError(implode("<br />", $arResult["ERRORS"]));
				endif ?>

				<? if ($arResult["SHOW_SMS_FIELD"] == true) : ?>

					<form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform">
						<input type="hidden" name="TYPE" value="REGISTRATION"/>
						<input type="hidden" name="register_submit_button" value="Y"/>
						<?
						if ($arResult["BACKURL"] <> '') :
						?>
							<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
						<?
						endif;
						?>
						<input type="hidden" name="SIGNED_DATA" value="<?= htmlspecialcharsbx($arResult["SIGNED_DATA"]) ?>" />
						<table>
							<tbody>
								<tr>
									<td><? echo GetMessage("main_register_sms") ?><span class="starrequired">*</span></td>
									<td><input size="30" type="text" name="SMS_CODE" value="<?= htmlspecialcharsbx($arResult["SMS_CODE"]) ?>" autocomplete="off" /></td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td></td>
									<td><input type="submit" name="code_submit_button" value="<? echo GetMessage("main_register_sms_send") ?>" /></td>
								</tr>
							</tfoot>
						</table>
					</form>

					<script>
						new BX.PhoneAuth({
							containerId: 'bx_register_resend',
							errorContainerId: 'bx_register_error',
							interval: <?= $arResult["PHONE_CODE_RESEND_INTERVAL"] ?>,
							data: <?= CUtil::PhpToJSObject([
										'signedData' => $arResult["SIGNED_DATA"],
									]) ?>,
							onError: function(response) {
								var errorDiv = BX('bx_register_error');
								var errorNode = BX.findChildByClassName(errorDiv, 'errortext');
								errorNode.innerHTML = '';
								for (var i = 0; i < response.errors.length; i++) {
									errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br>';
								}
								errorDiv.style.display = '';
							}
						});
					</script>

					<div id="bx_register_error" style="display:none"><? ShowError("error") ?></div>

					<div id="bx_register_resend"></div>

				<? else : ?>
					<div class="popup__title"><?=Loc::getMessage("AUTH_REGISTER")?></div>
					<form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform" enctype="multipart/form-data">
						<?if ($arResult["BACKURL"] <> '') :?>
							<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
						<?endif;?>


								<? foreach ($arResult["SHOW_FIELDS"] as $FIELD) : 
									switch ($FIELD) {
										case "PASSWORD":?>
											<div class="form-data">
												<div class="form-data__wrap">
													<input class="form-data__input js__default" placeholder="<?= Loc::getMessage("REGISTER_FIELD_" . $FIELD) ?>" type="password" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off">
												</div>
												<div class="form-data__error">Заполните поле "<?= Loc::getMessage("REGISTER_FIELD_" . $FIELD) ?>"</div>
											</div>
											<?break;
										case "CONFIRM_PASSWORD":?>
											<div class="form-data">
												<div class="form-data__wrap">
													<input class="form-data__input js__default" placeholder="<?= Loc::getMessage("REGISTER_FIELD_" . $FIELD) ?>" type="password" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off">
												</div>
												<div class="form-data__error">Заполните поле "<?= Loc::getMessage("REGISTER_FIELD_" . $FIELD) ?>"</div>
											</div>
											<?break;
										case "LOGIN":?>
											<div class="form-data" style="display: none;">
												<div class="form-data__wrap">
													<input class="form-data__input js__default" id="login-registration" placeholder="<?= Loc::getMessage("REGISTER_FIELD_" . $FIELD) ?>" type="text" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off">
												</div>
												<div class="form-data__error">Заполните поле "<?= Loc::getMessage("REGISTER_FIELD_" . $FIELD) ?>"</div>
											</div>
											<?break;
										case "EMAIL":?>
											<div class="form-data">
												<div class="form-data__wrap">
													<input class="form-data__input js__email" id="email-registration" placeholder="<?= Loc::getMessage("REGISTER_FIELD_" . $FIELD) ?>" type="text" name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off">
												</div>
												<div class="form-data__error">Заполните поле "<?= Loc::getMessage("REGISTER_FIELD_" . $FIELD) ?>"</div>
											</div>
											<?break;
										case "NAME":?>
											<div class="form-data">
												<div class="form-data__wrap">
													<input class="form-data__input js__name" type="text" placeholder="<?=GetMessage("REGISTER_FIELD_".$FIELD)?>" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>">
												</div>
												<div class="form-data__error">Заполните поле "<?= Loc::getMessage("REGISTER_FIELD_" . $FIELD) ?>"</div>
											</div>
											<?break;
										default:?>
											<div class="form-data">
												<div class="form-data__wrap">
													<input class="form-data__input js__default" type="text" placeholder="<?=GetMessage("REGISTER_FIELD_".$FIELD)?>" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>">
												</div>
												<div class="form-data__error">Заполните поле "<?= Loc::getMessage("REGISTER_FIELD_" . $FIELD) ?>"</div>
											</div>
											<?
									}

								endforeach; ?>
								<? // ********************* User properties ***************************************************
								?>
								<? if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y") : ?>
									<tr>
										<td colspan="2"><?= trim($arParams["USER_PROPERTY_NAME"]) <> '' ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB") ?></td>
									</tr>
									<? foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField) : ?>
										<tr>
											<td><?= $arUserField["EDIT_FORM_LABEL"] ?>:<? if ($arUserField["MANDATORY"] == "Y") : ?><span class="starrequired">*</span><? endif; ?></td>
											<td>
												<? $APPLICATION->IncludeComponent(
													"bitrix:system.field.edit",
													$arUserField["USER_TYPE"]["USER_TYPE_ID"],
													array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"),
													null,
													array("HIDE_ICONS" => "Y")
												); ?></td>
										</tr>
									<? endforeach; ?>
								<? endif; ?>
								<? // ******************** /User properties ***************************************************
								?>
								<?
								/* CAPTCHA */
								if ($arResult["USE_CAPTCHA"] == "Y") {
								?>
									<div class="form-data">
										<div class="captcha">
											<b><?= GetMessage("REGISTER_CAPTCHA_TITLE") ?></b>
											<input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>" />
											<img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" />
										</div>
										<div class="captcha-word">
											<?= Loc::getMessage("REGISTER_CAPTCHA_PROMT") ?>:<span class="starrequired">*</span>
											<input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" />
										</div>
									</div>
								<?
								}
								/* !CAPTCHA */
								?>
								<div class="form-data">
									<input id="agreement-registration" class="form-data__checkbox js__checkbox" type="checkbox" name="agreement-registration" required="">
									<label for="agreement-registration" class="form-data__label">Я даю согласие на обработку своих <a href="/info/policy/">персональных данных</a>.</label>
								</div>
								<div class="form-data">
									<div class="form-data__wrap-submit">
										<input class="js__disabled-btn form-data__submit blue-button" type="submit" name="register_submit_button" value="<?= Loc::getMessage("AUTH_REGISTER") ?>" />
									</div>
								</div>
								<div class="popup__change-popup">
									<div class="popup__link" data-popup-btn="login"><?= Loc::getMessage("AUTH_BTN") ?></div>
								</div>
					</form>


				<? endif //$arResult["SHOW_SMS_FIELD"] == true 
				?>
			<? endif ?>
		</div>
	</div>
</div>
