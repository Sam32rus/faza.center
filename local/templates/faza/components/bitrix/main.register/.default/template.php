<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;

if ($arResult["SHOW_SMS_FIELD"] == true) {
	CJSCore::Init('phone_auth');
}
?>


<? if ($USER->IsAuthorized()) : ?>
	<p><? echo GetMessage("MAIN_REGISTER_AUTH") ?></p>
<? else : ?>
	<form method="post" name="regform">
		<input type="hidden" name="TYPE" value="REGISTRATION" />
		<div class="form-data">
			<div class="form-data__wrap">
				<input class="form-data__input js__name" placeholder="Имя" name="name" type="text" required="">
			</div>
			<div class="form-data__error" data-error="name">Заполните поле "Имя"</div>
		</div>
		<div class="form-data">
			<div class="form-data__wrap">
				<input class="form-data__input js__email" placeholder="Адрес электронной почты" name="email" type="text" required="">
			</div>
			<div class="form-data__error" data-error="email">Заполните поле "Адрес электронной почты"</div>
			<div class="form-data__error" data-error="email_is_used">Пользователь с таким E-mail уже зарегистрирован</div>
		</div>
		<div class="form-data">
			<div class="form-data__wrap">
				<input class="form-data__input js__default" placeholder="Пароль" name="password" type="password" required="">
			</div>
			<div class="form-data__error" data-error="password">Введите "Ваш пароль" (мин. 6 имволов)</div>
		</div>
		<div class="form-data">
			<div class="form-data__wrap">
				<input class="form-data__input js__default" placeholder="Повторите пароль" name="confirm_password" type="password" required="">
			</div>
			<div class="form-data__error" data-error="confirm_password">Заполните поле "Повторите пароль"</div>
		</div>
		<div class="form-data">
			<input id="agreement" class="form-data__checkbox js__checkbox" type="checkbox" name="agreement" required="">
			<label for="agreement" class="form-data__label">Я даю согласие на обработку своих <a href="/info/policy/">персональных данных</a>.</label>
		</div>
		<div class="form-data">
			<div class="form-data__wrap-submit">
				<button class="js__disabled-btn form-data__submit blue-button" type="submit">Зарегистрироваться</button>
			</div>
		</div>
		<div class="popup__change-popup">
			<div class="popup__link" data-popup-btn="login">Войти</div>
		</div>

	</form>
<? endif ?>