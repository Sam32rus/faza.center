<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arResult['SHOW_FIELDS'] = array( 
    'NAME',
    'LOGIN',
    'EMAIL',
    'PASSWORD',
    'CONFIRM_PASSWORD'
 );