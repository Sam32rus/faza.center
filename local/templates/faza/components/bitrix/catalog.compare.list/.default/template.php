<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$itemCount = count($arResult);
$needReload = (isset($_REQUEST["compare_list_reload"]) && $_REQUEST["compare_list_reload"] == "Y");
$idCompareCount = 'compareList'.$this->randString();
$obCompare = 'ob'.$idCompareCount;
?>
<div id="<?=$idCompareCount;?>" style="display: none;">
	<?
	if ($needReload)
	{
		$APPLICATION->RestartBuffer();
	}
	$frame = $this->createFrame($idCompareCount)->begin('');

	if ($itemCount > 0){
		echo $itemCount;
	}else{
		echo '0';
	}
	$frame->end();
	if ($needReload){
		die();
	}
	$currentPath = CHTTP::urlDeleteParams(
		$APPLICATION->GetCurPageParam(),
		array(
			$arParams['PRODUCT_ID_VARIABLE'],
			$arParams['ACTION_VARIABLE'],
			'ajax_action'
		),
		array("delete_system_params" => true)
	);

	$jsParams = array(
		'VISUAL' => array(
			'ID' => $idCompareCount,
		),
		'AJAX' => array(
			'url' => $currentPath,
			'params' => array(
				'ajax_action' => 'Y'
			),
			'reload' => array(
				'compare_list_reload' => 'Y'
			),
			'templates' => array(
				'delete' => (mb_strpos($currentPath, '?') === false ? '?' : '&').$arParams['ACTION_VARIABLE'].'=DELETE_FROM_COMPARE_LIST&'.$arParams['PRODUCT_ID_VARIABLE'].'='
			)
		),
		'POSITION' => array(
			'fixed' => $arParams['POSITION_FIXED'] == 'Y',
			'align' => array(
				'vertical' => $arParams['POSITION'][0],
				'horizontal' => $arParams['POSITION'][1]
			)
		)
	);
	?>
</div>
<script type="text/javascript">
var <?=$obCompare; ?> = new JCCatalogCompareList(<? echo CUtil::PhpToJSObject($jsParams, false, true); ?>)
</script>
