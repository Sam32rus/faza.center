<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="promo-sliders__big-slider">
    <div class="promo-big-slider js__promo-big-slider">
    	<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="promo-big-slider__item-slid" id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="background-image: url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>')" title="<?=$arItem['PREVIEW_PICTURE']['TITLE']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>">
				<?if($arItem['PROPERTIES']['POPUP_ID']['VALUE']):?>
					<span class="white-button promo-big-slider__button" data-popup-btn="<?=$arItem['PROPERTIES']['POPUP_ID']['VALUE']?>">Подробнее</span>
				<?else:?>
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="white-button promo-big-slider__button">Подробнее</a>
				<?endif;?>
			</div>
		<?endforeach;?>
    </div>
</div>
