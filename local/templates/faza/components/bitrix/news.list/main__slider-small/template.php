<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
global $USER;
?>

<div class="promo-sliders__small-slider">
    <div class="promo-small-slider js__promo-small-slider">
    	<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="promo-small-slider__item-slid" id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="background-image: url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>')" title="<?=$arItem['PREVIEW_PICTURE']['TITLE']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>"></div>
		<?endforeach;?>
    </div>
    <?if($USER->IsAuthorized()):?>
        <div class="white-button promo-small-slider__button"><a href="/personal/">Кабинет</a></div>
    <?else:?>
        <div class="white-button promo-small-slider__button" data-popup-btn="registration">Зарегистрироваться</div>
    <?endif;?>
</div>
