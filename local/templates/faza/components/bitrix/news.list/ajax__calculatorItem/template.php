<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
use Bitrix\Main\Localization\Loc;

if(!empty($arResult["ITEMS"])):?>
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="choice">
            <div class="choice__inner" style="background-image: url('<?=$arItem["DETAIL_PICTURE"]["SRC"] ? $arItem["DETAIL_PICTURE"]["SRC"] : $arItem["PREVIEW_PICTURE"]["SRC"]?>')">
            	<?foreach ($arItem['PROPERTIES']['DOTS']['VALUE'] as $key => $arPoint) :?>
            		<?if(!empty($arPoint['PROPERTIES']['ADDED_CSS']['VALUE']['TEXT'])):?>
            			<style>
            				<?=$arPoint['PROPERTIES']['ADDED_CSS']['VALUE']['TEXT']?>
            			</style>
            		<?endif;?>
            		<div class="choice__point<?=$arPoint['PROPERTIES']['CLASS_NAME']['VALUE'] ? ' ' . $arPoint['PROPERTIES']['CLASS_NAME']['VALUE'] : ''?> tooltip" id="point__<?=$arPoint['ID']?>">
                        <span class="tooltip__content">
                            <a href="<?=$arPoint['PROPERTIES']['LINK']['VALUE']?>" class="choice__tooltip-link"><?=$arPoint['PROPERTIES']['TITLE']['VALUE'] ? $arPoint['PROPERTIES']['TITLE']['VALUE'] : $arPoint['NAME']?></a>
                        </span>
                    </div>
            	<?endforeach;?>
            </div>
        </div>
	<?endforeach;?>
<?endif;?>
