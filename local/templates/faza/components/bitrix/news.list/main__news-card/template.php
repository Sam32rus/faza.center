<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>

<?if(!empty($arResult['ITEMS'])):?>
	<div class="news__right-block">
		<div class="news__wrap-title">
			<div class="news__title"><?=Loc::getMessage('NEWS_CARDS_TITLE');?></div>
			<a href="/info/news/" class="news__link"><?=Loc::getMessage('NEWS_CARDS_LINK');?></a>
		</div>
		<div class="news__wrap-articles">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="item-article news__item-article" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="item-article__inner">
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item-article__image" style="background-image: url(<?=$arItem['RESIZE_PICTURE']['src'] ? $arItem['RESIZE_PICTURE']['src'] : $arItem["PREVIEW_PICTURE"]["SRC"]?>)"></a>
						<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
							<div class="item-article__date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
						<?endif?>
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item-article__title"><?=$arItem["NAME"]?></a>
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item-article__description"><?=$arItem["PREVIEW_TEXT"]?></a>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
<?endif;?>
