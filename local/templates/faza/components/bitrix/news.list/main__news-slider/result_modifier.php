<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$imageWidth = 768;
$imageHeight = 488;

foreach ($arResult["ITEMS"] as $key => $arItem)
{
    $arResult["ITEMS"][$key]['RESIZE_PICTURE'] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], Array("width" => $imageWidth, "height" => $imageHeight));
}
?>