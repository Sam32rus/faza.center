<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>

<?if(!empty($arResult['ITEMS'])):?>
	<div class="news__left-block">
		<div class="news__title"><?=Loc::getMessage('NEWS_SLIDER_TITLE');?></div>
		<div class="news-slider">
			<div class="news-slider__inner js__news-slider">
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>

					<div class="news-slider__item-slid" style="background-image: url(<?=$arItem['RESIZE_PICTURE']['src'] ? $arItem['RESIZE_PICTURE']['src'] : $arItem["PREVIEW_PICTURE"]["SRC"]?>)" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news-slider__title"><?=$arItem["NAME"]?></a>
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news-slider__description"><?=$arItem["PREVIEW_TEXT"]?></a>
					</div>
				<?endforeach;?>
			</div>
		</div>
	</div>
	
<?endif;?>
