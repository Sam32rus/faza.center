<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if(count($arResult['ITEMS'])):?>
	<div class="anim-num blogs__anim-num">
	    <div class="anim-num__inner">
	        <div class="anim-num__wrap-numbers">
	            <div class="anim-num__line anim-num__line_black js__anim-num"></div>
	            <?foreach ($arResult['ITEMS'] as $key => $value):?>
	            	<div class="anim-num__item-number<?=$value == current($arResult['ITEMS']) ? ' active' : ''?>">0<?=$key + 1?></div>
	            <?endforeach;?>
	        </div>
	        <div class="anim-num__blue">
	            <div class="anim-num__line anim-num__line_white js__anim-num"></div>
	            <?foreach($arResult["ITEMS"] as $key => $arItem):?>
		            <div data-number="<?=$key + 1?>" class="anim-num__article<?=$arItem == current($arResult['ITEMS']) ? ' active' : ''?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="anim-num__title"><?=$arItem['NAME']?></a>
		                <?if($arItem['PREVIEW_TEXT']):?>
		                	<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="anim-num__description"><?=$arItem['PREVIEW_TEXT']?></a>
		                <?endif;?>
		            </div>
		        <?endforeach;?>
	            <?=$arResult['IBLOCK_DATA']['DESCRIPTION']?>
	        </div>
	    </div>
	</div>
<?endif;?>
