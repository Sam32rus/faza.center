<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$arResult['IBLOCK_DATA'] = \Bitrix\Iblock\IblockTable::getList(array(
  'filter' => ['ID' => $arParams["IBLOCK_ID"]],
  'select' => ['DESCRIPTION']
))->fetch();
?>
