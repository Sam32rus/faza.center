<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
use Bitrix\Main\Localization\Loc;
?>
<section class="advantages">
    <div class="content">
        <div class="advantages__inner js__margin-scroll">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<a href="<?=$arItem["DISPLAY_PROPERTIES"]['LINK']['VALUE']?>" class="advantages__item js__advantages-anim" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	                <span class="advantages__title"><?=$arItem["NAME"]?></span>
	                <span class="link-arrow advantages__link js__active"><?=Loc::getMessage('SHOW_BTN');?></span>
	                <span class="circles-pulse advantages__circle js__active">
	                    <span class="circles-pulse__c1">
	                        <span class="circles-pulse__c2"></span>
	                    </span>
	                </span>
	            </a>
			<?endforeach;?>
		</div>
    </div>
</section>
