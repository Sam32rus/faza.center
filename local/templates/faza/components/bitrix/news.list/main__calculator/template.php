<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
use Bitrix\Main\Localization\Loc;

$active = false;
if(!empty($arResult["ITEMS"])):?>
	<section class="repairs">
        <div class="content">
            <div class="repairs__inner">
                <div class="repairs__head">
                    <div class="repairs__title"><?=Loc::getMessage('REPAIR_TITLE');?></div>
                    <?/*<div class="repairs__link">
                        <a href="/calculator/" class="link-arrow-top"><?=Loc::getMessage('REPAIR_LINK');?></a>
                    </div>*/?>
                </div>
                <div class="repairs__content">
					<?foreach($arResult["ITEMS"] as $arItem):?>
						<?
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
						?>
						<?if(!$active):?>
							<div class="choice" data-target="active__room">
		                        <div class="choice__inner" style="background-image: url('<?=$arItem["DETAIL_PICTURE"]["SRC"] ? $arItem["DETAIL_PICTURE"]["SRC"] : $arItem["PREVIEW_PICTURE"]["SRC"]?>')">
		                        	<?foreach ($arItem['PROPERTIES']['DOTS']['VALUE'] as $key => $arPoint) :?>
		                        		<?if(!empty($arPoint['PROPERTIES']['ADDED_CSS']['VALUE']['TEXT'])):?>
		                        			<style>
		                        				<?=$arPoint['PROPERTIES']['ADDED_CSS']['VALUE']['TEXT']?>
		                        			</style>
		                        		<?endif;?>
		                        		<div class="choice__point<?=$arPoint['PROPERTIES']['CLASS_NAME']['VALUE'] ? ' ' . $arPoint['PROPERTIES']['CLASS_NAME']['VALUE'] : ''?> tooltip" id="point__<?=$arPoint['ID']?>">
			                                <span class="tooltip__content">
			                                    <a href="<?=$arPoint['PROPERTIES']['LINK']['VALUE']?>" class="choice__tooltip-link"><?=$arPoint['PROPERTIES']['TITLE']['VALUE'] ? $arPoint['PROPERTIES']['TITLE']['VALUE'] : $arPoint['NAME']?></a>
			                                </span>
			                            </div>
		                        	<?endforeach;?>
		                        </div>
		                    </div>
		                    <div class="repairs__rooms">
		                    	<?$active = $arItem['ID'];?>
						<?endif;?>

						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" data-id="<?=$arItem['ID']?>" class="repairs__item-room<?=$active == $arItem['ID'] ? ' active' : ''?>" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>')" id="<?=$this->GetEditAreaId($arItem['ID']);?>"></a>

						<?if($arItem == end($arResult['ITEMS'])):?>
							</div>
						<?endif;?>
					<?endforeach;?>
				</div>
            </div>
        </div>
    </section>
<?endif;?>
