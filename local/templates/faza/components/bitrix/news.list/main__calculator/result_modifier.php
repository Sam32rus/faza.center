<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
foreach ($arResult['ITEMS'] as $key => $arItem) {
    foreach ($arItem['PROPERTIES'] as $keyProperty => $arProperty) {
    	switch ($arProperty['PROPERTY_TYPE']) {
    		case 'F':
    			if($arProperty['MULTIPLE'] == 'N'){
    				$arResult['ITEMS'][$key]['PROPERTIES'][$keyProperty]['FILE'][] = CFile::GetFileArray($arProperty['VALUE']);
    			}else{
    				foreach ($arProperty['VALUE'] as $keyValue => $value) {
    					$arResult['ITEMS'][$key]['PROPERTIES'][$keyProperty]['FILE'][$keyValue] = CFile::GetFileArray($value);
    				}
    			}
    			break;
    		case 'L':
    			if($arProperty['MULTIPLE'] == 'Y'){
    				$arResult['ITEMS'][$key]['PROPERTIES'][$keyProperty]['DISPLAY_VALUE_HTML'] = "";
    				foreach ($arProperty['VALUE'] as $keyValue => $value) {
    					if($value != end($arProperty['VALUE'])){
    						$arResult['ITEMS'][$key]['PROPERTIES'][$keyProperty]['DISPLAY_VALUE_HTML'] .= $value . ", ";
    					}else{
    						$arResult['ITEMS'][$key]['PROPERTIES'][$keyProperty]['DISPLAY_VALUE_HTML'] .= $value;
    					}
    				}
    			}
    			break;
    		case 'E':
                $arSelect = ['ID', 'NAME', 'IBLOCK_ID'];
                $arFilter = ['ID' => $arProperty['VALUE'], 'IBLOCK_ID' => $arProperty['LINK_IBLOCK_ID']];
                $res = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
                unset($arResult['ITEMS'][$key]['PROPERTIES'][$keyProperty]['VALUE']);
                while($ob = $res->GetNextElement()){
                    $arFields = $ob->GetFields();
                    $arProps = $ob->GetProperties();
                    $arResult['ITEMS'][$key]['PROPERTIES'][$keyProperty]['VALUE'][$arFields['ID']] = $arFields;
    				$arResult['ITEMS'][$key]['PROPERTIES'][$keyProperty]['VALUE'][$arFields['ID']]['PROPERTIES'] = $arProps;
    			}
    			break;
    	}
    	if($arProperty["PROPERTY_TYPE"]=="S" && !empty($arProperty['USER_TYPE_SETTINGS']['TABLE_NAME'])):
    		if(CModule::IncludeModule('highloadblock')){
    			//получим данынй HL
    			$hldata = array_pop(HL\HighloadBlockTable::getList(array('filter' => array('TABLE_NAME'=>$arProperty["USER_TYPE_SETTINGS"]["TABLE_NAME"])))->fetchAll());
    			//затем инициализировать класс сущности
    			$entityClass = HL\HighloadBlockTable::compileEntity($hldata)->getDataClass();
    			$res = $entityClass::getList(array('select' => array('*'),'order' => array('ID' => 'ASC'),'filter' => array('UF_XML_ID' => $arProperty["VALUE"])))->fetchAll();
    			if(is_array($res)&&!empty($res)){
    				$arResult['ITEMS'][$key]['PROPERTIES'][$keyProperty]["VALUE"] = $res;
    			}
    		}
    	endif;
    }
}

