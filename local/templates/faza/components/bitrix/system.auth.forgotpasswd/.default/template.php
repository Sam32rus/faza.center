<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

if(!empty($arParams["API_SEND"])){
	echo json_encode(array(
		'status' => 'success',
		'popupID' => 'recovery-ok'
	));
	die();
}
?>

<form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?
if ($arResult["BACKURL"] <> '')
{
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
}
?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="SEND_PWD">

	<div class="form-data">
		<div class="form-data__wrap">
			<input class="form-data__input js__email" placeholder="Адрес электронной почты" name="USER_EMAIL" type="text">
			<input type="hidden" name="USER_LOGIN" />
		</div>
		<div class="form-data__error">Заполните поле "Адрес электронной почты"</div>
	</div>

<?if($arResult["PHONE_REGISTRATION"]):?>

	<div style="margin-top: 16px">
		<div><b><?=GetMessage("sys_forgot_pass_phone")?></b></div>
		<div><input type="text" name="USER_PHONE_NUMBER" value="<?=$arResult["USER_PHONE_NUMBER"]?>" /></div>
		<div><?echo GetMessage("sys_forgot_pass_note_phone")?></div>
	</div>
<?endif;?>

<?if($arResult["USE_CAPTCHA"]):?>
	<div style="margin-top: 16px">
		<div>
			<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
		</div>
		<div><?echo GetMessage("system_auth_captcha")?></div>
		<div><input type="text" name="captcha_word" maxlength="50" value="" /></div>
	</div>
<?endif?>
	<div class="form-data">
		<div class="form-data__wrap-submit">
			<input class="js__disabled-btn form-data__submit blue-button" type="submit" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" />
		</div>
	</div>
</form>
