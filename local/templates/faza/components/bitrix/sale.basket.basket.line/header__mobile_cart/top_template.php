<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');

$arParamsToDelete = array(
	"login",
	"login_form",
	"logout",
	"register",
	"forgot_password",
	"change_password",
	"confirm_registration",
	"confirm_code",
	"confirm_user_id",
	"logout_butt",
	"auth_service_id",
	"clear_cache",
	"backurl",
);

$currentUrl = urlencode($APPLICATION->GetCurPageParam("", $arParamsToDelete));
if ($arParams['AJAX'] == 'N')
{
	?><script type="text/javascript"><?=$cartId?>.currentUrl = '<?=$currentUrl?>';</script><?
}
else
{
	$currentUrl = '#CURRENT_URL#';
}

$pathToAuthorize = $arParams['PATH_TO_AUTHORIZE'];
$pathToAuthorize .= (mb_stripos($pathToAuthorize, '?') === false ? '?' : '&');
$pathToAuthorize .= 'login=yes&backurl='.$currentUrl;

if (!$compositeStub){
	if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y')){?>
		    <span class="user-menu__basket-sum"><?=$arResult['NUM_PRODUCTS']?></span>
		    Корзина
		<?if ($arParams['SHOW_TOTAL_PRICE'] == 'Y'){?>
			    <span class="user-menu__basket-sum"><?=$arResult['NUM_PRODUCTS']?></span>
			    Корзина
			<?
		}
	}
}
?>
