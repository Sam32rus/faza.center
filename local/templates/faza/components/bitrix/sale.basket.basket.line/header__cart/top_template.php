<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
<div id="tester" >
	<?if (!$compositeStub && $arParams['SHOW_AUTHOR'] == 'Y'):?>
		<?if ($USER->IsAuthorized()):
			$name = trim($USER->GetFullName());
			if (! $name)
				$name = trim($USER->GetLogin());
			if (mb_strlen($name) > 15)
				$name = mb_substr($name, 0, 12).'...';
			?>
		<?else:
			$arParamsToDelete = array(
				"login",
				"login_form",
				"logout",
				"register",
				"forgot_password",
				"change_password",
				"confirm_registration",
				"confirm_code",
				"confirm_user_id",
				"logout_butt",
				"auth_service_id",
				"clear_cache",
				"backurl",
			);

			$currentUrl = urlencode($APPLICATION->GetCurPageParam("", $arParamsToDelete));
			if ($arParams['AJAX'] == 'N')
			{
				?><script type="text/javascript"><?=$cartId?>.currentUrl = '<?=$currentUrl?>';</script><?
			}
			else
			{
				$currentUrl = '#CURRENT_URL#';
			}

			$pathToAuthorize = $arParams['PATH_TO_AUTHORIZE'];
			$pathToAuthorize .= (mb_stripos($pathToAuthorize, '?') === false ? '?' : '&');
			$pathToAuthorize .= 'login=yes&backurl='.$currentUrl;

			if ($arParams['SHOW_REGISTRATION'] === 'Y')
			{
				$pathToRegister = $arParams['PATH_TO_REGISTER'];
				$pathToRegister .= (mb_stripos($pathToRegister, '?') === false ? '?' : '&');
				$pathToRegister .= 'register=yes&backurl='.$currentUrl;
				?>
				<a href="<?=$pathToRegister?>">
					<?=GetMessage('TSB1_REGISTER')?>
				</a>
				<?
			}
			?>
		<?endif?>
	<?endif?>


	<?if (!$compositeStub){
		if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y')){?>
			<a href="<?=$arParams['PATH_TO_BASKET']?>" class="user-menu__item-point user-menu__item-point_basket">
		        <div class="user-menu__basket-sum"><?=$arResult['NUM_PRODUCTS']?></div>
		        Корзина
		    </a>
			<?if ($arParams['SHOW_TOTAL_PRICE'] == 'Y'){?>
				<a href="<?=$arParams['PATH_TO_BASKET']?>" class="user-menu__item-point user-menu__item-point_basket">
		        	<div class="user-menu__basket-sum"><?=$arResult['NUM_PRODUCTS']?></div>
		        	Корзина
		        </a>
			<?}
		}
	}

	/*if ($arParams['SHOW_PERSONAL_LINK'] == 'Y'):?>
		<!-- <div style="padding-top: 4px;">
		<span class="icon_info"></span>
		<a href="<?=$arParams['PATH_TO_PERSONAL']?>"><?=GetMessage('TSB1_PERSONAL')?></a>
		</div> -->
	<?endif*/?>
</div>
