<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
foreach ($arResult['PROPERTIES'] as $keyProperty => $arProperty) {
    switch ($arProperty['PROPERTY_TYPE']) {
        case 'F':
            if($arProperty['MULTIPLE'] == 'N'){
                $arResult['PROPERTIES'][$keyProperty]['FILE'][] = CFile::GetFileArray($arProperty['VALUE']);
            }else{
                foreach ($arProperty['VALUE'] as $keyValue => $value) {
                    $arResult['PROPERTIES'][$keyProperty]['FILE'][$keyValue] = CFile::GetFileArray($value);
                }
            }
            break;
        case 'L':
            if($arProperty['MULTIPLE'] == 'Y'){
                $arResult['PROPERTIES'][$keyProperty]['DISPLAY_VALUE_HTML'] = "";
                foreach ($arProperty['VALUE'] as $keyValue => $value) {
                    if($value != end($arProperty['VALUE'])){
                        $arResult['PROPERTIES'][$keyProperty]['DISPLAY_VALUE_HTML'] .= $value . ", ";
                    }else{
                        $arResult['PROPERTIES'][$keyProperty]['DISPLAY_VALUE_HTML'] .= $value;
                    }
                }
            }
            break;
        case 'E':
            $arSelect = ['ID', 'NAME', 'IBLOCK_ID'];
            $arFilter = ['ID' => $arProperty['VALUE'], 'IBLOCK_ID' => $arProperty['LINK_IBLOCK_ID']];
            $res = CIBlockElement::GetList([], $arFilter, false, [], $arSelect);
            unset($arResult['PROPERTIES'][$keyProperty]['VALUE']);
            while($ob = $res->GetNextElement()){
                $arFields = $ob->GetFields();
                $arProps = $ob->GetProperties();
                $arResult['PROPERTIES'][$keyProperty]['VALUE'][$arFields['ID']] = $arFields;
                $arResult['PROPERTIES'][$keyProperty]['VALUE'][$arFields['ID']]['PROPERTIES'] = $arProps;
            }
            break;
        case 'G':
            $rsSections = \Bitrix\Iblock\SectionTable::getList([
                'filter' => ['=ID' => $arProperty['VALUE'], 'IBLOCK_ID' => $arProperty['LINK_IBLOCK_ID'], '<DEPTH_LEVEL' => 3],
                'select' => ['ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'NAME', 'DEPTH_LEVEL', 'SECTION_PAGE_URL' => 'IBLOCK.SECTION_PAGE_URL']
            ]);
            unset($arResult['PROPERTIES'][$keyProperty]['VALUE']);
            while ($arSection = $rsSections->fetch()) {
                $arSection['SECTION_PAGE_URL'] = CIBlock::ReplaceDetailUrl($arSection['SECTION_PAGE_URL'], $arSection, false, 'S');

                if (array_key_exists($arSection['IBLOCK_SECTION_ID'], $arResult['PROPERTIES'][$keyProperty]['VALUE']))
                {   
                    $arResult['PROPERTIES'][$keyProperty]['VALUE'][$arSection['IBLOCK_SECTION_ID']]['SUBSECTIONS'][$arSection['ID']] = $arSection;
                }
                else
                {
                    $arResult['PROPERTIES'][$keyProperty]['VALUE'][$arSection['ID']] = $arSection;
                }
                
            }
            break;
    }
    if($arProperty["PROPERTY_TYPE"]=="S" && !empty($arProperty['USER_TYPE_SETTINGS']['TABLE_NAME'])):
        if(CModule::IncludeModule('highloadblock')){
            //получим данынй HL
            $hldata = array_pop(HL\HighloadBlockTable::getList(array('filter' => array('TABLE_NAME'=>$arProperty["USER_TYPE_SETTINGS"]["TABLE_NAME"])))->fetchAll());
            //затем инициализировать класс сущности
            $entityClass = HL\HighloadBlockTable::compileEntity($hldata)->getDataClass();
            $res = $entityClass::getList(array('select' => array('*'),'order' => array('ID' => 'ASC'),'filter' => array('UF_XML_ID' => $arProperty["VALUE"])))->fetchAll();
            if(is_array($res)&&!empty($res)){
                $arResult['PROPERTIES'][$keyProperty]["VALUE"] = $res;
            }
        }
    endif;
}