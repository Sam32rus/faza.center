<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="calculator__inner">
	<div class="hide-phone">
		<img src="/f/i/calculator/calculator.png" class="calculator__img">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="calculator__button calculator__button_<?=$arItem["CODE"]?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>"><?=$arItem["TITLE"] ? $arItem["TITLE"] : $arItem["NAME"]?></a>
			<div class="calculator__point calculator__point_<?=$arItem["CODE"]?>"></div>
		<?endforeach;?>
	</div>
	<div class="show-phone">
		<div class="calculator__wrap-img-button">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="calculator__img-button" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]['SRC']?>')">
					<span class="calculator__text-img-button"><?=$arItem["NAME"]?></span> 
				</a>
			<?endforeach;?>
		</div>
	</div>
</div>
