<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arSort = [
    'default' => [
        'BX_FIELD_CODE' => 'CATALOG_PRICE_1',
        'TITLE' => 'по цене',
        'ORDER' => 'asc,nulls',
        'ICON' => 'аscending',
    ],

    'price' => [
        'BX_FIELD_CODE' => 'CATALOG_PRICE_1',
        'TITLE' => 'по цене',
        'ORDER' => 'asc,nulls',
        'ICON' => 'аscending',
    ],

    'name' => [
        'BX_FIELD_CODE' => 'NAME',
        'TITLE' => 'по названию',
        'ORDER' => 'asc,nulls',
        'ICON' => 'аscending',
    ],

    'rating' => [
        'BX_FIELD_CODE' => 'RATING_TOTAL_VALUE',
        'TITLE' => 'по рейтингу',
        'ORDER' => 'desc,nulls',
        'ICON' => 'descending',
    ],

    // 'review' => [
    //     'BX_FIELD_CODE' => 'PROPERTY_forum_message_cnt',
    //     'TITLE' => 'по отзывам',
    //     'ORDER' => 'desc,nulls',
    //     'ICON' => 'descending',
    // ],
];

// получим текущую сортировку
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$currentSort = $request->getQuery('sort');
// если пуст параметр сортировки или в массиве сортировок нет переданной, то применим сортировку по умолчанию
if (!($currentSort && isset($arSort[$currentSort]))) {
    $currentSort = 'default';
}

// получим направление сортировки
$order = $request->getQuery('order');
switch ($order) {
    case 'asc,nulls':
        $arSort[$currentSort]['ORDER'] = 'desc,nulls';
        $arSort[$currentSort]['ICON'] = 'descending';
        break;
    case 'desc,nulls':
        $arSort[$currentSort]['ORDER'] = 'asc,nulls';
        $arSort[$currentSort]['ICON'] = 'аscending';
        break;
    case 'asc':
        $arSort[$currentSort]['ORDER'] = 'desc';
        $arSort[$currentSort]['ICON'] = 'descending';
        break;
    case 'desc':
        $arSort[$currentSort]['ORDER'] = 'asc';
        $arSort[$currentSort]['ICON'] = 'аscending';
        break;
}

// переопределим настройки сортировки для передачи в компонент
$arParams["ELEMENT_SORT_FIELD"] = $arSort[$currentSort]['BX_FIELD_CODE'];
$arParams["ELEMENT_SORT_ORDER"] = $arSort[$currentSort]['ORDER'];

global $arrFilter;
$arrFilter = ['SECTION_ID' => $arBrandData['UF_XML_ID']];

if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y') {
    $basketAction = isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '';
} else {
    $basketAction = isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '';
}
?>
<section class="result">
    <div class="content">
        <div class="sorting">
            <div class="sorting__inner">
                <? $APPLICATION->ShowViewContent('product__count'); ?>
                <div class="sorting__filter">
                    <div class="filter-button sorting__filter-button js__mobile-filter-open">Фильтр</div>
                    <? foreach ($arSort as $key => $sort) :
                        if ($key === 'default') {
                            continue;
                        } ?>

                        <a href="<? echo $APPLICATION->GetCurPageParam("sort={$key}&order={$sort['ORDER']}", array(
                                        "sort",
                                        "order",
                                    )); ?>" class="sorting__button<?= ($key === $currentSort) ? ' active' : '' ?> <?= $sort['ICON'] ? $sort['ICON'] : '' ?>" rel="nofollow"><?= $sort['TITLE'] ?></a>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
        <div class="result__inner">
        
            <!--PRODUCTS-->
            <? $intSectionID = $APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "catalog__products",
                Array(
                    'VIEW_TYPE' => 'brand',
                    "ACTION_VARIABLE" => "action",
                    "ADD_PICT_PROP" => "-",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "ADD_TO_BASKET_ACTION" => "ADD",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "BACKGROUND_IMAGE" => "-",
                    "BASKET_URL" => "/personal/basket.php",
                    "BROWSER_TITLE" => "-",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COMPATIBLE_MODE" => "Y",
                    "CONVERT_CURRENCY" => "N",
                    "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
                    "DETAIL_URL" => "",
                    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_COMPARE" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_FIELD2" => "id",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "ELEMENT_SORT_ORDER2" => "desc",
                    "ENLARGE_PRODUCT" => "STRICT",
                    "FILE_404" => "",
                    "FILTER_NAME" => "arrFilter",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                    "IBLOCK_ID" => "7",
                    "IBLOCK_TYPE" => "catalog",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "LABEL_PROP" => array(),
                    "LAZY_LOAD" => "N",
                    "LINE_ELEMENT_COUNT" => "5",
                    "LOAD_ON_SCROLL" => "N",
                    "MESSAGE_404" => "",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "META_DESCRIPTION" => "-",
                    "META_KEYWORDS" => "-",
                    "OFFERS_LIMIT" => "5",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "round",
                    "PAGER_TITLE" => "Товары",
                    "PAGE_ELEMENT_COUNT" => "20",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRICE_CODE" => array("RUB"),
                    "PRICE_VAT_INCLUDE" => "N",
                    "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                    "PRODUCT_SUBSCRIPTION" => "Y",
                    "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                    "RCM_TYPE" => "personal",
                    "SECTION_CODE" => "",
                    "SECTION_CODE_PATH" => "",
                    "SECTION_ID" => $_REQUEST["SECTION_ID"],
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "SECTION_URL" => "",
                    "SECTION_USER_FIELDS" => array("",""),
                    "SEF_MODE" => "N",
                    "SEF_RULE" => "",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "Y",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "Y",
                    "SHOW_ALL_WO_SECTION" => "Y",
                    "SHOW_CLOSE_POPUP" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_FROM_SECTION" => "N",
                    "SHOW_MAX_QUANTITY" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "SHOW_SLIDER" => "N",
                    "SLIDER_INTERVAL" => "3000",
                    "SLIDER_PROGRESS" => "N",
                    "TEMPLATE_THEME" => "blue",
                    "USE_ENHANCED_ECOMMERCE" => "N",
                    "USE_MAIN_ELEMENT_SECTION" => "N",
                    "USE_PRICE_COUNT" => "N",
                    "USE_PRODUCT_QUANTITY" => "N"
                )
            );?>
            <!--/PRODUCTS-->

            <!--COMPARE-->
            <?php if ($arParams["USE_COMPARE"] == "Y") {
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.compare.list",
                    "",
                    array(
                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "NAME" => $arParams["COMPARE_NAME"],
                        "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                        "COMPARE_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["compare"],
                        "ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action"),
                        "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                        'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
                        'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
                    ),
                    $component,
                    array("HIDE_ICONS" => "Y")
                );
            } ?>
            <!--/COMPARE-->
        </div>
    </div>
</section>