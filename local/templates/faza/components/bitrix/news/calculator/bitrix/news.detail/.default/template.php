<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
?>
<h1 class="title-page"><span class="content"><?=Loc::getMessage('CALCULATOR_DETAIL_TITLE', ['#CATEGORY#' => $arResult["NAME"]]);?></span></h1>

<section class="repairs">
	<div class="content">
		<div class="repairs__inner">
			<div class="repairs__content">
				<div class="choice hide-phone">
					<div class="choice__inner" style="background-image: url('<?=$arResult["DETAIL_PICTURE"]["SRC"]?>')">
						<?foreach ($arResult['PROPERTIES']['DOTS']['VALUE'] as $key => $arPoint) :?>
							<?if(!empty($arPoint['PROPERTIES']['ADDED_CSS']['VALUE']['TEXT'])):?>
								<style>
									<?=$arPoint['PROPERTIES']['ADDED_CSS']['VALUE']['TEXT']?>
								</style>
							<?endif;?>
							<div class="choice__point<?=$arPoint['PROPERTIES']['CLASS_NAME']['VALUE'] ? ' ' . $arPoint['PROPERTIES']['CLASS_NAME']['VALUE'] : ''?> tooltip" id="point__<?=$arPoint['ID']?>">
								<span class="tooltip__content">
									<a href="<?=$arPoint['PROPERTIES']['LINK']['VALUE']?>" class="choice__tooltip-link"><?=$arPoint['PROPERTIES']['TITLE']['VALUE'] ? $arPoint['PROPERTIES']['TITLE']['VALUE'] : $arPoint['NAME']?></a>
								</span>
							</div>
						<?endforeach;?>
					</div>
				</div>
				<?if (!empty($arResult['PROPERTIES']['SECTIONS']['VALUE'])):?>
					<div class="repairs__other">
						<div class="repairs__other-head">Вам также может быть интересно:</div>
						<ul class="repairs__other-ul">
							<?foreach($arResult['PROPERTIES']['SECTIONS']['VALUE'] as $section):?>
								<li class="repairs__other-li">
									<a href="<?=$section['SECTION_PAGE_URL']?>" class="repairs__item-other"><?=$section['NAME']?></a>
									<?if(!empty($section['SUBSECTIONS'])):?>
										<ul class="repairs__other-ul repairs__other-ul_child">
									<?endif;?>
									<?foreach($section['SUBSECTIONS'] as $subsection):?>
										<li class="repairs__other-li repairs__other-li_child"><a href="<?=$subsection['SECTION_PAGE_URL']?>" class="repairs__item-other"><?=$subsection['NAME']?></a></li>
									<?endforeach;?>
									<?if(!empty($section['SUBSECTIONS'])):?>
										</ul>
									<?endif;?>
								</li>
							<?endforeach;?>
						</ul>
					</div>
				<?endif;?>
			</div>
		</div>
	</div>
</section>