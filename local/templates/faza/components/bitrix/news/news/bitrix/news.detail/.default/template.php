<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<div class="article__wrap-head-img">
	<img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>" class="article__item-head-img">
</div>
<h1><?= $arResult['NAME'] ?></h1>
<? if ($arResult["NAV_RESULT"]) : ?>
	<? if ($arParams["DISPLAY_TOP_PAGER"]) : ?><?= $arResult["NAV_STRING"] ?><br /><? endif; ?>
<? echo $arResult["NAV_TEXT"]; ?>
<? if ($arParams["DISPLAY_BOTTOM_PAGER"]) : ?><br /><?= $arResult["NAV_STRING"] ?><? endif; ?>
<? elseif ($arResult["DETAIL_TEXT"] <> '') : ?>
	<? echo $arResult["DETAIL_TEXT"]; ?>
<? else : ?>
	<? echo $arResult["PREVIEW_TEXT"]; ?>
<? endif ?>