<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<section class="all-news">
	<div class="content">
		<div class="sorting">
			<div class="sorting__inner">
				<div class="sorting__found"><span><?= $arResult['NAV_RESULT']->NavRecordCount ?></span> новостей</div>
			</div>
		</div>

		<div class="all-news__inner">
			<? foreach ($arResult["ITEMS"] as $arItem) : ?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="item-article all-news__item-article" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
					<div class="item-article__inner">
						<? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])) : ?>
							<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="item-article__image" style="background-image: url('<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>')"></a>

						<? endif ?>
						<? if ($arParams["DISPLAY_DATE"] != "N" && $arItem["DISPLAY_ACTIVE_FROM"]) : ?>
							<span class="item-article__date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						<? endif ?>
						<? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]) : ?>
							<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="item-article__title"><?= $arItem["NAME"] ?></a>
						<? endif; ?>
						<? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]) : ?>
							<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="item-article__description"><?= $arItem["PREVIEW_TEXT"]; ?></a>
						<? endif; ?>
					</div>
				</div>
			<? endforeach; ?>
		</div>
		<? if ($arParams["DISPLAY_BOTTOM_PAGER"]) : ?>
			<?= $arResult["NAV_STRING"] ?>
		<? endif; ?>
	</div>
</section>