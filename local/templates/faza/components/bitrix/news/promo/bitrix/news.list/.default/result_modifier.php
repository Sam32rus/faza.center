<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;

$this->SetViewTarget('rows__count');
    echo '<div class="sorting__found"><span>' . $arResult['NAV_RESULT']->NavRecordCount . '</span> ' . getNumericCase($arResult['NAV_RESULT']->NavRecordCount, 'акция', 'акции', 'акций') . '</div>';
$this->EndViewTarget();

$arResult['SCRIPT'] = '';
foreach ($arResult['ITEMS'] as $key => $arItem)
{
    if(!empty($arItem['DATE_ACTIVE_FROM']) && !empty($arItem['DATE_ACTIVE_TO']))
    {
        $arResult['ITEMS'][$key]['ACTIVE_DATE_TEXT'] = Loc::getMessage('ACTIVE_DATE_FROM_TO', [
            '#FROM#' => strtolower(FormatDate("d F", MakeTimeStamp($arItem['DATE_ACTIVE_FROM']))),
            '#TO#' => strtolower(FormatDate("d F Y", MakeTimeStamp($arItem['DATE_ACTIVE_TO'])))
        ]);
    }
    elseif(!empty($arItem['DATE_ACTIVE_TO']))
    {
        $arResult['ITEMS'][$key]['ACTIVE_DATE_TEXT'] = Loc::getMessage('ACTIVE_DATE_TO', [
            '#TO#' => strtolower(FormatDate("d F Y", MakeTimeStamp($arItem['DATE_ACTIVE_TO'])))
        ]);
    }

    if(!empty($arItem['DATE_ACTIVE_TO']))
    {
        $arResult['SCRIPT'] .= "$('#counter-" . $arItem['ID'] . "').timeTo(new Date('" . FormatDate("m d Y H:i:s", MakeTimeStamp($arItem['DATE_ACTIVE_TO'])) . "'));";
    }
}
