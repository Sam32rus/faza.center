<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>
	<div class="promotions__title">Действующие акции</div>

	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="promo-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<span class="promo-item__inner">
				<span class="promo-item__img" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>')"></span>
				<span class="promo-item__content">
					<span class="promo-item__title"><?=$arItem["NAME"]?></span>
					<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["ACTIVE_DATE_TEXT"]):?>
						<span class="promo-item__date"><?=$arItem["ACTIVE_DATE_TEXT"]?></span>
					<?endif?>
					<span class="promo-item__desc"><?=$arItem["PREVIEW_TEXT"];?></span>
				</span>
				<span class="promo-item__time">
					<?if(!empty($arResult['SCRIPT'])):?>
						<span class="promo-item__title-time">До конца акции осталось:</span>
						<span id="counter-<?=$arItem['ID']?>" class="js__counter promo-item__counter"></span>
					<?endif;?>
					<div class="promo-item__all-product">Смотреть все товары акции</div>
				</span>
			</span>
		</a>
	<?endforeach;?>
<?endif;?>

<?
if(!empty($arResult['SCRIPT']))
{
	echo '<script>';
	echo $arResult['SCRIPT'];
	echo '</script>';
}
?>