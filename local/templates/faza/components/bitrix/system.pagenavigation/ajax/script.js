$(document).ready(function(){

    $(document).on('click', '.show_more', function(){

        var targetContainer = $('[data-target="element-container"]'),
            paginationContainer = $('[data-target="pagination-container"]'),
            url =  $('.show_more').attr('data-url');    //  URL, из которого будем брать элементы

        if (url !== undefined) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data){

                    //  Удаляем старую навигацию
                    $('.show_more').remove();

                    var elements = $(data).find('[data-target="element-container"] [data-entity="item"]'),  //  Ищем элементы
                        pagination = $(data).find('.show_more');//  Ищем навигацию

                    targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                    paginationContainer.append(pagination); //  добавляем навигацию следом

                }
            })
        }

    });

});