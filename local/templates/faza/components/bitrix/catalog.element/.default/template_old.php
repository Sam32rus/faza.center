<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES'])){
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
	'STICKER_ID' => $mainId.'_sticker',
	'BIG_SLIDER_ID' => $mainId.'_big_slider',
	'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId.'_slider_cont',
	'OLD_PRICE_ID' => $mainId.'_old_price',
	'PRICE_ID' => $mainId.'_price',
	'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
	'PRICE_TOTAL' => $mainId.'_price_total',
	'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
	'QUANTITY_ID' => $mainId.'_quantity',
	'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
	'QUANTITY_UP_ID' => $mainId.'_quant_up',
	'QUANTITY_MEASURE' => $mainId.'_quant_measure',
	'QUANTITY_LIMIT' => $mainId.'_quant_limit',
	'BUY_LINK' => $mainId.'_buy_link',
	'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
	'COMPARE_LINK' => $mainId.'_compare_link',
	'TREE_ID' => $mainId.'_skudiv',
	'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
	'OFFER_GROUP' => $mainId.'_set_group_',
	'BASKET_PROP_DIV' => $mainId.'_basket_prop',
	'SUBSCRIBE_LINK' => $mainId.'_subscribe',
	'TABS_ID' => $mainId.'_tabs',
	'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
	'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer)
	{
		if ($offer['MORE_PHOTO_COUNT'] > 1)
		{
			$showSliderControls = true;
			break;
		}
	}
}
else
{
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['PRODUCT']['SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}
?>
<?//showData($arResult);?>
<div class="bx-catalog-element bx-<?=$arParams['TEMPLATE_THEME']?>" id="<?=$itemIds['ID']?>" itemscope itemtype="http://schema.org/Product">
	<section class="info-product">
	    <div class="content">
	        <div class="info-product__inner">
	            <div class="rating info-product__rating">
	                <div class="rating__inner">
	                	<?foreach ($arParams['PRODUCT_PAY_BLOCK_ORDER'] as $blockName){
							switch ($blockName){
								case 'rating':
									if ($arParams['USE_VOTE_RATING'] === 'Y'){
										$APPLICATION->IncludeComponent(
											'bitrix:iblock.vote',
											'stars',
											array(
												'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
												'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
												'IBLOCK_ID' => $arParams['IBLOCK_ID'],
												'ELEMENT_ID' => $arResult['ID'],
												'ELEMENT_CODE' => '',
												'MAX_VOTE' => '5',
												'VOTE_NAMES' => array('1', '2', '3', '4', '5'),
												'SET_STATUS_404' => 'N',
												'DISPLAY_AS_RATING' => $arParams['VOTE_DISPLAY_AS_RATING'],
												'CACHE_TYPE' => $arParams['CACHE_TYPE'],
												'CACHE_TIME' => $arParams['CACHE_TIME']
											),
											$component,
											array('HIDE_ICONS' => 'Y')
										);
									}

									break;
							}
						}?>

						<div class="rating__info rating__heart hide-tablet">
			                <input id="<?=$actualItem['ID']?>" class="rating__heart-input" type="checkbox">
			                <label for="<?=$actualItem['ID']?>" class="rating__heart-label favorite__btn" data-item="<?=$actualItem['ID']?>">В избранное</label>
			            </div>

	                    <?if ($arParams['DISPLAY_COMPARE']){?>
	                    	<div class="rating__chart" id="<?=$itemIds['COMPARE_LINK']?>">
			                    <input id="compare__<?=$actualItem['ID']?>" class="rating__chart-input" type="checkbox" data-entity="compare-checkbox">
			                    <label for="compare__<?=$actualItem['ID']?>" class="rating__chart-label" data-entity="compare-title"><?=$arParams['MESS_BTN_COMPARE']?></label>
			                </div>
						<?}?>
	                </div>
	            </div>
	            <div class="share info-product__share" data-popup-btn="share"><?=Loc::getMessage('SHARE_BTN');?></div>
	        </div>
	    </div>
	</section>

	<section class="card-product">
	    <div class="content">
	        <div class="card-product__inner">
	        	<div style="display: none;" class="product-item-detail-slider-container" id="<?=$itemIds['BIG_SLIDER_ID']?>">
					<span class="product-item-detail-slider-close" data-entity="close-popup"></span>
					<div class="product-item-detail-slider-block
						<?=($arParams['IMAGE_RESOLUTION'] === '1by1' ? 'product-item-detail-slider-block-square' : '')?>"
						data-entity="images-slider-block">
						<span class="product-item-detail-slider-left" data-entity="slider-control-left" style="display: none;"></span>
						<span class="product-item-detail-slider-right" data-entity="slider-control-right" style="display: none;"></span>
						<div class="product-item-label-text <?=$labelPositionClass?>" id="<?=$itemIds['STICKER_ID']?>"
							<?=(!$arResult['LABEL'] ? 'style="display: none;"' : '' )?>>
							<?
							if ($arResult['LABEL'] && !empty($arResult['LABEL_ARRAY_VALUE']))
							{
								foreach ($arResult['LABEL_ARRAY_VALUE'] as $code => $value)
								{
									?>
									<div<?=(!isset($arParams['LABEL_PROP_MOBILE'][$code]) ? ' class="hidden-xs"' : '')?>>
										<span title="<?=$value?>"><?=$value?></span>
									</div>
									<?
								}
							}
							?>
						</div>
						<?
						if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
						{
							if ($haveOffers)
							{
								?>
								<div class="product-item-label-ring <?=$discountPositionClass?>" id="<?=$itemIds['DISCOUNT_PERCENT_ID']?>"
									style="display: none;">
								</div>
								<?
							}
							else
							{
								if ($price['DISCOUNT'] > 0)
								{
									?>
									<div class="product-item-label-ring <?=$discountPositionClass?>" id="<?=$itemIds['DISCOUNT_PERCENT_ID']?>"
										title="<?=-$price['PERCENT']?>%">
										<span><?=-$price['PERCENT']?>%</span>
									</div>
									<?
								}
							}
						}
						?>
						<div class="product-item-detail-slider-images-container" data-entity="images-container">
							<?
							if (!empty($actualItem['MORE_PHOTO']))
							{
								foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
								{
									?>
									<div class="product-item-detail-slider-image<?=($key == 0 ? ' active' : '')?>" data-entity="image" data-id="<?=$photo['ID']?>">
										<img src="<?=$photo['SRC']?>" alt="<?=$alt?>" title="<?=$title?>"<?=($key == 0 ? ' itemprop="image"' : '')?>>
									</div>
									<?
								}
							}

							if ($arParams['SLIDER_PROGRESS'] === 'Y')
							{
								?>
								<div class="product-item-detail-slider-progress-bar" data-entity="slider-progress-bar" style="width: 0;"></div>
								<?
							}
							?>
						</div>
					</div>
					<?
					if ($showSliderControls)
					{
						if ($haveOffers)
						{
							foreach ($arResult['OFFERS'] as $keyOffer => $offer)
							{
								if (!isset($offer['MORE_PHOTO_COUNT']) || $offer['MORE_PHOTO_COUNT'] <= 0)
									continue;

								$strVisible = $arResult['OFFERS_SELECTED'] == $keyOffer ? '' : 'none';
								?>
								<div class="product-item-detail-slider-controls-block" id="<?=$itemIds['SLIDER_CONT_OF_ID'].$offer['ID']?>" style="display: <?=$strVisible?>;">
									<?
									foreach ($offer['MORE_PHOTO'] as $keyPhoto => $photo)
									{
										?>
										<div class="product-item-detail-slider-controls-image<?=($keyPhoto == 0 ? ' active' : '')?>"
											data-entity="slider-control" data-value="<?=$offer['ID'].'_'.$photo['ID']?>">
											<img src="<?=$photo['SRC']?>">
										</div>
										<?
									}
									?>
								</div>
								<?
							}
						}
						else
						{
							?>
							<div class="product-item-detail-slider-controls-block" id="<?=$itemIds['SLIDER_CONT_ID']?>">
								<?
								if (!empty($actualItem['MORE_PHOTO']))
								{
									foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
									{
										?>
										<div class="product-item-detail-slider-controls-image<?=($key == 0 ? ' active' : '')?>"
											data-entity="slider-control" data-value="<?=$photo['ID']?>">
											<img src="<?=$photo['SRC']?>">
										</div>
										<?
									}
								}
								?>
							</div>
							<?
						}
					}
					?>
				</div>

	            <!--  Фото товара-->
	            <div class="card-product__image-product image-product">
	                <div class="image-product__inner">
	                    <div class="image-product__slider_navigation js__image-product-nav">
	                    	<?if (!empty($actualItem['MORE_PHOTO_NAV'])){
								foreach ($actualItem['MORE_PHOTO_NAV'] as $key => $photo){?>
									<div class="image-product__item-nav">
			                            <img class="image-product__img-nav" src="<?=$photo['src']?>" alt="<?=$alt?>" title="<?=$title?>">
			                        </div>
								<?}
							}?>
	                    </div>

	                    <div class="image-product__slider js__image-product">
	                    	<?if (!empty($actualItem['MORE_PHOTO_PREVIEW'])){
								foreach ($actualItem['MORE_PHOTO_PREVIEW'] as $key => $photo){?>
									<div class="image-product__item">
			                            <img class="image-product__img js__zoom" src="<?=$photo['src']?>" data-zoom-image="<?=$actualItem['MORE_PHOTO'][$key]['SRC']?>" alt="<?=$alt?>" title="<?=$title?>">
			                        </div>
								<?}
							}?>
	                    </div>
	                </div>
	            </div>
	            <!--  /Фото товара-->

	            <!--  Информация о продукте, для планшета и мобильного-->
	            <section class="info-product-tablet show-tablet" <?/*id="<?=$itemIds['SMALL_CARD_PANEL_ID']?>"*/?>>
	                <div class="title-page info-product-tablet__title-page"><?=$name?></div>

	                <?foreach ($arParams['PRODUCT_PAY_BLOCK_ORDER'] as $blockName){
						switch ($blockName){
							case 'rating':
								if ($arParams['USE_VOTE_RATING'] === 'Y'){
									$APPLICATION->IncludeComponent(
										'bitrix:iblock.vote',
										'stars__mobile',
										array(
											'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
											'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
											'IBLOCK_ID' => $arParams['IBLOCK_ID'],
											'ELEMENT_ID' => $arResult['ID'],
											'ELEMENT_CODE' => '',
											'MAX_VOTE' => '5',
											'VOTE_NAMES' => array('1', '2', '3', '4', '5'),
											'SET_STATUS_404' => 'N',
											'DISPLAY_AS_RATING' => $arParams['VOTE_DISPLAY_AS_RATING'],
											'CACHE_TYPE' => $arParams['CACHE_TYPE'],
											'CACHE_TIME' => $arParams['CACHE_TIME']
										),
										$component,
										array('HIDE_ICONS' => 'Y')
									);
								}

								break;
						}
					}?>

	                <?if($actualItem['DISPLAY_PROPERTIES']['artikul']['VALUE']):?>
	                	<div class="info-product-tablet__code"><?=$actualItem['DISPLAY_PROPERTIES']['artikul']['NAME']?>: <span><?=$actualItem['DISPLAY_PROPERTIES']['artikul']['VALUE']?></span></div>
	                <?endif;?>

	                <div class="price-rub info-product-tablet__price-rub">
	                    <div class="price-rub__inner"><?=$price['RATIO_PRICE']?></div>
	                </div>
	                <?/*if ($showAddBtn){?>
						<td rowspan="2" class="product-item-detail-short-card-btn"
							style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;"
							data-entity="panel-add-button">
							<a class="blue-button blue-button_basket order__blue-button <?=$showButtonClassName?> product-item-detail-buy-button"
								id="<?=$itemIds['ADD_BASKET_LINK']?>"
								href="javascript:void(0);">
								<span><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></span>
							</a>
						</td>
					<?}

					if ($showBuyBtn){?>
						<td rowspan="2" class="product-item-detail-short-card-btn"
							style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;"
							data-entity="panel-buy-button">
							<a class="transparent-button order__transparent-button product-item-detail-buy-button" id="<?=$itemIds['BUY_LINK']?>"
								href="javascript:void(0);">
								<span><?=$arParams['MESS_BTN_BUY']?></span>
							</a>
						</td>
					<?}*/?>
					<div class="product-item-detail-short-card-btn"
						style="display: <?=(!$actualItem['CAN_BUY'] ? '' : 'none')?>;"
						data-entity="panel-not-available-button">
						<a class="btn btn-link product-item-detail-buy-button" href="javascript:void(0)"
							rel="nofollow">
							<?=$arParams['MESS_NOT_AVAILABLE']?>
						</a>
					</div>
	            </section><?/**/?>
	            <!--  /Информация о продукте, для планшета и мобильного-->

	            <!-- О товаре-->
	            <div class="card-product__about-product hide-tablet">
	                <div class="card-product__about-list">
	                    <div class="deploy">
	                    	<?foreach ($arParams['PRODUCT_INFO_BLOCK_ORDER'] as $blockName){
								switch ($blockName){
									case 'props':
										if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']){?>
											<?$visiblePropsCount = 7;?>
											<div class="card-product__about-title"><?=Loc::getMessage('ABOUT_PRODUCT');?></div>

											<?if (!empty($arResult['DISPLAY_PROPERTIES'])){
												foreach ($arResult['DISPLAY_PROPERTIES'] as $property){
													if (isset($arParams['MAIN_BLOCK_PROPERTY_CODE'][$property['CODE']])){?>
														<?if($visiblePropsCount == 0):?>
															<div class="deploy__content" style="display: none">
															<?$visiblePropsCount--;?>
														<?endif;?>
														<div class="card-product__item-point">
															<?=$property['NAME']?>
															<span class="card-product__item-desc js__shorten"><?=(is_array($property['DISPLAY_VALUE'])
																? implode(' / ', $property['DISPLAY_VALUE'])
																: $property['DISPLAY_VALUE'])?></span>
														</div>
														<?if($property == end($arResult['DISPLAY_PROPERTIES']) && $visiblePropsCount < 0):?>
															</div>
                        									<div class="catalog__add-brand catalog__add-brand_arrow js__deploy-toggle" data-deploy-open="Скрыть" data-deploy-close="Все характеристики"><?=Loc::getMessage('ALL_PROPERTIES');?></div>
														<?endif;?>
													<?
													$visiblePropsCount--;
													}
												}
												unset($property);
											}?>

										<?}

										break;
								}
							}?>
	                    </div>
	                </div>
	                <div class="card-product__about-order">
	                    <div class="order">
	                        <div class="order__inner">
	                        	<?if($actualItem['DISPLAY_PROPERTIES']['artikul']['VALUE']):?>
				                	<div class="order__code"><?=$actualItem['DISPLAY_PROPERTIES']['artikul']['NAME']?>: <span><?=$actualItem['DISPLAY_PROPERTIES']['artikul']['VALUE']?></span></div>
				                <?endif;?>

				                <?foreach ($arParams['PRODUCT_PAY_BLOCK_ORDER'] as $blockName){
									switch ($blockName)
									{
										case 'price':
											?>
											<div class="price-rub order__price-rub">
												<?if ($arParams['SHOW_OLD_PRICE'] === 'Y'){?>
													<div class="product-item-detail-price-old" id="<?=$itemIds['OLD_PRICE_ID']?>"
														style="display: <?=($showDiscount ? '' : 'none')?>;">
														<?=($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '')?>
													</div>
												<?}?>

												<div class="price-rub__inner" id="<?=$itemIds['PRICE_ID']?>"><?=$price['RATIO_PRICE']?></div>

												<?if ($arParams['SHOW_OLD_PRICE'] === 'Y'){?>
													<div class="item_economy_price" id="<?=$itemIds['DISCOUNT_PRICE_ID']?>" style="display: <?=($showDiscount ? '' : 'none')?>;">
														<?if ($showDiscount){
															echo Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO2', array('#ECONOMY#' => $price['PRINT_RATIO_DISCOUNT']));
														}?>
													</div>
												<?}?>
											</div>
											<?
											break;

										case 'priceRanges':
											if ($arParams['USE_PRICE_COUNT'])
											{
												$showRanges = !$haveOffers && count($actualItem['ITEM_QUANTITY_RANGES']) > 1;
												$useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';
												?>
												<div class="product-item-detail-info-container"
													<?=$showRanges ? '' : 'style="display: none;"'?>
													data-entity="price-ranges-block">
													<div class="product-item-detail-info-container-title">
														<?=$arParams['MESS_PRICE_RANGES_TITLE']?>
														<span data-entity="price-ranges-ratio-header">
															(<?=(Loc::getMessage(
																'CT_BCE_CATALOG_RATIO_PRICE',
																array('#RATIO#' => ($useRatio ? $measureRatio : '1').' '.$actualItem['ITEM_MEASURE']['TITLE'])
															))?>)
														</span>
													</div>
													<dl class="product-item-detail-properties" data-entity="price-ranges-body">
														<?
														if ($showRanges)
														{
															foreach ($actualItem['ITEM_QUANTITY_RANGES'] as $range)
															{
																if ($range['HASH'] !== 'ZERO-INF')
																{
																	$itemPrice = false;

																	foreach ($arResult['ITEM_PRICES'] as $itemPrice)
																	{
																		if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
																		{
																			break;
																		}
																	}

																	if ($itemPrice)
																	{
																		?>
																		<dt>
																			<?
																			echo Loc::getMessage(
																					'CT_BCE_CATALOG_RANGE_FROM',
																					array('#FROM#' => $range['SORT_FROM'].' '.$actualItem['ITEM_MEASURE']['TITLE'])
																				).' ';

																			if (is_infinite($range['SORT_TO']))
																			{
																				echo Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
																			}
																			else
																			{
																				echo Loc::getMessage(
																					'CT_BCE_CATALOG_RANGE_TO',
																					array('#TO#' => $range['SORT_TO'].' '.$actualItem['ITEM_MEASURE']['TITLE'])
																				);
																			}
																			?>
																		</dt>
																		<dd><?=($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE'])?></dd>
																		<?
																	}
																}
															}
														}
														?>
													</dl>
												</div>
												<?
												unset($showRanges, $useRatio, $itemPrice, $range);
											}

											break;

										case 'quantityLimit':
											if ($arParams['SHOW_MAX_QUANTITY'] !== 'N')
											{
												if ($haveOffers)
												{
													?>
													<div class="product-item-detail-info-container" id="<?=$itemIds['QUANTITY_LIMIT']?>" style="display: none;">
														<div class="product-item-detail-info-container-title">
															<?=$arParams['MESS_SHOW_MAX_QUANTITY']?>:
															<span class="product-item-quantity" data-entity="quantity-limit-value"></span>
														</div>
													</div>
													<?
												}
												else
												{
													if (
														$measureRatio
														&& (float)$actualItem['PRODUCT']['QUANTITY'] > 0
														&& $actualItem['CHECK_QUANTITY']
													)
													{
														?>
														<div class="product-item-detail-info-container" id="<?=$itemIds['QUANTITY_LIMIT']?>">
															<div class="product-item-detail-info-container-title">
																<?=$arParams['MESS_SHOW_MAX_QUANTITY']?>:
																<span class="product-item-quantity" data-entity="quantity-limit-value">
																	<?
																	if ($arParams['SHOW_MAX_QUANTITY'] === 'M')
																	{
																		if ((float)$actualItem['PRODUCT']['QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR'])
																		{
																			echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
																		}
																		else
																		{
																			echo $arParams['MESS_RELATIVE_QUANTITY_FEW'];
																		}
																	}
																	else
																	{
																		echo $actualItem['PRODUCT']['QUANTITY'].' '.$actualItem['ITEM_MEASURE']['TITLE'];
																	}
																	?>
																</span>
															</div>
														</div>
														<?
													}
												}
											}

											break;

										case 'quantity':
											if ($arParams['USE_PRODUCT_QUANTITY'])
											{
												?>
												<div class="product-item-detail-info-container" style="<?=(!$actualItem['CAN_BUY'] ? 'display: none;' : '')?>"
													data-entity="quantity-block">
													<div class="product-item-detail-info-container-title"><?=Loc::getMessage('CATALOG_QUANTITY')?></div>
													<div class="product-item-amount">
														<div class="product-item-amount-field-container">
															<span class="product-item-amount-field-btn-minus no-select" id="<?=$itemIds['QUANTITY_DOWN_ID']?>"></span>
															<input class="product-item-amount-field" id="<?=$itemIds['QUANTITY_ID']?>" type="number"
																value="<?=$price['MIN_QUANTITY']?>">
															<span class="product-item-amount-field-btn-plus no-select" id="<?=$itemIds['QUANTITY_UP_ID']?>"></span>
															<span class="product-item-amount-description-container">
																<span id="<?=$itemIds['QUANTITY_MEASURE']?>">
																	<?=$actualItem['ITEM_MEASURE']['TITLE']?>
																</span>
																<span id="<?=$itemIds['PRICE_TOTAL']?>"></span>
															</span>
														</div>
													</div>
												</div>
												<?
											}

											break;

										case 'buttons':
											?>
											<div data-entity="main-button-container">
												<div id="<?=$itemIds['BASKET_ACTIONS_ID']?>" style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;">
													<?if ($showAddBtn){?>
														<button class="blue-button blue-button_basket order__blue-button" id="<?=$itemIds['ADD_BASKET_LINK']?>">
															<?=$arParams['MESS_BTN_ADD_TO_BASKET']?>
														</button>
													<?}?>
													<button class="transparent-button order__transparent-button">
														<?=$arParams['MESS_BTN_BUY']?>
													</button>
												</div>

												<?if ($showSubscribe){?>
													<div class="product-item-detail-info-container">
														<?
														$APPLICATION->IncludeComponent(
															'bitrix:catalog.product.subscribe',
															'',
															array(
																'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
																'PRODUCT_ID' => $arResult['ID'],
																'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
																'BUTTON_CLASS' => 'btn btn-default product-item-detail-buy-button',
																'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
																'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
															),
															$component,
															array('HIDE_ICONS' => 'Y')
														);
														?>
													</div>
												<?}?>

												<div class="product-item-detail-info-container">
													<a class="btn btn-link product-item-detail-buy-button" id="<?=$itemIds['NOT_AVAILABLE_MESS']?>"
														href="javascript:void(0)"
														rel="nofollow" style="display: <?=(!$actualItem['CAN_BUY'] ? '' : 'none')?>;">
														<?=$arParams['MESS_NOT_AVAILABLE']?>
													</a>
												</div>
											</div>
											<?
											break;
									}
								}?>
	                        </div>
	                    </div>
	                </div>

	            </div>
	            <!-- /О товаре-->
	        </div>
	    </div>
	</section>

	<section class="product-tabs">
		<?$active = false;?>
	    <div class="content">
	        <div class="product-tabs__inner">
	            <div class="tabs js__tabs">
	            	<ul class="product-tabs__switch js__tabs-caption">
						<?if ($showDescription){?>
							<li class="product-item-detail-tab switch-tab active" data-entity="tab" data-value="description">
								<a href="javascript:void(0);" class="product-item-detail-tab-link">
									<span><?=$arParams['MESS_DESCRIPTION_TAB']?></span>
								</a>
							</li>
							<?$active = 1;?>
						<?}?>
						<?if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']){?>
							<li class="product-item-detail-tab switch-tab<?=!$active ? ' active' : '';?>" data-entity="tab" data-value="properties">
								<a href="javascript:void(0);" class="product-item-detail-tab-link">
									<span><?=$arParams['MESS_PROPERTIES_TAB']?></span>
								</a>
							</li>
							<?!$active ? $active = 2 : true;?>
						<?}?>
						<?if ($arParams['USE_COMMENTS'] === 'Y'){?>
							<li class="product-item-detail-tab switch-tab<?=!$active ? ' active' : '';?>" data-entity="tab" data-value="comments">
								<a href="javascript:void(0);" class="product-item-detail-tab-link">
									<span><?=$arParams['MESS_COMMENTS_TAB']?></span>
								</a>
							</li>
							<?!$active ? $active = 3 : true;?>
						<?}?>
						<?if(!empty($arResult['DELIVERY_CONTENT'])){?>
							<li class="product-item-detail-tab switch-tab<?=!$active ? ' active' : '';?>" data-entity="tab" data-value="dalivery">
								<a href="javascript:void(0);" class="product-item-detail-tab-link">
									<span><?=Loc::getMessage('MESS_DELIVERY_TAB')?></span>
								</a>
							</li>
							<?!$active ? $active = 4 : true;?>
						<?}?>
					</ul>
					<?if ($showDescription){?>
						<div class="tabs__content product-tabs__content<?=$active == '1' ? ' active' : '';?>" data-entity="tab-container" data-value="description" itemprop="description">
							<div class="description-product">
								<div class="description-product__inner">
									<div class="description-product__title">Описание</div>
									<div class="description-product__content">
										<?=$arResult['~DETAIL_TEXT'] ? $arResult['~DETAIL_TEXT'] : $arResult['~PREVIEW_TEXT']?>
									</div>
								</div>
							</div>
						</div>
					<?}?>
					<?if(!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']){?>
						<div class="tabs__content product-tabs__content<?=$active == '2' ? ' active' : '';?>" data-entity="tab-container" data-value="properties">
							<div class="description-product">
								<div class="description-product__inner">
									<div class="description-product__title"><?=Loc::getMessage('MESS_PROPERTIES_TAB_TITLE')?></div>
									<div class="description-product__content">
										<div class="specifications description-product__specifications">
											<div class="specifications__inner">
												<div class="specifications__list">
													<?foreach ($arParams['PRODUCT_INFO_BLOCK_ORDER'] as $blockName){
														switch ($blockName){
															case 'props':
																if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']){?>
																	<?if (!empty($arResult['DISPLAY_PROPERTIES'])){
																		foreach ($arResult['DISPLAY_PROPERTIES'] as $property){
																			if (isset($arParams['MAIN_BLOCK_PROPERTY_CODE'][$property['CODE']])){?>
																				<div class="specifications__item-point">
																					<div class="specifications__point-left"><?=$property['NAME']?></div>
																					<div class="specifications__line"></div>
																					<div class="specifications__point-right"><?=(is_array($property['DISPLAY_VALUE'])
																							? implode(' / ', $property['DISPLAY_VALUE'])
																							: $property['DISPLAY_VALUE'])?></div>
																				</div>
																			<?
																			}
																		}
																		unset($property);
																	}
																}
																break;
														}
													}?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?}?>
					<?if ($arParams['USE_COMMENTS'] === 'Y'){?>
						<div class="tabs__content product-tabs__content<?=$active == '3' ? ' active' : '';?>" data-entity="tab-container" data-value="comments">

							<div class="description-product">
								<div class="description-product__inner">
									<div class="description-product__title">Отзывы</div>
									<?if(!empty($arResult['REVIEWS'])):?>
										<div class="appraisals description-product__appraisals">
											<div class="appraisals__inner">
												<div class="appraisals__title">Рейтинг</div>
												<div class="rating appraisals__rating">
													<div class="rating__inner">
														<div class="rating__star" data-rating="<?=$arResult['REVIEWS_RATING']?>">
															<div class="rating__fill"></div>
														</div>
													</div>
												</div>

												<?foreach($arResult['REVIEWS_STATISTIC']['PERCENT'] as $keyStatistic => $value):?>
													<div class="appraisals__item-data">
														<div class="appraisals__grade"><?=$keyStatistic?></div>
														<div class="appraisals__line">
															<div style="width: <?=$value?>%" class="appraisals__fill"></div>
														</div>
														<div class="appraisals__sum"><?=$arResult['REVIEWS_STATISTIC']['RATING'][$keyStatistic]?></div>
													</div>
												<?endforeach;?>

												<div class="appraisals__wrap-btn">
													<div class="transparent-button appraisals__transparent-button show-tablet" data-popup-btn="all-reviews">Все отзывы</div>
													<div class="transparent-button appraisals__transparent-button" data-popup-btn="add-review">Оставить отзыв</div>
												</div>
											</div>
										</div>
									<?else:?>
										<div class="appraisals description-product__appraisals-empty">
											<div class="appraisals__inner">
												<div class="appraisals__wrap-btn">
													<div class="transparent-button appraisals__transparent-button" data-popup-btn="add-review">Оставить отзыв</div>
												</div>
											</div>
										</div>
									<?endif;?>

									<div class="description-product__content">
										<div class="reviews description-product__reviews">
											<div class="reviews__inner">
												<?if(!empty($arResult['REVIEWS'])):?>
													<?foreach($arResult['REVIEWS'] as $keyReview => $arReview):?>
														<div class="reviews__item" itemprop="review" itemscope itemtype="http://schema.org/Review">
															<?=$arReview['NAME']['VALUE'] ? '<div class="reviews__author" itemprop="author">' . $arReview['NAME']['VALUE'] . '</div>' : ''?>
															<?=$arReview['DATE']['VALUE'] ? '<div class="reviews__date" itemprop="datePublished">' . $arReview['DATE']['VALUE'] . '</div>' : ''?>
															<?if($arReview['RATING']['VALUE']):?>
																<div class="rating reviews__rating">
																	<div class="rating__inner">
																		<div class="rating__star" itemprop="reviewRating" data-rating="<?=$arReview['RATING']['VALUE']?>">
																			<div class="rating__fill"></div>
																		</div>
																	</div>
																</div>
															<?endif;?>
															<?=$arReview['TEXT_REVIEW']['VALUE']['TEXT'] ? '<div class="reviews__text" itemprop="reviewBody">' . $arReview['TEXT_REVIEW']['VALUE']['TEXT'] . '</div>' : ''?>
															
															<div class="merit">
																<div class="merit__inner">
																	<?=$arReview['ADVANTAGERS']['VALUE']['TEXT'] ? '<div class="merit__item merit__item_like">' . $arReview['ADVANTAGERS']['VALUE']['TEXT'] . '</div>' : ''?>
																	<?=$arReview['DISADVANTAGERS']['VALUE']['TEXT'] ? '<div class="merit__item merit__item_dislike">' . $arReview['DISADVANTAGERS']['VALUE']['TEXT'] . '</div>' : ''?>
																</div>
															</div>
														</div>
													<?endforeach;?>
												<?else:?>
													<div class="reviews__item-empty">
														Оставьте свой отзыв на этот товар!
													</div>
												<?endif;?>

											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					<?}?>
					<?if(!empty($arResult['DELIVERY_CONTENT'])){?>
						<div class="tabs__content product-tabs__content<?=$active == '4' ? ' active' : '';?>">
							<div class="description-product">
								<div class="description-product__inner">
									<div class="description-product__title">Доставка и оплата</div>
									<div class="description-product__content">
										<?=$arResult['DELIVERY_CONTENT']?>
									</div>
								</div>
							</div>
						</div>
					<?}?>
	            </div>
	        </div>
	    </div>
	</section>

	<?unset($active);?>

	<meta itemprop="name" content="<?=$name?>" />
	<meta itemprop="category" content="<?=$arResult['CATEGORY_PATH']?>" />
	<?if ($haveOffers){
		foreach ($arResult['JS_OFFERS'] as $offer){
			$currentOffersList = array();

			if (!empty($offer['TREE']) && is_array($offer['TREE'])){
				foreach ($offer['TREE'] as $propName => $skuId){
					$propId = (int)mb_substr($propName, 5);

					foreach ($skuProps as $prop){
						if ($prop['ID'] == $propId){
							foreach ($prop['VALUES'] as $propId => $propValue){
								if ($propId == $skuId){
									$currentOffersList[] = $propValue['NAME'];
									break;
								}
							}
						}
					}
				}
			}

			$offerPrice = $offer['ITEM_PRICES'][$offer['ITEM_PRICE_SELECTED']];
			?>
			<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<meta itemprop="sku" content="<?=htmlspecialcharsbx(implode('/', $currentOffersList))?>" />
				<meta itemprop="price" content="<?=$offerPrice['RATIO_PRICE']?>" />
				<meta itemprop="priceCurrency" content="<?=$offerPrice['CURRENCY']?>" />
				<link itemprop="availability" href="http://schema.org/<?=($offer['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
			</span>
			<?
		}

		unset($offerPrice, $currentOffersList);
	}else{?>
		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<meta itemprop="price" content="<?=$price['RATIO_PRICE']?>" />
			<meta itemprop="priceCurrency" content="<?=$price['CURRENCY']?>" />
			<link itemprop="availability" href="http://schema.org/<?=($actualItem['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
		</span>
	<?}?>
</div>
<?if ($haveOffers)
{
	$offerIds = array();
	$offerCodes = array();

	$useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

	foreach ($arResult['JS_OFFERS'] as $ind => &$jsOffer)
	{
		$offerIds[] = (int)$jsOffer['ID'];
		$offerCodes[] = $jsOffer['CODE'];

		$fullOffer = $arResult['OFFERS'][$ind];
		$measureName = $fullOffer['ITEM_MEASURE']['TITLE'];

		$strAllProps = '';
		$strMainProps = '';
		$strPriceRangesRatio = '';
		$strPriceRanges = '';

		if ($arResult['SHOW_OFFERS_PROPS'])
		{
			if (!empty($jsOffer['DISPLAY_PROPERTIES']))
			{
				foreach ($jsOffer['DISPLAY_PROPERTIES'] as $property)
				{
					$current = '<dt>'.$property['NAME'].'</dt><dd>'.(
						is_array($property['VALUE'])
							? implode(' / ', $property['VALUE'])
							: $property['VALUE']
						).'</dd>';
					$strAllProps .= $current;

					if (isset($arParams['MAIN_BLOCK_OFFERS_PROPERTY_CODE'][$property['CODE']]))
					{
						$strMainProps .= $current;
					}
				}

				unset($current);
			}
		}

		if ($arParams['USE_PRICE_COUNT'] && count($jsOffer['ITEM_QUANTITY_RANGES']) > 1)
		{
			$strPriceRangesRatio = '('.Loc::getMessage(
					'CT_BCE_CATALOG_RATIO_PRICE',
					array('#RATIO#' => ($useRatio
							? $fullOffer['ITEM_MEASURE_RATIOS'][$fullOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']
							: '1'
						).' '.$measureName)
				).')';

			foreach ($jsOffer['ITEM_QUANTITY_RANGES'] as $range)
			{
				if ($range['HASH'] !== 'ZERO-INF')
				{
					$itemPrice = false;

					foreach ($jsOffer['ITEM_PRICES'] as $itemPrice)
					{
						if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
						{
							break;
						}
					}

					if ($itemPrice)
					{
						$strPriceRanges .= '<dt>'.Loc::getMessage(
								'CT_BCE_CATALOG_RANGE_FROM',
								array('#FROM#' => $range['SORT_FROM'].' '.$measureName)
							).' ';

						if (is_infinite($range['SORT_TO']))
						{
							$strPriceRanges .= Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
						}
						else
						{
							$strPriceRanges .= Loc::getMessage(
								'CT_BCE_CATALOG_RANGE_TO',
								array('#TO#' => $range['SORT_TO'].' '.$measureName)
							);
						}

						$strPriceRanges .= '</dt><dd>'.($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']).'</dd>';
					}
				}
			}

			unset($range, $itemPrice);
		}

		$jsOffer['DISPLAY_PROPERTIES'] = $strAllProps;
		$jsOffer['DISPLAY_PROPERTIES_MAIN_BLOCK'] = $strMainProps;
		$jsOffer['PRICE_RANGES_RATIO_HTML'] = $strPriceRangesRatio;
		$jsOffer['PRICE_RANGES_HTML'] = $strPriceRanges;
	}

	$templateData['OFFER_IDS'] = $offerIds;
	$templateData['OFFER_CODES'] = $offerCodes;
	unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);

	$jsParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => true,
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
			'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
			'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
			'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
			'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
			'USE_STICKERS' => true,
			'USE_SUBSCRIBE' => $showSubscribe,
			'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
			'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
			'ALT' => $alt,
			'TITLE' => $title,
			'MAGNIFIER_ZOOM_PERCENT' => 200,
			'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
			'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
			'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
				? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
				: null
		),
		'PRODUCT_TYPE' => $arResult['PRODUCT']['TYPE'],
		'VISUAL' => $itemIds,
		'DEFAULT_PICTURE' => array(
			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
		),
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'ACTIVE' => $arResult['ACTIVE'],
			'NAME' => $arResult['~NAME'],
			'CATEGORY' => $arResult['CATEGORY_PATH']
		),
		'BASKET' => array(
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'BASKET_URL' => $arParams['BASKET_URL'],
			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		),
		'OFFERS' => $arResult['JS_OFFERS'],
		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
		'TREE_PROPS' => $skuProps
	);
}else{
	$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
	if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !$emptyProductProperties){?>
		<div id="<?=$itemIds['BASKET_PROP_DIV']?>" style="display: none;">
			<?if (!empty($arResult['PRODUCT_PROPERTIES_FILL'])){
				foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo){?>
					<input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=htmlspecialcharsbx($propInfo['ID'])?>">
					<?
					unset($arResult['PRODUCT_PROPERTIES'][$propId]);
				}
			}

			$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
			if (!$emptyProductProperties)
			{
				?>
				<table>
					<?
					foreach ($arResult['PRODUCT_PROPERTIES'] as $propId => $propInfo)
					{
						?>
						<tr>
							<td><?=$arResult['PROPERTIES'][$propId]['NAME']?></td>
							<td>
								<?
								if (
									$arResult['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L'
									&& $arResult['PROPERTIES'][$propId]['LIST_TYPE'] === 'C'
								)
								{
									foreach ($propInfo['VALUES'] as $valueId => $value)
									{
										?>
										<label>
											<input type="radio" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]"
												value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"checked"' : '')?>>
											<?=$value?>
										</label>
										<br>
										<?
									}
								}
								else
								{
									?>
									<select name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]">
										<?
										foreach ($propInfo['VALUES'] as $valueId => $value)
										{
											?>
											<option value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"selected"' : '')?>>
												<?=$value?>
											</option>
											<?
										}
										?>
									</select>
									<?
								}
								?>
							</td>
						</tr>
						<?
					}
					?>
				</table>
				<?
			}
			?>
		</div>
		<?
	}

	$jsParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
			'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
			'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
			'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
			'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
			'USE_STICKERS' => true,
			'USE_SUBSCRIBE' => $showSubscribe,
			'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
			'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
			'ALT' => $alt,
			'TITLE' => $title,
			'MAGNIFIER_ZOOM_PERCENT' => 200,
			'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
			'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
			'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
				? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
				: null
		),
		'VISUAL' => $itemIds,
		'PRODUCT_TYPE' => $arResult['PRODUCT']['TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'ACTIVE' => $arResult['ACTIVE'],
			'PICT' => reset($arResult['MORE_PHOTO']),
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
			'ITEM_PRICES' => $arResult['ITEM_PRICES'],
			'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
			'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
			'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
			'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
			'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
			'MAX_QUANTITY' => $arResult['PRODUCT']['QUANTITY'],
			'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
			'CATEGORY' => $arResult['CATEGORY_PATH']
		),
		'BASKET' => array(
			'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		)
	);
	unset($emptyProductProperties);
}

if ($arParams['DISPLAY_COMPARE'])
{
	$jsParams['COMPARE'] = array(
		'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
		'COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
		'COMPARE_PATH' => $arParams['COMPARE_PATH']
	);
}
?>
<script>
	BX.message({
		ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
		TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
		TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
		BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
		BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
		BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
		BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
		TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
		COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
		COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
		COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
		PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
		PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
		SITE_ID: '<?=CUtil::JSEscape($component->getSiteId())?>'
	});

	var <?=$obName?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
</script>
<?
unset($actualItem, $itemIds, $jsParams);
?>
<!--Попап Добавить отзыв-->
<div class="popup" data-popup-id="add-review">
	<div class="popup__inner w-800">
		<div class="popup__close" data-popup-close="add-review"></div>
		<div class="popup__content">
			<div class="popup__title">Ваш отзыв</div>
			<div class="popup__text">
				<form method="POST" id="new-review" data-block="formController">
					<input type="hidden" name="SiteVerify" value="N">
					<input type="hidden" name="subject" value="Добавить отзыв">
					<input type="hidden" name="page" value="<?=$APPLICATION->GetCurPage(false);?>">
					<input type="hidden" name="productID" value="<?=$arResult['ID'];?>">
					<input type="hidden" name="productName" value="<?=$arResult['NAME'];?>">

					<div class="form-data">
						<div class="form-data__wrap">
							<input class="form-data__input js__default" placeholder="Ваше имя" type="text" name="NAME" required="">
						</div>
						<div class="form-data__error">Заполните поле "Ваше имя"</div>
					</div>

					<div class="form-data">
						<div class="form-data__wrap">
							<textarea class="form-data__textarea js__default" rows="7" placeholder="Отзыв" name="TEXT_REVIEW" required=""></textarea>
						</div>
						<div class="form-data__error">Заполните поле "Отзыв"</div>
					</div>

					<div class="popup__cols-2">

						<div class="form-data popup__col-1">
							<div class="form-data__wrap">
								<textarea class="form-data__textarea js__default" rows="4" placeholder="Дстоинства" name="ADVANTAGERS"></textarea>
							</div>
							<div class="form-data__error">Заполните поле "Дстоинства"</div>
						</div>

						<div class="form-data popup__col-1">
							<div class="form-data__wrap">
								<textarea class="form-data__textarea js__default" rows="4" placeholder="Недостатки" name="DISADVANTAGERS"></textarea>
							</div>
							<div class="form-data__error">Заполните поле "Недостатки"</div>
						</div>
					</div>

					<div class="form-data">
						<div class="form-data__stars">
							<div class="form-data__stars-title">Ваша оценка</div>
							<div class="rating-stars">
								<div class="rating-stars__inner">
									<input type="radio" name="rating" value="5" id="5"><label for="5"></label>
									<input type="radio" name="rating" value="4" id="4"><label for="4"></label>
									<input type="radio" name="rating" value="3" id="3"><label for="3"></label>
									<input type="radio" name="rating" value="2" id="2"><label for="2"></label>
									<input type="radio" name="rating" value="1" id="1"><label for="1"></label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-data__wrap-submit">
						<button class="js__disabled-btn form-data__submit blue-button" type="submit">Опубликовать отзыв</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--/Попап Добавить отзыв-->
