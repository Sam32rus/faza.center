<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

// Loader::includeModule('blog');
// $SORT = Array("DATE_PUBLISH" => "DESC", "NAME" => "ASC");
// $arFilter = Array(
//     "BLOG_ID" => 2,
// );

// $dbPosts = CBlogPost::GetList(
//     $SORT,
//     $arFilter
// );

// while ($arPost = $dbPosts->Fetch())
// {
//     echo "<pre>";
//     print_r($arPost);
//     echo "</pre>";
// }

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers){
    $actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
    ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
    : reset($arResult['OFFERS']);
}else{
    $actualItem = $arResult;
}

foreach ($actualItem['MORE_PHOTO'] as $key => $photo){
    $arResult['MORE_PHOTO_NAV'][$key] = CFile::ResizeImageGet($photo['ID'], array('width'=>80, 'height'=>80), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    $arResult['MORE_PHOTO_PREVIEW'][$key] = CFile::ResizeImageGet($photo['ID'], array('width'=>550, 'height'=>550), BX_RESIZE_IMAGE_PROPORTIONAL, true);
}


foreach ($arResult['PROPERTIES'] as $keyProperty => $arProperty) {
    switch ($arProperty['PROPERTY_TYPE']) {
        case 'F':
        if($arProperty['MULTIPLE'] == 'N'){
            $arResult['PROPERTIES'][$keyProperty]['FILE'][] = CFile::GetFileArray($arProperty['VALUE']);
        }else{
            foreach ($arProperty['VALUE'] as $keyValue => $value) {
                $arResult['PROPERTIES'][$keyProperty]['FILE'][$keyValue] = CFile::GetFileArray($value);
            }
        }
        break;
        case 'L':
        if($arProperty['MULTIPLE'] == 'Y'){
            $arResult['PROPERTIES'][$keyProperty]['DISPLAY_VALUE_HTML'] = "";
            foreach ($arProperty['VALUE'] as $keyValue => $value) {
                if($value != end($arProperty['VALUE'])){
                    $arResult['PROPERTIES'][$keyProperty]['DISPLAY_VALUE_HTML'] .= $value . ", ";
                }else{
                    $arResult['PROPERTIES'][$keyProperty]['DISPLAY_VALUE_HTML'] .= $value;
                }
            }
        }
        break;
        case 'E':
        if($arProperty['MULTIPLE'] == 'N'){
            $parameters = [
                'select' => ['ID', 'NAME', 'CODE', 'DETAIL_PAGE_URL' => 'IBLOCK.DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'PREVIEW_TEXT'],
                'filter' => ['ID' => $arProperty['VALUE'], 'IBLOCK_ID' => $arProperty['LINK_IBLOCK_ID']],
                'limit' => 1
            ];
        }else{
            $parameters = [
                'select' => ['ID', 'NAME', 'CODE', 'DETAIL_PAGE_URL' => 'IBLOCK.DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'PREVIEW_TEXT'],
                'filter' => ['ID' => $arProperty['VALUE'], 'IBLOCK_ID' => $arProperty['LINK_IBLOCK_ID']],
                'limit' => count($arProperty['VALUE'])
            ];
        }
        $rows = \Bitrix\Iblock\ElementTable::getList($parameters);
        unset($arResult['PROPERTIES'][$keyProperty]['VALUE']);
        while ($row = $rows->fetch()) {
            $arResult['PROPERTIES'][$keyProperty]['VALUE'][$row['ID']] = $row;
            $arResult['PROPERTIES'][$keyProperty]['VALUE'][$row['ID']]['DETAIL_PAGE_URL'] = CIBlock::ReplaceDetailUrl($row['DETAIL_PAGE_URL'], $row, false, 'E');
            if(!empty($row['PREVIEW_PICTURE'])):
                $arResult['PROPERTIES'][$keyProperty]['VALUE'][$row['ID']]['PREVIEW_PICTURE'] = CFile::GetFileArray($row['PREVIEW_PICTURE']);
            endif;
        }
        break;
    }
    if($arProperty["PROPERTY_TYPE"]=="S" && !empty($arProperty['USER_TYPE_SETTINGS']['TABLE_NAME'])):
        if(CModule::IncludeModule('highloadblock')){
            //получим данынй HL
            $hldata = array_pop(HL\HighloadBlockTable::getList(array('filter' => array('TABLE_NAME'=>$arProperty["USER_TYPE_SETTINGS"]["TABLE_NAME"])))->fetchAll());
            //затем инициализировать класс сущности
            $entityClass = HL\HighloadBlockTable::compileEntity($hldata)->getDataClass();
            $res = $entityClass::getList(array('select' => array('*'),'order' => array('ID' => 'ASC'),'filter' => array('UF_XML_ID' => $arProperty["VALUE"])))->fetchAll();
            if(is_array($res)&&!empty($res)){
                $arResult['PROPERTIES'][$keyProperty]["VALUE"] = $res;
            }
        }
    endif;
}

$arResult['DELIVERY_CONTENT'] = current($arResult['PROPERTIES']['brand']['VALUE'])['UF_DESCRIPTION'];

$arResult['REVIEWS'] = [];
$arResult['REVIEWS_STATISTIC']['RATING'] = [
    '5' => 0,
    '4' => 0,
    '3' => 0,
    '2' => 0,
    '1' => 0
];
$arSelect = Array("ID", "IBLOCK_ID");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array("IBLOCK_ID" => IntVal(REVIEWS_IBLOCK), "ACTIVE"=>"Y", '=PROPERTY_PRODUCT' => $arResult['ID']);
$res = CIBlockElement::GetList(Array('ACTIVE_DATE' => 'DESC'), $arFilter, false, [], $arSelect);
$maxRating = 0;
$currentRating = 0;
while($ob = $res->GetNextElement()){  
    $arProps = $ob->GetProperties();
    $arResult['REVIEWS'][] = $arProps;
    $arResult['REVIEWS_STATISTIC']['RATING'][$arProps['RATING']['VALUE']]++;
    $currentRating += IntVal($arProps['RATING']['VALUE']);
}
$maxRating = IntVal(count($arResult['REVIEWS']) * 5);
$arResult['REVIEWS_RATING'] = round(FloatVal($currentRating / $maxRating) * 5, 1);

foreach($arResult['REVIEWS_STATISTIC']['RATING'] as $key => $val){
    $arResult['REVIEWS_STATISTIC']['PERCENT'][$key] = round($val / count($arResult['REVIEWS']) * 100, 1);
}
