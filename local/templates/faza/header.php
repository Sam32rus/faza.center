<!DOCTYPE html>
<html lang="ru">

<head>
    <?

    use Bitrix\Main\Page\Asset;

    Asset::getInstance()->addString('<meta charset="UTF-8">');
    Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">');
    Asset::getInstance()->addString('<meta http-equiv="X-UA-Compatible" content="ie=edge">');
    Asset::getInstance()->addString('<link rel="icon" href="/favicon.ico" type="image/x-icon">');
    Asset::getInstance()->addCss("/f/css/styles.css");
    // Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/styles.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/common.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/animation.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/preloader.css");
    $APPLICATION->ShowHead();
    CJSCore::Init(array("core"));
    ?>

    <title><? $APPLICATION->ShowTitle() ?></title>
    <!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(50804464, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/50804464" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>

<body>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
    <div class="wrapper">

        <header class="header">
            <div class="content">
                <div class="header__inner">
                    <div class="header__left-part">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "top__main",
                            array(
                                "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                                "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
                                "DELAY" => "N",    // Откладывать выполнение шаблона меню
                                "MAX_LEVEL" => "2",    // Уровень вложенности меню
                                "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                                    0 => "",
                                ),
                                "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                                "MENU_CACHE_TYPE" => "A",    // Тип кеширования
                                "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                                "ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
                                "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                            ),
                            false
                        ); ?>

                        <div class="header__main-top main-top">
                            <div class="main-top__inner">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => SITE_TEMPLATE_PATH . "/include/header/logo.php"
                                    )
                                ); ?>

                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => SITE_TEMPLATE_PATH . "/include/header/catalog__btn.php"
                                    )
                                ); ?>

                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:search.title",
                                    "header__search",
                                    array(
                                        "CATEGORY_0" => array(
                                            0 => "iblock_catalog",
                                        ),
                                        "CATEGORY_0_TITLE" => GetMessage("CATEGORY_PRODUСTCS_SEARCH_NAME"),
                                        "CATEGORY_0_iblock_aspro_optimus_catalog" => array(
                                            0 => "52",
                                        ),
                                        "CHECK_DATES" => "Y",
                                        "CONTAINER_ID" => "title-search",
                                        "CONVERT_CURRENCY" => "N",
                                        "INPUT_ID" => "title-searchs-input",
                                        "NUM_CATEGORIES" => "1",
                                        "ORDER" => "date",
                                        "PAGE" => SITE_DIR . "catalog/search/",
                                        "PREVIEW_HEIGHT" => "38",
                                        "PREVIEW_TRUNCATE_LEN" => "50",
                                        "PREVIEW_WIDTH" => "38",
                                        "PRICE_CODE" => array(
                                            0 => "RUB",
                                        ),
                                        "PRICE_VAT_INCLUDE" => "Y",
                                        "SHOW_ANOUNCE" => "N",
                                        "SHOW_INPUT" => "Y",
                                        "SHOW_OTHERS" => "N",
                                        "SHOW_PREVIEW" => "Y",
                                        "TOP_COUNT" => "8",
                                        "USE_LANGUAGE_GUESS" => "N",
                                        "COMPONENT_TEMPLATE" => "header__search",
                                        "CATEGORY_0_iblock_catalog" => array(
                                            0 => "7",
                                        )
                                    ),
                                    false,
                                    array(
                                        "ACTIVE_COMPONENT" => "Y"
                                    )
                                ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="header__right-part js__open-menu-mobile">
                        <div class="user-menu header__menu">
                            <div class="user-menu__inner">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => SITE_TEMPLATE_PATH . "/include/header/icons.php"
                                    )
                                ); ?>
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:sale.basket.basket.line",
                                    "header__cart",
                                    array(
                                        "HIDE_ON_BASKET_PAGES" => "N",
                                        "PATH_TO_AUTHORIZE" => SITE_DIR . "auth/",
                                        "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                                        "PATH_TO_ORDER" => SITE_DIR . "personal/order/",
                                        "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                                        "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                                        "PATH_TO_REGISTER" => SITE_DIR . "login/",
                                        "POSITION_FIXED" => "N",
                                        "SHOW_AUTHOR" => "Y",
                                        "SHOW_EMPTY_VALUES" => "Y",
                                        "SHOW_NUM_PRODUCTS" => "Y",
                                        "SHOW_PERSONAL_LINK" => "N",
                                        "SHOW_PRODUCTS" => "N",
                                        "SHOW_REGISTRATION" => "N",
                                        "SHOW_TOTAL_PRICE" => "N",
                                        "COMPONENT_TEMPLATE" => "header__cart"
                                    ),
                                    false
                                ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
                                
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => SITE_TEMPLATE_PATH . "/include/header/menu__catalog.php"
            )
        ); ?>

        <menu class="mobile-menu js__mobile-menu">
            <div class="mobile-menu__inner">
                <div class="user-menu mobile-menu__user-menu">
                    <div class="user-menu__inner">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => SITE_TEMPLATE_PATH . "/include/header/icons_mobile.php"
                            )
                        ); ?>
                    </div>
                </div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_TEMPLATE_PATH . "/include/header/menu__catalog.php",
                        "TYPE" => 'mobile'
                    )
                ); ?>
            </div>
        </menu><?/**/?>

        <? if (!$APPLICATION->GetDirProperty('hide_breadcrumbs') && $APPLICATION->GetCurPage(false) !== '/') :
            $APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "",
                array(
                    "PATH" => "",   // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                    "SITE_ID" => "s1",  // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                    "START_FROM" => "0",    // Номер пункта, начиная с которого будет построена навигационная цепочка
                    "COMPONENT_TEMPLATE" => ".default"
                ),
                false
            );
        endif; ?>

        <? if ($APPLICATION->GetDirProperty('default_html') && $APPLICATION->GetCurPage(false) !== '/') : ?>
            <main class="main">
                <article class="article">
                    <div class="article__inner">
        <? endif; ?>