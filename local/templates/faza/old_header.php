<html lang="ru">
<head>
	<title><?$APPLICATION->ShowTitle()?></title>
	<?
	use Bitrix\Main\Page\Asset;

	Asset::getInstance()->addString('<meta charset="UTF-8">');
	Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">');
	Asset::getInstance()->addString('<meta http-equiv="X-UA-Compatible" content="ie=edge">');
	Asset::getInstance()->addString('<link rel="shortcut icon" href="'.SITE_TEMPLATE_PATH.'/assets/i/favicon.ico" type="image/x-icon">');
	// Asset::getInstance()->addString('<script>BX.message('.CUtil::PhpToJSObject( $MESS, false ).')</script>', true);

    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/styles.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/common.css");

	$APPLICATION->ShowHead();
	?>
</head>
<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<div class="wrapper">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.register",
			".default",
			array(
				"AUTH" => "Y",
				"REQUIRED_FIELDS" => array(
					0 => "EMAIL",
					1 => "NAME",
				),
				"SET_TITLE" => "N",
				"SHOW_FIELDS" => array(
					0 => "EMAIL",
					1 => "NAME",
				),
				"SUCCESS_PAGE" => "/auth/confirmation/",
				"USER_PROPERTY" => array(
				),
				"USER_PROPERTY_NAME" => "",
				"USE_BACKURL" => "N",
				"COMPONENT_TEMPLATE" => ".default"
			),
			false
		);?>

		<!--Попап авторизации-->
		<div class="popup <?=$classalert;?>" data-popup-id="login">
			<div class="popup__inner">
				<div class="popup__close" data-popup-close="login"></div>
				<div class="popup__content">
					<div class="popup__title">Вход</div>

					<?$APPLICATION->IncludeComponent(
						"bitrix:system.auth.form",
						"template",
						array(
							"FORGOT_PASSWORD_URL" => "",
							"PROFILE_URL" => "",
							"REGISTER_URL" => "",
							"SHOW_ERRORS" => "Y",
							"AJAX_MODE" => "N",
							"COMPONENT_TEMPLATE" => "template"
						),
						false
					);?>
				</div>
			</div>
		</div>
		<!--/Попап авторизации-->

		<!--Попап восстановления пароля-->
		<div class="popup <?=$allertstil;?>" data-popup-id="recovery">
			<div class="popup__inner">
				<div class="popup__close" data-popup-close="recovery"></div>
				<div class="popup__content">
					<div class="popup__title">Восстановление пароля</div>
					<div class="popup__text">Ссылка для восстановления пароля будет отправлена на указанный адрес электронной почты</div>


					<?$APPLICATION->IncludeComponent(
						"bitrix:main.auth.forgotpasswd",
						".default",
						Array(
							"AUTH_AUTH_URL" => "/auth/forgot-password/",
							"AJAX_MODE" => "Y",
							"AUTH_REGISTER_URL" => "",
						)
					);?>
				</div>
			</div>
		</div>
		<!--/Попап восстановления пароля-->

		<header class="header">
			<div class="content">
				<div class="header__inner">
					<div class="header__left-part">
						<div class="top-menu header__top-menu">
							<div class="top-menu__inner">
								<?$APPLICATION->IncludeComponent("bitrix:sale.location.selector.steps", "templateloc",
									Array(
										"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
										"CACHE_TYPE" => "A",	// Тип кеширования
										"CODE" => "",	// Символьный код местоположения
										"FILTER_BY_SITE" => "N",	// Фильтровать по сайту
										"ID" => "",	// ID местоположения
										"INPUT_NAME" => "LOCATION",	// Имя поля ввода
										"JS_CALLBACK" => "",	// Javascript-функция обратного вызова
										"JS_CONTROL_GLOBAL_ID" => "",	// Идентификатор javascript-контрола
										"PRECACHE_LAST_LEVEL" => "N",	// Предварительно загружать последний выбранный уровень
										"PRESELECT_TREE_TRUNK" => "N",	// Отображать статичный ствол дерева
										"PROVIDE_LINK_BY" => "id",	// Сохранять связь через
										"SHOW_DEFAULT_LOCATIONS" => "N",	// Отображать местоположения по-умолчанию
									),
									false
								);?>

								<?$APPLICATION->IncludeComponent("bitrix:menu", "horizontal_multileveltop",
									Array(
										"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
										"MENU_CACHE_TYPE" => "A",	// Тип кеширования
										"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
										"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
										"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
										"CACHE_SELECTED_ITEMS" => "N",
										"MAX_LEVEL" => "2",	// Уровень вложенности меню
										"CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
										"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
										"DELAY" => "N",	// Откладывать выполнение шаблона меню
										"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
										"COMPONENT_TEMPLATE" => "horizontal_multilevel"
									),
									false
								);?>
                            </div>
                        </div>
                        <div class="header__main-top main-top">
                        	<div class="main-top__inner">
                        		<a href="/" class="main-top__logo"></a>
                        		<div class="button-catalog main-top__button-catalog js__close-open-menu">Каталог</div>
                        		<div class="search main-top__search">
                        			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                        				array(
                        					"COMPONENT_TEMPLATE" => ".default",
                        					"PATH" => SITE_DIR."include/top_page/search.title.catalog.php",
                        					"AREA_FILE_SHOW" => "file",
                        					"AREA_FILE_SUFFIX" => "",
                        					"AREA_FILE_RECURSIVE" => "Y",
                        					"EDIT_TEMPLATE" => "standard.php"
                        				),
                        				false
                        			);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header__right-part js__open-menu-mobile">
                    	<div class="user-menu header__menu">
                    		<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                    			array(
                    				"COMPONENT_TEMPLATE" => ".default",
                    				"PATH" => SITE_DIR."include/top_text_right.php",
                    				"AREA_FILE_SHOW" => "file",
                    				"AREA_FILE_SUFFIX" => "",
                    				"AREA_FILE_RECURSIVE" => "Y",
                    				"EDIT_TEMPLATE" => "standard.php"
                    			),
                    			false
                    		);?>
                    	</div>
                    </div>
                </div>
            </div>
        </header>
        <menu class="menu js__menu">
        	<div class="menu__inner js__menu-inner">
        		<div class="menu__tabs js__tabs">
        			<?$APPLICATION->IncludeComponent("bitrix:menu", "horizontal_multileveltop_top_big_menu",
        				Array(
							"ROOT_MENU_TYPE" => "alltop",	// Тип меню для первого уровня
							"MENU_CACHE_TYPE" => "A",	// Тип кеширования
							"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
							"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
							"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
							"CACHE_SELECTED_ITEMS" => "N",
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"COMPONENT_TEMPLATE" => "horizontal_multileveltop"
						),
	        			false,
	        			array(
	        				"ACTIVE_COMPONENT" => "Y"
	        			)
	        		);?>

        			<?foreach ($GLOBALS['IndexMenu'] as $key => $value) {?>
        				<div class="tabs__content menu__tabs-content ">
        					<ul class="menu__list-content">
        						<?foreach ($value as $keys => $values) {?>
        							<li class="menu__item-tab"><a class="menu__link-tab" href="<?=$values['LINK']?>"><?=$values["TEXT"]?></a></li>
        						<?}?>
    						</ul>
    					</div>
					<?}?>
        		</div>

				<div class="menu__partners">
					<?
					$arSelectPartner = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*","SECTION_ID","PREVIEW_PICTURE","DETAIL_PAGE_URL");
					$arFilterPartner = Array("IBLOCK_ID"=>13, "ACTIVE"=>"Y");
					$resPartner = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilterPartner, false, Array("nPageSize"=>18), $arSelectPartner);
					while($obPartner = $resPartner->GetNextElement()){
						$arFieldsPartner = $obPartner->GetFields();
						$arPropsPartner = $obPartner->GetProperties();
						$urlPartner = CFile::GetPath($arFieldsPartner["PREVIEW_PICTURE"]);
						?>
						<a href="<?=$arFieldsPartner['DETAIL_PAGE_URL']?>" class="menu__partners-item"><img src="<?=$urlPartner?>" class="menu__partners-img"></a>
					<?}?>
				</div>
			</div>
		</menu>
		<menu class="mobile-menu js__mobile-menu">
			<div class="mobile-menu__inner">
				<div class="user-menu mobile-menu__user-menu">
					<div class="user-menu__inner">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
							array(
								"COMPONENT_TEMPLATE" => ".default",
								"PATH" => SITE_DIR."include/top_text_right.php",
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "",
								"AREA_FILE_RECURSIVE" => "Y",
								"EDIT_TEMPLATE" => "standard.php"
							),
							false
                        );?>
		            </div>
		        </div>
				<div class="mobile-menu__close js__close-mobile-menu"></div>

		        <?$APPLICATION->IncludeComponent("bitrix:menu", "horizontal_multileveltop_top_big_menu_mobile",
		        	Array(
				    	"ROOT_MENU_TYPE" => "alltop",   // Тип меню для первого уровня
				        "MENU_CACHE_TYPE" => "A",   // Тип кеширования
				        "MENU_CACHE_TIME" => "3600000", // Время кеширования (сек.)
				        "MENU_CACHE_USE_GROUPS" => "N", // Учитывать права доступа
				        "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
				        "CACHE_SELECTED_ITEMS" => "N",
				        "MAX_LEVEL" => "1", // Уровень вложенности меню
				        "CHILD_MENU_TYPE" => "",    // Тип меню для остальных уровней
				        "USE_EXT" => "Y",   // Подключать файлы с именами вида .тип_меню.menu_ext.php
				        "DELAY" => "N", // Откладывать выполнение шаблона меню
				        "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
				        "COMPONENT_TEMPLATE" => "horizontal_multileveltop"
				    ),
			        false,
			        array(
			        	"ACTIVE_COMPONENT" => "Y"
			        )
			    );?>

		        <?foreach ($GLOBALS['IndexMenu_mobile'] as $key => $value) {?>
		        	<ul class="mobile-menu__second-list js__mobile-menu-second-list">
		        		<li class="mobile-menu__item mobile-menu__item_back js__mobile-menu-back"><?=$key;?></li>

		        		<?foreach ($value as $keys => $values) {?>
		        			<li class="mobile-menu__item"><a href="/catalog<?=$values['LINK']?>"><?=$values["TEXT"]?></a></li>
		        		<?}?>
		    		</ul>
		        <?}?>
		    </div>
		</menu>
	<? if ($APPLICATION->GetCurPage(false) !== '/'/* || $APPLICATION->GetCurPage(false) !== '/catalog/' || $APPLICATION->GetCurPage(false) !== '/personal/'*/){?>
		<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", Array(
                "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
				"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
				"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
				"COMPONENT_TEMPLATE" => ".default"
			),
			false
		);?>


		<main class="main">
			<h1 class="title-page"><span class="content"><?=$APPLICATION->ShowTitle(false);?></span></h1>
			<?/*<section class="<?if($APPLICATION->GetCurPage(false) == '/catalog/'){?>posters<?}else{?>result<?}?>">
				<div class="content">*/?>
	<?}else{?>
		<main class="main">
	<?}?>
