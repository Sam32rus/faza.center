			<? if ($APPLICATION->GetDirProperty('default_html') && $APPLICATION->GetCurPage(false) !== '/') : ?>
				</div>
				</article>
				</main>
			<? endif; ?>
			<footer class="footer">
				<div class="content">
					<div class="footer__inner">
						<? $APPLICATION->IncludeComponent(
							"bitrix:menu",
							"bottom__main",
							array(
								"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
								"CHILD_MENU_TYPE" => "submenu_footer",	// Тип меню для остальных уровней
								"DELAY" => "N",	// Откладывать выполнение шаблона меню
								"MAX_LEVEL" => "2",	// Уровень вложенности меню
								"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
									0 => "",
								),
								"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
								"MENU_CACHE_TYPE" => "A",	// Тип кеширования
								"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
								"ROOT_MENU_TYPE" => "botoom",	// Тип меню для первого уровня
								"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							),
							false
						); ?>

						<div class="footer__coll footer__coll_contacts">
							<? $APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								array(
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "inc",
									"EDIT_TEMPLATE" => "",
									"PATH" => SITE_TEMPLATE_PATH . "/include/footer/socials.php"
								)
							); ?>
							<? $APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								array(
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "inc",
									"EDIT_TEMPLATE" => "",
									"PATH" => SITE_TEMPLATE_PATH . "/include/footer/phones.php"
								)
							); ?>
						</div>
					</div>
				</div>
			</footer>
			
			<div class="wishlist">
				<div class="wishlist__inner">
					<? $APPLICATION->ShowViewContent('DETAIL_BTNS')?>

					<div class="user-menu">
						<div class="user-menu__inner">
							<? $APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								array(
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "inc",
									"EDIT_TEMPLATE" => "",
									"PATH" => SITE_TEMPLATE_PATH . "/include/header/icons_mobile.php"
								)
							); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="preloader__container">
				<div class="preloader__content">
					<img src="<?= SITE_TEMPLATE_PATH ?>/assets/i/preloader.gif" alt="" class="preloader">
				</div>
			</div>

			<? include($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . "/include/forms/specifyForm.php"); ?>
			<? $APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"EDIT_TEMPLATE" => "",
					"PATH" => SITE_TEMPLATE_PATH . "/include/forms/feedback.php"
				)
			); ?>
			<? $APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"EDIT_TEMPLATE" => "",
					"PATH" => SITE_TEMPLATE_PATH . "/include/forms/oneClick.php"
				)
			); ?>
			<div class="popup" data-popup-id="ok-basket">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="ok-basket"></div>
					<div class="popup__content">
						<div class="popup__title">Товар добавлен в корзину</div>
						<div class="form-data">
							<div class="form-data__wrap-submit">
								<a href="/personal/cart/" class="js__add-phone-personal form-data__submit blue-button">Перейти в корзину</a>
							</div>
						</div>
						<div class="popup__change-popup">
							<div class="popup__link" data-popup-close="ok-basket">Продолжить покупки</div>
						</div>
					</div>
				</div>
			</div>

			<div class="popup" data-popup-id="compare-result">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="compare-result"></div>
					<div class="popup__content">
						<div class="popup__title">Товар добавлен в сравнение</div>
						<div class="form-data">
							<div class="form-data__wrap-submit">
								<a href="/catalog/compare/" class="js__add-phone-personal form-data__submit blue-button">Перейти к сравнению</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--Попап Результат формы-->
			<div class="popup" data-popup-id="form-result">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="form-result"></div>
					<div class="popup__content">
						<div class="popup__title" data-target="title"></div>
						<div class="popup__text" data-target="message"></div>
					</div>
				</div>
			</div>
			<!--/Попап Результат формы-->


			<!--Попап регистрации-->
			<div class="popup" data-popup-id="registration" id="modal-register">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="registration"></div>
					<div class="popup__content">
						<div class="popup__title">Регистрация</div>
						<? $APPLICATION->IncludeComponent(
							"bitrix:main.register",
							"",
							array(
								"AUTH" => "Y",
								"REQUIRED_FIELDS" => array("NAME", "EMAIL"),
								"SET_TITLE" => "N",
								"SHOW_FIELDS" => array("NAME", "EMAIL"),
								"SUCCESS_PAGE" => "",
								"USER_PROPERTY" => array(),
								"USER_PROPERTY_NAME" => "",
								"USE_BACKURL" => "Y"
							)
						); ?>


					</div>
				</div>
			</div>
			<!--/Попап регистрации-->

			<!--Попап авторизации-->
			<div class="popup" data-popup-id="login" id="modal-login">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="login"></div>
					<div class="popup__content">
						<div class="popup__title">Вход</div>
						<? $APPLICATION->IncludeComponent(
							"bitrix:system.auth.form",
							"",
							array(
								"REGISTER_URL" => "",
								"FORGOT_PASSWORD_URL" => "",
								"PROFILE_URL" => "/personal/",
								"SHOW_ERRORS" => "Y"
							)
						); ?>
					</div>
				</div>
			</div>
			<!--/Попап авторизации-->

			<!--Попап восстановления пароля-->
			<div class="popup" data-popup-id="recovery" id="modal-forgot-password">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="recovery"></div>
					<div class="popup__content">
						<div class="popup__title">Восстановление пароля</div>
						<div class="popup__text">Ссылка для восстановления пароля будет отправлена на указанный адрес электронной почты</div>
						<? $APPLICATION->IncludeComponent("bitrix:system.auth.forgotpasswd", "", array()); ?>
						<?/*<form action="">
							<div class="form-data">
								<div class="form-data__wrap">
									<input class="form-data__input js__email" placeholder="Адрес электронной почты" name="email" type="text">
								</div>
								<div class="form-data__error">Заполните поле "Адрес электронной почты"</div>
							</div>
							<div class="form-data">
								<div class="form-data__wrap-submit">
									<button class="js__disabled-btn form-data__submit blue-button" type="submit">Отправить</button>
								</div>
							</div>
						</form>*/ ?>
					</div>
				</div>
			</div>
			<!--/Попап восстановления пароля-->

			<!--Попап пароль восстановлен-->
			<div class="popup" data-popup-id="recovery-ok">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="recovery-ok"></div>
					<div class="popup__content">
						<div class="popup__title">Восстановление пароля</div>
						<div class="popup__text">Ссылка для восстановления пароля отправлена на электронную почту, указанную при регитрации</div>
					</div>
				</div>
			</div>
			<!--/Попап пароль восстановлен-->

			<!--Попап Вы зарегистрированы-->
			<div class="popup" data-popup-id="registration-ok">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="registration-ok"></div>
					<div class="popup__content">
						<div class="popup__title">Регистрация</div>
						<div class="popup__text">Вы успешно зарегистрированы!</div>
					</div>
				</div>
			</div>
			<!--/Попап Вы зарегистрированы-->

			<!--Попап Вы зарегистрированы-->
			<div class="popup" data-popup-id="authorization-ok">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="authorization-ok"></div>
					<div class="popup__content">
						<div class="popup__title">Авторизация</div>
						<div class="popup__text">Вы успешно авторизованы!</div>
					</div>
				</div>
			</div>
			<!--/Попап Вы зарегистрированы-->

			<!--Попап Вы успешно подписаны на нашу e-mail рассылку-->
			<div class="popup" data-popup-id="mail-ok">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="mail-ok"></div>
					<div class="popup__content">
						<div class="popup__title">Подписаться на рассылку</div>
						<div class="popup__text">Вы успешно подписаны на нашу e-mail рассылку!</div>
					</div>
				</div>
			</div>
			<!--/Попап Вы успешно подписаны на нашу e-mail рассылку-->



			<!--Попап Все отзывы-->
			<div class="popup" data-popup-id="all-reviews">
				<div class="popup__inner w-800">
					<div class="popup__close" data-popup-close="all-reviews"></div>
					<div class="popup__content">
						<div class="popup__title">Все отзывы</div>
						<div class="popup__text">
							<div class="reviews">
								<div class="reviews__inner">

									<!--Конкретный отзыв-->
									<div class="reviews__item" itemprop="review" itemscope="" itemtype="http://schema.org/Review">
										<div class="reviews__author" itemprop="author">Игорь</div>
										<div class="reviews__date" itemprop="datePublished">20.10.2020</div>
										<div class="rating reviews__rating">
											<div class="rating__inner">
												<div class="rating__star" itemprop="reviewRating" data-rating="1.5">
													<div class="rating__fill"></div>
												</div>
											</div>
										</div>
										<div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
										<div class="merit">
											<div class="merit__inner">
												<div class="merit__item merit__item_like">Работает стабильно, светит ярко, есть гарантия</div>
												<div class="merit__item merit__item_dislike">Нет</div>
											</div>
										</div>
									</div>
									<!--/Конкретный отзыв-->

									<!--Конкретный отзыв-->
									<div class="reviews__item" itemprop="review" itemscope="" itemtype="http://schema.org/Review">
										<div class="reviews__author" itemprop="author">Михаил</div>
										<div class="reviews__date" itemprop="datePublished">20.10.2020</div>
										<div class="rating reviews__rating">
											<div class="rating__inner">
												<div class="rating__star" itemprop="reviewRating" data-rating="1.5">
													<div class="rating__fill"></div>
												</div>
											</div>
										</div>
										<div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
										<div class="merit">
											<div class="merit__inner">
												<div class="merit__item merit__item_like">Рживучесть, яркость</div>
												<div class="merit__item merit__item_dislike">не замечено</div>
											</div>
										</div>
									</div>
									<!--/Конкретный отзыв-->

									<!--Конкретный отзыв-->
									<div class="reviews__item" itemprop="review" itemscope="" itemtype="http://schema.org/Review">
										<div class="reviews__author" itemprop="author">Ирина</div>
										<div class="reviews__date" itemprop="datePublished">20.10.2020</div>
										<div class="rating reviews__rating">
											<div class="rating__inner">
												<div class="rating__star" itemprop="reviewRating" data-rating="1.5">
													<div class="rating__fill"></div>
												</div>
											</div>
										</div>
										<div class="reviews__text" itemprop="reviewBody"> С производителем Эра уже немного был до этого знаком, покупал до этого момента другие лампочки. Решили многое переделать в доме, поэтому купили сразу 10 лампочек таких, так как ко многим светильникам и бра они идеально подходят. Понравились — ничего лишнего, свет нормальный, не отталкивает, не тревожит глаза, при длительном освещении данной лампы глаза не устают, хотя брали другую фирму долго не смогли с ними находится. Работают стабильно — включение быстрое, без моргания, как отключается вполне быстро. В целом довольны покупкой, цена доступная, качество хорошее, от производителя идет и гарантия на год</div>
										<div class="merit">
											<div class="merit__inner">
												<div class="merit__item merit__item_like">не замечено</div>
												<div class="merit__item merit__item_dislike">Работает не стабильно, светит не ярко, нет гарантии</div>
											</div>
										</div>
									</div>
									<!--/Конкретный отзыв-->

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/Попап Все отзывы-->

			<!--Попап поделиться-->
			<div class="popup" data-popup-id="share">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="share"></div>
					<div class="popup__content">
						<div class="popup__title">Поделиться</div>
						<div class="popup__text popup__text_center">
							<div class="ya-share2" data-curtain data-size="l" data-services="twitter,vkontakte,facebook,odnoklassniki"></div>
						</div>
					</div>
				</div>
			</div>
			<!--/Попап поделиться-->

			<!--Попап Карта отправлена на активацию-->
			<div class="popup" data-popup-id="card-activ">
				<div class="popup__inner">
					<div class="popup__close" data-popup-close="card-activ"></div>
					<div class="popup__content">
						<div class="popup__title">Поделиться</div>
						<div class="popup__text">Карта отправлена на активацию. После успешной модерации она станет активной.</div>
					</div>
				</div>
			</div>
			<!--/Попап Карта отправлена на активацию-->

			<!--Попап Добавление новой скидочной карты-->
			<div class="popup" data-popup-id="add-card">
				<div class="popup__inner w-600">
					<div class="popup__close" data-popup-close="add-card"></div>
					<div class="popup__content">
						<form class="discount-card popup__discount-card">
							<div class="discount-card__inner">
								<div class="discount-card__title">Введите номер скидочной&nbsp;карты</div>
								<input type="text" class="discount-card__input js__discount-card">
								<button class="discount-card__button blue-button" type="submit">Активировать</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!--/Попап Добавление новой скидочной карты-->

			
			</div>
			<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-169238477-1"></script>
			<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-169238477-1');
			</script>
			<?

			use Bitrix\Main\Page\Asset;

			Asset::getInstance()->addJs("/f/js/jquery.min.js");
			Asset::getInstance()->addJs("/f/js/jquery.plugin.js");
			Asset::getInstance()->addJs("/f/js/jquery.modulecreator.min.js");
			Asset::getInstance()->addJs("/f/js/vendor.js");
			Asset::getInstance()->addJs("/f/js/app.js");
			// Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.min.js");
			// Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.plugin.js");
			// Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.modulecreator.min.js");
			// Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/vendor.js");
			// Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/app.js");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/lib/slick.min.js");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/favorites.js");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/custom.js");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/formController.js");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/auth.js");
			Asset::getInstance()->addString('<script src="https://yastatic.net/share2/share.js"></script>');
			?>
			<div id="phase"></div>
			</body>

			</html>

			<? global $APPLICATION;

			if (!$USER->IsAuthorized()) {
				$arFavorites = unserialize($APPLICATION->get_cookie("favorites"));
				//print_r($arFavorites);
			} else {
				$idUser = $USER->GetID();
				$rsUser = CUser::GetByID($idUser);
				$arUser = $rsUser->Fetch();
				$arFavorites = $arUser['UF_FAVORITES'];  // Достаём избранное пользователя
			}
			count($arFavorites);
			/* Меняем отображение сердечка товара */
			foreach ($arFavorites as $k => $favoriteItem) : ?>
				<script>
					if ($('.favorite__btn[data-item="<?= $favoriteItem ?>"]'))
						$('.favorite__btn[data-item="<?= $favoriteItem ?>"]').addClass('active');
				</script>
			<? endforeach; ?>