<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
use Neti\SimpleDraw\CSimpleDraw;
if (!CModule::IncludeModule('neti.simpledraw')) { return; }

Loc::loadMessages(__FILE__);

class simpledraw_detail extends CBitrixComponent
{
    public $mode, $ID, $errors;
    
    function executeComponent()
    {
        global $APPLICATION;
        
        $arParams = $this->arParams;
        $this->errors = array();
        $componentTemplate = '';
        $arPictureData = array();
        $hasPermission = FALSE ;
        // сохраним предыдущее значение кэш мода для сброса кэширования детальной картинки
        $componentCacheType = $this->arParams['CACHE_TYPE'];

        // проверяем на наличие объявлений
        $successMessage = '';
        if (isset($_SESSION['neti.simpledraw']['SUCCESS_MESSAGE'])) {
            $successMessage = $_SESSION['neti.simpledraw']['SUCCESS_MESSAGE'];
            unset($_SESSION['neti.simpledraw']['SUCCESS_MESSAGE']);
            // если есть объявление, не используем кэширование
            $this->arParams['CACHE_TYPE'] = 'N';
        }
        // если в сессии нет данных о разрешениях, тогда создадим их
        if (!isset($_SESSION['neti.simpledraw']['PERMISSIONS'])) {
            $_SESSION['neti.simpledraw']['PERMISSIONS'] = array();
        }
        
        // устанавливаем режим работы компонента
        $this->mode = empty($arParams['MODE']) ? 'view' : $arParams['MODE'];
        $this->ID = empty($arParams['PICTURE_ID']) ? 0 : $arParams['PICTURE_ID'];
        if (empty($this->arParams['MODE'])) {
            $this->arParams['MODE'] = $this->mode; // понадобится для кэширования
        }

        // если это режим редактирования, тогда отключаем кэширование компонента
        if ($this->mode != 'view') {
            $this->arParams['CACHE_TYPE'] = 'N';
        }
        
        // новый рисунок можно создать всегда
        if ($this->mode == 'new') {
            $hasPermission = TRUE;
        // для режима редактирования или удаления надо проверить доступ к рисунку
        } elseif ($this->mode == 'edit' || $this->mode == 'delete') {
            $hasPermission = in_array($this->ID, $_SESSION['neti.simpledraw']['PERMISSIONS']);
        }

        if ($this->startResultCache())
        {
            $needLoadData = TRUE;
            $request = \Bitrix\Main\Context::getCurrent()->getRequest();
            
            // если это у нас пост-запрос
            if ($request->isPost() && check_bitrix_sessid())
            {
                $postData = $request->getPostList()->toArray();
                // это запрос разрешения на изменения данных
                if (isset($postData['permission_check']))
                {
                    if ($this->setPermission($postData)) {
                        if ($this->mode == 'edit') {
                            $url = CComponentEngine::MakePathFromTemplate($arParams['~EDIT_URL'], array('ID' => $this->ID));
                        } elseif ($this->mode == 'delete') {
                            $url = CComponentEngine::MakePathFromTemplate($arParams['~DELETE_URL'], array('ID' => $this->ID));
                        } else {
                            $url = CComponentEngine::MakePathFromTemplate($arParams['~LIST_URL']);
                        }
                        LocalRedirect($url, TRUE);
                        die();
                    }
                }
                // это запрос на изменение или удаление рисунка
                else
                {
                    // упс! как так, без доступа?
                    if (!$hasPermission)
                    {
                        $url = CComponentEngine::MakePathFromTemplate($arParams['~EDIT_URL'], array('ID' => $this->ID));
                        LocalRedirect($url, TRUE);
                        die();
                    }
                    // выполняем работу с базой и файлами
                    else
                    {
                        if ($this->processData($postData)) 
                        {
                            // перед перенаправлением очистим кэши страниц
                            $this->clearComponentCache('neti.simpledraw:simpledraw.list');
                            // и укажем, что надо сбросить кэш детальной картинки
                            $_SESSION['neti.simpledraw']['CLEAR_CACHE'][] = $this->ID;
                            $this->arParams['MODE'] = 'view';
                            $this->arParams['CACHE_TYPE'] = $componentCacheType;
                            $this->clearResultCache();
                            $this->arParams['MODE'] = $this->mode; // и вернем назад, а тип кэширования уже не нужен
                            
                            // перенаправляемся
                            if ($this->mode == 'delete') {
                                $url = CComponentEngine::MakePathFromTemplate($arParams['~LIST_URL']);
                                $_SESSION['neti.simpledraw']['SUCCESS_MESSAGE'] = Loc::getMessage('SUCCESS_DELETE');
                            } else {
                                $url = CComponentEngine::MakePathFromTemplate($arParams['~DETAIL_URL'], array('ID' => $this->ID));
                                $_SESSION['neti.simpledraw']['SUCCESS_MESSAGE'] = Loc::getMessage('SUCCESS_SAVE');
                                $_SESSION['neti.simpledraw']['PERMISSIONS'][] = $this->ID;
                            }
                            LocalRedirect($url, TRUE);
                            die();
                        }
                        else
                        {
                            $needLoadData = FALSE;
                            $arPictureData = array(
                                'CANVAS_DATA' => $postData['CANVAS_DATA'],
                                'UF_NAME' => $postData['UF_NAME'],
                            );
                        }
                    }
                }

            } 
            
            // загрузим данные, если это требуется
            if (!empty($this->ID) && $needLoadData)
            {
                $entity = CSimpleDraw::getEntity();
                $arEntityParams = array(
                    'select' => array('*'),
                    'filter' => array('ID' => $this->ID),
                    'limit' => 1,
                );
                $dbItem = $entity::getList($arEntityParams);
                if ($arPictureData = $dbItem->fetch()) {
                    $arFile = CFile::getFileArray($arPictureData['UF_FILE']);
                    $arPictureData['UF_FILE_SRC'] = $arFile['SRC'];
                    $arPictureData['CANVAS_DATA'] = $arFile['SRC'];
                }
                
            }
            
            // определим выводимый шаблон
            if ($this->mode == 'new') {
                $componentTemplate = 'edit';
            } elseif(($this->mode == 'edit' || $this->mode == 'delete')) {
                if (!$hasPermission) {
                    $componentTemplate = 'permission';
                } else {
                    $componentTemplate = $this->mode;
                }
            } else {
                $componentTemplate = '';
            }
            
            // адреса страниц для данного рисунка
            $arURLs = array(
                'LIST' => CComponentEngine::MakePathFromTemplate($arParams['~LIST_URL']),
                'DETAIL' => CComponentEngine::MakePathFromTemplate($arParams['~DETAIL_URL'], array('ID' => $this->ID)),
                'EDIT' => CComponentEngine::MakePathFromTemplate($arParams['~EDIT_URL'], array('ID' => $this->ID)),
                'DELETE' => CComponentEngine::MakePathFromTemplate($arParams['~DELETE_URL'], array('ID' => $this->ID)),
            );
            
            $this->arResult = array(
                'DATA' => $arPictureData,
                'MODE' => $this->mode,
                'ERRORS' => $this->errors,
                'SUCCESS' => $successMessage,
                'URLS' => $arURLs,
            );
            
            $this->includeComponentTemplate($componentTemplate);
        }
        
        if ($this->mode == 'view') {
            $APPLICATION->addChainItem($this->arResult['DATA']['UF_NAME']);
        } elseif ($this->mode == 'new') {
            $APPLICATION->addChainItem(Loc::getMessage('CHAIN_ITEM_NEW'));
        } elseif ($this->mode == 'edit') {
            $APPLICATION->addChainItem(Loc::getMessage('CHAIN_ITEM_EDIT', $this->arResult['DATA']));
        } elseif ($this->mode == 'delete') {
            $APPLICATION->addChainItem(Loc::getMessage('CHAIN_ITEM_DELETE', $this->arResult['DATA']));
        }
        
        if ($arParams['SET_TITLE']) {
            if ($this->mode == 'new') {
                $APPLICATION->setTitle(Loc::getMessage('PAGE_TITLE_NEW'));
            } else {
                $title_mes = Loc::getMessage('PAGE_TITLE_'.strtoupper($this->mode), $this->arResult['DATA']);
                if (strlen($title_mes) > 0) {
                    $APPLICATION->setTitle($title_mes);
                } else {
                    $APPLICATION->setTitle($this->arResult['DATA']['UF_NAME']);
                }
            }
        }
        
    }
    
    /**
     * Производит проверку данных и их запись в базу
     * @param array $postData данные пост-запроса
     * @return boolean результат обработки
     */
    private function processData($postData)
    {
        global $CACHE_MANAGER;
        
        if ($this->mode == 'delete')
        {
            $entity = CSimpleDraw::getEntity();
            $result = $entity::delete($this->ID);
            if ($result->isSuccess()) {
                \CFile::Delete($oldFileID);
                return TRUE;
            } else {
                $this->errors = array_merge($this->errors, $result->getErrorMessages());
                return FALSE;
            }
        }
        else
        {
            if (empty($postData['CANVAS_DATA'])) {
                $this->errors[] = Loc::getMessage('ERROR_NO_IMAGE');
            }
            if (empty($postData['UF_NAME'])) {
                $this->errors[] = Loc::getMessage('ERROR_NO_NAME');
            }
            if ($this->mode == 'new' && empty($postData['UF_PASSWORD'])) {
                $this->errors[] = Loc::getMessage('ERROR_NO_PASSWORD');
            }
            if ($postData['UF_PASSWORD'] != $postData['UF_PASSWORD_CONFIRM']) {
                $this->errors[] = Loc::getMessage('ERROR_PASSWORD_CONFIRM');
            }

            if (!empty($this->errors)) {
                return FALSE;
            }

            $fileID = $this->saveImage($postData['CANVAS_DATA']);
            if (empty($fileID)) {
                $this->errors[] = Loc::getMessage('ERROR_FILE_SAVE');
                return FALSE;
            }
            
            if ($this->mode == 'new')
            {
                $entity = CSimpleDraw::getEntity();
                
                $arData = array(
                    'UF_NAME' => $postData['UF_NAME'],
                    'UF_PASS' => md5($postData['UF_PASSWORD']),
                    'UF_FILE' => $fileID,
                    'UF_DATETIME' => new \Bitrix\Main\Type\DateTime(),
                );
                $result = $entity::add($arData);
                if ($result->isSuccess()) {
                    $this->ID = $result->getID();
                    return TRUE;
                } else {
                    \CFile::Delete($fileID);
                    $this->errors = array_merge($this->errors, $result->getErrorMessages());
                    return FALSE;
                }
            }
            elseif ($this->mode == 'edit')
            {
                $entity = CSimpleDraw::getEntity();
                
                // получим старый идентификатор файла - его надо будет удалить
                $oldFileID = NULL;
                $dbResult = $entity::getList(array(
                    'select' => array('UF_FILE'),
                    'filter' => array('ID' => $this->ID),
                    'limit' => 1,
                ));
                if ($arOldData = $dbResult->fetch()) {
                    $oldFileID = $arOldData['UF_FILE'];
                }

                $arData = array(
                    'UF_NAME' => $postData['UF_NAME'],
                    'UF_FILE' => $fileID,
                    'UF_DATETIME' => new \Bitrix\Main\Type\DateTime(),
                );
                if (!empty($postData['UF_PASSWORD'])) {
                    $arData['UF_PASS'] = md5($postData['UF_PASSWORD']);
                }

                $result = $entity::update($this->ID, $arData);
                if ($result->isSuccess()) {
                    \CFile::Delete($oldFileID);
                    return TRUE;
                } else {
                    \CFile::Delete($fileID);
                    $this->errors = array_merge($this->errors, $result->getErrorMessages());
                    return FALSE;
                }
            }
        } // else !delete
    }
    
    /**
     * Сохраняет данные рисунка в файл и регистрирует его в системе
     * @param string $strImage base64 строка с данными рисунка
     * @return integer Идентификатор файла или FALSE в случае ошибки
     */
    private function saveImage($strImage)
    {
        // задаем название файла
        $filename = rand(1000, 10000) . '.png';
        // получаем данные рисунка
        $data = explode(',', $strImage);

        if(empty($data[1])) {
            $this->errors[] = Loc::GetMessage('ERROR_IMAGE_STRUCTURE');
            return FALSE;
        }
        
        $content = base64_decode($data[1]);

        if(\CUtil::BinStrlen($content) > $this->arParams['MAX_FILE_SIZE']) {
            $this->errors[] = Loc::GetMessage('ERROR_MAX_FILE_SIZE');
            return FALSE;
        }

        $arFileParams = array(
            'name' => $filename,
            'tmp_name' => '',
            'type' => 'image/png',
            'MODULE_ID' => 'neti.simpledraw',
            'content' => $content,
        );
        return \CFile::SaveFile($arFileParams, 'neti.simpledraw');
    }

    /**
     * Производит проверку пароля для получения доступа к редактированию картинки
     * @param array $postData данные пост запроса с паролем
     * @return boolean результат проверки
     */
    private function setPermission($postData)
    {
        $entity = CSimpleDraw::getEntity();
        $arEntityParams = array(
            'select' => array('ID', 'UF_PASS'),
            'filter' => array('ID' => $this->ID),
            'limit' => 1,
        );
        $dbResult = $entity::getList($arEntityParams);
        
        if ($arData = $dbResult->fetch()) {
            if ($arData['UF_PASS'] == md5($postData['PASS'])) {
                $_SESSION['neti.simpledraw']['PERMISSIONS'][] = $arData['ID'];
                return TRUE;
            }
        }
        
        $this->errors[] = Loc::getMessage('ERROR_PASSWORD_WRONG');
        return FALSE;
    }
    
}