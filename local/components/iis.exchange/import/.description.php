<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    'NAME' => Loc::getMessage('NETI_SIMPLEDRAW_COMPONENT_DETAIL_NAME'),
    'DESCRIPTION' => Loc::getMessage('NETI_SIMPLEDRAW_COMPONENT_DETAIL_DESCRIPTION'),
    'COMPLEX' => 'N',
    'PATH' => array(
        'ID' => 'neti',
        'NAME' => Loc::getMessage('NETI_SIMPLEDRAW_COMPONENT_PARTNER_NAME'),
        'CHILD' => array(
            'ID' => 'simpledraw',
            'NAME' => Loc::getMessage('NETI_SIMPLEDRAW_COMPONENT_PATH_NAME'),
            'CHILD' => array(
                'ID' => 'simpledraw_cmpx',
            ),
        )
    ),
);