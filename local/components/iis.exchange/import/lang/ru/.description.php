<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$MESS['NETI_SIMPLEDRAW_COMPONENT_DETAIL_NAME'] = 'Картинка детально (+редактирование)';
$MESS['NETI_SIMPLEDRAW_COMPONENT_DETAIL_DESCRIPTION'] = 'Отображает детально картинку и позволяет ее сохранить/изменить/удалить';