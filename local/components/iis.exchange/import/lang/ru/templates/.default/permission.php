<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$MESS['HELLO_MESSAGE'] = 'Вы пытаетесь получить доступ к редактированию или удалению сохраненного изображения. Для этого необходимо указать пароль к нему.';
$MESS['LABEL_PASSWORD'] = 'Пароль';
$MESS['BTN_SUBMIT'] = 'ОК';
$MESS['BTN_CANCEL'] = 'Отмена';