<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$MESS['CANVAS_BROWSER_NOT_SUPPORT'] = 'Ваш браузер не поддерживает Canvas';


$MESS['CANVAS_CLEAN'] = 'Очистить холст';
$MESS['CANVAS_TRICKNESS'] = 'Кисть';
$MESS['CANVAS_SMOOTHING'] = 'Сглаживание';
$MESS['CANVAS_ADAPTIVE'] = 'Адаптивный ход';
$MESS['CANVAS_MODE'] = 'Режим рисования';
$MESS['CANVAS_MODE_DRAW'] = 'Рисование';
$MESS['CANVAS_MODE_FILL'] = 'Заливка';
$MESS['CANVAS_MODE_ERASE'] = 'Стерка';
$MESS['CANVAS_COLOR'] = 'Цвет';
$MESS['CANVAS_OPACITY'] = 'Прозрачность';

$MESS['UF_NAME'] = 'Название рисунка';
$MESS['UF_PASSWORD'] = 'Пароль для редактирования';
$MESS['UF_PASSWORD_CONFIRM'] = 'Повторите пароль';

$MESS['BTN_PICTURE_SUBMIT'] = 'Сохранить изображение';
$MESS['BTN_CANCEL'] = 'Отмена';
