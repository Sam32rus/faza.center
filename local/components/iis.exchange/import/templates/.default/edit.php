<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

// подключаем библиотеку jquery
CJSCore::Init(array('jquery'));
$this->addExternalJS($templateFolder . '/atrament.min.js');

?>

<? if (!empty($arResult['SUCCESS'])) { ?>
<div class="simpledraw-alert success">
    <p><?=$arResult['SUCCESS']?></p>
</div>
<? } ?>

<? if (!empty($arResult['ERRORS'])) { ?>
<div class="simpledraw-alert danger">
    <? foreach ($arResult['ERRORS'] AS $error) { ?>
        <p><?=$error?></p>
    <? } ?>
</div>
<? } ?>

<div class="simpledraw">
    <div class="f-left">
        <form>
            <div class="simpledraw-form-group">
                <label for="CANVAS_TRICKNESS"><?=Loc::getMessage('CANVAS_TRICKNESS')?></label>
                <input id="CANVAS_TRICKNESS" class="simpledraw-form-control" type="range" min="1" max="40" oninput="atrament.weight = parseFloat(event.target.value);" value="2" step="0.1" autocomplete="off"/>
            </div>
            <div class="simpledraw-form-group">
                <input id="CANVAS_SMOOTHING" type="checkbox" onchange="atrament.smoothing = event.target.checked;" checked autocomplete="off">
                <label for="CANVAS_SMOOTHING"><?=Loc::getMessage('CANVAS_SMOOTHING')?></label>
            </div>
            <div class="simpledraw-form-group">
                <input id="CANVAS_ADAPTIVE" type="checkbox" onchange="atrament.adaptiveStroke = event.target.checked;" checked autocomplete="off">
                <label for="CANVAS_ADAPTIVE"><?=Loc::getMessage('CANVAS_ADAPTIVE')?></label>
            </div>
            <div class="simpledraw-form-group">
                <label for="CANVAS_MODE"><?=Loc::getMessage('CANVAS_MODE')?></label>
                <select id="CANVAS_MODE" class="simpledraw-form-control" onchange="atrament.mode = event.target.value;">
                    <option value="draw" default><?=Loc::getMessage('CANVAS_MODE_DRAW')?></option>
                    <option value="fill" default><?=Loc::getMessage('CANVAS_MODE_FILL')?></option>
                    <option value="erase" default><?=Loc::getMessage('CANVAS_MODE_ERASE')?></option>
                </select>
            </div>
            <div class="simpledraw-form-group">
                <label for="CANVAS_COLOR"><?=Loc::getMessage('CANVAS_COLOR')?></label>
                <input id="CANVAS_COLOR" class="simpledraw-form-control" type="color" onchange="atrament.color = event.target.value;" value="#000000" autocomplete="off">
            </div>
            <div class="simpledraw-form-group">
                <label for="CANVAS_OPACITY"><?=Loc::getMessage('CANVAS_OPACITY')?></label>
                <input id="CANVAS_OPACITY" class="simpledraw-form-control" type="range" min="0" max="1" onchange="atrament.opacity = parseFloat(event.target.value);" value="1" step="0.05" autocomplete="off">
            </div>
            <div class="simpledraw-form-group">
                <button id="clear" class="simpledraw-btn gray" onclick="event.preventDefault(); atrament.clear();" style="display:none;"><?=Loc::getMessage('CANVAS_CLEAN')?></button>
            </div>
        </form>
    </div>
    <div class="f-right canvas-container">
        <canvas id="simpledraw-canvas" width="<?=$arParams['PICTURE_WIDTH']?>" height="<?=$arParams['PICTURE_HEIGHT']?>">
            <p><?=Loc::GetMessage('CANVAS_BROWSER_NOT_SUPPORT')?></p>
        </canvas>
    </div>
    <div class="clear"></div>
</div>
<div class="picture-form">
    <form id="simpledraw-form" method="POST" action="<?=POST_FORM_ACTION_URI?>" enctype="multipart/form-data">
        <?= bitrix_sessid_post() ?>
        <input type="hidden" id="CANVAS_DATA" name="CANVAS_DATA" value="<?= htmlspecialcharsEx($arResult['DATA']['CANVAS_DATA']) ?>"/>
        <div class="simpledraw">
            <div class="f-left-50">
                <div class="simpledraw-form-group">
                    <label for="UF_NAME"><?=Loc::getMessage('UF_NAME')?></label>
                    <input type="text" class="simpledraw-form-control" id="UF_NAME" name="UF_NAME" value="<?=$arResult['DATA']['UF_NAME']?>"/>
                </div>
            </div>
            <div class="f-right-50">
                <div class="simpledraw-form-group">
                    <label for="UF_PASSWORD"><?=Loc::getMessage('UF_PASSWORD')?></label>
                    <input type="password" class="simpledraw-form-control" id="UF_PASSWORD" name="UF_PASSWORD"/>
                </div>
                <div class="simpledraw-form-group">
                    <label for="UF_PASSWORD_CONFIRM"><?=Loc::getMessage('UF_PASSWORD_CONFIRM')?></label>
                    <input type="password" class="simpledraw-form-control" id="UF_PASSWORD_CONFIRM" name="UF_PASSWORD_CONFIRM"/>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="submit-btn">
            <input type="submit" class="simpledraw-btn green" value="<?=Loc::GetMessage('BTN_PICTURE_SUBMIT')?>"/>
        </div>
    </form>
</div>

<script>
    var canvas = document.getElementById('simpledraw-canvas');
    var atrament = atrament(canvas, <?=$arParams['PICTURE_WIDTH']?>, <?=$arParams['PICTURE_HEIGHT']?>);

    var clearButton = document.getElementById('clear');
    canvas.addEventListener('dirty', function(e) {
            clearButton.style.display = atrament.dirty ? 'inline-block' : 'none';
    });
    
    <? if (!empty($arResult['DATA']['CANVAS_DATA'])) { ?>
        var context = canvas.getContext('2d');
        var img = new Image();
        img.src = '<?=$arResult['DATA']['CANVAS_DATA']?>';
        img.onload = function(){
            context.drawImage(img, 0, 0, canvas.width, canvas.height);
        };
    <? } ?> 
    
    $(document).ready(function(){
        $('#simpledraw-form').on('submit', function(event){
            var canvas = document.getElementById('simpledraw-canvas');
            document.getElementById('CANVAS_DATA').value = canvas.toDataURL('image/png');
        });
    });
</script>