<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

?>

<? if (!empty($arResult['SUCCESS'])) { ?>
<div class="simpledraw-alert success">
    <p><?=$arResult['SUCCESS']?></p>
</div>
<? } ?>

<div class="simpledraw-alert info">
    <?=Loc::getMessage('HELLO_MESSAGE')?>
</div>

<? if (!empty($arResult['ERRORS'])) { ?>
<div class="simpledraw-alert danger">
    <? foreach ($arResult['ERRORS'] AS $error) { ?>
        <p><?=$error?></p>
    <? } ?>
</div>
<? } ?>

<div class="simpledraw">
    <div class="f-left">
        <div class="form-bg">
            <form method="POST" action="<?=POST_FORM_ACTION_URI?>" enctype="multipart/form-data">
                <?= bitrix_sessid_post() ?>
                <div class="simpledraw-form-group">
                    <label for="PASS"><?=Loc::getMessage('LABEL_PASSWORD')?></label>
                    <input type="password" name="PASS" id="PASS" class="simpledraw-form-control"/>
                </div>
                <div class="simpledraw-form-group submit-btn">
                    <input type="submit" name="permission_check" class="simpledraw-btn green" value="<?=Loc::getMessage('BTN_SUBMIT')?>"/>
                    <a href="#" class="simpledraw-btn gray" onclick="history.back();"><?=Loc::getMessage('BTN_CANCEL')?></a>
                </div>
            </form>
        </div>
    </div>
    <div class="f-right">
        <h3><?=$arResult['DATA']['UF_NAME']?></h3>
        <div class="image">
            <img src="<?=$arResult['DATA']['UF_FILE_SRC']?>" alt="<?=$arResult['DATA']['UF_NAME']?>"/>
        </div>
    </div>
    <div class="clear"></div>
</div>