<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

//------------------------------------------------------------------------------
$arComponentParameters = array(
    'GROUPS' => array(
        'IMAGE' => array(
            'NAME' => Loc::getMessage('GROUP_IMAGE'),
            'SORT' => 150,
        ),
    ),
    'PARAMETERS' => array(
        
        'PICTURE_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('PICTURE_ID'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => '',
        ),
        'MODE' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('MODE'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 'view',
        ),
        
        'PICTURE_WIDTH' => array(
            'PARENT' => 'IMAGE',
            'NAME' => Loc::getMessage('PICTURE_WIDTH'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 500,
        ),
        'PICTURE_HEIGHT' => array(
            'PARENT' => 'IMAGE',
            'NAME' => Loc::getMessage('PICTURE_HEIGHT'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 500,
        ),
        'MAX_FILE_SIZE' => array(
            'PARENT' => 'IMAGE',
            'NAME' => Loc::getMessage('MAX_FILE_SIZE'),
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 20*1024*1024,
        ),
        
        'CACHE_TIME' => array('DEFAULT' => 3600000),
        'SET_TITLE' => array(),
    )
);