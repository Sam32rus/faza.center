<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
?>

<?
$APPLICATION->includeComponent(
        'neti.simpledraw:simpledraw.list',
        '',
        array(
            'PREVIEW_WIDTH'     => $arParams['PREVIEW_WIDTH'],
            'PREVIEW_HEIGHT'    => $arParams['PREVIEW_HEIGHT'],
            'CACHE_TYPE'        => $arParams['CACHE_TYPE'],
            'CACHE_TIME'        => $arParams['CACHE_TIME'],
            
            'NEW_URL'       => $arResult['SEF_FOLDER'].$arResult['URL_TEMPLATES']['new'],
            'DETAIL_URL'    => $arResult['SEF_FOLDER'].$arResult['URL_TEMPLATES']['detail'],
            'EDIT_URL'      => $arResult['SEF_FOLDER'].$arResult['URL_TEMPLATES']['edit'],
            'DELETE_URL'    => $arResult['SEF_FOLDER'].$arResult['URL_TEMPLATES']['delete'],
            
            'PICTURES_COUNT'            => $arParams['PICTURES_COUNT'],
            'DISPLAY_TOP_PAGER'         => $arParams['DISPLAY_TOP_PAGER'],
            'DISPLAY_BOTTOM_PAGER'      => $arParams['DISPLAY_BOTTOM_PAGER'],
            'SET_TITLE'                 => $arParams['SET_TITLE'],
        ),
        $component
    );
?>