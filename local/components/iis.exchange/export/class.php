<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule('neti.simpledraw')) {
    return;
}

class simpledraw extends CBitrixComponent
{
    function executeComponent()
    {
        global $APPLICATION;
        
        $arParams = $this->arParams;
        
        // адресация компонента
        $arDefaultVariableAliases404 = array();
        $arDefaultVariableAliases = array();
        
        $arDefaultUrlTemplates404 = array(
            'list' => '',
            'new' => "new/",
            'detail' => "#ID#/",
            'edit' => "#ID#/edit/",
            'delete' => "#ID#/del/",
        );
        $arComponentVariables = array('ID', 'edit', 'new', 'del');

        if($arParams['SEF_MODE'] == 'Y')
        {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams['SEF_URL_TEMPLATES']);
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams['VARIABLE_ALIASES']);

            $engine = new CComponentEngine($this);
            $componentPage = $engine->guessComponentPath(
                $arParams['SEF_FOLDER'],
                $arUrlTemplates,
                $arVariables
            );
            
            $set404 = FALSE;
            if(!$componentPage)
            {
                $componentPage = 'list';
                $set404 = TRUE;
            }
            
            CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
            
            $this->arResult = array(
		'SEF_FOLDER' => $arParams['SEF_FOLDER'],
		'URL_TEMPLATES' => $arUrlTemplates,
		'VARIABLES' => $arVariables,
		'ALIASES' => $arVariableAliases,
            );
        }
        else
        {
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams['VARIABLE_ALIASES']);
            CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);
            
            $componentPage = '';
            
            if (isset($arVariables['new'])) { $componentPage = 'new'; }
            elseif (isset($arVariables['edit']) && isset($arVariables['ID']) && intval($arVariables['ID']) > 0) { $componentPage = 'edit'; }
            elseif (isset($arVariables['del']) && isset($arVariables['ID']) && intval($arVariables['ID']) > 0) { $componentPage = 'delete'; }
            elseif (isset($arVariables['ID']) && intval($arVariables['ID']) > 0) { $componentPage = 'detail'; }
            else { $componentPage = 'list'; }
            
            $this->arResult = array(
		'SEF_FOLDER' => '',
		'URL_TEMPLATES' => Array(
                    'list' => htmlspecialcharsbx($APPLICATION->GetCurPage()),
                    'new'  => htmlspecialcharsbx($APPLICATION->GetCurPage().'?new=Y'),
                    'detail' => htmlspecialcharsbx($APPLICATION->GetCurPage().'?'.$arVariableAliases['ID'].'=#ID#'),
                    'edit' => htmlspecialcharsbx($APPLICATION->GetCurPage().'?'.$arVariableAliases['ID'].'=#ID#&edit=Y'),
                    'delete' => htmlspecialcharsbx($APPLICATION->GetCurPage().'?'.$arVariableAliases['ID'].'=#ID#&del=Y'),
		),
		'VARIABLES' => $arVariables,
		'ALIASES' => $arVariableAliases
            );
        }
        
        // определяем шаблоны для вывода
        if ($componentPage == 'edit') {
            $componentPage = 'detail';
            $this->arResult['DETAIL_MODE'] = 'edit';
        } elseif ($componentPage == 'new') {
            $componentPage = 'detail';
            $this->arResult['DETAIL_MODE'] = 'new';
        } elseif ($componentPage == 'delete') {
            $componentPage = 'detail';
            $this->arResult['DETAIL_MODE'] = 'delete';
        } else {
            $this->arResult['DETAIL_MODE'] = 'view';
        }
        
        if ($set404) {
            CHTTP::SetStatus('404 Not Found');
            @define('ERROR_404', 'Y');
        }

        $this->includeComponentTemplate($componentPage);
    }
        
}