<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

$MESS['GROUP_IMAGE'] = 'Настройки изображений и холста';
$MESS['PICTURE_WIDTH'] = 'Ширина холста';
$MESS['PICTURE_HEIGHT'] = 'Высота холста';
$MESS['PREVIEW_WIDTH'] = 'Ширина картинки превью (в списке)';
$MESS['PREVIEW_HEIGHT'] = 'Высота картинки превью (в списке)';
$MESS['MAX_FILE_SIZE'] = 'Максимальный размер файла (в байтах)';

$MESS['SIMPLEDRAW_ELEMENT_ID_DESC'] = 'Идентификатор картинки';
$MESS['SIMPLEDRAW_SEF_LIST'] = 'Страница списка';
$MESS['SIMPLEDRAW_SEF_DETAIL'] = 'Страница детального просмотра';
$MESS['SIMPLEDRAW_SEF_EDIT'] = 'Страница редактирования';
$MESS['SIMPLEDRAW_SEF_NEW'] = 'Страница создания картинки';
$MESS['SIMPLEDRAW_SEF_DELETE'] = 'Страница удаления картинки';

$MESS['GROUP_PAGER'] = 'Настройки постраничной навигации';
$MESS['PICTURES_COUNT'] = 'Количество элементов на страницу';
$MESS['DISPLAY_TOP_PAGER'] = 'Выводить навигацию выше списка';
$MESS['DISPLAY_BOTTOM_PAGER'] = 'Выводить навигацию ниже списка';