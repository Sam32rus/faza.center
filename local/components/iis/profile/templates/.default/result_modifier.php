<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$cp = $this->__component; // объект компонента

if (is_object($cp))
{
   $cp->arResult['CONFIRMED'] = $arResult['CONFIRMED'];
   $cp->SetResultCacheKeys(array('CONFIRMED','CACHED_TPL'));
}