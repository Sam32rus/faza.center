<? ob_start(); ?>
<main class="main">
    <div class="content">
        <section class="cabinet">
            <div class="cabinet__inner">
                <div class="cabinet__menu js__cabinet-menu">
                    #SIDEBAR_COMPONENT#
                </div>

                <div class="cabinet__main">
                    <div class="cabinet__title js__cabinet-menu-open">Личные данные</div>
                    <div class="cabinet__content" data-target="ajax-content">
                        <form action="" class="personal" method="POST" action="<?=POST_FORM_ACTION_URI?>">
                            <input type="hidden" name="formType" value="main">
                            <div class="personal__inner">
                                <div class="form-data">
                                    <div class="form-data__wrap">
                                        <input class="form-data__input js__default" placeholder="ФИО" value="<?= $arResult['FULL_NAME'] ?>" type="text" name="FIO">
                                    </div>
                                    <div class="form-data__error" data-error="FIO">Заполните поле "ФИО"</div>
                                </div>
                                <div class="personal__wrap-two">
                                    <div class="form-data personal__date">
                                        <div class="form-data__wrap">
                                            <input class="form-data__input js__date" value="<?= $arResult['PERSONAL_BIRTHDAY'] ?>" onfocus="(this.type='date')" placeholder="Дата рождения" name="BIRTHDAY" type="text">
                                        </div>
                                        <div class="form-data__error" data-error="BIRTHDAY">Заполните поле "Дата рождения"</div>
                                    </div>
                                    <div class="form-data personal__email">
                                        <div class="form-data__wrap">
                                            <input class="form-data__input js__email" value="<?= $arResult["EMAIL"] ?>" placeholder="E-mail" name="EMAIL" type="email">
                                        </div>
                                        <div class="form-data__error" data-error="EMAIL">Заполните поле "E-mail"</div>
                                    </div>
                                </div>

                                <div class="personal__title">Телефон</div>
                                <? if (!empty($arResult["PERSONAL_PHONE"])) { ?>
                                    <div class="js__personal-phone-field">
                                        <div class="form-data">
                                            <div class="form-data__wrap form-data__wrap_editing">
                                                <input class="form-data__input js__phone" disabled="disabled" value="<?= $arResult["PERSONAL_PHONE"] ?>" name="PHONE" type="phone">
                                                <div class="form-data__editing js__editing"></div>
                                            </div>
                                            <div class="form-data__error" data-error="PHONE">Заполните поле "Телефон"</div>
                                        </div>
                                    </div>
                                <? } ?>
                                <? foreach($arResult['UF_PHONES'] as $keyPhone => $phone): ?>
                                    <div class="js__personal-phone-field">
                                        <div class="form-data">
                                            <div class="form-data__wrap form-data__wrap_editing">
                                                <input class="form-data__input js__phone" disabled="disabled" value="<?= $phone ?>" name="PHONE" type="phone">
                                                <div class="form-data__editing js__editing"></div>
                                            </div>
                                            <div class="form-data__error" data-error="PHONE">Заполните поле "Телефон"</div>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                                <? if(count($arResult['UF_PHONES']) < 2) { ?>
                                    <div class="button-add" data-popup-btn="add-phone">Добавить телефон</div>
                                <? } ?>

                                <div class="personal__title">Адреса</div>
                                <? if (!empty($arResult['UF_ADDRESS'])) { ?>
                                    <? foreach($arResult['UF_ADDRESS'] as $keyAddress => $address): ?>
                                        <div class="js__personal-address-field">
                                            <div class="form-data">
                                                <div class="form-data__wrap form-data__wrap_editing">
                                                    <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="address_<?=$keyAddress?>" rows="1"><?= $address ?></textarea>
                                                    <div class="form-data__editing js__editing">
                                                        <div class="form-data__error">Заполните поле "Адреса"</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <? endforeach; ?>
                                <? } ?>
                                <? if(count($arResult['UF_ADDRESS']) < 2) { ?>
                                    <div class="button-add" data-popup-btn="add-address">Добавить адрес</div>
                                <? } ?>

                                <div class="personal__title">Юридические реквизиты</div>
                                <? if ($arResult['CONFIRMED']) { ?>
                                    <div class="js__personal-requisites-field">
                                        <? if ($arResult["UF_ORGANIZATION"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">Наименование организации</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="2"><?= $arResult["UF_ORGANIZATION"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["UF_UR_ADDRESS"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">Юридический адрес</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="2"><?= $arResult["UF_UR_ADDRESS"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["UF_FACT_ADDRESS"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">Фактический адрес</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="2"><?= $arResult["UF_FACT_ADDRESS"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["UF_INN_KPP"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">ИНН/КПП</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="1"><?= $arResult["UF_INN_KPP"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["UF_OGRN"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">ОГРН</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="1"><?= $arResult["UF_OGRN"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["UF_OKPO"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">ОКПО</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="1"><?= $arResult["UF_OKPO"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["UF_RS"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">Расчетный счет</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="1"><?= $arResult["UF_RS"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["UF_UCH_BANK"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">Учреждение Банка</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="1"><?= $arResult["UF_UCH_BANK"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["UF_BIK_BANK"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">БИК Банка</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="1"><?= $arResult["UF_BIK_BANK"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["UF_KS_BANK"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">К/с Банка</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="1"><?= $arResult["UF_KS_BANK"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["UF_UPRAV"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">ФИО руководителя</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="1"><?= $arResult["UF_UPRAV"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["UF_DOGOVOR_DATA"]) : ?>
                                            <div class="personal__row">
                                                <div class="personal__coll-title">Для заключения договора прописывать</div>
                                                <div class="personal__coll-data">
                                                    <div class="form-data">
                                                        <div class="form-data__wrap form-data__wrap_editing">
                                                            <textarea class="form-data__textarea js__default" disabled="disabled" name="" id="" rows="3"><?= $arResult["UF_DOGOVOR_DATA"] ?></textarea>
                                                            <div class="form-data__editing js__editing"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                    </div>
                                <? } else { ?>
                                    <div class="button-add" data-popup-btn="add-legal">Добавить реквизиты</div>
                                <? } ?>

                                <div class="personal__wrap-btn">
                                    <button class="js__disabled-btn form-data__submit blue-button" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>

<!--Попап добавления моб. телефона -->
<div class="popup" data-popup-id="add-phone">
    <div class="popup__inner">
        <div class="popup__close" data-popup-close="add-phone"></div>
        <div class="popup__content">
            <div class="popup__title">Добавление телефона</div>

            <form class="js__form-phone" method="POST" action="<?=POST_FORM_ACTION_URI?>">
                <input type="hidden" name="formType" value="phone">
                <input type="hidden" >
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__phone required" placeholder="Телефон" type="phone" name="PHONE">
                    </div>
                    <div class="form-data__error">Заполните поле "Телефон"</div>
                </div>
                <div class="form-data">
                    <input id="make-main-phone" class="form-data__checkbox js__make-main-phone" type="checkbox" name="mainPhone">
                    <label for="make-main-phone" class="form-data__label">Сделать основным номером для связи</label>
                </div>
                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <input type="submit" class="js__add-phone-personal form-data__submit blue-button" value="Готово">
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<!--/Попап добавления моб. телефона -->

<!--Попап добавления адреса -->
<div class="popup" data-popup-id="add-address">
    <div class="popup__inner w-640">
        <div class="popup__close" data-popup-close="add-address"></div>
        <div class="popup__content">
            <div class="popup__title">Добавление адреса</div>

            <form class="js__form-address" method="POST" action="<?=POST_FORM_ACTION_URI?>">
                <input type="hidden" name="formType" value="address">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__city required" required placeholder="Населенный пункт" type="text" name="CITY">
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="flex-wrap flex-wrap_jc-sb">

                    <div class="form-data flex-wrap__item-73">
                        <div class="form-data__wrap">
                            <input class="form-data__input js__street required" required placeholder="Улица" type="text" name="STREET">
                        </div>
                        <div class="form-data__error">Заполните поле</div>
                    </div>

                    <div class="form-data  flex-wrap__item-22">
                        <div class="form-data__wrap">
                            <input class="form-data__input js__house-number required" required placeholder="Дом" type="text" name="HOUSE">
                        </div>
                        <div class="form-data__error">Заполните поле</div>
                    </div>

                </div>

                <div class="flex-wrap flex-wrap_jc-sb">

                    <div class="form-data flex-wrap__item-30">
                        <div class="form-data__wrap">
                            <input class="form-data__input js__entrance" placeholder="Подъезд" type="text" name="ENTRANCE">
                        </div>
                        <div class="form-data__error">Заполните поле</div>
                    </div>

                    <div class="form-data flex-wrap__item-30">
                        <div class="form-data__wrap">
                            <input class="form-data__input js__floor" placeholder="Этаж" type="number" name="FLOOR">
                        </div>
                        <div class="form-data__error">Заполните поле</div>
                    </div>

                    <div class="form-data flex-wrap__item-30">
                        <div class="form-data__wrap">
                            <input class="form-data__input js__apartment-number" placeholder="Кв/офис" type="number" name="FLAT">
                        </div>
                        <div class="form-data__error">Заполните поле</div>
                    </div>

                </div>

                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <input type="submit" class="js__add-address-personal form-data__submit blue-button" value="Готово">
                    </div>
                </div>

                <div class="popup__gray-text">
                    Нажимая кнопку вы соглашаетесь с условиями <a href="/info/polzovatelskoe-soglashenie/">пользовательского&nbsp;соглашения</a>
                </div>

            </form>
        </div>
    </div>
</div>
<!--/Попап добавления адреса -->

<!-- Попап добавления юридического адреса или ИП -->
<div class="popup" data-popup-id="add-legal">
    <div class="popup__inner w-640">
        <div class="popup__close" data-popup-close="add-legal"></div>
        <div class="popup__content">
            <div class="popup__title">Добавление юридического адреса</div>
            
            <div class="form-data-radio">
                <div class="form-data-radio__title">Вид предпринимательской деятельности:</div>
                <div class="form-data-radio__inner">

                    <div class="form-data-radio__item">
                        <input id="radio-1" type="radio" name="radio" value="ИП" checked>
                        <label for="radio-1" class="form-data-radio__label js__label-ip">ИП</label>
                    </div>

                    <div class="form-data-radio__item">
                        <input id="radio-2" type="radio" name="radio" value="OOO">
                        <label for="radio-2" class="form-data-radio__label js__label-ooo">OOO</label>
                    </div>

                </div>
            </div>

            <!-- Форма ИП-->
            <form class="js__form-address-ip" method="POST" action="<?=POST_FORM_ACTION_URI?>">
                <input type="hidden" name="formType" value="ip">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__fio-ip required" rows="1" placeholder="ФИО" name="FULL_NAME"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__ogrn-ip required" rows="1" placeholder="ОГРН ИП" name="OGRN_IP"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__inn-ip required" rows="1" placeholder="ИНН" name="INN"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <input class="form-data__input js__date-ip required" placeholder="Дата регистрации" type="date" name="REG_DATE">
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__point-reg-ip required" rows="2" placeholder="Адрес регистрации" name="REG_PLACE"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__invoice-ip required" rows="1" placeholder="Расчетный счет" name="RS"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea required js__bank-ip" rows="2" placeholder="Учреждение Банка" name="UCH_BANK"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__bik-bank-ip required" rows="1" placeholder="БИК Банка" name="BIK_BANK"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__ks-bank-ip required" rows="1" placeholder="К/с Банка" name="KS_BANK"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>


                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <input type="submit" class="js__add-requisites-personal-ip form-data__submit blue-button" value="Готово">
                    </div>
                </div>

            </form>
            <!-- /Форма ИП-->

            <!-- Форма ООО-->
            <form class="js__form-address-ooo" style="display: none;" method="POST" action="<?=POST_FORM_ACTION_URI?>">
                <input type="hidden" name="formType" value="ooo">
                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__name-ooo required" rows="2" placeholder="Наименование организации" name="ORGANIZATION"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__address-ooo required" rows="3" placeholder="Юридический адрес" name="UR_ADDRESS"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__address-ooo required" rows="3" placeholder="Фактический адрес" name="FACT_ADDRESS"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__inn-ooo required" rows="1" placeholder="ИНН/КПП" name="INN_KPP"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__ogrn-ooo required" rows="1" placeholder="ОГРН" name="OGRN"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__invoice-ooo required" rows="1" placeholder="Расчетный счет" name="RS"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__bank-ooo required" rows="2" placeholder="Учреждение Банка" name="UCH_BANK"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__bik-bank-ooo required" rows="1" placeholder="БИК Банка" name="BIK_BANK"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__ks-bank-ooo required" rows="1" placeholder="К/с Банка" name="KS_BANK"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap">
                        <textarea class="form-data__textarea js__fio-ooo required" rows="2" placeholder="ФИО руководителя" name="UPRAV"></textarea>
                    </div>
                    <div class="form-data__error">Заполните поле</div>
                </div>

                <div class="form-data">
                    <div class="form-data__wrap-submit">
                        <input type="submit" class="js__add-requisites-personal-ooo form-data__submit blue-button" value="Готово">
                    </div>
                </div>

            </form>
            <!-- /Форма ООО-->

            <div class="popup__gray-text">
                Нажимая кнопку вы соглашаетесь с условиями <a href="/info/polzovatelskoe-soglashenie/">пользовательского&nbsp;соглашения</a>
            </div>
        </div>
    </div>
</div>
<!--/Попап добавления юридического адреса или ИП -->
<? $component->arResult['CACHED_TPL'] = ob_get_clean(); ?>