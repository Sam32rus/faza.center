<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arResult['CACHED_TPL'] = preg_replace_callback(
    '/#SIDEBAR_COMPONENT#/ims' . BX_UTF_PCRE_MODIFIER,
    function ($matches) use ($arResult, $arParams)
    {
        global $APPLICATION;

        ob_start();

        $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "template_personal",
            array(
                "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
                "DELAY" => "N",    // Откладывать выполнение шаблона меню
                "MAX_LEVEL" => "1",    // Уровень вложенности меню
                "MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
                    0 => "",
                ),
                "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                "ROOT_MENU_TYPE" => "left",    // Тип меню для первого уровня
                "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                "CONFIRMED" => $arResult['CONFIRMED']
            ),
            false
        );

        return ob_get_clean();
    },
    $arResult['CACHED_TPL']
);

echo $arResult['CACHED_TPL'];
