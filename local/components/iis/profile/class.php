<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Engine\CurrentUser;
use Bitrix\Main\UserTable;
use Bitrix\Main\ORM\Fields\ExpressionField;
use Bitrix\Main\Loader;

Loader::includeModule("main");
class iisProfileUserComponent extends CBitrixComponent
{
    public function executeComponent()
    {
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        $user = UserTable::getList([
            'filter' => ['ID' => CurrentUser::get()->getId()],
            'select' => [
                new ExpressionField('FULL_NAME', 'CONCAT(%s, " ", %s, " ", %s)', ['NAME', 'LAST_NAME', 'SECOND_NAME']), 
                'LOGIN', 
                'EMAIL', 
                'PERSONAL_BIRTHDAY',
                'PERSONAL_PHONE',
                'CONFIRMED' => 'UF_UR_CONFIRMED',
                'UF_*'
            ],
        ])->Fetch();
        // $user['PERSONAL_BIRTHDAY'] = $user['PERSONAL_BIRTHDAY']->toString();
        if ($request->isPost())
        {
            $fields = [];
            $postData = $request->getPostList()->toArray();
            $userObject = new CUser;
            switch($postData['formType'])
            {
                case "phone":
                    if(!empty($postData['mainPhone']))
                    {
                        $fields['PERSONAL_PHONE'] = $postData['PHONE'];
                    }
                    else
                    {
                        if(empty($user['PERSONAL_PHONE']))
                        {
                            $fields['PERSONAL_PHONE'] = $postData['PHONE'];
                        }
                        else
                        {
                            $user['UF_PHONES'][] = $postData['PHONE'];
                            $fields['UF_PHONES'] = $user['UF_PHONES'];
                        }
                        
                    }
                    $userObject->Update(CurrentUser::get()->getId(), $fields);
                    break;
                case "address":
                    $address = $postData['CITY'] . ', ' .  $postData['STREET'] . ', ' . $postData['HOUSE'];
                    if(!empty($postData['ENTRANCE']))
                    {
                        $address .= ', ' .  $postData['ENTRANCE'];
                    }
                    if(!empty($postData['FLOOR']))
                    {
                        $address .= ', ' .  $postData['FLOOR'];
                    }
                    if(!empty($postData['FLAT']))
                    {
                        $address .= ', ' .  $postData['FLAT'];
                    }
                    $user['UF_ADDRESS'][] = $address;
                    $fields['UF_ADDRESS'] = $user['UF_ADDRESS'];
                    $userObject->Update(CurrentUser::get()->getId(), $fields);
                    break;
                case 'ip':
                    $fields['UF_TYPE'] = 4;
                    $fullName = explode(' ', $postData['FULL_NAME']);
                    $fields['SECOND_NAME'] = $fullName[0];
                    $fields['NAME'] = $fullName[1];
                    $fields['LAST_NAME'] = $fullName[2];
                    
                    unset($postData['formType'], $postData['FULL_NAME']);
                    unset($postData['formType']);
                    foreach($postData as $key => $value)
                    {
                        $fields['UF_' . $key] = $value;
                    }
                    if($userObject->Update(CurrentUser::get()->getId(), $fields))
                    {
                        $sendMessage = true;
                    }
                    break;
                case 'ooo':
                    $fields['UF_TYPE'] = 5;
                    
                    unset($postData['formType']);
                    foreach($postData as $key => $value)
                    {
                        $fields['UF_' . $key] = $value;
                    }
                    if($userObject->Update(CurrentUser::get()->getId(), $fields))
                    {
                        $sendMessage = true;
                    }
                    break;
                // case 'main':

                //     break;
            }
            if($sendMessage){

            }
            // echo '<pre>';
            // print_r($postData);
            // echo '</pre>';
        }

        
        $this->arResult = $user;
        $this->includeComponentTemplate();
    }
}
