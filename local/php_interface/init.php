<?
$eventManager = \Bitrix\Main\EventManager::getInstance();

define("CATALOG_URL"           	, SITE_DIR."catalog/");
define("COMPARE_URL"           	, SITE_DIR."catalog/compare/");
define("FAVORITE_URL"          	, SITE_DIR."personal/favorites/");
define("PERSONAL_URL"          	, SITE_DIR."personal/");
define("BASKET_URL"          	, SITE_DIR."personal/cart/");
define("SUBCATEGORY_COUNT"     	, 4);
define("CATALOG_FILTER_SHOW"   	, 5);
define("REVIEWS_IBLOCK"			, 17);
define("REVIEW_PROPERTY"		, 'review_count');

include('functions.php');
include('include/registerListeners.php');
require("exchange.php");

define("CATALOG_IBLOCK_ID", 7); //ид инфоблока 

//добавление агента 
// $obRes = CAgent::GetList($arOrder = Array("ID" => "DESC", array("NAME"=>"%::exchangeAgent(%")));
// if(!$obRes->Fetch()){
//     Exchange1C::addAgent();
// }

$eventManager->addEventHandler("search","BeforeIndex","BeforeIndexHandler");

$eventManager->addEventHandler("iblock","OnAfterIBlockElementAdd","reviewCountChange");
$eventManager->addEventHandler("iblock","OnAfterIBlockElementUpdate","reviewCountChange");
$eventManager->addEventHandler("iblock","OnAfterIBlockElementDelete","reviewCountReduce");

$eventManager->addEventHandler("main","OnBeforeUserRegister","OnBeforeUserRegisterHandler");


function BeforeIndexHandler($arFields) {
	$arrIblock = array(7);
	/*исключаем детальное описание и анонс*/
	$arDelFields = array("DETAIL_TEXT", "PREVIEW_TEXT") ;
	if (CModule::IncludeModule('iblock') && $arFields["MODULE_ID"] == 'iblock' && in_array($arFields["PARAM2"], $arrIblock) && intval($arFields["ITEM_ID"]) > 0){
		$dbElement = CIblockElement::GetByID($arFields["ITEM_ID"]) ;
		if ($arElement = $dbElement->Fetch()){
			foreach ($arDelFields as $value){
				if (isset ($arElement[$value]) && strlen($arElement[$value]) > 0){
					$arFields["BODY"] = str_replace (CSearch::KillTags($arElement[$value]) , "", CSearch::KillTags($arFields["BODY"]) );
					$arFields["BODY"] = trim($arFields["BODY"]);
				}
			}
		}
		return $arFields;
	}
}
function reviewCountChange(&$arFields){
	if($arFields['IBLOCK_ID'] == REVIEWS_IBLOCK && $arFields['ACTIVE'] == 'Y'){
		reviewCountAdd($arFields);
	}elseif($arFields['IBLOCK_ID'] == REVIEWS_IBLOCK && $arFields['ACTIVE'] == 'N'){
		reviewCountReduce($arFields);
	}
}
function reviewCountAdd($arFields){
	if($arFields['IBLOCK_ID'] == REVIEWS_IBLOCK && $arFields['ACTIVE'] == 'Y'){
		$db_props = CIBlockElement::GetProperty(
			REVIEWS_IBLOCK,
			$productID,
			[],
			["CODE" => REVIEW_PROPERTY]
		);
        if($ar_props = $db_props->Fetch()){
            $reviewCount = IntVal($ar_props["VALUE"]);
        }else{
            $reviewCount = false;
        }

        if($reviewCount !== false){
            CIBlockElement::SetPropertyValuesEx($productID, false, array('review_count' => $reviewCount + 1));
        }
	}
}
function reviewCountReduce($arFields){
	if($arFields['IBLOCK_ID'] == REVIEWS_IBLOCK){
		$db_props = CIBlockElement::GetProperty(
			REVIEWS_IBLOCK,
			$productID,
			[],
			["CODE" => REVIEW_PROPERTY]
		);
        if($ar_props = $db_props->Fetch())
            $reviewCount = IntVal($ar_props["VALUE"]);
        else
            $reviewCount = false;

        if($reviewCount !== false)
            CIBlockElement::SetPropertyValuesEx($productID, false, array('review_count' => $reviewCount - 1));
	}
}
function getNumericCase($number, $once, $multi2_4, $multi5_20): string
{
	if ($number > 20)
	{
		$number %= 10;
	}
	if ($number == 1)
	{
		return $once;
	}
	elseif($number >= 2 && $number <= 4)
	{
		return $multi2_4;
	}
	else
	{
		return $multi5_20;
	}
}
?>
