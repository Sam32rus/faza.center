<?
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

function getHighloadElements($HL_ID, $arrKeys = false, $filter = false){
  $hlblock = HL\HighloadBlockTable::getById($HL_ID)->fetch();
  $entity = HL\HighloadBlockTable::compileEntity($hlblock);
  $entity_data_class = $entity->getDataClass();
  if(!$filter){
    $rsData = $entity_data_class::getList(
      array(
       "select" => array("*"),
       "order" => array("ID" => "ASC"),
      )
    );
  }else{
    $rsData = $entity_data_class::getList(
      array(
        "filter" => $filter,
        "select" => array("*"),
        "order" => array("ID" => "ASC"),
      )
    );
  }
  
  $arResult = array();
  while($arData = $rsData->Fetch()){
    $arResult[$arrKeys ? $arData[$arrKeys] : $arData['ID']] = $arData;
  }
  return $arResult;
}

function getHLElementByXML($HL_ID, $XML_ID){
  Loader::includeModule("iblock");
  Loader::includeModule("highloadblock");

  $hlblock = HL\HighloadBlockTable::getById($HL_ID)->fetch(); // id highload блока
  $entity = HL\HighloadBlockTable::compileEntity($hlblock);
  $entityClass = $entity->getDataClass();

  $res = $entityClass::getList(array(
    'select' => array('*'),
    'filter' => array('UF_XML_ID' => $XML_ID)
  ));

  return $res->fetch();
}

function showData($data){
  echo '<pre>';
  print_r($data);
  echo '</pre>';
}

function saleFilter(){
  Loader::includeModule("sale");
  Loader::includeModule("catalog");

  $arSales = [];
  $arSaleFilter = [];
  $dbProductDiscounts = CSaleDiscount::GetList(
      array("SORT" => "ASC"),
      array(),
      false,
      false,
      array()
  );

  while ($arProductDiscounts = $dbProductDiscounts->Fetch()){
    $arSales[] = unserialize(CSaleDiscount::GetByID($arProductDiscounts['ID'])['ACTIONS']);
  }
  foreach ($arSales as $key => $arSale) {
    if(!empty($arSale['CHILDREN'])){
      foreach ($arSale['CHILDREN'] as $keyChildren => $arChildren) {
        if(!empty($arChildren['CHILDREN'])){
          foreach ($arChildren['CHILDREN'] as $keyChildren2 => $arChildren2) {
            if($arChildren2['CLASS_ID'] == 'CondIBSection'){
              if(is_array($arChildren2['DATA']['value'])){
                foreach ($arChildren2['DATA']['value'] as $item) {
                  $arSaleFilter['IBLOCK_SECTION_ID'][$item] = $item;
                }
              }else{
                $arSaleFilter['IBLOCK_SECTION_ID'][$arChildren2['DATA']['value']] = $arChildren2['DATA']['value'];
              }
              
            }elseif($arChildren2['CLASS_ID'] == 'CondIBElement'){
              if(is_array($arChildren2['DATA']['value'])){
                foreach ($arChildren2['DATA']['value'] as $item) {
                  $arSaleFilter['ID'][] = $item;
                }
              }else{
                $arSaleFilter['ID'][] = $arChildren2['DATA']['value'];
              }
            }
          }
        }
      }
    }
  }
  return $arSaleFilter;
}

function html_remove_attributes($text, $allowed = []){
    $attributes = implode('|', $allowed);
    $reg = '/(<[\w]+)([^>]*)(>)/i';
    $text = preg_replace_callback(
        $reg,
        function ($matches) use ($attributes) {
            // Если нет разрешенных атрибутов, возвращаем пустой тег
            if (!$attributes) {
                return $matches[1] . $matches[3];
            }

            $attr = $matches[2];
            $reg = '/(' . $attributes . ')="[^"]*"/i';
            preg_match_all($reg, $attr, $result);
            $attr = implode(' ', $result[0]);
            $attr = ($attr ? ' ' : '') . $attr;

            return $matches[1] . $attr . $matches[3];
        },
        $text
    );

    return $text;
}

function getMainPageBrandsData(){
  $brands = getHighloadElements(2, false, ['!UF_MAIN_PAGE_SHOW' => false]);
  foreach($brands as $key => $brand){
    if($brand['UF_BANNER_MAIN']):
      $brands[$key]['UF_BANNER_MAIN'] = CFile::GetFileArray($brand['UF_BANNER_MAIN']);
    elseif($brand['UF_FILE']):
      $brands[$key]['UF_BANNER_MAIN'] = CFile::GetFileArray($brand['UF_FILE']);
    else:
      $brands[$key]['UF_BANNER_MAIN'] = [
        'SRC' => '/local/templates/faza/assets/i/brand_no_photo.png'
      ];
    endif;
  }
  return $brands;
}