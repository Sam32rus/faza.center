<?
use \Bitrix\Main\Loader;
use \Bitrix\Iblock;
use \Bitrix\Catalog\StoreProductTable;
use \Bitrix\Catalog\StoreTable;
use \Bitrix\Main\Text\HtmlFilter;
use \Bitrix\Highloadblock as HL;
use \Bitrix\Main\Entity;

class Exchange1C {
    const MAX_PAGE_SIZE = 5000;
    const MAX_EXECUTION_TIME = 30;
    const AGENT_TIME_INTERVAL = 1; //минут

    public static function exchangeAgent($lastId = 0, $num = 1)
    {
        $startAgentTimestamp = time();
        $nextStep = $num + 1;
        if (
            !Loader::includeModule('iblock')
            || !Loader::includeModule('catalog')
            || !Loader::includeModule('highloadblock')
        )
        {
            die();
        }
        

        ini_set('memory_limit', '1G');

        $catalogIblock = CATALOG_IBLOCK_ID;
        $entityBrand = HL\HighloadBlockTable::compileEntity(2);
        $file = $_SERVER['DOCUMENT_ROOT'].'/upload/exchange-1c-'.$num.'.xml';
        unlink($file);
        $fp = fopen($file, 'a+');
        $data = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
        $data .= '<yml_catalog date="'.date("Y-m-d H:i").'">'.PHP_EOL;

        $data .= '<shop>'.PHP_EOL;
        $data .= '<name>Faza</name>'.PHP_EOL;
        $data .= '<company>Faza</company>'.PHP_EOL;
        $data .= '<url>https://faza.center/</url>'.PHP_EOL;

        fwrite($fp, $data);
        if ($num == 1)
        {
            $data = '<categories>'.PHP_EOL;

            $iterator = Iblock\SectionTable::query()
                ->addSelect('ID')
                ->addSelect('IBLOCK_SECTION_ID')
                ->addSelect('NAME')
                ->where('IBLOCK_ID', $catalogIblock)
                ->where('GLOBAL_ACTIVE', 'Y')
                ->where('ACTIVE', 'Y')
                ->addOrder('LEFT_MARGIN', 'ASC')
                ->exec();
            
            foreach ($iterator as $item)
            {
                if($item['IBLOCK_SECTION_ID'] > 0)
                {
                    $data .= '<category id="'.$item['ID'].'" parentId="'.$item['IBLOCK_SECTION_ID'].'">'.HtmlFilter::encode($item['NAME']).'</category>'.PHP_EOL;
                }
                else
                {
                    $data .= '<category id="'.$item['ID'].'">'.HtmlFilter::encode($item['NAME']).'</category>'.PHP_EOL;
                }
            }
            
            $data .= '</categories>'.PHP_EOL;
        }

        $data .= '<products>'.PHP_EOL;
        fwrite($fp, $data);

        $filter = [
            ">ID" => $lastId,
            'IBLOCK_ID' => $catalogIblock,
            'ACTIVE' => 'Y',
            'ACTIVE_DATE' => 'Y',
            '!IBLOCK_SECTION_ID' => false
        ];

        $fields = [
            0 => "ID",
            1 => "NAME",
            2 => "PREVIEW_PICTURE",
            3 => "DETAIL_PICTURE",
            4 => "CODE",
            5 => "IBLOCK_SECTION_ID",
            6 => "XML_ID",
            7 => 'PREVIEW_TEXT',
            8 => 'DETAIL_TEXT',
            9 => 'IBLOCK_ID'
        ];

        $pageSize = self::MAX_PAGE_SIZE;
        $pageNum = $num;

        do {
            $result = [];

            $iterator = \CIBlockElement::GetList(['ID' => 'ASC'], $filter, false, ['nPageSize' => $pageSize, 'iNumPage' => $pageNum, 'checkOutOfRange' => true], $fields);
            while ($item = $iterator->fetch())
            {
                if ((time()-$startAgentTimestamp) > self::MAX_EXECUTION_TIME){
                    
                    //Удаление старого агента
                    $obRes = CAgent::GetList(Array("ID" => "DESC"), array("NAME"=>"%::exchangeAgent(%"));
                    while ($obj = $obRes->GetNext())
                    {
                        $pos = strpos($obj["NAME"], get_called_class()."::exchangeAgent(");

                        if ($pos !== false) {
                            CAgent::RemoveAgent($obj["NAME"], "");
                        }
                    }
                    //Добавляем новый агент
                    self::addOneMoreStepAgent($item['ID'], $nextStep);
    
                    return false;
                    break;
                }
                
                $item['PROPERTIES']['artikul'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"artikul"))->Fetch();
                $item['PROPERTIES']['shtrihkod'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"shtrihkod"))->Fetch();

                $item['PROPERTIES']['obem_m3'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"obem_m3"))->Fetch();
                $item['PROPERTIES']['obem'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"obyom"))->Fetch();

                if (empty($item['PROPERTIES']['obem_m3']['VALUE']) && empty($item['PROPERTIES']['obem']['VALUE']))
                {
                    $item['PROPERTIES']['dlina_mm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"dlina_mm"))->Fetch();
                    $item['PROPERTIES']['dlina_m'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"dlinna_m"))->Fetch();
                    $item['PROPERTIES']['dlina_sm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"Dlina_sm"))->Fetch();

                    if (empty($item['PROPERTIES']['dlina_mm']['VALUE']))
                    {
                        $item['PROPERTIES']['dlina_mm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"dlinna_mm"))->Fetch();
                    }

                    if (empty($item['PROPERTIES']['dlina_mm']['VALUE']))
                    {
                        $item['PROPERTIES']['dlina_mm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"obschaya_dlina_mm"))->Fetch();
                    }

                    if (empty($item['PROPERTIES']['dlina_mm']['VALUE']))
                    {
                        $item['PROPERTIES']['dlina_mm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"dlina_instrumenta_mm"))->Fetch();
                    }

                    if (empty($item['PROPERTIES']['dlina_m']['VALUE']))
                    {
                        $item['PROPERTIES']['dlina_m'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"dlina_polotna_m"))->Fetch();
                    }
                    
                    
                    $item['PROPERTIES']['shirina_mm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"shirinf_mm"))->Fetch();
                    $item['PROPERTIES']['shirina_sm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"schirina_sm"))->Fetch();

                    if (empty($item['PROPERTIES']['shirina_mm']['VALUE']))
                    {
                        $item['PROPERTIES']['shirina_mm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"shirina_korpusa_mm"))->Fetch();
                    }

                    if (empty($item['PROPERTIES']['shirina_mm']['VALUE']))
                    {
                        $item['PROPERTIES']['shirina_mm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"shirina_polotna_mm"))->Fetch();
                    }

                    $item['PROPERTIES']['visota_mm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"visota_mm"))->Fetch();
                    $item['PROPERTIES']['visota_sm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"visota_sm"))->Fetch();

                    if (empty($item['PROPERTIES']['visota_sm']['VALUE']))
                    {
                        $item['PROPERTIES']['visota_sm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"vysota_sm"))->Fetch();
                    }

                    if (empty($item['PROPERTIES']['visota_sm']['VALUE']))
                    {
                        $item['PROPERTIES']['visota_sm'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"vysota_v_slozhennom_sostojanii_sm"))->Fetch();
                    }
                }
                
                $item['PROPERTIES']['ves'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"ves"))->Fetch();
                $item['PROPERTIES']['ves_g'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"ves_g"))->Fetch();
                $item['PROPERTIES']['ves_kg'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"ves_kg"))->Fetch();

                if (empty($item['PROPERTIES']['ves_g']['VALUE']) && empty($item['PROPERTIES']['ves_kg']['VALUE']) && empty($item['PROPERTIES']['ves']['VALUE']))
                {
                    $item['PROPERTIES']['ves'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"ves_bez_upakovki"))->Fetch();
                }
                
                $item['PROPERTIES']['brand'] = CIBlockElement::GetProperty($item['IBLOCK_ID'], $item['ID'], array("sort" => "asc"), Array("CODE"=>"brand"))->Fetch();

                if ($item['DETAIL_PICTURE']) {
                    $item['PICTURE'] = CFile::GetPath($item['DETAIL_PICTURE']);
                } else if ($item['PREVIEW_PICTURE']) {
                    $item['PICTURE'] = CFile::GetPath($item['PREVIEW_PICTURE']);
                }
                

                $main_query = new Entity\Query($entityBrand);
                $main_query->setSelect(array('UF_NAME'));
                $main_query->setFilter(array('=UF_XML_ID' => $item['PROPERTIES']['brand']['VALUE']));

                $brand = $main_query->exec();
                $brand = new CDBResult($brand);
                $item['PROPERTIES']['brand']['VALUE'] = $brand->Fetch()['UF_NAME'];

                $arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($item['ID']);  
                $item['MEASURE'] = $arMeasure[$item['ID']]['MEASURE']['SYMBOL_RUS'];

                fwrite($fp, self::writeItem($item));
            }

             

            $selectedCountOnCurrentStep = 1;
            // $selectedCountOnCurrentStep = count($result['ITEMS']);
            $pageNum++;
        }
        while($selectedCountOnCurrentStep >= self::MAX_PAGE_SIZE);

        $data = '</products>'.PHP_EOL;
        $data .= '</shop>'.PHP_EOL;
        $data .= '</yml_catalog>'.PHP_EOL;
        fwrite($fp, $data);
        fclose($fp);

        return get_called_class()."::exchangeAgent();";
    }

    public static function addAgent()
    {
        CAgent::AddAgent(
            get_called_class()."::exchangeAgent();", // имя функции
            "",                          // идентификатор модуля
            "N",                                  // агент не критичен к кол-ву запусков
            // 30,                                // интервал запуска - 1 сутки
            86400,                                // интервал запуска - 1 сутки
            date("d.m.Y 05:00:00",strtotime("+1 day")),// дата первой проверки на запуск
            "Y",                                  // агент активен
            date("d.m.Y 05:00:00",strtotime("+1 day")),// дата первого запуска
            30);

    }

    protected  function addOneMoreStepAgent($id, $step)
    {
        CAgent::AddAgent(
            get_called_class()."::exchangeAgent(".intval($id).", ".intval($step).");", // имя функции
            "",                          // идентификатор модуля
            "N",                                  // агент не критичен к кол-ву запусков
            86400,                                // интервал запуска - 1 сутки
            date("d.m.Y H:i:s",strtotime("+".self::AGENT_TIME_INTERVAL." minute")),// дата первой проверки на запуск
            "Y",                                  // агент активен
            date("d.m.Y H:i:s",strtotime("+".self::AGENT_TIME_INTERVAL." minute")),// дата первого запуска
            30);

    }

    public static function writeItem($product)
    {
        $data = '';

        $data .= '<product id="' . $product['ID'] .'">'.PHP_EOL;
        $data .= '<url>https://faza.center/product/'.$product['CODE'].'/</url>'.PHP_EOL;

        if ($product['PICTURE'])
            $data .= '<picture>https://faza.center'.$product['PICTURE'].'</picture>'.PHP_EOL;
        
        if($product['IBLOCK_SECTION_ID'] > 0)
        {
            $data .='<categoryId>'.$product['IBLOCK_SECTION_ID'].'</categoryId>'.PHP_EOL;
        }

        if ($product['MEASURE']) {
            $data .= '<measure name="Единица измерения">'.$product['MEASURE'].'</measure>'.PHP_EOL;
        }

        $data .= '<name>'.HtmlFilter::encode($product['NAME']).'</name>'.PHP_EOL;

        if (!empty($product['DETAIL_TEXT']))
        {
            $data .= '<description>'.HtmlFilter::encode($product['DETAIL_TEXT']).'</description>'.PHP_EOL;
        }

        if (!empty($product['PREVIEW_TEXT']))
        {
            $data .= '<short_description>'.HtmlFilter::encode($product['PREVIEW_TEXT']).'</short_description>'.PHP_EOL;
        }

        foreach ($product['PROPERTIES'] as $keyProperty => $property) 
        {
            if (!empty($property['VALUE']))
                $data .= '<'.$keyProperty.' name="' . $property['NAME'] . '">'.$property['VALUE'].'</'.$keyProperty.'>'.PHP_EOL;
        }

        $data .= '</product>'.PHP_EOL;

        return $data;
    }

}

?>