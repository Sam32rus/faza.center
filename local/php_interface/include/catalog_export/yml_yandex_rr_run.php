<?
//<title>ymlYandex-rr</title>

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CAT_CRON", true);
define('NO_AGENT_CHECK', true);

use \Bitrix\Main\Loader;
use \Bitrix\Iblock;
use \Bitrix\Catalog\StoreProductTable;
use \Bitrix\Catalog\StoreTable;
use \Bitrix\Main\Text\HtmlFilter;
use \Zenden\Base\Component\Tools;
use \Zenden\Base\Helpers\Image;
use \Zenden\Shop\Component\Catalog\Element;
use Zenden\Shop\Configuration;

if (empty($_SERVER['DOCUMENT_ROOT']))
{
    $_SERVER['DOCUMENT_ROOT'] = dirname(__DIR__, 4);
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (
    !Loader::includeModule('iblock')
    || !Loader::includeModule('catalog')
    || !Loader::includeModule('zenden.shop')
    || !Loader::includeModule('zenden.base')
)
{
    die();
}

set_time_limit(0);
ignore_user_abort(true);
ini_set('memory_limit', '1G');

$catalogIblock = Configuration::getCatalogIblockId();

$file = $_SERVER['DOCUMENT_ROOT'].'/upload/ymlYandex-rr.xml';
unlink($file);
$fp = fopen($file, 'a+');
$data = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
$data .= '<yml_catalog date="'.date("Y-m-d H:i").'">'.PHP_EOL;

$data .= '<shop>'.PHP_EOL;
$data .= '<name>Mascotte</name>'.PHP_EOL;
$data .= '<company>ООО "РАДОСТЬ"</company>'.PHP_EOL;
$data .= '<url>https://mascotte.ru</url>'.PHP_EOL;
$data .= '<currencies>'.PHP_EOL;
$data .= '<currency id="RUB" rate="1" />'.PHP_EOL;
$data .= '</currencies>'.PHP_EOL;

fwrite($fp, $data);

$data = '<categories>'.PHP_EOL;

$iterator = Iblock\SectionTable::query()
    ->addSelect('ID')
    ->addSelect('IBLOCK_SECTION_ID')
    ->addSelect('NAME')
    ->where('IBLOCK_ID', $catalogIblock)
    ->where('GLOBAL_ACTIVE', 'Y')
    ->where('ACTIVE', 'Y')
    ->addOrder('LEFT_MARGIN', 'ASC')
    ->exec();

foreach ($iterator as $item)
{
    if($item['IBLOCK_SECTION_ID'] > 0)
    {
        $data .= '<category id="'.$item['ID'].'" parentId="'.$item['IBLOCK_SECTION_ID'].'">'.HtmlFilter::encode($item['NAME']).'</category>'.PHP_EOL;
    }
    else
    {
        $data .= '<category id="'.$item['ID'].'">'.HtmlFilter::encode($item['NAME']).'</category>'.PHP_EOL;
    }
}

$data .= '</categories>'.PHP_EOL;
$data .= '<offers>'.PHP_EOL;
fwrite($fp, $data);

$filter = [
    'IBLOCK_ID' => $catalogIblock,
    'ACTIVE' => 'Y',
    'ACTIVE_DATE' => 'Y',
    '!PREVIEW_PICTURE' => false,
    'SECTION_GLOBAL_ACTIVE'=>'Y',
    '!=PROPERTY_PHOTOS' => false,
    '=INCLUDE_SUBSECTIONS' => 'Y',
    '!=IBLOCK_SECTION_ID' => false,
];

$fields = [
    0 => "ID",
    1 => "NAME",
    2 => "PREVIEW_PICTURE",
    3 => "CODE",
    4 => "IBLOCK_SECTION_ID",
    5 => "DETAIL_PAGE_URL",
    6 => "PRICE_1",
    7 => "PRICE_2",
    8 => "XML_ID",
    9 => 'PREVIEW_TEXT',
    10 => 'IBLOCK_ID',
];

$pageSize = 100;
$pageNum = 1;

do {
    $result = [];

    $iterator = \CIBlockElement::GetList([], $filter, false, ['nPageSize' => $pageSize, 'iNumPage' => $pageNum, 'checkOutOfRange' => true], $fields);
    while ($item = $iterator->fetch())
    {
        $result['ITEMS'][$item['ID']] = Element::normalize($item);
    }
    $selectedCountOnCurrentStep = count($result['ITEMS']);
    $pageNum++;

    if (Tools::isNonEmptyArray($result['ITEMS']))
    {
        $result['ITEMS'] = Element::getProperties($catalogIblock, $result['ITEMS']);
        $result['OFFERS'] = CCatalogSKU::getOffersList(array_keys($result['ITEMS']), null, ['ACTIVE' => 'Y', 'ACTIVE_DATE' => 'Y'], ['ID', 'XML_ID', 'PROPERTY_SIZE', 'PROPERTY_BARCODES', 'AVAILABLE']);

        $result['PROPERTY_VALUES'] = [];

        foreach ($result['ITEMS'] as $id => $item)
        {
            foreach ($item['PROPERTIES']['PHOTOS']['VALUE'] as $photo)
            {
                $result['PHOTOS'][$photo] = $photo;
            }

            foreach ($item['PROPERTIES'] as $prop)
            {
                if ($prop['PROPERTY_TYPE'] == \Bitrix\Iblock\PropertyTable::TYPE_ELEMENT && !is_array($prop['VALUE']))
                {
                    $result['PROPERTY_VALUES'][$prop['VALUE']] = $prop['VALUE'];
                }
            }
        }

        if (count($result['PROPERTY_VALUES']))
        {
            $iterator = Iblock\ElementTable::query()
                ->addSelect('ID')
                ->addSelect('NAME')
                ->whereIn('ID', array_keys($result['PROPERTY_VALUES']))
                ->exec();

            foreach ($iterator as $item)
            {
                $result['PROPERTY_VALUES'][$item['ID']] = $item['NAME'];
            }
        }

        if(Tools::isNonEmptyArray($result['PHOTOS']))
        {
            $result['PHOTOS'] = Image::resizeMulti($result['PHOTOS'], 400, 400);
        }

        $result['OFFERS_IDS'] = [];

        foreach ($result['OFFERS'] as $productId => $offers)
        {
            foreach ($offers as $offerId => $offer)
            {
                $result['OFFERS_IDS'][$offerId] = $offerId;
            }
        }

        if (count($result['OFFERS_IDS']))
        {
            $iterator = StoreProductTable::query()
                ->addSelect('STORE_ID')
                ->addSelect('PRODUCT_ID')
                ->addSelect('AMOUNT')
                ->whereIn('PRODUCT_ID', array_keys($result['OFFERS_IDS']))
                ->exec();

            foreach ($iterator as $storeItem)
            {
                $result['STORE_ITEMS'][$storeItem['PRODUCT_ID']][$storeItem['STORE_ID']] = $storeItem['AMOUNT'];
                $result['STORE'][$storeItem['STORE_ID']] = $storeItem['STORE_ID'];
            }

            if (count($result['STORE']))
            {
                $iterator = StoreTable::query()
                    ->addSelect('ID')
                    ->addSelect('XML_ID')
                    ->whereIn('ID', array_keys($result['STORE']))
                    ->exec();

                foreach ($iterator as $store)
                {
                    $result['STORE'][$store['ID']] = $store['XML_ID'];
                }
            }
        }
    }
    $sizeEqual = Element::getSizesEqual();
    foreach ($result['ITEMS'] as $productId => $product)
    {
        foreach ($result['OFFERS'][$productId] as $offerId => $offer)
        {
            $data = '';
            if (!ctype_digit($offer['PROPERTY_SIZE_VALUE']))
            {
                if (isset($sizeEqual[$offer['PROPERTY_SIZE_VALUE']]))
                {
                    $offer['XML_ID'] = str_replace($offer['PROPERTY_SIZE_VALUE'], $sizeEqual[$offer['PROPERTY_SIZE_VALUE']], $offer['XML_ID']);
                }
            }

            $offer['XML_ID'] = str_replace('#', "0000", $offer['XML_ID']);
            $product['DETAIL_PAGE_URL'] = ltrim($product['DETAIL_PAGE_URL'], '/');
            $data .= '<offer id="'.$offer['XML_ID'].'" group_id="'. $product['XML_ID'] .'" available="' . ($offer['AVAILABLE'] === 'Y' ? 'true' : 'false') . '">'.PHP_EOL;
            $data .= '<url>https://mascotte.ru/'.$product['DETAIL_PAGE_URL'].'</url>'.PHP_EOL;
            $data .= '<price>'.$product['PRICE_1'].'</price>'.PHP_EOL;
            if($product['PRICE_2'] > $product['PRICE_1'])
            {
                $data .= '<oldprice>'.$product['PRICE_2'].'</oldprice>'.PHP_EOL;
            }
            $data .= '<currencyId>RUB</currencyId>'.PHP_EOL;

            if($product['IBLOCK_SECTION_ID'] > 0)
            {
                $data .='<categoryId>'.$product['IBLOCK_SECTION_ID'].'</categoryId>'.PHP_EOL;
            }

            foreach($product['PROPERTIES']['PHOTOS']['VALUE'] as $picture)
            {
                if ($result['PHOTOS'][$picture]['RESIZE']['SRC'])
                {
                    $data .='<picture>https://mascotte.ru'.$result['PHOTOS'][$picture]['RESIZE']['SRC'].'</picture>'.PHP_EOL;
                }
            }
            $data .= '<delivery>true</delivery>'.PHP_EOL;
            if ($result['PROPERTY_VALUES'][$product['PROPERTIES']['BRANDS']['VALUE']])
            {
                $data .= '<vendor>'.HtmlFilter::encode($result['PROPERTY_VALUES'][$product['PROPERTIES']['BRANDS']['VALUE']]).'</vendor>'.PHP_EOL;
            }
            $data .= '<vendorCode>'.HtmlFilter::encode($product['PROPERTIES']['ARTICLE']['VALUE']).'</vendorCode>'.PHP_EOL;
            $name = $product['PROPERTIES']['CATALOG_NAME']['VALUE'] ? $product['PROPERTIES']['CATALOG_NAME']['VALUE'] : $product['NAME'];
            $data .= '<name>'.HtmlFilter::encode($name).'</name>'.PHP_EOL;

            if (!empty($product['PREVIEW_TEXT']))
            {
                $data .= '<description><![CDATA['.$product['PREVIEW_TEXT'].']]></description>'.PHP_EOL;
            }

            if (end($product['PROPERTIES']['PHOTOS']['VALUE']))
            {
                $data .= '<param name="Дополнительное изображение">https://mascotte.ru'.$result['PHOTOS'][end($product['PROPERTIES']['PHOTOS']['VALUE'])]['RESIZE']['SRC'].'</param>'.PHP_EOL;
            }

            if ($result['PROPERTY_VALUES'][$product['PROPERTIES']['COLORS']['VALUE']])
            {
                $data .= '<param name="Цвет">'.HtmlFilter::encode($result['PROPERTY_VALUES'][$product['PROPERTIES']['COLORS']['VALUE']]).'</param>'.PHP_EOL;
            }

            if ($result['PROPERTY_VALUES'][$product['PROPERTIES']['STYLES']['VALUE']])
            {
                $data .= '<param name="Стиль">'.HtmlFilter::encode($result['PROPERTY_VALUES'][$product['PROPERTIES']['STYLES']['VALUE']]).'</param>'.PHP_EOL;
            }

            if ($result['PROPERTY_VALUES'][$product['PROPERTIES']['CLASP_TYPE']['VALUE']])
            {
                $data .= '<param name="Тип каблука">'.HtmlFilter::encode($result['PROPERTY_VALUES'][$product['PROPERTIES']['CLASP_TYPE']['VALUE']]).'</param>'.PHP_EOL;
            }

            if ($result['PROPERTY_VALUES'][$product['PROPERTIES']['SEASONS']['VALUE']])
            {
                $data .= '<param name="Сезон">'.HtmlFilter::encode($result['PROPERTY_VALUES'][$product['PROPERTIES']['SEASONS']['VALUE']]).'</param>'.PHP_EOL;
            }

            if ($result['PROPERTY_VALUES'][$product['PROPERTIES']['MATERIAL_LINING_1']['VALUE']])
            {
                $data .= '<param name="Материал подкладки">' . HtmlFilter::encode($result['PROPERTY_VALUES'][$product['PROPERTIES']['MATERIAL_LINING_1']['VALUE']]) . '</param>' . PHP_EOL;
            }

            if ($result['PROPERTY_VALUES'][$product['PROPERTIES']['MATERIAL_TOP_1']['VALUE']])
            {
                $data .= '<param name="Материал верха">' . HtmlFilter::encode($result['PROPERTY_VALUES'][$product['PROPERTIES']['MATERIAL_TOP_1']['VALUE']]) . '</param>' . PHP_EOL;
            }

            if ($result['PROPERTY_VALUES'][$product['PROPERTIES']['MATERIAL_SOLE']['VALUE']])
            {
                $data .= '<param name="Материал подошвы">' . HtmlFilter::encode($result['PROPERTY_VALUES'][$product['PROPERTIES']['MATERIAL_SOLE']['VALUE']]) . '</param>' . PHP_EOL;
            }

            if ($result['PROPERTY_VALUES'][$product['PROPERTIES']['MATERIAL_INSOLE']['VALUE']])
            {
                $data .= '<param name="Материал стельки">' . HtmlFilter::encode($result['PROPERTY_VALUES'][$product['PROPERTIES']['MATERIAL_INSOLE']['VALUE']]) . '</param>' . PHP_EOL;
            }

            if ($result['PROPERTY_VALUES'][$product['PROPERTIES']['TYPES']['VALUE']])
            {
                $data .= '<param name="Тип">' . HtmlFilter::encode($result['PROPERTY_VALUES'][$product['PROPERTIES']['TYPES']['VALUE']]) . '</param>' . PHP_EOL;
            }

            if (!empty($product['PROPERTIES']['HEEL_HEIGHT']['VALUE']))
            {
                $data .= '<param name="Высота каблука">' . HtmlFilter::encode($product['PROPERTIES']['HEEL_HEIGHT']['VALUE']) . ' мм</param>' . PHP_EOL;
            }

            if (!empty($product['PROPERTIES']['SHAFT_COVERAGE']['VALUE']))
            {
                $data .= '<param name="Охват голенища">' . HtmlFilter::encode($product['PROPERTIES']['SHAFT_COVERAGE']['VALUE']) . ' мм</param>' . PHP_EOL;
            }

            if ($result['PROPERTY_VALUES'][$product['PROPERTIES']['GENDERS']['VALUE']])
            {
                $data .= '<param name="Пол">'.HtmlFilter::encode($result['PROPERTY_VALUES'][$product['PROPERTIES']['GENDERS']['VALUE']]).'</param>'.PHP_EOL;
            }

            if (count($result['OFFERS'][$productId]) == 1)
            {
                $data .= '<param name="Размер">ONE SIZE</param>'.PHP_EOL;
            }
            else
            {
                $data .= '<param name="Размер" unit="RU">'.$offer['PROPERTY_SIZE_VALUE'].'</param>'.PHP_EOL;
            }

            foreach ($offer['PROPERTY_BARCODES_VALUE'] as $barcode)
            {
                $data .= '<barcode>' . $barcode . '</barcode>' . PHP_EOL;
            }

            $data .= '</offer>'.PHP_EOL;
            fwrite($fp, $data);
        }
    }
}
while($selectedCountOnCurrentStep >= $pageSize);

$data = '</offers>'.PHP_EOL;
$data .= '</shop>'.PHP_EOL;
$data .= '</yml_catalog>'.PHP_EOL;
fwrite($fp, $data);
fclose($fp);