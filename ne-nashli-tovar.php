<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Ищите товар?");
$APPLICATION->SetPageProperty("description", "Не нашли товар? Мы сможем его привести под заказ");
$APPLICATION->SetTitle("Не нашли товар?");
?>Если вы не нашли товар или его нет в наличии, вы всегда можете оставить заявку и мы привезём его в кратчайший срок! Со списком всех производителей вы можете ознакомится на <a href="https://faza.center/info/brands/">странице</a>.<br>
 <br>
 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"inline",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "1"
	)
);?><br>
 <br>
 <i>*Чем подробнее вы опишите свой товар, тем проще нам будет его найти у поставщиков</i><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>