<?php
if(empty($_SERVER["DOCUMENT_ROOT"])) $_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Diag\Debug;

CModule::IncludeModule("iblock");
CModule::IncludeModule("file");

define("IBLOCK_CATALOG_ID", 7); // id инфоблока каталога

// Debug::startTimeLabel('update_image');

$products = CIBlockElement::GetList(
    ['ID' => 'ASC'],
    ['IBLOCK_ID' => IBLOCK_CATALOG_ID, 'ACTIVE' => 'N'],
    false,
    false,
    ['ID', 'IBLOCK_ID', 'ACTIVE', 'PREVIEW_PICTURE', 'DETAIL_PICTURE']
);

while ($product = $products->getNext()) {
    $el = new CIBlockElement;

    $arFields = [];

    // if ($product['DETAIL_PICTURE']) {
    //     $arFields['PREVIEW_PICTURE'] = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . CFile::GetFileArray($product["DETAIL_PICTURE"])["SRC"]);
    //     $arFields['PREVIEW_PICTURE']["del"] = "Y";
        $arFields['ACTIVE'] = 'Y';

        $res = $el->Update(
            $product['ID'],
            $arFields,
            false,
            false,
            true
        );
    // }
    // echo "<pre>";
    // print_r($product);
    // echo "</pre>";
    // exit();
}

// Debug::endTimeLabel('update_image');

// $time = Debug::getTimeLabels()['update_image']['time'];
// echo "Время выполнения: " . date("H:i:s", mktime(0, 0,  $time));
?>
