<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule('iblock');

$file = 'catalog.csv';
$tofile = "Название раздела;Идентификатор раздела;Уровень раздела;Родительский раздел\n";
$bom = "\xEF\xBB\xBF";

$arFilter = array('IBLOCK_ID' => 7, 'ACTIVE' => 'Y'); 
$arSelect = array('ID', 'NAME', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID');
$rsSection = CIBlockSection::GetTreeList($arFilter, $arSelect); 
while($arSection = $rsSection->Fetch()) {
    $tofile .= $arSection['NAME'] . ";" . $arSection['ID'] . ";" . $arSection['DEPTH_LEVEL'] . ";" . $arSection['IBLOCK_SECTION_ID'] . "\n";
}

@file_put_contents($file, $bom . $tofile . file_get_contents($file));
?>