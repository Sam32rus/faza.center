<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница не найдена");
?>
<main class="main">
    <h1 class="title-page"><span class="content">Ошибка 404</span></h1>

    <section class="empty">
        <div class="content">
            <div class="empty__inner">
                <img src="/f/i/icons/bug.svg" class="empty__center-img">
                <div class="empty__center-text">Страница не существует. <a href="/catalog/" class="link-blue">Нажмите&nbsp;здесь</a>, чтобы продолжить покупки.</div>
            </div>
        </div>
    </section>

</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>