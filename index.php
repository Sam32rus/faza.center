<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("description", "Интернет-магазин Фаза - широкий ассортимент электротоваров с возможностью продажи по всей России.");
$APPLICATION->SetPageProperty("keywords", "Интернет-магазин, фаза, электротовары");
$APPLICATION->SetPageProperty("title", "Интернет-магазин электротоваров Фаза");
$APPLICATION->SetPageProperty("viewed_show", "Y");

require($_SERVER["DOCUMENT_ROOT"]. SITE_TEMPLATE_PATH . "/content.php");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
