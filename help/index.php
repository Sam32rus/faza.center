<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как купить? - Фаза");
?><h1>Как купить товар?</h1>
<p>
	 В нашем интернет-магазине широкий ассортимент различных товаров, которые разделены на категории для удобства поиска.&nbsp;
</p>
<p>
	 &nbsp; &nbsp;&nbsp;<img src="/upload/medialibrary/572/nrivag3hlq8mp5phv446wu7e12p4jr9u.png">
</p>
<p>
	 Выбрав понравившийся товар нажмите кнопку “Добавить в корзину”. После этого вы можете продолжить покупки в нашем магазине или перейти к оформлению заказа через корзину. Если вы нажмете “Добавить в корзину”, то перед вами появится окно, в котором выберете “Перейти в корзину”. В корзине проверьте позицию товара, укажите количество и переходите к оформлению.
</p>
<p>
 <img src="/upload/medialibrary/ffc/bt3b14553i01o5g6u46gv8q367urd745.png"><br>
</p>
<p>
	 &nbsp;При оформлении заказа заполните небольшую анкету с вашими контактными данными, номером телефона и адресом доставки. Также выберете способ оплаты: наличными или безналичный расчет. После оформления заказа с вами свяжется менеджер магазина, который уточнит детали и подтвердит заказ.
</p>
<p>
	 &nbsp; &nbsp;&nbsp;<img src="/upload/medialibrary/c62/t14osd14qu34qkzkplahg6refrp4g2lp.png">
</p>
<p>
	 Кроме этого, в правом верхнем углу нашего сайта расположен значок корзины, куда вы всегда можете перейти для оформления заказа, когда добавите все желаемые позиции товара. Также вы имеете возможность сравнить несколько позиций товара. Для этого рядом с рейтингом нажмите три черточки, чтобы добавить товар для сравнения.
</p>
<p>
	 &nbsp; &nbsp;&nbsp;<img src="/upload/medialibrary/1c9/8xloo0dx14wmyzp2x0uxof242fsk44l2.png">
</p>
<p>
	 У нас есть возможность оформить быструю покупку товара в один клик без необходимости добавления его в корзину. Для этого выберете понравившийся товар и нажмите “Оформить в 1 клик”.
</p>
<p>
	&nbsp; &nbsp;&nbsp;<img src="/upload/medialibrary/270/3yormzol8npmhprrqvpdrdot3lawsmj9.png">
</p>
<p>
	 После оформления заказа вы сможете его отслеживать в личном кабинете. При возникновении вопросов или если вам будет что-то непонятно, то обязательно свяжитесь с нашими менеджерами, которые всегда готовы помочь.
</p>
<p>
	 &nbsp; &nbsp;<img src="/upload/medialibrary/bba/g5c93k20u5v1di310p1azzdr5ows6tik.png">
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>