$(function() {

    $('.js__deploy-open, .js__deploy-close, .js__deploy-toggle, .js__deploy-hidden-toggle').css({
        'cursor': 'pointer'
    })

    let deploy = {
        open: function(element){
            let deploy = $(element).parent('.deploy');
            deploy.children('.deploy__content').show( "slow");
            $(element).addClass('open')
        },
        close: function(element){
            let deploy = $(element).parent('.deploy');
            deploy.children('.deploy__content').hide( "slow");
            $(element).removeClass('open')
        },
        toggle: function(element){
            let deploy = $(element).parent('.deploy');
            let deployContent = deploy.children('.deploy__content');

            deployContent.toggle( "slow", function() {
                if($(this).is(':visible') ){
                    $(element).addClass('open')
                    if( $(element).data("deploy-open")){
                        $(element).html($(element).data("deploy-open"));
                    }
                }
                else if($(this).is(':hidden') ){
                    $(element).removeClass('open')
                    if( $(element).data("deploy-close")){
                        $(element).html($(element).data("deploy-close"));
                    }

                }
            });


        },
        toggleHidden: function(element){
            let deployWrapper = $(element).parent('.deploy__content');
            let deployHidden = deployWrapper.find('.form-data.hidden')
            deployHidden.toggle( "slow", function() {
                if($(this).is(':visible') ){
                    $(element).addClass('open')
                    if( $(element).data("deploy-open")){
                        $(element).html($(element).data("deploy-open"));
                    }
                }
                else if($(this).is(':hidden') ){
                    $(element).removeClass('open')
                    if( $(element).data("deploy-close")){
                        $(element).html($(element).data("deploy-close"));
                    }

                }
            });

        }
    }

    $('.js__deploy-open').on('click', function(){
        deploy.open(this);
    });

    $('.js__deploy-close').on('click', function(){
        deploy.close(this);
    });

    $('.js__deploy-toggle').on('click', function(){
        deploy.toggle(this);
    });

    $('.js__deploy-hidden-toggle').on('click', function(){
        deploy.toggleHidden(this);

        setTimeout(function(){
            if($(".js__mobile-filter").outerHeight() > $(".catalog__inner").height() && $(window).width() > 1064){
                $(".js__mobile-filter").css({
                    'overflow-y': 'scroll',
                    'overflow-x': 'hidden',
                    'height': $(".catalog__inner").height()
                });
            }
            else if($(window).width() <= 1064){
                $(".js__mobile-filter").css({
                    'overflow-y': 'scroll',
                    'overflow-x': 'visible',
                    'height': '100vh'
                });
            }
            else{
                $(".js__mobile-filter").css({
                    'overflow-y': 'visible',
                    'overflow-x': 'visible',
                    'height': 'auto'
                });
            }
        }, 1000);
    });

});