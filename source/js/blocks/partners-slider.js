$(function() {
    $(document).ready(function() {
        $('.js__partners-slider').slick({
            rows: 1,
            dots: false,
            arrows: true,
            infinite: true,
            slidesToShow: 10,
            slidesToScroll: 1,
            nextArrow: '<div class="partners-slider__arrow partners-slider__arrow_right"></div>',
            prevArrow: '<div class="partners-slider__arrow partners-slider__arrow_left"></div>',
            responsive: [
                {
                    breakpoint: 1639,
                    settings: {
                        slidesToShow: 8,
                    }
                },
                {
                    breakpoint: 1319,
                    settings: {
                        slidesToShow: 6,
                    }
                },
                {
                    breakpoint: 1064,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        });
    });
});