$(function() {
    $(".js__shorten").text(function(i, text) {

        if (text.length >= 20) {
            text = text.substring(0, 20);
            var lastIndex = text.lastIndexOf(" ");       // позиция последнего пробела
            text = text.substring(0, lastIndex) + '...'; // обрезаем до последнего слова
        }
        $(this).text(text);
    });
});