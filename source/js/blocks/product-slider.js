$(function() {
    $(document).ready(function() {
        $('.js__product-slider').slick({
            dots: true,
            dotsClass: 'product-slider__dots js__product-slider-dots',
            slidesToShow: 6,
            slidesToScroll: 1,
            infinite: false,
            arrows : true,
            nextArrow: '<div class="product-slider__arrow product-slider__arrow_right"></div>',
            prevArrow: '<div class="product-slider__arrow product-slider__arrow_left"></div>',
            responsive: [
                {
                    breakpoint: 1319,
                    settings: {
                        slidesToShow: 5,
                    }
                },
                {
                    breakpoint: 1064,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]

        }
        );

    });

});