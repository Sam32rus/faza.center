$(function() {
    if( $(window).width() <= 1024) {
        var marginRight = ($(window).width() -  $('.content').width()) / 2;
        $('.js__margin-scroll').css('margin-right', '-' + marginRight + 'px');
    }
});