$(function() {
    $('.js__mobile-filter').css({
        'cursor': 'pointer'
    })

    let mobileFilter = {
        open: function(element){
            $('.js__mobile-filter').addClass('catalog__sorting_mobile').show('slow');
            $('body').addClass('opened-popup');
        },
        close: function(element){
            $('.js__mobile-filter').hide('slow').removeClass('catalog__sorting_mobile');
            $('body').removeClass('opened-popup');
        }
    }

    $('.js__mobile-filter-open').on('click', function(){
        mobileFilter.open(this);
    });

    $('.js__mobile-filter-close').on('click', function(){
        mobileFilter.close(this);
    });

});