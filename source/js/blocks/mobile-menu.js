$(function() {
    $('ul.js__mobile-menu-main-list').on('click', 'li', function() {
        $(this).parent().removeClass('active').closest('.js__wrap-mobile-menu').find('.js__mobile-menu-second-list').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('.js__mobile-menu-back').on('click', function() {
        $(this).parent().removeClass('active');
        $('.js__mobile-menu-main-list').addClass('active');
    });
});