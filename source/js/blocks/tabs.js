/**
 *<div class="tabs js__tabs">
 *    <ul class="js__tabs-caption">
 *        <li class="active">1-я вкладка</li>
 *        <li>2-я вкладка</li>
 *    </ul>
 *    <div class="tabs__content active">
 *       Содержимое первого блока
 *    </div>
 *    <div class="tabs__content">
 *       Содержимое второго блока
 *    </div>
 *</div>
 **/


$(function() {
    $('ul.js__tabs-caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.js__tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
});

$(function() {
    $('.js__menu ul.js__tabs-caption').on('mouseenter', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.js__tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
});



// В карточке товара скролл к характеристикам и открытие их
$(function(){

    $('.js__down-feature').on('click', function() {
        $('.js__tab-feature')
            .addClass('active').siblings().removeClass('active')
            .closest('.js__tabs').find('div.tabs__content').removeClass('active');
        $('.js__content-feature').addClass('active');
    });

    $(".js__down-feature").click(function(){
        $("html, body").animate({scrollTop: $('.js__content-feature').offset().top - 100 + "px"});
        return false;
});


});

