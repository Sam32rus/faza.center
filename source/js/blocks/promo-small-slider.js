$(function() {
    $('.js__promo-small-slider').slick({
        dots: true,
        dotsClass: 'promo-small-slider__dots',
        arrows : false
    });
});