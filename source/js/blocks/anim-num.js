$(function() {
    let animNum = {
        activeNumber: function(element){
            $('.anim-num__item-number').removeClass('active');
            $(element).addClass('active');
            let posTop = $(element).position().top + $(element).innerHeight() / 2;
            $('.js__anim-num').css({
                'top': posTop
            })
        },
        activeArticle: function(element){
            let elemIndex = $(element).index();
            $('.anim-num__article').removeClass('active');
            $(`.anim-num__article[data-number = ${elemIndex}]`).addClass('active');

            console.log(elemIndex)
        }

    }
    console.log(1000)
    $('.anim-num__item-number').on('click', function(){
        animNum.activeNumber(this)
        animNum.activeArticle(this)
    });

});