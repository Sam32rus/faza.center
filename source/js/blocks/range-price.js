$(function() {
    $(".js__range-price").ionRangeSlider({
        type: "double",
        min:  $(".js__range-price-min").val(),
        max: $(".js__range-price-max").val(),
        from: 0,
        to: $(".js__range-price-max").val(),
        hide_min_max: true,
        hide_from_to: true,
        onStart: function (data) {
            console.log(data.from);

            $(".js__range-price-min").val()
            $(".js__range-price-max").val()


        },
        onChange: function (data) {
            $(".js__range-price-min").val(data.from)
            $(".js__range-price-max").val(data.to)
        }
    });

    $('.js__range-price-min, .js__range-price-max').on('keyup click', function(){
        $(".js__range-price").data("ionRangeSlider").update({
            from:  $(".js__range-price-min").val(),
            to:  $(".js__range-price-max").val()
        });;
    });
});