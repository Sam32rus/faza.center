$(function() {
    let wrapSubcategory = $('.js__show-subcategory').parent('.general__inner');
    let heighSubcategory = $('.subcategory').innerHeight();
    $('.js__show-subcategory').on('click', function() {
        if(wrapSubcategory.children('.subcategory_hidden').hasClass('show')){
            wrapSubcategory.children('.subcategory_hidden').removeClass('show').css({
                height: 0
            });
            $(this).children('span').html('Показать еще');
        }
        else{
            wrapSubcategory.children('.subcategory_hidden').addClass('show').css({
                height: heighSubcategory
            });
            $(this).children('span').html('Скрыть');

        }
    });
});