$(function() {
    $('.js__cabinet-menu-open').on("click", function() {
        if ($(window).width() <= 1064 ){
            $('body').addClass('have-opened-popup');
            $('.js__cabinet-menu').addClass('open');
            $('.js__cabinet-menu-user-menu').show();
        }
    });
});