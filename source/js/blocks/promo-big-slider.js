$(function() {
    $('.js__promo-big-slider').slick({
        dotsClass: 'promo-big-slider__dots',
        arrows : false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000
    });
});