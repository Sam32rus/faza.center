$(function compareSliderInit() {
    let qtyChildren = $('.js__compare').children('.compare__item').length
    let sliderCompare = {
        init: function(){
            $('.js__compare').slick({
                    dots: false,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: false,
                    arrows : true,
                    nextArrow: '<div class="compare__arrow compare__arrow_right"></div>',
                    prevArrow: '<div class="compare__arrow compare__arrow_left"></div>',
                    responsive: [
                    {
                        breakpoint: 1319,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 1064,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1,
                        }
                    }
                ]

                }
            );
        }
    }

    if( $(window).width() >= 1319 && qtyChildren > 4){
        sliderCompare.init();
    }
    else if( $(window).width() < 1319 && qtyChildren > 3){
        sliderCompare.init();
    }
    else if( $(window).width() < 1064 && qtyChildren > 2){
        sliderCompare.init();
    }
    else if( $(window).width() < 767 && qtyChildren > 1){
        sliderCompare.init();
    }
    else {
        $('.js__compare').children('.compare__item').css({
            width: 100/qtyChildren + "%"
        });
    }


});