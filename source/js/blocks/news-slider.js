$(function() {
    $(document).ready(function() {
        $('.js__news-slider').slick({
                dots: true,
                dotsClass: 'news-slider__dots',
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                arrows : true,
                nextArrow: '<div class="news-slider__arrow news-slider__arrow_right"></div>',
                prevArrow: '<div class="news-slider__arrow news-slider__arrow_left"></div>'

            }
        );

    });

});