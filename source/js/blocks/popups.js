
/**
 * @ModuleCreator version 1.3.0
 * @module Popup
 * @example $.popup(object)
 * @author Author name
 **/

$.CreateModule({
    name: 'Popup',
    options: {
        btnDataName: 'popup-btn',
        popupDataName: 'popup-id',
        closeDataName: 'popup-close',
        hashDataName: 'popup-hash',
        bodyOpenedClass: 'have-opened-popup',
        setHash: true
    },
    hooks: {
        create: function () {
            this._init();
        },
        bindEvent: function () {
            $(document).on(this._getEventName('click'), `[data-${this.options.btnDataName}]`, this._onBtnClick);
            $(document).on(this._getEventName('click'), `[data-${this.options.closeDataName}]`, this._onCloseClick);
        }
    },
    privateMethods: {
        _init: function () {
            if (this.options.setHash){
                const activePopupId = this._checkHash()

                if (activePopupId) {
                    this._open(activePopupId)
                }
            }

        },
        _onBtnClick (event) {
            const btn = $(event.currentTarget)
            const id = btn.data(this.options.btnDataName)

            this._open(id)
        },
        _onCloseClick (event) {
            const btn = $(event.currentTarget)
            const id = btn.data(this.options.closeDataName)

            this._close(id)
        },
        _open (id) {
            const inst = this;
            inst._closeAll(function () {
                const popup = $(`[data-${inst.options.popupDataName}="${id}"]`);

                if(popup.length > 0) {
                    inst._show(popup);
                } else {
                    $.ajax({
                        url: `/include/popups/${id}.php`,
                        success: function (data) {
                            const loadedPopup = $(data)
                            $(".wrapper").prepend(loadedPopup);

                            inst._show(loadedPopup);

                        }
                    })
                }
            })
        },
        _show (popup, id) {
            this._setHash(popup);

            popup.addClass('popup-active');
            $('body').addClass(this.options.bodyOpenedClass)

            $(document).trigger('popupopen', popup, id);

            setTimeout(function() {
                $(".popup__inner").addClass('show');
                // capcha();
            }, 100);

            if(popup.find('form').length > 0){
                popup.find('form')[0].reset();
            }
        },
        _closeAll (callback = $.noop) {
            const popups = $(`[data-${this.options.popupDataName}].popup-active`)

            for (var i = 0; i < popups.length; i++) {
                let id = popups.eq(i).data(this.options.popupDataName)
                this._close(id)
            }

            if (callback) {
                callback()
            }
        },
        _close (id) {
            $(".popup__inner").removeClass('show');
            const inst = this;
            const popup = $(`[data-${inst.options.popupDataName}="${id}"]`)
            setTimeout(function() {
                popup.removeClass('popup-active')
                $('body').removeClass(inst.options.bodyOpenedClass)

                inst._hide(popup)
                if (inst.options.setHash) {
                    history.pushState('', document.title, window.location.pathname);
                }
                $(document).trigger('popupclose', popup, id)

            }, 100);

        },
        _hide (popup) {
            // popup.hide()
        },
        _setHash (popup) {
            if (this.options.setHash) {
                let hash = popup.data(this.options.hashDataName)
                let id = popup.data(this.options.popupDataName)

                if (hash !== false) {
                    if (hash === undefined) {
                        hash = id
                    }
                    location.hash = hash;
                }
            }
        },
        _checkHash () {
            let id = location.hash.slice(1)
            let popup = $('[' + this.options.popupDataName + '="' + id + '"]')
            if(popup.length > 0){
                return id
            }

            return false
        }
    },
    publicMethods: {
        open (id) {
            this.private._open(id)
        },
        close (id) {
            this.private._close(id)
        },
        closeAll () {
            this.private._closeAll()
        }
    }
});

$(function () {
    $.popup();
    // $.popup('open', '0122');
    // $.popup('close', 'ololo-popup');
    // $.popup('closeAll');

    $(document).on('popupopen', function (event, popup, id) {
        // code...
    })
});
