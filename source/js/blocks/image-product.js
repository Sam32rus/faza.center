// Slider | Product
$('.js__image-product').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.js__image-product-nav',
    arrows: false,
    dots: false,
    infinite: true,
    focusOnSelect: true,
    fade: true,
    afterChange: function(){
        if ($(window).width() > 1064){
            $(".js__zoom").elevateZoom({
                cursor: "crosshair",
                zoomType   : "lens",
                scrollZoom : true,
            });
        }
    },
    cssEase: 'linear',
    responsive: [
        {
            breakpoint: 1064,
            settings: {
                focusOnSelect: false,
                asNavFor: false,
                fade: false,
                dots: true,
                dotsClass: 'image-product__dots',
            }
        }
    ]

});

$('.js__image-product-nav:visible').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    infinite: true,
    asNavFor: '.js__image-product',
    focusOnSelect: true,
    centerMode: false,
    vertical: true,
    nextArrow: '<div class="image-product__arrow image-product__arrow_next"></div>',
    prevArrow: '<div class="image-product__arrow image-product__arrow_prev"></div>',
    // afterChange: function(){
    //     $(".js__zoom").elevateZoom({
    //         cursor: "crosshair",
    //         zoomType   : "lens",
    //         scrollZoom : true,
    //     });
    // },
    responsive: [
        {
            breakpoint: 500,
            settings: {
                arrows: false,
                vertical: false,
            }
        }
    ]
});

$('.js__image-product').on('afterChange', function(event, slick, currentSlide){
    if ($(window).width() > 1064){
        // $(".slick-active .js__zoom").elevateZoom({
        //     cursor: "crosshair",
        //     zoomType   : "lens",
        //     scrollZoom : true,
        //     responsive: true,
        //     easing: true,
        //     lensSize: 200
        // });
    }
});


$(document).ready(function() {
    if ($(window).width() > 1064){
        // $(".slick-active .js__zoom").elevateZoom({
        //     cursor: "crosshair",
        //     zoomType   : "lens",
        //     scrollZoom : true,
        //     responsive: true,
        //     easing: true,
        //     lensSize: 200
        // });
    }
});


