$(function() {
    //Если полей уже добавленр >=2, то скрываем кнопку вызова попапа
    if($('.js__personal-phone-field').find('.form-data').length >= 2){
        $('.js__personal-phone-field').next('.button-add').hide();
    }
    if($('.js__personal-address-field').find('.form-data').length >= 2){
        $('.js__personal-address-field').next('.button-add').hide();
    }
    if($('.js__personal-requisites-field').find('.form-data').length >= 2){
        $('.js__personal-requisites-field').next('.button-add').hide();
    }


    //--------------------
    // Добавления телефона
    //--------------------

    $('.js__add-phone-personal').on('click', function() {

        //Форма в которой нажата кнопка
        let clickForm = $(this).parents('form');

        // Проверка на заполненность полей
        if($('.js__form-phone').valid()){

            //Проверяем есть ли ранее заполненное поле, формируем 'name'
            let lengthField = $('.js__personal-phone-field').find('.form-data').length + 1;
            let phoneName = `phone-${lengthField}`

            //Если полей уже добавленр >=2, то скрываем кнопку вызова попапа
            if(lengthField >= 2){
                $('.js__personal-phone-field').next('.button-add').hide();
            }

            //Забираем данные из поля '.js__phone'
            let dataPhone = clickForm.find('.js__phone').val();

            let classMain = ''

            //Проверяем на активность галочку 'Сделать основным номером для связи'
            if($('.js__make-main-phone').prop("checked")){
                // Доабвляем класс 'main-phone'
                classMain = 'main-phone'
            }


            //Шаблон кода который вставляем в '.js__personal-phone-field'
            let tmpPhone = `<div class="form-data form-data_small-indent">
                    <div class="form-data__wrap form-data__wrap_editing">
                        <input class="form-data__input js__phone ${classMain}" disabled="disabled" value="${dataPhone}" name="${phoneName}" type="phone">
                        <div class="form-data__editing js__editing"></div>
                    </div>
                </div>`;

            //Проверяем на активность галочку 'Сделать основным номером для связи'
            if($('.js__make-main-phone').prop("checked")){
                // Вставляем шаблон в начало
                $('.js__personal-phone-field').prepend(tmpPhone);
            }
            else{
                // Вставляем шаблон в конец
                $('.js__personal-phone-field').append(tmpPhone);
            }

            // Закрываем попап
            $.popup('closeAll');

        }else{}
    });


    //---------------------------
    // Добавления адреса доставки
    //---------------------------


    $('.js__add-address-personal').on('click', function() {

        //Сбор данных из полей
        let cityDelivery =  $('.js__city').val().length > 0 ? 'г. ' + $('.js__city').val() : '';
        let cityStreet =  $('.js__street').val().length > 0 ? ', ул. ' + $('.js__street').val() : '';
        let cityHouseNumber =  $('.js__house-number').val().length > 0 ? ', д. ' + $('.js__house-number').val() : '';
        let cityEntrance =  $('.js__entrance').val().length > 0 ? ', под. ' + $('.js__entrance').val() : '';
        let cityFloor =  $('.js__floor').val().length > 0 ? ', эт. ' + $('.js__floor').val() : '';
        let cityApartmentNumber =  $('.js__apartment-number').val().length > 0 ? ', кв. ' + $('.js__apartment-number').val() : '';
        let addressDelivery = cityDelivery + cityStreet + cityHouseNumber + cityEntrance + cityFloor + cityApartmentNumber;

        // Проверка на заполненность полей
        if($('.js__form-address').valid()){

            //Проверяем есть ли ранее заполненное поле, формируем 'name'
            let lengthField = $('.js__personal-address-field').find('.form-data').length + 1;
            let addressName = `address-${lengthField}`

            //Если полей уже добавленр >=2, то скрываем кнопку вызова попапа
            if(lengthField >= 2){
                $('.js__personal-address-field').next('.button-add').hide();
            }

            //Шаблон кода который вставляем в '.js__personal-phone-field'
            let tmpAddress = `<div class="form-data form-data_small-indent">
                    <div class="form-data__wrap form-data__wrap_editing">
                        <input class="form-data__input" disabled="disabled" value="${addressDelivery}" name="${addressName}" type="text">
                        <div class="form-data__editing js__editing"></div>
                    </div>
                </div>`;

            // Вставляем шаблон в конец
            $('.js__personal-address-field').append(tmpAddress);

            // Закрываем попап
            $.popup('closeAll');

        }else{}
    });


    // ----------------------------
    // Добавления юридических данных
    // ----------------------------

    // переключение форм ИП или OOO
    $('.js__label-ip').on('click', function() {
        $('.js__form-address-ip').show('slow');
        $('.js__form-address-ooo').hide('slow');
    });
    $('.js__label-ooo').on('click', function() {
        $('.js__form-address-ip').hide('slow');
        $('.js__form-address-ooo').show('slow');
    });


    //Добавление данных по ИП

    $('.js__add-requisites-personal-ip').on('click', function() {

        // Собираем данные с полей
        let ipFio = $('.js__fio-ip').val(); // ФИО
        let ipOgrn = $('.js__ogrn-ip').val(); // ОГРН
        let ipInn = $('.js__inn-ip').val(); // ИНН
        let ipDate = $('.js__date-ip').val(); // Дата регистрации
        let ipPointReg = $('.js__point-reg-ip').val(); // Место регистрации
        let ipInvoice = $('.js__invoice-ip').val(); // Расчетный счет
        let ipBank = $('.js__bank-ip').val(); // Учреждение Банка
        let ipBikBank = $('.js__bik-bank-ip').val(); // Бик Банка
        let ipKsBank = $('.js__ks-bank-ip').val(); // К/С Банка

        // Проверка на заполненность полей
        if($('.js__form-address').valid()){

            //Проверяем есть ли ранее заполненное поле, формируем 'name'
            let lengthField = $('.js__personal-address-field').find('.form-data').length + 1;
            let phoneName = `phone-${lengthField}`

            //Если полей уже добавленр >=2, то скрываем кнопку вызова попапа
            if(lengthField >= 2){
                $('.js__personal-phone-field').next('.button-add').hide();
            }

            //Шаблон кода который вставляем в '.js__personal-requisites-field'
            let tmpRequisitesIP = `
                <div class="personal__row">
                    <div class="personal__coll-title">Вид предпринимательской деятельности:</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="type-business" rows="1">ИП</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">ФИО</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="fio-ip" rows="1">${ipFio}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">ОГРН</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="ogrn-ip" rows="1">${ipOgrn}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">ИНН</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="inn-ip" rows="1">${ipInn}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="personal__row">
                    <div class="personal__coll-title">Дата регистрации</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="date-ip" rows="1">${ipDate}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">Место регистрации</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="point-reg-ip" rows="1">${ipPointReg}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">Расчетный счет</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="invoice-ip" rows="1">${ipInvoice}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">Учреждение Банка</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="bank-ip" rows="1">${ipBank}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">Бик Банка</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="bik-bank-ip" rows="1">${ipBikBank}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="personal__row">
                    <div class="personal__coll-title">К/С Банка</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="ks-bank-ip" rows="1">${ipKsBank}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
            `;

            // Вставляем шаблон в конец
            $('.js__personal-requisites-field').append(tmpRequisitesIP).next('.button-add').hide();

            // Закрываем попап
            $.popup('closeAll');

        }else{}
    });

    //Добавление данных по ООО

    $('.js__add-requisites-personal-ooo').on('click', function() {

        // Собираем данные с полей
        let oooName = $('.js__name-ooo').val(); // Наименование организации
        let oooAddress = $('.js__address-ooo').val(); // Юридический адрес
        let oooInn = $('.js__inn-ooo').val(); // ИНН/КПП
        let oooOgrn = $('.js__ogrn-ooo').val(); // ОГРН
        let oooInvoice = $('.js__invoice-ooo').val(); // Расчетный счет
        let oooBank = $('.js__bank-ooo').val(); // Учреждение Банка
        let oooBikBank = $('.js__bik-bank-ooo').val(); // Бик Банка
        let oooKsBank = $('.js__ks-bank-ooo').val(); // К/С Банка
        let oooFio = $('.js__fio-ooo').val(); // ФИО Директора

        // Проверка на заполненность полей
        if($('.js__form-address').valid()){

            //Проверяем есть ли ранее заполненное поле, формируем 'name'
            let lengthField = $('.js__personal-requisites-field').find('.form-data').length + 1;

            //Если полей уже добавленр >=2, то скрываем кнопку вызова попапа
            if(lengthField >= 2){
                $('.js__personal-requisites-field').next('.button-add').hide();
            }

            //Шаблон кода который вставляем в '.js__personal-requisites-field'
            let tmpRequisitesOOO = `
                <div class="personal__row">
                    <div class="personal__coll-title">Вид предпринимательской деятельности:</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="type-business" rows="1">ООО</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">Наименование организации</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="inn-ooo" rows="1">${oooName}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">Юридический адрес</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="inn-ooo" rows="1">${oooAddress}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">ИНН/КПП</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="inn-ooo" rows="1">${oooInn}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">ОГРН</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="ogrn-ooo" rows="1">${oooOgrn}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">Расчетный счет</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="invoice-ooo" rows="1">${oooInvoice}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">Учреждение Банка</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="bank-ooo" rows="1">${oooBank}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">Бик Банка</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="bik-bank-ooo" rows="1">${oooBikBank}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="personal__row">
                    <div class="personal__coll-title">К/С Банка</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="ks-bank-ooo" rows="1">${oooKsBank}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="personal__row">
                    <div class="personal__coll-title">ФИО Директора</div>
                    <div class="personal__coll-data">
                        <div class="form-data">
                            <div class="form-data__wrap form-data__wrap_editing">
                                <textarea class="form-data__textarea" disabled="disabled" name="fio-ooo" rows="1">${oooFio}</textarea>
                                <div class="form-data__editing js__editing"></div>
                            </div>
                        </div>
                    </div>
                </div>
            `;

            // Вставляем шаблон в конец
            $('.js__personal-requisites-field').append(tmpRequisitesOOO).next('.button-add').hide();

            // Закрываем попап
            $.popup('closeAll');

        }else{}
    });



});