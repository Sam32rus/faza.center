# РАЗМЕРЫ ДЛЯ БАНЕРОВ:
*Все банеры перед добавлением на сайт должны быть конвертированы в формат '.wepb'. для конвертирования можно 
использовать сервис [convertio.co](https://convertio.co/ru/webp-converter/)*
*Шрифт текста на баннерах должен быть "Geometria"*
- 2160x904 - Большой баннер в шапке на главной
- 960x904 - Маленький баннер в шапке на главной
- 1024x800 - Баннеры партнеров на главной
- 3200x904 - Большой баннер в шапке на лендинке партнера
- 744x744 - Маленький баннер на лендинге партнера

# ПРИНЦИП РАБОТЫ ПОПАПОВ:
**Если открывать или закрывать из js то вот так:**
```js
$.popup('open', 'name-popup'); // Открыть
$.popup('close', 'name-popup'); // Закрыть
$.popup('closeAll'); // Закрыть все сразу
```
`name-popup` - у каждого попапа уникальный

**Для открытия/закрытия попапов из верстки, нужно навесить на элемент аттрибуты:**
- `data-popup-btn="name-popup"` - открытие
- `data-popup-close="name-popup"` - закрытие

**Пример попапа:**
```html
    <div class="popup" data-popup-id="name-popup">
        <div class="popup__inner">
            <div class="popup__close" data-popup-close="name-popup"></div>
            <div class="popup__content">
                <div class="popup__title">Товар добавлен в корзину</div>
                    <div class="form-data">
                        <div class="form-data__wrap-submit">
                            <div class="js__add-phone-personal form-data__submit blue-button">Перейти в корзину</div>
                        </div>
                    </div>
                <div class="popup__change-popup">
                    <div class="popup__link" data-popup-close="name-popup">Продолжить покупки</div>
                </div>
            </div>
        </div>
    </div>
```

# РАБОТА С GIT:
*Применить изменения в гите на сервер:*
- `git pull` - затягивает изменения из git на сервер

*Сделать коммит на сервере:*
- `git add .` - добавляет содержимое рабочего каталога в индекс
- `git commit -m "..."` - добавляет коммит
- `git push` - проталкивает изменения на сервер

# СТИЛИЗАЦИЯ КОНТЕНТА:
- `class="blue-link"` - синяя ссылка
- `class="white-link"` - белая ссылка
- `class="white-button"` - белая кнопка
- `class="blue-button"` - синяя кнопка
- `class="transparent-button"` - прозрачная кнопка

- `class="hide-desktop"` - элемент скроется при ширине экрана меньше 1639px
- `class="hide-laptop"` - элемент скроется на ноутбуке (ширина экрана меньше 1319px)
- `class="hide-tablet"` - элемент скроется на планшете (ширина экрана меньше 1064px)
- `class="hide-phone"` - элемент скроется на мобильном (ширина экрана меньше 767px)
- `class="hide-middlephone"` - элемент скроется на мобильном (ширина экрана меньше 490px)
- `class="hide-smallphone"` - элемент скроется на мобильном (ширина экрана меньше 420px)

- `<h1>...</h1>` - Заголовок 1го уровня
- `<h2>...</h2>` - Заголовок 2го уровня
- `<h3>...</h3>` - Заголовок 3го уровня
- `<h4>...</h4>` - Заголовок 4го уровня
- `<p>...</p>` - Абзац
- `<ul><li>...</li></ul>` - Маркированный список
- `<ol><li>...</li></ol>` - Нумерованный список
- `<blockquote>...</blockquote>` - Выделение длинных цитат






 

